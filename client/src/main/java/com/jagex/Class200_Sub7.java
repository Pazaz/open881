/* Class200_Sub7 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class200_Sub7 extends Class200
{
    int anInt9907;
    
    public void method3846() {
	Class65.aClass192Array712[-949174787 * anInt9907]
	    .method3773(1165416002);
    }
    
    Class200_Sub7(Class534_Sub40 class534_sub40) {
	super(class534_sub40);
	anInt9907 = class534_sub40.method16529((byte) 1) * -613480619;
    }
    
    public void method3845(int i) {
	Class65.aClass192Array712[-949174787 * anInt9907]
	    .method3773(1587804297);
    }
    
    public void method3847() {
	Class65.aClass192Array712[-949174787 * anInt9907]
	    .method3773(-521304565);
    }
    
    static final void method15577(Class669 class669, byte i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class243 class243 = class666.aClass243_8575;
	Class502.method8304(class247, class243, class669, -2106288582);
    }
    
    static void method15578(int i, int i_0_, int i_1_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(12, (long) i);
	class534_sub18_sub6.method18121(-129819002);
	class534_sub18_sub6.anInt11666 = 517206857 * i_0_;
    }
}
