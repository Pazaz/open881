/* Class534_Sub1_Sub4 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class534_Sub1_Sub4 extends Class534_Sub1
{
    byte aByte11731;
    String aString11732;
    int anInt11733;
    Class351 this$0;
    
    void method16011(Class534_Sub26 class534_sub26, int i) {
	Class337 class337 = new Class337();
	class337.aString3523 = aString11732;
	class337.anInt3522 = -305619187 * anInt11733;
	class337.aByte3521 = aByte11731;
	class534_sub26.method16295(class337, -568293251);
    }
    
    void method16012(Class534_Sub40 class534_sub40, int i) {
	if (class534_sub40.method16527(542873853) != 255) {
	    class534_sub40.anInt10811 -= -1387468933;
	    class534_sub40.method16537(1359621443);
	}
	aString11732 = class534_sub40.method16540(76978635);
	anInt11733 = class534_sub40.method16529((byte) 1) * 1399139691;
	aByte11731 = class534_sub40.method16586((byte) 1);
	class534_sub40.method16537(1359621443);
    }
    
    Class534_Sub1_Sub4(Class351 class351) {
	this$0 = class351;
	aString11732 = null;
    }
    
    void method16014(Class534_Sub40 class534_sub40) {
	if (class534_sub40.method16527(1821327198) != 255) {
	    class534_sub40.anInt10811 -= -1387468933;
	    class534_sub40.method16537(1359621443);
	}
	aString11732 = class534_sub40.method16540(76978635);
	anInt11733 = class534_sub40.method16529((byte) 1) * 1399139691;
	aByte11731 = class534_sub40.method16586((byte) 1);
	class534_sub40.method16537(1359621443);
    }
    
    void method16015(Class534_Sub26 class534_sub26) {
	Class337 class337 = new Class337();
	class337.aString3523 = aString11732;
	class337.anInt3522 = -305619187 * anInt11733;
	class337.aByte3521 = aByte11731;
	class534_sub26.method16295(class337, 502508081);
    }
    
    void method16013(Class534_Sub26 class534_sub26) {
	Class337 class337 = new Class337();
	class337.aString3523 = aString11732;
	class337.anInt3522 = -305619187 * anInt11733;
	class337.aByte3521 = aByte11731;
	class534_sub26.method16295(class337, -1404459527);
    }
}
