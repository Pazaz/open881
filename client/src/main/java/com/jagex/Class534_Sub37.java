/* Class534_Sub37 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class534_Sub37 extends Class534
{
    public int anInt10803;
    public int anInt10804;
    
    public boolean method16499(byte i) {
	return true;
    }
    
    public boolean method16500() {
	return true;
    }
    
    public Class534_Sub37(int i, int i_0_) {
	anInt10803 = -1269674451 * i;
	anInt10804 = i_0_ * -202574929;
    }
    
    static final void method16501(Class669 class669, int i) {
	class669.anInt8600 -= 926998689;
	client.anInt11295 = -905836865 * (class669.anIntArray8595
					  [class669.anInt8600 * 2088438307]);
	Class394_Sub3.aClass692_10247
	    = Class44_Sub22.method17373((class669.anIntArray8595
					 [1 + (2088438307
					       * class669.anInt8600)]),
					-7597847);
	if (Class394_Sub3.aClass692_10247 == null)
	    Class394_Sub3.aClass692_10247 = Class692.aClass692_8761;
	client.anInt11069
	    = (class669.anIntArray8595[2 + class669.anInt8600 * 2088438307]
	       * -81702317);
	Class100 class100 = Class201.method3864(2095398292);
	Class534_Sub19 class534_sub19
	    = Class346.method6128(Class404.aClass404_4208,
				  class100.aClass13_1183, 1341391005);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16506(-700159681 * client.anInt11295, 625224661);
	class534_sub19.aClass534_Sub40_Sub1_10513.method16506
	    (1457930057 * Class394_Sub3.aClass692_10247.anInt8763, 820222262);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16506(client.anInt11069 * -1256537637, 1163574902);
	class100.method1863(class534_sub19, (byte) 13);
    }
}
