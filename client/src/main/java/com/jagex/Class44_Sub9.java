/* Class44_Sub9 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class44_Sub9 extends Class44
{
    public static final int anInt10998 = 64;
    public static final int anInt10999 = 128;
    
    public void method1087() {
	super.method1085(1919827532);
	((Class281) anInterface6_326).aClass203_3060.method3884((byte) -1);
    }
    
    public void method17322(int i, int i_0_, int i_1_) {
	super.method1079(i, -1604157870);
	((Class281) anInterface6_326).aClass203_3060 = new Class203(i_0_);
    }
    
    public void method1080(int i) {
	super.method1080(65280);
	((Class281) anInterface6_326).aClass203_3060.method3877(896879135);
    }
    
    public void method1081(int i, short i_2_) {
	super.method1081(i, (short) 25467);
	((Class281) anInterface6_326).aClass203_3060.method3876(i, (byte) 0);
    }
    
    public void method1085(int i) {
	super.method1085(2129909214);
	((Class281) anInterface6_326).aClass203_3060.method3884((byte) -28);
    }
    
    public void method1077() {
	super.method1080(65280);
	((Class281) anInterface6_326).aClass203_3060.method3877(1954897813);
    }
    
    public void method1078() {
	super.method1080(65280);
	((Class281) anInterface6_326).aClass203_3060.method3877(761406056);
    }
    
    public void method1076() {
	super.method1080(65280);
	((Class281) anInterface6_326).aClass203_3060.method3877(-344192426);
    }
    
    public void method1098() {
	super.method1080(65280);
	((Class281) anInterface6_326).aClass203_3060.method3877(-347338849);
    }
    
    public void method1091() {
	super.method1085(1911511363);
	((Class281) anInterface6_326).aClass203_3060.method3884((byte) -52);
    }
    
    public void method17323(int i, int i_3_) {
	super.method1079(i, 1634969653);
	((Class281) anInterface6_326).aClass203_3060 = new Class203(i_3_);
    }
    
    public void method1092(int i) {
	super.method1081(i, (short) 28874);
	((Class281) anInterface6_326).aClass203_3060.method3876(i, (byte) 0);
    }
    
    public void method1088() {
	super.method1085(2111437176);
	((Class281) anInterface6_326).aClass203_3060.method3884((byte) -51);
    }
    
    public void method1089() {
	super.method1085(2066809433);
	((Class281) anInterface6_326).aClass203_3060.method3884((byte) -78);
    }
    
    public void method1090() {
	super.method1085(1923330603);
	((Class281) anInterface6_326).aClass203_3060.method3884((byte) -58);
    }
    
    public void method1084(int i) {
	super.method1081(i, (short) 16962);
	((Class281) anInterface6_326).aClass203_3060.method3876(i, (byte) 0);
    }
    
    public void method17324(int i, int i_4_) {
	super.method1079(i, -353898725);
	((Class281) anInterface6_326).aClass203_3060 = new Class203(i_4_);
    }
    
    public Class44_Sub9(Class675 class675, Class672 class672,
			Class472 class472, Class472 class472_5_) {
	super(class675, class672, class472, Class649.aClass649_8414, 128,
	      new Class281_Sub1(class472_5_, 64));
    }
    
    public void method17325(int i, int i_6_) {
	super.method1079(i, -1232157830);
	((Class281) anInterface6_326).aClass203_3060 = new Class203(i_6_);
    }
}
