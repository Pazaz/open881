/* Class703 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class703 implements Interface76
{
    static Class703 aClass703_8816;
    public static Class703 aClass703_8817;
    static Class703 aClass703_8818;
    public static Class703 aClass703_8819;
    static Class703 aClass703_8820;
    int anInt8821;
    public static Class703 aClass703_8822 = new Class703(-2);
    public static int[] anIntArray8823;
    
    Class703(int i) {
	anInt8821 = i * -403066555;
    }
    
    public int method93() {
	return 9255309 * anInt8821;
    }
    
    static {
	aClass703_8817 = new Class703(-3);
	aClass703_8818 = new Class703(2);
	aClass703_8819 = new Class703(3);
	aClass703_8820 = new Class703(21);
	aClass703_8816 = new Class703(20);
    }
    
    public int method22() {
	return 9255309 * anInt8821;
    }
    
    public int method53() {
	return 9255309 * anInt8821;
    }
    
    public static Class703[] method14218() {
	return (new Class703[]
		{ aClass703_8820, aClass703_8818, aClass703_8817,
		  aClass703_8822, aClass703_8816, aClass703_8819 });
    }
    
    public static final Class534_Sub37 method14219
	(int i, Class534_Sub37 class534_sub37, int[] is, boolean bool,
	 int i_0_) {
	Class534_Sub37 class534_sub37_1_
	    = (Class534_Sub37) client.aClass9_11224.method579((long) i);
	if (null != class534_sub37_1_)
	    Class534_Sub41.method16766(class534_sub37_1_,
				       ((1225863589
					 * class534_sub37_1_.anInt10803)
					!= (class534_sub37.anInt10803
					    * 1225863589)),
				       bool, -501970604);
	client.aClass9_11224.method581(class534_sub37, (long) i);
	Class672.method11095(class534_sub37.anInt10803 * 1225863589, is,
			     1693264167);
	Class247 class247 = Class112.method2017(i, 1311147155);
	if (class247 != null)
	    Class454.method7416(class247, -1544550961);
	if (null != client.aClass247_11119) {
	    Class454.method7416(client.aClass247_11119, -1422101889);
	    client.aClass247_11119 = null;
	}
	if (null != class247)
	    Class606.method10054((Class44_Sub11.aClass243Array11006
				  [-1278450723 * class247.anInt2454 >>> 16]),
				 class247, !bool, -58917470);
	if (!bool)
	    Class463.method7544(class534_sub37.anInt10803 * 1225863589, is,
				-1743179775);
	if (!bool && -1 != -993629849 * client.anInt11185)
	    Class67.method1385(-993629849 * client.anInt11185, 1, 743051080);
	return class534_sub37;
    }
    
    static final void method14220(Class669 class669, byte i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = client.anInt11024 * -1608886643;
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = client.anInt11276 * 1306630125;
    }
}
