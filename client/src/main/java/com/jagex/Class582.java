/* Class582 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class582 implements Interface13, Interface7
{
    int[] anIntArray7766;
    int anInt7767;
    int anInt7768;
    Class571 aClass571_7769;
    int anInt7770 = -1996279377;
    static Class171 aClass171_7771;
    
    public void method83() {
	/* empty */
    }
    
    public void method67(int i, int i_0_) {
	/* empty */
    }
    
    public void method79(Class534_Sub40 class534_sub40, byte i) {
	for (;;) {
	    int i_1_ = class534_sub40.method16527(1670862852);
	    if (i_1_ == 0)
		break;
	    method9832(class534_sub40, i_1_, 397353038);
	}
    }
    
    public static Class505 method9829(int i, int i_2_, int i_3_, int i_4_,
				      Interface14 interface14,
				      Interface14 interface14_5_) {
	Class516[] class516s = null;
	Class582 class582 = (Class582) interface14.method91(i, -815604511);
	if (class582.anIntArray7766 != null) {
	    class516s = new Class516[class582.anIntArray7766.length];
	    for (int i_6_ = 0; i_6_ < class516s.length; i_6_++) {
		Class299 class299
		    = ((Class299)
		       interface14_5_.method91(class582.anIntArray7766[i_6_],
					       -2129233995));
		class516s[i_6_]
		    = new Class516(class299.anInt3214 * -1842236195,
				   class299.anInt3219 * -970761915,
				   class299.anInt3212 * -768505479,
				   -626850589 * class299.anInt3213,
				   -1997297471 * class299.anInt3216,
				   class299.anInt3210 * -154850755,
				   class299.anInt3217 * 565548279,
				   class299.aBool3215,
				   class299.anInt3218 * 950333711,
				   943734367 * class299.anInt3211,
				   class299.anInt3220 * -1786753521);
	    }
	}
	return new Class505(2085042865 * class582.anInt7770, class516s,
			    1576956907 * class582.anInt7768, i_2_, i_3_, i_4_,
			    class582.aClass571_7769,
			    class582.anInt7767 * 761892639);
    }
    
    public void method82(int i) {
	/* empty */
    }
    
    Class582() {
	anInt7768 = 1753211709;
	aClass571_7769 = Class571.aClass571_7666;
	anInt7767 = -1546464479;
    }
    
    public void method78(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(234377236);
	    if (i == 0)
		break;
	    method9832(class534_sub40, i, 1341786920);
	}
    }
    
    public void method81(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(-927601828);
	    if (i == 0)
		break;
	    method9832(class534_sub40, i, -1373116035);
	}
    }
    
    public void method80(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(-1910178710);
	    if (i == 0)
		break;
	    method9832(class534_sub40, i, -1455454150);
	}
    }
    
    public void method84() {
	/* empty */
    }
    
    public void method66(int i) {
	/* empty */
    }
    
    public void method65(int i) {
	/* empty */
    }
    
    void method9830(Class534_Sub40 class534_sub40, int i) {
	if (1 == i)
	    anInt7770 = class534_sub40.method16529((byte) 1) * 1996279377;
	else if (2 == i) {
	    anIntArray7766 = new int[class534_sub40.method16527(-1279092361)];
	    for (int i_7_ = 0; i_7_ < anIntArray7766.length; i_7_++)
		anIntArray7766[i_7_] = class534_sub40.method16529((byte) 1);
	} else if (3 == i)
	    anInt7768 = class534_sub40.method16527(1539205936) * -1753211709;
	else if (i == 4)
	    aClass571_7769
		= ((Class571)
		   Class448.method7319(Class252.method4629((byte) 7),
				       class534_sub40.method16527(819918109),
				       2088438307));
	else if (i == 5)
	    anInt7767 = class534_sub40.method16550((byte) -23) * 1546464479;
	else if (6 == i)
	    class534_sub40.method16550((byte) -37);
    }
    
    void method9831(Class534_Sub40 class534_sub40, int i) {
	if (1 == i)
	    anInt7770 = class534_sub40.method16529((byte) 1) * 1996279377;
	else if (2 == i) {
	    anIntArray7766 = new int[class534_sub40.method16527(-1676615632)];
	    for (int i_8_ = 0; i_8_ < anIntArray7766.length; i_8_++)
		anIntArray7766[i_8_] = class534_sub40.method16529((byte) 1);
	} else if (3 == i)
	    anInt7768 = class534_sub40.method16527(647071400) * -1753211709;
	else if (i == 4)
	    aClass571_7769
		= ((Class571)
		   Class448.method7319(Class252.method4629((byte) 11),
				       class534_sub40.method16527(549598376),
				       2088438307));
	else if (i == 5)
	    anInt7767 = class534_sub40.method16550((byte) -102) * 1546464479;
	else if (6 == i)
	    class534_sub40.method16550((byte) 15);
    }
    
    void method9832(Class534_Sub40 class534_sub40, int i, int i_9_) {
	if (1 == i)
	    anInt7770 = class534_sub40.method16529((byte) 1) * 1996279377;
	else if (2 == i) {
	    anIntArray7766 = new int[class534_sub40.method16527(1232268934)];
	    for (int i_10_ = 0; i_10_ < anIntArray7766.length; i_10_++)
		anIntArray7766[i_10_] = class534_sub40.method16529((byte) 1);
	} else if (3 == i)
	    anInt7768 = class534_sub40.method16527(1710361144) * -1753211709;
	else if (i == 4)
	    aClass571_7769
		= ((Class571)
		   Class448.method7319(Class252.method4629((byte) 111),
				       class534_sub40.method16527(-1794434618),
				       2088438307));
	else if (i == 5)
	    anInt7767 = class534_sub40.method16550((byte) 64) * 1546464479;
	else if (6 == i)
	    class534_sub40.method16550((byte) 72);
    }
    
    static final void method9833(Class669 class669, int i) {
	class669.anInt8600 -= 617999126;
	int i_11_ = class669.anIntArray8595[class669.anInt8600 * 2088438307];
	int i_12_
	    = class669.anIntArray8595[class669.anInt8600 * 2088438307 + 1];
	String string
	    = (String) (class669.anObjectArray8593
			[(class669.anInt8594 -= 1460193483) * 1485266147]);
	if (99 == i_11_)
	    Class73.method1567(string, -1775561912);
	else if (i_11_ == 98)
	    Class654_Sub1_Sub5_Sub2.method18630(string, (byte) -72);
	else
	    Class272.method5067(i_11_, i_12_, "", "", "", string, null,
				(byte) 5);
    }
    
    static void method9834(byte i) {
	if (null != Class72.aClass534_Sub18_Sub11_760) {
	    Class72.aClass534_Sub18_Sub11_760 = null;
	    Class316.method5724(-1803884121 * Class112.anInt1364,
				Class150.anInt1699 * 892411561,
				-2123561997 * Class536_Sub4.anInt10366,
				-417346889 * Class281.anInt3062, (byte) 1);
	}
    }
    
    static final void method9835(Class669 class669, int i) {
	Class534_Sub36 class534_sub36
	    = ((Class534_Sub36)
	       (class669.anObjectArray8593
		[(class669.anInt8594 -= 1460193483) * 1485266147]));
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = 955276319 * class534_sub36.anInt10796;
    }
    
    public static int method9836(Class665 class665, Class668 class668, int i,
				 short i_13_) {
	if (Class665.aClass665_8561 == class665)
	    return 80;
	if (class668 == Class668.aClass668_8584)
	    return i + 12000;
	return 7000 + i;
    }
}
