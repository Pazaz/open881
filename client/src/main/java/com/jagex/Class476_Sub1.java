/* Class476_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class476_Sub1 extends Class476 implements Interface39
{
    Class173 aClass173_10287;
    
    public long method230(int i, int i_0_) {
	return super.method227(i, i_0_);
    }
    
    public boolean method231(int i, int i_1_, long l) {
	return super.method235(i, i_1_, l);
    }
    
    public void method141() {
	super.method142();
    }
    
    public void method277(int i) {
	super.method277(i * (aClass173_10287.anInt1825 * 1899960707));
    }
    
    public void method142() {
	super.method142();
    }
    
    public boolean method235(int i, int i_2_, long l) {
	return super.method235(i, i_2_, l);
    }
    
    public long method227(int i, int i_3_) {
	return super.method227(i, i_3_);
    }
    
    public Class173 method278() {
	return aClass173_10287;
    }
    
    public void method143() {
	super.method142();
    }
    
    public boolean method232(int i, int i_4_, long l) {
	return super.method235(i, i_4_, l);
    }
    
    public void method144() {
	super.method142();
    }
    
    public int method9() {
	return super.method53();
    }
    
    public int method145() {
	return super.method53();
    }
    
    public int method234() {
	return super.method53();
    }
    
    public int method53() {
	return super.method53();
    }
    
    Class476_Sub1(Class185_Sub1_Sub2 class185_sub1_sub2, Class173 class173,
		  boolean bool) {
	super(class185_sub1_sub2, 34963, bool);
	aClass173_10287 = class173;
    }
    
    public boolean method233(int i, int i_5_, long l) {
	return super.method235(i, i_5_, l);
    }
    
    public Class173 method276() {
	return aClass173_10287;
    }
    
    public void method229() {
	super.method228();
    }
    
    public void method226() {
	super.method228();
    }
    
    public void method228() {
	super.method228();
    }
    
    public void method279(int i) {
	super.method277(i * (aClass173_10287.anInt1825 * 1899960707));
    }
}
