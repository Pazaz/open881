/* Class532 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class532
{
    static final int anInt7138 = 16;
    static final int anInt7139 = 14;
    static final int anInt7140 = 2;
    static final int anInt7141 = 4;
    static final int anInt7142 = 2;
    static final int anInt7143 = 4;
    static final int anInt7144 = 4;
    static final int anInt7145 = 8;
    static final int anInt7146 = 128;
    static final int anInt7147 = 6;
    static final int anInt7148 = 4;
    static final int anInt7149 = 8;
    static final int anInt7150 = 4;
    static final int anInt7151 = 16;
    static final int anInt7152 = 3;
    static final int anInt7153 = 3;
    static final int anInt7154 = 12;
    static final int anInt7155 = 8;
    static final int anInt7156 = 8;
    
    static final int method8862(int i) {
	if (i < 4)
	    return 0;
	if (i < 10)
	    return i - 3;
	return i - 6;
    }
    
    static final int method8863() {
	return 0;
    }
    
    static final int method8864() {
	return 0;
    }
    
    static final int method8865() {
	return 0;
    }
    
    Class532() throws Throwable {
	throw new Error();
    }
    
    static final int method8866(int i) {
	return i < 7 ? 9 : 11;
    }
    
    static final int method8867(int i) {
	return i < 7 ? 7 : 10;
    }
    
    static final int method8868(int i) {
	return i < 7 ? 7 : 10;
    }
    
    static final int method8869(int i) {
	return i < 7 ? 7 : 10;
    }
    
    static final int method8870(int i) {
	return i < 7 ? 8 : 11;
    }
    
    static final int method8871(int i) {
	if (i < 4)
	    return 0;
	if (i < 10)
	    return i - 3;
	return i - 6;
    }
    
    static final int method8872(int i) {
	return i < 7 ? 9 : 11;
    }
    
    static final int method8873(int i) {
	i -= 2;
	if (i < 4)
	    return i;
	return 3;
    }
    
    static final int method8874(int i) {
	i -= 2;
	if (i < 4)
	    return i;
	return 3;
    }
    
    static final boolean method8875(int i) {
	return i < 7;
    }
    
    static final int method8876(int i) {
	i -= 2;
	if (i < 4)
	    return i;
	return 3;
    }
    
    static final int method8877(int i) {
	i -= 2;
	if (i < 4)
	    return i;
	return 3;
    }
    
    public static int method8878(long l) {
	Class699.method14128(l);
	return Class84.aCalendar838.get(1);
    }
    
    static final void method8879(Class669 class669, int i) {
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub28_10784,
	     (class669.anIntArray8595
	      [(class669.anInt8600 -= 308999563) * 2088438307]),
	     -1703225248);
	Class672.method11096((byte) 1);
	client.aBool11048 = false;
    }
    
    static final void method8880(int i) {
	Class679.anInt8657 -= -672955027;
    }
}
