/* Class534_Sub18_Sub11 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class534_Sub18_Sub11 extends Class534_Sub18
{
    String aString11793;
    Class696 aClass696_11794;
    int anInt11795;
    
    int method18342(byte i) {
	if (aClass696_11794.aClass534_Sub18_8785
	    != aClass696_11794.aClass534_Sub18_8785.aClass534_Sub18_10510)
	    return -1986934021 * ((Class534_Sub18_Sub7)
				  (aClass696_11794.aClass534_Sub18_8785
				   .aClass534_Sub18_10510)).anInt11706;
	return -1;
    }
    
    Class534_Sub18_Sub11(String string) {
	aString11793 = string;
	aClass696_11794 = new Class696();
    }
    
    boolean method18343(Class534_Sub18_Sub7 class534_sub18_sub7, int i) {
	boolean bool = true;
	class534_sub18_sub7.method16180(-421776830);
	Class534_Sub18_Sub7 class534_sub18_sub7_0_
	    = (Class534_Sub18_Sub7) aClass696_11794.method14078(1221951837);
	while (null != class534_sub18_sub7_0_) {
	    if (Class260.method4805((-1986934021
				     * class534_sub18_sub7.anInt11706),
				    (class534_sub18_sub7_0_.anInt11706
				     * -1986934021),
				    1181791465)) {
		Class246.method4507(class534_sub18_sub7,
				    class534_sub18_sub7_0_, 1944722575);
		anInt11795 += -2069883529;
		return !bool;
	    }
	    class534_sub18_sub7_0_
		= (Class534_Sub18_Sub7) aClass696_11794.method14080((byte) 2);
	    bool = false;
	}
	aClass696_11794.method14076(class534_sub18_sub7, (byte) 73);
	anInt11795 += -2069883529;
	return bool;
    }
    
    boolean method18344(Class534_Sub18_Sub7 class534_sub18_sub7) {
	int i = method18342((byte) -75);
	class534_sub18_sub7.method16180(-421776830);
	anInt11795 -= -2069883529;
	if (0 == -475442105 * anInt11795) {
	    method8892((byte) 1);
	    method16180(-421776830);
	    Class72.anInt789 -= -210574503;
	    Class72.aClass203_794.method3893(this, (-6387465159951953483L
						    * (class534_sub18_sub7
						       .aLong11709)));
	    return false;
	}
	return i != method18342((byte) 21);
    }
    
    int method18345() {
	if (aClass696_11794.aClass534_Sub18_8785
	    != aClass696_11794.aClass534_Sub18_8785.aClass534_Sub18_10510)
	    return -1986934021 * ((Class534_Sub18_Sub7)
				  (aClass696_11794.aClass534_Sub18_8785
				   .aClass534_Sub18_10510)).anInt11706;
	return -1;
    }
    
    int method18346() {
	if (aClass696_11794.aClass534_Sub18_8785
	    != aClass696_11794.aClass534_Sub18_8785.aClass534_Sub18_10510)
	    return -1986934021 * ((Class534_Sub18_Sub7)
				  (aClass696_11794.aClass534_Sub18_8785
				   .aClass534_Sub18_10510)).anInt11706;
	return -1;
    }
    
    boolean method18347(Class534_Sub18_Sub7 class534_sub18_sub7, int i) {
	int i_1_ = method18342((byte) 3);
	class534_sub18_sub7.method16180(-421776830);
	anInt11795 -= -2069883529;
	if (0 == -475442105 * anInt11795) {
	    method8892((byte) 1);
	    method16180(-421776830);
	    Class72.anInt789 -= -210574503;
	    Class72.aClass203_794.method3893(this, (-6387465159951953483L
						    * (class534_sub18_sub7
						       .aLong11709)));
	    return false;
	}
	return i_1_ != method18342((byte) -10);
    }
    
    int method18348() {
	if (aClass696_11794.aClass534_Sub18_8785
	    != aClass696_11794.aClass534_Sub18_8785.aClass534_Sub18_10510)
	    return -1986934021 * ((Class534_Sub18_Sub7)
				  (aClass696_11794.aClass534_Sub18_8785
				   .aClass534_Sub18_10510)).anInt11706;
	return -1;
    }
    
    int method18349() {
	if (aClass696_11794.aClass534_Sub18_8785
	    != aClass696_11794.aClass534_Sub18_8785.aClass534_Sub18_10510)
	    return -1986934021 * ((Class534_Sub18_Sub7)
				  (aClass696_11794.aClass534_Sub18_8785
				   .aClass534_Sub18_10510)).anInt11706;
	return -1;
    }
    
    boolean method18350(Class534_Sub18_Sub7 class534_sub18_sub7) {
	int i = method18342((byte) -61);
	class534_sub18_sub7.method16180(-421776830);
	anInt11795 -= -2069883529;
	if (0 == -475442105 * anInt11795) {
	    method8892((byte) 1);
	    method16180(-421776830);
	    Class72.anInt789 -= -210574503;
	    Class72.aClass203_794.method3893(this, (-6387465159951953483L
						    * (class534_sub18_sub7
						       .aLong11709)));
	    return false;
	}
	return i != method18342((byte) -92);
    }
    
    boolean method18351(Class534_Sub18_Sub7 class534_sub18_sub7) {
	int i = method18342((byte) 15);
	class534_sub18_sub7.method16180(-421776830);
	anInt11795 -= -2069883529;
	if (0 == -475442105 * anInt11795) {
	    method8892((byte) 1);
	    method16180(-421776830);
	    Class72.anInt789 -= -210574503;
	    Class72.aClass203_794.method3893(this, (-6387465159951953483L
						    * (class534_sub18_sub7
						       .aLong11709)));
	    return false;
	}
	return i != method18342((byte) 12);
    }
    
    int method18352() {
	if (aClass696_11794.aClass534_Sub18_8785
	    != aClass696_11794.aClass534_Sub18_8785.aClass534_Sub18_10510)
	    return -1986934021 * ((Class534_Sub18_Sub7)
				  (aClass696_11794.aClass534_Sub18_8785
				   .aClass534_Sub18_10510)).anInt11706;
	return -1;
    }
    
    static int method18353(Class247 class247, int i, byte i_2_) {
	if (!client.method17392(class247).method16264(i, (byte) -32)
	    && class247.anObjectArray2619 == null)
	    return -1;
	return ((class247.anIntArray2554 != null
		 && class247.anIntArray2554.length > i)
		? class247.anIntArray2554[i] : -1);
    }
}
