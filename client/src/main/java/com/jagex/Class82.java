/* Class82 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

public class Class82
{
    public Interface47[] anInterface47Array835;
    
    Interface47 method1639(Class534_Sub40 class534_sub40, Class397 class397) {
	if (Class397.aClass397_4112 == class397)
	    return Class707.method14258(class534_sub40, (byte) -5);
	if (Class397.aClass397_4109 == class397)
	    return Class285.method5260(class534_sub40, 1094221263);
	if (Class397.aClass397_4108 == class397)
	    return Class382.method6449(class534_sub40, -347490132);
	if (class397 == Class397.aClass397_4114)
	    return Class200_Sub19.method15633(class534_sub40, -1680134285);
	if (Class397.aClass397_4113 == class397)
	    return Class681.method13865(class534_sub40, (byte) 60);
	if (class397 == Class397.aClass397_4110)
	    return Class681.method13867(class534_sub40, 1403351793);
	if (Class397.aClass397_4111 == class397)
	    return Class44.method1100(class534_sub40, 1775506700);
	if (Class397.aClass397_4116 == class397)
	    return Class53_Sub1.method17361(class534_sub40, 237302311);
	if (Class397.aClass397_4115 == class397)
	    return Class493.method8121(class534_sub40, -1712769137);
	if (Class397.aClass397_4117 == class397)
	    return Class536_Sub2.method15976(class534_sub40, (byte) 1);
	if (Class397.aClass397_4118 == class397)
	    return Class10.method606(class534_sub40, (byte) 50);
	return null;
    }
    
    void method1640(Class534_Sub40 class534_sub40, byte i) {
	anInterface47Array835
	    = new Interface47[class534_sub40.method16527(-2083222822)];
	Class397[] class397s = Class675.method11127(1085173643);
	for (int i_0_ = 0; i_0_ < anInterface47Array835.length; i_0_++)
	    anInterface47Array835[i_0_]
		= method1641(class534_sub40,
			     class397s[class534_sub40.method16527(-783849187)],
			     -1138515952);
    }
    
    Interface47 method1641(Class534_Sub40 class534_sub40, Class397 class397,
			   int i) {
	if (Class397.aClass397_4112 == class397)
	    return Class707.method14258(class534_sub40, (byte) 88);
	if (Class397.aClass397_4109 == class397)
	    return Class285.method5260(class534_sub40, 2060333770);
	if (Class397.aClass397_4108 == class397)
	    return Class382.method6449(class534_sub40, -347490132);
	if (class397 == Class397.aClass397_4114)
	    return Class200_Sub19.method15633(class534_sub40, -1680134285);
	if (Class397.aClass397_4113 == class397)
	    return Class681.method13865(class534_sub40, (byte) 55);
	if (class397 == Class397.aClass397_4110)
	    return Class681.method13867(class534_sub40, -747414264);
	if (Class397.aClass397_4111 == class397)
	    return Class44.method1100(class534_sub40, 2057677267);
	if (Class397.aClass397_4116 == class397)
	    return Class53_Sub1.method17361(class534_sub40, 237302311);
	if (Class397.aClass397_4115 == class397)
	    return Class493.method8121(class534_sub40, -1712769137);
	if (Class397.aClass397_4117 == class397)
	    return Class536_Sub2.method15976(class534_sub40, (byte) 1);
	if (Class397.aClass397_4118 == class397)
	    return Class10.method606(class534_sub40, (byte) 126);
	return null;
    }
    
    void method1642(Class534_Sub40 class534_sub40) {
	anInterface47Array835
	    = new Interface47[class534_sub40.method16527(1678794110)];
	Class397[] class397s = Class675.method11127(1085173643);
	for (int i = 0; i < anInterface47Array835.length; i++)
	    anInterface47Array835[i]
		= method1641(class534_sub40,
			     class397s[class534_sub40.method16527(-120263495)],
			     -1944119240);
    }
    
    Class82() {
	/* empty */
    }
    
    void method1643(Class534_Sub40 class534_sub40) {
	anInterface47Array835
	    = new Interface47[class534_sub40.method16527(680587235)];
	Class397[] class397s = Class675.method11127(1085173643);
	for (int i = 0; i < anInterface47Array835.length; i++)
	    anInterface47Array835[i]
		= method1641(class534_sub40,
			     class397s[class534_sub40.method16527(343975776)],
			     -992047190);
    }
    
    void method1644(Class534_Sub40 class534_sub40) {
	anInterface47Array835
	    = new Interface47[class534_sub40.method16527(-1034929024)];
	Class397[] class397s = Class675.method11127(1085173643);
	for (int i = 0; i < anInterface47Array835.length; i++)
	    anInterface47Array835[i]
		= method1641(class534_sub40,
			     class397s[class534_sub40.method16527(588571846)],
			     -404927823);
    }
    
    Interface47 method1645(Class534_Sub40 class534_sub40, Class397 class397) {
	if (Class397.aClass397_4112 == class397)
	    return Class707.method14258(class534_sub40, (byte) 84);
	if (Class397.aClass397_4109 == class397)
	    return Class285.method5260(class534_sub40, 2114701011);
	if (Class397.aClass397_4108 == class397)
	    return Class382.method6449(class534_sub40, -347490132);
	if (class397 == Class397.aClass397_4114)
	    return Class200_Sub19.method15633(class534_sub40, -1680134285);
	if (Class397.aClass397_4113 == class397)
	    return Class681.method13865(class534_sub40, (byte) 59);
	if (class397 == Class397.aClass397_4110)
	    return Class681.method13867(class534_sub40, 823479687);
	if (Class397.aClass397_4111 == class397)
	    return Class44.method1100(class534_sub40, 1363282808);
	if (Class397.aClass397_4116 == class397)
	    return Class53_Sub1.method17361(class534_sub40, 237302311);
	if (Class397.aClass397_4115 == class397)
	    return Class493.method8121(class534_sub40, -1712769137);
	if (Class397.aClass397_4117 == class397)
	    return Class536_Sub2.method15976(class534_sub40, (byte) 1);
	if (Class397.aClass397_4118 == class397)
	    return Class10.method606(class534_sub40, (byte) 107);
	return null;
    }
    
    void method1646(Class534_Sub40 class534_sub40) {
	anInterface47Array835
	    = new Interface47[class534_sub40.method16527(2143823447)];
	Class397[] class397s = Class675.method11127(1085173643);
	for (int i = 0; i < anInterface47Array835.length; i++)
	    anInterface47Array835[i]
		= method1641(class534_sub40,
			     class397s[class534_sub40.method16527(1253028414)],
			     -1940314778);
    }
    
    Interface47 method1647(Class534_Sub40 class534_sub40, Class397 class397) {
	if (Class397.aClass397_4112 == class397)
	    return Class707.method14258(class534_sub40, (byte) 39);
	if (Class397.aClass397_4109 == class397)
	    return Class285.method5260(class534_sub40, 1117008483);
	if (Class397.aClass397_4108 == class397)
	    return Class382.method6449(class534_sub40, -347490132);
	if (class397 == Class397.aClass397_4114)
	    return Class200_Sub19.method15633(class534_sub40, -1680134285);
	if (Class397.aClass397_4113 == class397)
	    return Class681.method13865(class534_sub40, (byte) 16);
	if (class397 == Class397.aClass397_4110)
	    return Class681.method13867(class534_sub40, 800532133);
	if (Class397.aClass397_4111 == class397)
	    return Class44.method1100(class534_sub40, 1815482294);
	if (Class397.aClass397_4116 == class397)
	    return Class53_Sub1.method17361(class534_sub40, 237302311);
	if (Class397.aClass397_4115 == class397)
	    return Class493.method8121(class534_sub40, -1712769137);
	if (Class397.aClass397_4117 == class397)
	    return Class536_Sub2.method15976(class534_sub40, (byte) 1);
	if (Class397.aClass397_4118 == class397)
	    return Class10.method606(class534_sub40, (byte) 29);
	return null;
    }
    
    public static final void method1648(String string, int i) {
	int i_1_ = client.aBool11296 ? 400 : 200;
	if (null != string) {
	    if (-1979292205 * client.anInt11324 >= i_1_)
		Class553.method9105
		    (4,
		     (client.aBool11296
		      ? Class58.aClass58_601
			    .method1245(Class539.aClass672_7171, (byte) -69)
		      : Class58.aClass58_518
			    .method1245(Class539.aClass672_7171, (byte) -85)),
		     -2122168838);
	    else {
		String string_2_
		    = Class465.method7570(string, Class39.aClass76_307,
					  (byte) -14);
		if (null != string_2_) {
		    for (int i_3_ = 0; i_3_ < client.anInt11324 * -1979292205;
			 i_3_++) {
			Class28 class28 = client.aClass28Array11327[i_3_];
			String string_4_
			    = Class465.method7570(class28.aString257,
						  Class39.aClass76_307,
						  (byte) -58);
			if (string_4_ != null && string_4_.equals(string_2_)) {
			    Class553.method9105(4,
						new StringBuilder().append
						    (string).append
						    (Class58.aClass58_475
							 .method1245
						     (Class539.aClass672_7171,
						      (byte) -101))
						    .toString(),
						-2122168838);
			    return;
			}
			if (null != class28.aString250) {
			    String string_5_
				= Class465.method7570(class28.aString250,
						      Class39.aClass76_307,
						      (byte) 86);
			    if (string_5_ != null
				&& string_5_.equals(string_2_)) {
				Class553.method9105(4,
						    new StringBuilder().append
							(string).append
							(Class58
							     .aClass58_475
							     .method1245
							 ((Class539
							   .aClass672_7171),
							  (byte) -46))
							.toString(),
						    -2122168838);
				return;
			    }
			}
		    }
		    for (int i_6_ = 0; i_6_ < client.anInt11329 * 2103713403;
			 i_6_++) {
			Class33 class33 = client.aClass33Array11299[i_6_];
			String string_7_
			    = Class465.method7570(class33.aString273,
						  Class39.aClass76_307,
						  (byte) 76);
			if (null != string_7_ && string_7_.equals(string_2_)) {
			    Class553.method9105(4,
						new StringBuilder().append
						    (Class58.aClass58_594
							 .method1245
						     (Class539.aClass672_7171,
						      (byte) -51))
						    .append
						    (string).append
						    (Class58.aClass58_615
							 .method1245
						     (Class539.aClass672_7171,
						      (byte) -49))
						    .toString(),
						-2122168838);
			    return;
			}
			if (null != class33.aString275) {
			    String string_8_
				= Class465.method7570(class33.aString275,
						      Class39.aClass76_307,
						      (byte) -4);
			    if (string_8_ != null
				&& string_8_.equals(string_2_)) {
				Class553.method9105
				    (4, new StringBuilder().append
					    (Class58.aClass58_594.method1245
					     (Class539.aClass672_7171,
					      (byte) -41))
					    .append
					    (string).append
					    (Class58.aClass58_615.method1245
					     (Class539.aClass672_7171,
					      (byte) -18))
					    .toString(), -2122168838);
				return;
			    }
			}
		    }
		    if (Class465.method7570
			    ((Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419
			      .aString12211),
			     Class39.aClass76_307, (byte) -3)
			    .equals(string_2_))
			Class553.method9105(4, (Class58.aClass58_611.method1245
						(Class539.aClass672_7171,
						 (byte) -23)), -2122168838);
		    else {
			Class100 class100 = Class201.method3864(2095398292);
			Class534_Sub19 class534_sub19
			    = Class346.method6128(Class404.aClass404_4171,
						  class100.aClass13_1183,
						  1341391005);
			class534_sub19.aClass534_Sub40_Sub1_10513.method16506
			    (Class668.method11029(string, (byte) 0),
			     2059852004);
			class534_sub19.aClass534_Sub40_Sub1_10513
			    .method16713(string, -1584879764);
			class100.method1863(class534_sub19, (byte) 101);
		    }
		}
	    }
	}
    }
    
    public static boolean method1649(int i) {
	if (Class503.aString5626.startsWith("win")) {
	    String string = "msvcr110.dll";
	    String string_9_ = "msvcp110.dll";
	    String[] strings = { string, string_9_ };
	    String string_10_ = System.getenv("WINDIR");
	    if (string_10_ == "")
		return false;
	    for (int i_11_ = 0; i_11_ < strings.length; i_11_++) {
		File file
		    = new File(new StringBuilder().append(string_10_).append
				   ("\\system32\\").append
				   (strings[i_11_]).toString());
		if (!file.exists() || file.isDirectory())
		    return false;
	    }
	    return true;
	}
	if (Class503.aString5626.startsWith("mac")) {
	    boolean bool;
	    try {
		Process process = Runtime.getRuntime().exec("ps -few");
		BufferedReader bufferedreader
		    = (new BufferedReader
		       (new InputStreamReader(process.getInputStream())));
		do {
		    boolean bool_12_;
		    try {
			int i_13_ = process.waitFor();
			if (i_13_ == 0)
			    break;
			bool_12_ = false;
		    } catch (InterruptedException interruptedexception) {
			return false;
		    }
		    return bool_12_;
		} while (false);
		boolean bool_14_ = false;
	    while_169_:
		do {
		    String string;
		    do {
			if ((string = bufferedreader.readLine()) == null)
			    break while_169_;
		    } while (string.toLowerCase().indexOf("soundflowerbed")
			     == -1);
		    bool_14_ = true;
		} while (false);
		bufferedreader.close();
		bool = bool_14_;
	    } catch (Exception exception) {
		return false;
	    }
	    return bool;
	}
	return false;
    }
    
    static final void method1650(Class669 class669, int i)
	throws Exception_Sub2 {
	class669.anInt8600 -= 1235998252;
	int i_15_ = class669.anIntArray8595[2088438307 * class669.anInt8600];
	int i_16_
	    = class669.anIntArray8595[1 + 2088438307 * class669.anInt8600];
	int i_17_
	    = class669.anIntArray8595[2 + class669.anInt8600 * 2088438307];
	Class438 class438
	    = Class438.method6996((float) i_15_, (float) i_16_, (float) i_17_);
	Class599.aClass298_Sub1_7871.method5410(class438, -1921677855);
	class438.method7074();
    }
}
