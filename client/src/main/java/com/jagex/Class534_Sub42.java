/* Class534_Sub42 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public abstract class Class534_Sub42 extends Class534
{
    public static final int anInt10835 = -1;
    public static final int anInt10836 = 0;
    static final int anInt10837 = 1;
    static final int anInt10838 = 3;
    static final int anInt10839 = 5;
    static final int anInt10840 = 4;
    public static final int anInt10841 = 2;
    public static final int anInt10842 = 6;
    
    public static boolean method16797(int i) {
	return i == 0 || 1 == i || i == 2;
    }
    
    public abstract int method16798(byte i);
    
    public abstract int method16799(byte i);
    
    public abstract int method16800(int i);
    
    public abstract void method16801();
    
    public abstract long method16802(int i);
    
    public abstract void method16803(byte i);
    
    public abstract int method16804();
    
    public abstract int method16805();
    
    public abstract int method16806();
    
    public abstract int method16807();
    
    public abstract int method16808();
    
    public abstract int method16809();
    
    public abstract void method16810();
    
    public abstract void method16811();
    
    Class534_Sub42() {
	/* empty */
    }
    
    public abstract void method16812();
    
    public abstract long method16813();
    
    public static boolean method16814(int i) {
	return i == 0 || 1 == i || i == 2;
    }
    
    public static boolean method16815(int i) {
	return i == 0 || 1 == i || i == 2;
    }
    
    public static boolean method16816(int i) {
	return i == 0 || 1 == i || i == 2;
    }
    
    public abstract int method16817(int i);
    
    public abstract int method16818();
    
    static final void method16819(Class669 class669, int i) {
	class669.anInt8596 -= -2111195934;
	if (class669.aLongArray8587[1 + 1572578961 * class669.anInt8596]
	    != class669.aLongArray8587[class669.anInt8596 * 1572578961])
	    class669.anInt8613
		+= (-793595371
		    * class669.anIntArray8591[662605117 * class669.anInt8613]);
    }
    
    public static short[] method16820(short[] is, byte i) {
	if (is == null)
	    return null;
	short[] is_0_ = new short[is.length];
	System.arraycopy(is, 0, is_0_, 0, is.length);
	return is_0_;
    }
}
