/* Class524 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class524 implements Interface64
{
    Class654_Sub1_Sub5_Sub4 aClass654_Sub1_Sub5_Sub4_7089;
    
    public boolean method435(Class654_Sub1_Sub5 class654_sub1_sub5) {
	return class654_sub1_sub5 == aClass654_Sub1_Sub5_Sub4_7089;
    }
    
    public boolean method433(Class654_Sub1_Sub5 class654_sub1_sub5) {
	return class654_sub1_sub5 == aClass654_Sub1_Sub5_Sub4_7089;
    }
    
    Class524(Class654_Sub1_Sub5_Sub4 class654_sub1_sub5_sub4) {
	aClass654_Sub1_Sub5_Sub4_7089 = class654_sub1_sub5_sub4;
    }
    
    public boolean method434(Class654_Sub1_Sub5 class654_sub1_sub5) {
	return class654_sub1_sub5 == aClass654_Sub1_Sub5_Sub4_7089;
    }
    
    public boolean method436(Class654_Sub1_Sub5 class654_sub1_sub5) {
	return class654_sub1_sub5 == aClass654_Sub1_Sub5_Sub4_7089;
    }
    
    public boolean method432(Class654_Sub1_Sub5 class654_sub1_sub5, byte i) {
	return class654_sub1_sub5 == aClass654_Sub1_Sub5_Sub4_7089;
    }
    
    static final void method8730(Class669 class669, int i) {
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub10_10751,
	     (class669.anIntArray8595
	      [(class669.anInt8600 -= 308999563) * 2088438307]) == 1 ? 1 : 0,
	     -1998008099);
	client.aClass512_11100.method8441(265773003);
	Class672.method11096((byte) 1);
	client.aBool11048 = false;
    }
    
    static final void method8731(Class669 class669, int i) {
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub28_10754,
	     (class669.anIntArray8595
	      [(class669.anInt8600 -= 308999563) * 2088438307]),
	     476912444);
	Class672.method11096((byte) 1);
	client.aBool11048 = false;
    }
    
    static final void method8732(Class669 class669, byte i) {
	class669.anInt8600 -= -586972540;
	Class44_Sub16.method17357((byte) 58);
	Class391.method6546((byte) 83);
	Class480.anInt5262
	    = (class669.anIntArray8595[2088438307 * class669.anInt8600]
	       * -1796460643);
	Class303.anInt3253
	    = (-1203498039
	       * class669.anIntArray8595[class669.anInt8600 * 2088438307 + 1]);
	Class392.anInt4082
	    = (2117099633
	       * class669.anIntArray8595[2 + 2088438307 * class669.anInt8600]);
	Class534_Sub27.anInt10588
	    = (class669.anIntArray8595[class669.anInt8600 * 2088438307 + 3]
	       * -16923865);
	Class676.anInt8651
	    = (class669.anIntArray8595[class669.anInt8600 * 2088438307 + 4]
	       * 488016457);
	Class331.anInt3499
	    = (class669.anIntArray8595[5 + 2088438307 * class669.anInt8600]
	       * -1353410753);
	Class232.anInt2356
	    = (class669.anIntArray8595[6 + 2088438307 * class669.anInt8600]
	       * -105699193);
	Class304.anInt3261
	    = (-1938831421
	       * class669.anIntArray8595[7 + class669.anInt8600 * 2088438307]);
	Class423.anInt4790
	    = (class669.anIntArray8595[class669.anInt8600 * 2088438307 + 8]
	       * -1811972999);
	Class200_Sub5.anInt9903
	    = (208033923
	       * class669.anIntArray8595[2088438307 * class669.anInt8600 + 9]);
	Class483.anInt5280
	    = 1838035759 * (class669.anIntArray8595
			    [2088438307 * class669.anInt8600 + 10]);
	Class534_Sub34.anInt10732
	    = (class669.anIntArray8595[2088438307 * class669.anInt8600 + 11]
	       * 1875364205);
	Class464.aClass472_5113.method7670(Class676.anInt8651 * -118519815,
					   (byte) -112);
	Class464.aClass472_5113.method7670(Class331.anInt3499 * 1950013631,
					   (byte) -35);
	Class464.aClass472_5113.method7670(Class232.anInt2356 * -746775241,
					   (byte) -7);
	Class464.aClass472_5113.method7670(Class304.anInt3261 * -1368137493,
					   (byte) 2);
	Class464.aClass472_5113.method7670(Class423.anInt4790 * 842411465,
					   (byte) -111);
	Class464.aClass472_5113
	    .method7670(1630972005 * Class534_Sub34.anInt10732, (byte) -76);
	Class606.aClass472_7988
	    .method7670(Class534_Sub34.anInt10732 * 1630972005, (byte) -23);
	Class43.aClass163_324 = null;
	Class573.aClass163_7673 = null;
	Class495.aClass163_5536 = null;
	Class274.aClass163_3040 = null;
	Class489.aClass163_5313 = null;
	Class47.aClass163_354 = null;
	Class245.aClass163_2418 = null;
	Class579.aClass163_7758 = null;
	Class72.aBool784 = true;
    }
    
    public static Class292 method8733(int i, int i_0_) {
	Class292[] class292s = Class298.method5508(1154038654);
	for (int i_1_ = 0; i_1_ < class292s.length; i_1_++) {
	    Class292 class292 = class292s[i_1_];
	    if (i == class292.anInt3122 * -919727987)
		return class292;
	}
	return null;
    }
    
    public static Class81 method8734(int i, short i_2_) {
	Class81 class81
	    = (Class81) client.aClass203_11082.method3871((long) i);
	if (class81 == null) {
	    class81 = new Class81(Class6.aClass472_57,
				  Class474.method7762(i, -1680511297),
				  Class497.method8140(i, (byte) -39));
	    client.aClass203_11082.method3893(class81, (long) i);
	}
	return class81;
    }
}
