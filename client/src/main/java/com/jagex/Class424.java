/* Class424 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import jagdx.IDirect3DTexture;
import jagdx.IUnknown;

public class Class424 implements Interface53, Interface51
{
    int anInt4791;
    Class420_Sub2_Sub1 aClass420_Sub2_Sub1_4792;
    long aLong4793;
    
    public long method365() {
	if (aLong4793 == 0L)
	    aLong4793
		= IDirect3DTexture.GetSurfaceLevel((aClass420_Sub2_Sub1_4792
						    .aLong4705),
						   anInt4791);
	return aLong4793;
    }
    
    public int method1() {
	return aClass420_Sub2_Sub1_4792.method1();
    }
    
    public long method369() {
	if (aLong4793 == 0L)
	    aLong4793
		= IDirect3DTexture.GetSurfaceLevel((aClass420_Sub2_Sub1_4792
						    .aLong4705),
						   anInt4791);
	return aLong4793;
    }
    
    void method6790() {
	method366();
    }
    
    public void method229() {
	if (aLong4793 != 0L) {
	    aClass420_Sub2_Sub1_4792.aClass185_Sub1_Sub1_4709
		.method18018(aLong4793);
	    aLong4793 = 0L;
	}
    }
    
    public void method366() {
	if (aLong4793 != 0L) {
	    aClass420_Sub2_Sub1_4792.aClass185_Sub1_Sub1_4709
		.method18018(aLong4793);
	    aLong4793 = 0L;
	}
    }
    
    public void finalize() {
	method366();
    }
    
    public int method22() {
	return aClass420_Sub2_Sub1_4792.method1();
    }
    
    public int method53() {
	return aClass420_Sub2_Sub1_4792.method1();
    }
    
    Class424(Class420_Sub2_Sub1 class420_sub2_sub1, int i) {
	anInt4791 = i;
	aClass420_Sub2_Sub1_4792 = class420_sub2_sub1;
	aClass420_Sub2_Sub1_4792.aClass185_Sub1_Sub1_4709.method14617(this);
    }
    
    public int method88() {
	return aClass420_Sub2_Sub1_4792.method93();
    }
    
    public int method8() {
	return aClass420_Sub2_Sub1_4792.method93();
    }
    
    public int method9() {
	return aClass420_Sub2_Sub1_4792.method93();
    }
    
    public int method145() {
	return aClass420_Sub2_Sub1_4792.method93();
    }
    
    public long method368() {
	if (aLong4793 == 0L)
	    aLong4793
		= IDirect3DTexture.GetSurfaceLevel((aClass420_Sub2_Sub1_4792
						    .aLong4705),
						   anInt4791);
	return aLong4793;
    }
    
    public void method141() {
	if (aLong4793 != 0L) {
	    IUnknown.Release(aLong4793);
	    aLong4793 = 0L;
	}
	aClass420_Sub2_Sub1_4792.aClass185_Sub1_Sub1_4709.method14582(this);
    }
    
    public void method144() {
	if (aLong4793 != 0L) {
	    IUnknown.Release(aLong4793);
	    aLong4793 = 0L;
	}
	aClass420_Sub2_Sub1_4792.aClass185_Sub1_Sub1_4709.method14582(this);
    }
    
    public void method359() {
	if (aLong4793 != 0L) {
	    aClass420_Sub2_Sub1_4792.aClass185_Sub1_Sub1_4709
		.method18018(aLong4793);
	    aLong4793 = 0L;
	}
    }
    
    public void method143() {
	if (aLong4793 != 0L) {
	    IUnknown.Release(aLong4793);
	    aLong4793 = 0L;
	}
	aClass420_Sub2_Sub1_4792.aClass185_Sub1_Sub1_4709.method14582(this);
    }
    
    void method6791() {
	method366();
    }
    
    public long method367() {
	if (aLong4793 == 0L)
	    aLong4793
		= IDirect3DTexture.GetSurfaceLevel((aClass420_Sub2_Sub1_4792
						    .aLong4705),
						   anInt4791);
	return aLong4793;
    }
    
    void method6792() {
	method366();
    }
    
    public void method142() {
	if (aLong4793 != 0L) {
	    IUnknown.Release(aLong4793);
	    aLong4793 = 0L;
	}
	aClass420_Sub2_Sub1_4792.aClass185_Sub1_Sub1_4709.method14582(this);
    }
    
    public int method85() {
	return aClass420_Sub2_Sub1_4792.method93();
    }
    
    public void method226() {
	if (aLong4793 != 0L) {
	    aClass420_Sub2_Sub1_4792.aClass185_Sub1_Sub1_4709
		.method18018(aLong4793);
	    aLong4793 = 0L;
	}
    }
    
    public int method93() {
	return aClass420_Sub2_Sub1_4792.method93();
    }
    
    public void method363() {
	if (aLong4793 != 0L) {
	    aClass420_Sub2_Sub1_4792.aClass185_Sub1_Sub1_4709
		.method18018(aLong4793);
	    aLong4793 = 0L;
	}
    }
}
