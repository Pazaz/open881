/* Class595 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class595 implements Interface76
{
    public static Class595 aClass595_7829;
    public static Class595 aClass595_7830;
    static Class595 aClass595_7831;
    public static Class595 aClass595_7832;
    public static Class595 aClass595_7833;
    public static Class595 aClass595_7834;
    public static Class595 aClass595_7835;
    public static Class595 aClass595_7836;
    public static Class595 aClass595_7837;
    public static Class595 aClass595_7838;
    static Class595 aClass595_7839;
    public static Class595 aClass595_7840;
    static Class595 aClass595_7841;
    static Class595 aClass595_7842;
    static Class595 aClass595_7843;
    static Class595 aClass595_7844;
    public static Class595 aClass595_7845;
    public static Class595 aClass595_7846;
    static Class595 aClass595_7847;
    static Class595 aClass595_7848;
    static Class595 aClass595_7849;
    public static Class595 aClass595_7850;
    public static Class595 aClass595_7851 = new Class595(0, 0);
    public int anInt7852;
    public int anInt7853;
    static int anInt7854;
    
    public static Class595[] method9909() {
	return new Class595[] { aClass595_7850, aClass595_7830, aClass595_7842,
				aClass595_7839, aClass595_7843, aClass595_7829,
				aClass595_7837, aClass595_7851, aClass595_7840,
				aClass595_7847, aClass595_7844, aClass595_7833,
				aClass595_7849, aClass595_7832, aClass595_7834,
				aClass595_7841, aClass595_7838, aClass595_7831,
				aClass595_7846, aClass595_7836, aClass595_7835,
				aClass595_7845, aClass595_7848 };
    }
    
    public int method93() {
	return 847393323 * anInt7852;
    }
    
    public static boolean method9910(int i) {
	return (i >= aClass595_7843.anInt7852 * 847393323
		&& i <= 847393323 * aClass595_7848.anInt7852);
    }
    
    Class595(int i, int i_0_) {
	anInt7852 = i * 16448643;
	anInt7853 = -1407500945 * i_0_;
    }
    
    public int method53() {
	return 847393323 * anInt7852;
    }
    
    public static Class595[] method9911() {
	return new Class595[] { aClass595_7850, aClass595_7830, aClass595_7842,
				aClass595_7839, aClass595_7843, aClass595_7829,
				aClass595_7837, aClass595_7851, aClass595_7840,
				aClass595_7847, aClass595_7844, aClass595_7833,
				aClass595_7849, aClass595_7832, aClass595_7834,
				aClass595_7841, aClass595_7838, aClass595_7831,
				aClass595_7846, aClass595_7836, aClass595_7835,
				aClass595_7845, aClass595_7848 };
    }
    
    public static boolean method9912(int i) {
	return (i >= aClass595_7839.anInt7852 * 847393323
		&& i <= 847393323 * aClass595_7844.anInt7852);
    }
    
    public static boolean method9913(int i) {
	return ((i >= aClass595_7851.anInt7852 * 847393323
		 && i <= aClass595_7832.anInt7852 * 847393323)
		|| 847393323 * aClass595_7833.anInt7852 == i);
    }
    
    public static boolean method9914(int i) {
	return (i >= aClass595_7839.anInt7852 * 847393323
		&& i <= 847393323 * aClass595_7844.anInt7852);
    }
    
    static {
	aClass595_7830 = new Class595(1, 0);
	aClass595_7829 = new Class595(2, 0);
	aClass595_7832 = new Class595(3, 0);
	aClass595_7833 = new Class595(9, 2);
	aClass595_7834 = new Class595(4, 1);
	aClass595_7835 = new Class595(5, 1);
	aClass595_7845 = new Class595(6, 1);
	aClass595_7837 = new Class595(7, 1);
	aClass595_7838 = new Class595(8, 1);
	aClass595_7839 = new Class595(12, 2);
	aClass595_7840 = new Class595(13, 2);
	aClass595_7841 = new Class595(14, 2);
	aClass595_7831 = new Class595(15, 2);
	aClass595_7849 = new Class595(16, 2);
	aClass595_7844 = new Class595(17, 2);
	aClass595_7843 = new Class595(18, 2);
	aClass595_7842 = new Class595(19, 2);
	aClass595_7847 = new Class595(20, 2);
	aClass595_7848 = new Class595(21, 2);
	aClass595_7836 = new Class595(10, 2);
	aClass595_7850 = new Class595(11, 2);
	aClass595_7846 = new Class595(22, 3);
    }
    
    public int method22() {
	return 847393323 * anInt7852;
    }
    
    static void method9915(Class185 class185, Class534_Sub29 class534_sub29,
			   Class272 class272, int i) {
	Class163 class163 = class272.method5058(class185, (byte) 10);
	if (class163 != null) {
	    int i_1_ = class163.method2647();
	    if (class163.method2649() > i_1_)
		i_1_ = class163.method2649();
	    int i_2_ = 10;
	    int i_3_ = 2139882933 * class534_sub29.anInt10654;
	    int i_4_ = class534_sub29.anInt10651 * 917865515;
	    int i_5_ = 0;
	    int i_6_ = 0;
	    if (null != class272.aString2969) {
		i_6_ = Class236.aClass16_2373.method737(class272.aString2969,
							(1771907305
							 * (Class706_Sub4
							    .anInt10994)),
							0, null, 2112849964);
		i_5_ = Class236.aClass16_2373.method747(class272.aString2969,
							((Class706_Sub4
							  .anInt10994)
							 * 1771907305),
							null, -1861002982);
	    }
	    int i_7_ = 2139882933 * class534_sub29.anInt10654 + i_1_ / 2;
	    int i_8_ = class534_sub29.anInt10651 * 917865515;
	    if (i_3_ < Class554_Sub1.anInt7410 + i_1_) {
		i_3_ = Class554_Sub1.anInt7410;
		i_7_ = 5 + (i_2_ + (Class554_Sub1.anInt7410 + i_1_ / 2)
			    + i_5_ / 2);
	    } else if (i_3_ > Class554_Sub1.anInt7412 - i_1_) {
		i_3_ = Class554_Sub1.anInt7412 - i_1_;
		i_7_
		    = Class554_Sub1.anInt7412 - i_1_ / 2 - i_2_ - i_5_ / 2 - 5;
	    }
	    if (i_4_ < Class554_Sub1.anInt7411 + i_1_) {
		i_4_ = Class554_Sub1.anInt7411;
		i_8_ = i_1_ / 2 + (Class554_Sub1.anInt7411 + i_2_);
	    } else if (i_4_ > Class554_Sub1.anInt7378 - i_1_) {
		i_4_ = Class554_Sub1.anInt7378 - i_1_;
		i_8_ = Class554_Sub1.anInt7378 - i_1_ / 2 - i_2_ - i_6_;
	    }
	    int i_9_
		= (int) (Math.atan2((double) (i_3_ - (class534_sub29.anInt10654
						      * 2139882933)),
				    (double) (i_4_ - (class534_sub29.anInt10651
						      * 917865515)))
			 / 3.141592653589793 * 32767.0) & 0xffff;
	    class163.method2665((float) i_1_ / 2.0F + (float) i_3_,
				(float) i_1_ / 2.0F + (float) i_4_, 4096,
				i_9_);
	    int i_10_ = -2;
	    int i_11_ = -2;
	    int i_12_ = -2;
	    int i_13_ = -2;
	    if (class272.aString2969 != null) {
		i_10_ = i_7_ - i_5_ / 2 - 5;
		i_11_ = i_8_;
		i_12_ = i_5_ + i_10_ + 10;
		i_13_ = i_11_ + i_6_ + 3;
		if (1313711519 * class272.anInt2976 != 0)
		    class185.method3292(i_10_, i_11_, i_12_ - i_10_,
					i_13_ - i_11_,
					1313711519 * class272.anInt2976,
					-1621754747);
		if (0 != class272.anInt3009 * -2045123201)
		    class185.method3425(i_10_, i_11_, i_12_ - i_10_,
					i_13_ - i_11_,
					-2045123201 * class272.anInt3009,
					-1992814835);
		Class582.aClass171_7771.method2844
		    (class272.aString2969, i_7_, i_8_, i_5_, i_6_,
		     ~0xffffff | class272.anInt2970 * -1512587879,
		     335392643 * Class554_Sub1.aClass622_7353.anInt8145, 1, 0,
		     0, null, null, null, 0, 0, 202025040);
	    }
	    if (1747122653 * class272.anInt2967 != -1
		|| class272.aString2969 != null) {
		Class534_Sub20 class534_sub20
		    = new Class534_Sub20(class534_sub29);
		class534_sub20.anInt10520 = -773567207 * (i_3_ - i_1_ / 2);
		class534_sub20.anInt10522 = (i_3_ + i_1_ / 2) * -1946346005;
		class534_sub20.anInt10519 = (i_4_ - i_1_) * -1190860309;
		class534_sub20.anInt10518 = i_4_ * -1010377381;
		class534_sub20.anInt10521 = i_10_ * 869904827;
		class534_sub20.anInt10517 = -536426739 * i_12_;
		class534_sub20.anInt10523 = -2069055837 * i_11_;
		class534_sub20.anInt10524 = -1973105707 * i_13_;
		Class615.aClass700_8055.method14131(class534_sub20,
						    (short) 789);
	    }
	}
    }
}
