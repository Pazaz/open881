/* Class394_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class394_Sub1 extends Class394
{
    public int anInt10142;
    public int anInt10143;
    public int anInt10144;
    public int anInt10145;
    public int anInt10146;
    public int anInt10147;
    public static Class44_Sub18 aClass44_Sub18_10148;
    
    Class394_Sub1(Class401 class401, Class391 class391, int i, int i_0_,
		  int i_1_, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_,
		  int i_7_, int i_8_, int i_9_, int i_10_, int i_11_) {
	super(class401, class391, i, i_0_, i_1_, i_2_, i_3_, i_4_, i_5_);
	anInt10143 = i_6_ * 766336471;
	anInt10142 = -2109621593 * i_7_;
	anInt10147 = -1143117661 * i_8_;
	anInt10145 = -1156263799 * i_9_;
	anInt10146 = i_10_ * 335735037;
	anInt10144 = i_11_ * 833295809;
    }
    
    public Class397 method348(int i) {
	return Class397.aClass397_4111;
    }
    
    public Class397 method351() {
	return Class397.aClass397_4111;
    }
    
    public Class397 method350() {
	return Class397.aClass397_4111;
    }
    
    public Class397 method349() {
	return Class397.aClass397_4111;
    }
    
    public static Class394 method15774(Class534_Sub40 class534_sub40) {
	Class394 class394
	    = Class44_Sub19.method17364(class534_sub40, 760219913);
	int i = class534_sub40.method16550((byte) 36);
	int i_12_ = class534_sub40.method16550((byte) 91);
	int i_13_ = class534_sub40.method16550((byte) -2);
	int i_14_ = class534_sub40.method16550((byte) -36);
	int i_15_ = class534_sub40.method16550((byte) 3);
	int i_16_ = class534_sub40.method16550((byte) 20);
	return new Class394_Sub1(class394.aClass401_4096,
				 class394.aClass391_4095,
				 class394.anInt4101 * -2127596367,
				 -1055236307 * class394.anInt4093,
				 -1607607997 * class394.anInt4097,
				 class394.anInt4098 * -228886179,
				 class394.anInt4099 * -81046249,
				 class394.anInt4100 * -120853723,
				 1210620409 * class394.anInt4094, i, i_12_,
				 i_13_, i_14_, i_15_, i_16_);
    }
    
    public static Class394 method15775(Class534_Sub40 class534_sub40) {
	Class394 class394
	    = Class44_Sub19.method17364(class534_sub40, -493282094);
	int i = class534_sub40.method16550((byte) -2);
	int i_17_ = class534_sub40.method16550((byte) 41);
	int i_18_ = class534_sub40.method16550((byte) -20);
	int i_19_ = class534_sub40.method16550((byte) -5);
	int i_20_ = class534_sub40.method16550((byte) -84);
	int i_21_ = class534_sub40.method16550((byte) 113);
	return new Class394_Sub1(class394.aClass401_4096,
				 class394.aClass391_4095,
				 class394.anInt4101 * -2127596367,
				 -1055236307 * class394.anInt4093,
				 -1607607997 * class394.anInt4097,
				 class394.anInt4098 * -228886179,
				 class394.anInt4099 * -81046249,
				 class394.anInt4100 * -120853723,
				 1210620409 * class394.anInt4094, i, i_17_,
				 i_18_, i_19_, i_20_, i_21_);
    }
}
