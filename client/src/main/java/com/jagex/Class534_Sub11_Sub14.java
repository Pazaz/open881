/* Class534_Sub11_Sub14 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class534_Sub11_Sub14 extends Class534_Sub11
{
    byte aByte11734;
    Class346 this$0;
    byte aByte11735;
    boolean aBool11736;
    byte aByte11737;
    byte aByte11738;
    
    void method16127(Class352 class352) {
	class352.aBool3641 = aBool11736;
	class352.aByte3630 = aByte11734;
	class352.aByte3631 = aByte11735;
	class352.aByte3623 = aByte11738;
	class352.aByte3633 = aByte11737;
    }
    
    void method16128(Class534_Sub40 class534_sub40, int i) {
	aBool11736 = class534_sub40.method16527(-183580542) == 1;
	aByte11734 = class534_sub40.method16586((byte) 1);
	aByte11735 = class534_sub40.method16586((byte) 1);
	aByte11738 = class534_sub40.method16586((byte) 1);
	aByte11737 = class534_sub40.method16586((byte) 1);
    }
    
    void method16129(Class352 class352, byte i) {
	class352.aBool3641 = aBool11736;
	class352.aByte3630 = aByte11734;
	class352.aByte3631 = aByte11735;
	class352.aByte3623 = aByte11738;
	class352.aByte3633 = aByte11737;
    }
    
    void method16130(Class534_Sub40 class534_sub40) {
	aBool11736 = class534_sub40.method16527(-299074733) == 1;
	aByte11734 = class534_sub40.method16586((byte) 1);
	aByte11735 = class534_sub40.method16586((byte) 1);
	aByte11738 = class534_sub40.method16586((byte) 1);
	aByte11737 = class534_sub40.method16586((byte) 1);
    }
    
    void method16132(Class352 class352) {
	class352.aBool3641 = aBool11736;
	class352.aByte3630 = aByte11734;
	class352.aByte3631 = aByte11735;
	class352.aByte3623 = aByte11738;
	class352.aByte3633 = aByte11737;
    }
    
    void method16131(Class352 class352) {
	class352.aBool3641 = aBool11736;
	class352.aByte3630 = aByte11734;
	class352.aByte3631 = aByte11735;
	class352.aByte3623 = aByte11738;
	class352.aByte3633 = aByte11737;
    }
    
    Class534_Sub11_Sub14(Class346 class346) {
	this$0 = class346;
    }
    
    static final void method18231(Class669 class669, int i) {
	class669.anInt8600 -= 617999126;
	int i_0_ = class669.anIntArray8595[2088438307 * class669.anInt8600];
	int i_1_
	    = class669.anIntArray8595[1 + 2088438307 * class669.anInt8600];
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = i_1_ + i_0_;
    }
}
