/* Class331_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class331_Sub1 extends Class331
{
    int anInt9986 = 0;
    
    Class331_Sub1(Class472 class472, Class392_Sub1 class392_sub1) {
	super(class472, (Class392) class392_sub1);
    }
    
    public void method205(boolean bool, byte i) {
	int i_0_
	    = (aClass392_3492.aClass401_4078.method6586(aClass163_3494
							    .method22(),
							(-321474631
							 * client.anInt11047),
							81478869)
	       + -1151439181 * aClass392_3492.anInt4079);
	int i_1_
	    = (aClass392_3492.aClass391_4080.method6544(aClass163_3494
							    .method2692(),
							(43072843
							 * client.anInt11192),
							(byte) 101)
	       + aClass392_3492.anInt4081 * -963484815);
	aClass163_3494.method2665((float) (i_0_
					   + aClass163_3494.method22() / 2),
				  (float) (i_1_
					   + aClass163_3494.method2692() / 2),
				  4096, -1631547953 * anInt9986);
	anInt9986 += 658027923 * ((Class392_Sub1) aClass392_3492).anInt10223;
    }
    
    public void method202(boolean bool) {
	int i
	    = (aClass392_3492.aClass401_4078.method6586(aClass163_3494
							    .method22(),
							(-321474631
							 * client.anInt11047),
							-1370424209)
	       + -1151439181 * aClass392_3492.anInt4079);
	int i_2_
	    = (aClass392_3492.aClass391_4080.method6544(aClass163_3494
							    .method2692(),
							(43072843
							 * client.anInt11192),
							(byte) 122)
	       + aClass392_3492.anInt4081 * -963484815);
	aClass163_3494.method2665((float) (i + aClass163_3494.method22() / 2),
				  (float) (i_2_
					   + aClass163_3494.method2692() / 2),
				  4096, -1631547953 * anInt9986);
	anInt9986 += 658027923 * ((Class392_Sub1) aClass392_3492).anInt10223;
    }
}
