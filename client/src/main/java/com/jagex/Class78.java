/* Class78 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class78 implements Interface13, Interface7
{
    public int anInt821;
    public int anInt822;
    public int anInt823 = 0;
    public int anInt824;
    public static Class103 aClass103_825;
    public static Class110_Sub1_Sub2 aClass110_Sub1_Sub2_826;
    static int anInt827;
    
    public void method65(int i) {
	/* empty */
    }
    
    public void method84() {
	/* empty */
    }
    
    void method1615(Class534_Sub40 class534_sub40, int i, int i_0_) {
	if (i == 1)
	    anInt823 = class534_sub40.method16527(-1338163803) * -1463508423;
	else if (2 == i)
	    anInt824 = class534_sub40.method16529((byte) 1) * 2084614735;
	else if (i == 3)
	    anInt821 = class534_sub40.method16529((byte) 1) * 1926199243;
	else if (i == 4)
	    anInt822 = class534_sub40.method16530((byte) -98) * 1100421249;
    }
    
    public void method67(int i, int i_1_) {
	/* empty */
    }
    
    public void method66(int i) {
	/* empty */
    }
    
    public void method83() {
	/* empty */
    }
    
    public void method78(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(-1061148867);
	    if (i == 0)
		break;
	    method1615(class534_sub40, i, 898245157);
	}
    }
    
    public void method81(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(1361584548);
	    if (i == 0)
		break;
	    method1615(class534_sub40, i, 898245157);
	}
    }
    
    public void method79(Class534_Sub40 class534_sub40, byte i) {
	for (;;) {
	    int i_2_ = class534_sub40.method16527(-1659171638);
	    if (i_2_ == 0)
		break;
	    method1615(class534_sub40, i_2_, 898245157);
	}
    }
    
    Class78() {
	anInt822 = 0;
	anInt821 = 2076071936;
	anInt824 = 93485056;
    }
    
    public void method80(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(-793127858);
	    if (i == 0)
		break;
	    method1615(class534_sub40, i, 898245157);
	}
    }
    
    public void method82(int i) {
	/* empty */
    }
    
    void method1616(Class534_Sub40 class534_sub40, int i) {
	if (i == 1)
	    anInt823 = class534_sub40.method16527(-1066662431) * -1463508423;
	else if (2 == i)
	    anInt824 = class534_sub40.method16529((byte) 1) * 2084614735;
	else if (i == 3)
	    anInt821 = class534_sub40.method16529((byte) 1) * 1926199243;
	else if (i == 4)
	    anInt822 = class534_sub40.method16530((byte) -108) * 1100421249;
    }
    
    void method1617(Class534_Sub40 class534_sub40, int i) {
	if (i == 1)
	    anInt823 = class534_sub40.method16527(-1553897221) * -1463508423;
	else if (2 == i)
	    anInt824 = class534_sub40.method16529((byte) 1) * 2084614735;
	else if (i == 3)
	    anInt821 = class534_sub40.method16529((byte) 1) * 1926199243;
	else if (i == 4)
	    anInt822 = class534_sub40.method16530((byte) -2) * 1100421249;
    }
    
    static final void method1618(int i) {
	int[] is = Class108.anIntArray1322;
	for (int i_3_ = 0; i_3_ < -1843550713 * Class108.anInt1321; i_3_++) {
	    Class654_Sub1_Sub5_Sub1_Sub2 class654_sub1_sub5_sub1_sub2
		= client.aClass654_Sub1_Sub5_Sub1_Sub2Array11279[is[i_3_]];
	    if (null != class654_sub1_sub5_sub1_sub2)
		class654_sub1_sub5_sub1_sub2.method18589(1588280675);
	}
	for (int i_4_ = 0; i_4_ < -1125820437 * client.anInt11321; i_4_++) {
	    long l = (long) client.anIntArray11088[i_4_];
	    Class534_Sub6 class534_sub6
		= (Class534_Sub6) client.aClass9_11331.method579(l);
	    if (class534_sub6 != null)
		((Class654_Sub1_Sub5_Sub1) class534_sub6.anObject10417)
		    .method18589(1732369643);
	}
	if (3 == client.anInt11155 * -1468443459) {
	    for (int i_5_ = 0; i_5_ < Class65.aClass192Array712.length;
		 i_5_++) {
		Class192 class192 = Class65.aClass192Array712[i_5_];
		if (class192.aBool2142)
		    class192.method3775(2088438307).method18589(1976963601);
	    }
	}
    }
    
    static void method1619(Class669 class669, byte i) {
	Class273 class273
	    = ((Class273)
	       Class223.aClass53_Sub2_2316.method91((class669.anIntArray8595
						     [((class669.anInt8600
							-= 308999563)
						       * 2088438307)]),
						    -1344082647));
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = (class273.anIntArray3013 == null ? 0
	       : class273.anIntArray3013.length);
    }
    
    public static void method1620(Class185 class185, int i) {
	Class219.aClass171_2307
	    = ((Class171)
	       Class351.aClass406_3620.method6650(client.anInterface52_11081,
						  (-1643399711
						   * Class67.anInt715),
						  true, true, (byte) 0));
	Class690_Sub9.aClass16_10875
	    = Class351.aClass406_3620.method6666(client.anInterface52_11081,
						 (-1643399711
						  * Class67.anInt715),
						 (byte) 31);
	Class539_Sub1.aClass171_10327
	    = ((Class171)
	       Class351.aClass406_3620.method6650(client.anInterface52_11081,
						  (-849564261
						   * Class67.anInt716),
						  true, true, (byte) 0));
	Class67.aClass16_720
	    = Class351.aClass406_3620.method6666(client.anInterface52_11081,
						 -849564261 * Class67.anInt716,
						 (byte) 17);
	Class231.aClass171_2325
	    = ((Class171)
	       Class351.aClass406_3620.method6650(client.anInterface52_11081,
						  Class67.anInt717 * 973015101,
						  true, true, (byte) 0));
	Class322.aClass16_3420
	    = Class351.aClass406_3620.method6666(client.anInterface52_11081,
						 973015101 * Class67.anInt717,
						 (byte) 83);
    }
}
