/* Class273 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class273 implements Interface13
{
    int[] anIntArray3012;
    public int[] anIntArray3013;
    public String aString3014;
    public int anInt3015;
    int[][] anIntArrayArray3016;
    public int anInt3017 = 0;
    public int anInt3018 = 0;
    public boolean aBool3019 = false;
    public int anInt3020;
    int[] anIntArray3021;
    int[] anIntArray3022;
    public int[][] anIntArrayArray3023;
    public int[] anIntArray3024;
    Interface14 anInterface14_3025;
    public String aString3026;
    int[][] anIntArrayArray3027;
    public String[] aStringArray3028;
    public int[] anIntArray3029;
    int[] anIntArray3030;
    int[] anIntArray3031;
    public String[] aStringArray3032;
    Class9 aClass9_3033;
    public int anInt3034;
    
    public boolean method5069(Interface20 interface20) {
	if (Class69.method1400(interface20, anInterface14_3025, (byte) 2)
	    < -950684159 * anInt3034)
	    return false;
	return true;
    }
    
    public void method79(Class534_Sub40 class534_sub40, byte i) {
	for (;;) {
	    int i_0_ = class534_sub40.method16527(-1454083663);
	    if (i_0_ == 0)
		break;
	    method5070(class534_sub40, i_0_, -1387266431);
	}
    }
    
    void method5070(Class534_Sub40 class534_sub40, int i, int i_1_) {
	if (1 == i)
	    aString3026 = class534_sub40.method16523(-1386869491);
	else if (2 == i)
	    aString3014 = class534_sub40.method16523(-1987454419);
	else if (i == 3) {
	    int i_2_ = class534_sub40.method16527(1569499136);
	    anIntArrayArray3027 = new int[i_2_][3];
	    for (int i_3_ = 0; i_3_ < i_2_; i_3_++) {
		anIntArrayArray3027[i_3_][0]
		    = class534_sub40.method16529((byte) 1);
		anIntArrayArray3027[i_3_][1]
		    = class534_sub40.method16533(-258848859);
		anIntArrayArray3027[i_3_][2]
		    = class534_sub40.method16533(-258848859);
	    }
	} else if (i == 4) {
	    int i_4_ = class534_sub40.method16527(-2101657792);
	    anIntArrayArray3016 = new int[i_4_][3];
	    for (int i_5_ = 0; i_5_ < i_4_; i_5_++) {
		anIntArrayArray3016[i_5_][0]
		    = class534_sub40.method16529((byte) 1);
		anIntArrayArray3016[i_5_][1]
		    = class534_sub40.method16533(-258848859);
		anIntArrayArray3016[i_5_][2]
		    = class534_sub40.method16533(-258848859);
	    }
	} else if (i == 5)
	    class534_sub40.method16529((byte) 1);
	else if (i == 6)
	    anInt3017 = class534_sub40.method16527(160366205) * 11786055;
	else if (7 == i)
	    anInt3018 = class534_sub40.method16527(-1126514580) * 309943947;
	else if (i == 8)
	    aBool3019 = true;
	else if (9 == i)
	    anInt3020 = class534_sub40.method16527(-1506804301) * 879390143;
	else if (10 == i) {
	    int i_6_ = class534_sub40.method16527(22578000);
	    anIntArray3021 = new int[i_6_];
	    for (int i_7_ = 0; i_7_ < i_6_; i_7_++)
		anIntArray3021[i_7_] = class534_sub40.method16533(-258848859);
	} else if (12 == i)
	    class534_sub40.method16533(-258848859);
	else if (13 == i) {
	    int i_8_ = class534_sub40.method16527(412222761);
	    anIntArray3024 = new int[i_8_];
	    for (int i_9_ = 0; i_9_ < i_8_; i_9_++)
		anIntArray3024[i_9_] = class534_sub40.method16529((byte) 1);
	} else if (14 == i) {
	    int i_10_ = class534_sub40.method16527(1315375605);
	    anIntArrayArray3023 = new int[i_10_][2];
	    for (int i_11_ = 0; i_11_ < i_10_; i_11_++) {
		anIntArrayArray3023[i_11_][0]
		    = class534_sub40.method16527(-1844526436);
		anIntArrayArray3023[i_11_][1]
		    = class534_sub40.method16527(-1512702943);
	    }
	} else if (15 == i)
	    anInt3034 = class534_sub40.method16529((byte) 1) * 2114865665;
	else if (17 == i)
	    anInt3015 = class534_sub40.method16550((byte) 10) * -68399271;
	else if (18 == i) {
	    int i_12_ = class534_sub40.method16527(-305282993);
	    anIntArray3013 = new int[i_12_];
	    anIntArray3012 = new int[i_12_];
	    anIntArray3022 = new int[i_12_];
	    aStringArray3028 = new String[i_12_];
	    for (int i_13_ = 0; i_13_ < i_12_; i_13_++) {
		anIntArray3013[i_13_] = class534_sub40.method16533(-258848859);
		anIntArray3012[i_13_] = class534_sub40.method16533(-258848859);
		anIntArray3022[i_13_] = class534_sub40.method16533(-258848859);
		aStringArray3028[i_13_]
		    = class534_sub40.method16541((byte) -59);
	    }
	} else if (i == 19) {
	    int i_14_ = class534_sub40.method16527(-278813145);
	    anIntArray3029 = new int[i_14_];
	    anIntArray3030 = new int[i_14_];
	    anIntArray3031 = new int[i_14_];
	    aStringArray3032 = new String[i_14_];
	    for (int i_15_ = 0; i_15_ < i_14_; i_15_++) {
		anIntArray3029[i_15_] = class534_sub40.method16533(-258848859);
		anIntArray3030[i_15_] = class534_sub40.method16533(-258848859);
		anIntArray3031[i_15_] = class534_sub40.method16533(-258848859);
		aStringArray3032[i_15_]
		    = class534_sub40.method16541((byte) -89);
	    }
	} else if (i == 249) {
	    int i_16_ = class534_sub40.method16527(1817413721);
	    if (null == aClass9_3033) {
		int i_17_ = Class162.method2640(i_16_, (byte) 119);
		aClass9_3033 = new Class9(i_17_);
	    }
	    for (int i_18_ = 0; i_18_ < i_16_; i_18_++) {
		boolean bool = class534_sub40.method16527(-70675273) == 1;
		int i_19_ = class534_sub40.method16531(1111123214);
		Class534 class534;
		if (bool)
		    class534 = new Class534_Sub6(class534_sub40
						     .method16541((byte) -37));
		else
		    class534
			= new Class534_Sub39(class534_sub40
						 .method16533(-258848859));
		aClass9_3033.method581(class534, (long) i_19_);
	    }
	}
    }
    
    public boolean method5071(Interface20 interface20, int i, int i_20_) {
	if (null == anIntArray3013 || i < 0 || i >= anIntArray3013.length)
	    return false;
	Class150 class150
	    = ((Interface18) interface20).method108(Class453.aClass453_4946,
						    anIntArray3013[i],
						    1917683065);
	int i_21_ = interface20.method120(class150, (byte) -111);
	if (i_21_ < anIntArray3012[i] || i_21_ > anIntArray3022[i])
	    return false;
	return true;
    }
    
    public int method5072(int i, int i_22_, byte i_23_) {
	if (null == aClass9_3033)
	    return i_22_;
	Class534_Sub39 class534_sub39
	    = (Class534_Sub39) aClass9_3033.method579((long) i);
	if (class534_sub39 == null)
	    return i_22_;
	return -705967177 * class534_sub39.anInt10807;
    }
    
    public String method5073(int i, String string, int i_24_) {
	if (aClass9_3033 == null)
	    return string;
	Class534_Sub6 class534_sub6
	    = (Class534_Sub6) aClass9_3033.method579((long) i);
	if (null == class534_sub6)
	    return string;
	return (String) class534_sub6.anObject10417;
    }
    
    public boolean method5074(Interface20 interface20, int i) {
	if (anIntArrayArray3023 == null || i < 0
	    || i >= anIntArrayArray3023.length)
	    return false;
	if (((Interface73) interface20).method491(anIntArrayArray3023[i][0],
						  (byte) 0)
	    < anIntArrayArray3023[i][1])
	    return false;
	return true;
    }
    
    public boolean method5075(Interface20 interface20, byte i) {
	if (anIntArrayArray3027 != null) {
	    for (int i_25_ = 0; i_25_ < anIntArrayArray3027.length; i_25_++) {
		Class150 class150
		    = (((Interface18) interface20).method108
		       (Class453.aClass453_4946, anIntArrayArray3027[i_25_][0],
			1633439447));
		if (interface20.method120(class150, (byte) -55)
		    >= anIntArrayArray3027[i_25_][2])
		    return true;
	    }
	}
	if (anIntArrayArray3016 != null) {
	    for (int i_26_ = 0; i_26_ < anIntArrayArray3016.length; i_26_++) {
		Class318 class318
		    = (((Interface18) interface20).method107
		       (anIntArrayArray3016[i_26_][0], 1504047109));
		if (interface20.method119(class318, -1385072891)
		    >= anIntArrayArray3016[i_26_][2])
		    return true;
	    }
	}
	return false;
    }
    
    public boolean method5076(Interface20 interface20, byte i) {
	if (Class69.method1400(interface20, anInterface14_3025, (byte) 2)
	    < -950684159 * anInt3034)
	    return false;
	if (null != anIntArrayArray3023) {
	    for (int i_27_ = 0; i_27_ < anIntArrayArray3023.length; i_27_++) {
		if (((Interface73) interface20)
			.method491(anIntArrayArray3023[i_27_][0], (byte) 0)
		    < anIntArrayArray3023[i_27_][1])
		    return false;
	    }
	}
	if (anIntArray3024 != null) {
	    for (int i_28_ = 0; i_28_ < anIntArray3024.length; i_28_++) {
		if (!((Class273)
		      anInterface14_3025.method91(anIntArray3024[i_28_],
						  -1316742063))
			 .method5075(interface20, (byte) 16))
		    return false;
	    }
	}
	if (null != anIntArray3013) {
	    for (int i_29_ = 0; i_29_ < anIntArray3013.length; i_29_++) {
		Class150 class150
		    = ((Interface18) interface20).method108((Class453
							     .aClass453_4946),
							    (anIntArray3013
							     [i_29_]),
							    1630405252);
		int i_30_ = interface20.method120(class150, (byte) -108);
		if (i_30_ < anIntArray3012[i_29_]
		    || i_30_ > anIntArray3022[i_29_])
		    return false;
	    }
	}
	if (anIntArray3029 != null) {
	    for (int i_31_ = 0; i_31_ < anIntArray3029.length; i_31_++) {
		Class318 class318
		    = ((Interface18) interface20)
			  .method107(anIntArray3029[i_31_], 1504047109);
		int i_32_ = interface20.method119(class318, -1688366002);
		if (i_32_ < anIntArray3030[i_31_]
		    || i_32_ > anIntArray3031[i_31_])
		    return false;
	    }
	}
	return true;
    }
    
    public boolean method5077(Interface20 interface20, int i) {
	if (Class69.method1400(interface20, anInterface14_3025, (byte) 2)
	    < -950684159 * anInt3034)
	    return false;
	return true;
    }
    
    public boolean method5078(Interface20 interface20, int i, byte i_33_) {
	if (anIntArrayArray3023 == null || i < 0
	    || i >= anIntArrayArray3023.length)
	    return false;
	if (((Interface73) interface20).method491(anIntArrayArray3023[i][0],
						  (byte) 0)
	    < anIntArrayArray3023[i][1])
	    return false;
	return true;
    }
    
    Class273(int i, Interface14 interface14) {
	anInt3015 = 68399271;
	anInterface14_3025 = interface14;
    }
    
    public boolean method5079(Interface20 interface20, int i) {
	if (null == anIntArray3029 || i < 0 || i >= anIntArray3029.length)
	    return false;
	Class318 class318
	    = ((Interface18) interface20).method107(anIntArray3029[i],
						    1504047109);
	int i_34_ = interface20.method119(class318, -1873774663);
	if (i_34_ < anIntArray3030[i] || i_34_ > anIntArray3031[i])
	    return false;
	return true;
    }
    
    public boolean method5080(Interface20 interface20, int i, byte i_35_) {
	if (null == anIntArray3029 || i < 0 || i >= anIntArray3029.length)
	    return false;
	Class318 class318
	    = ((Interface18) interface20).method107(anIntArray3029[i],
						    1504047109);
	int i_36_ = interface20.method119(class318, -1806905066);
	if (i_36_ < anIntArray3030[i] || i_36_ > anIntArray3031[i])
	    return false;
	return true;
    }
    
    public void method80(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(-2015515401);
	    if (i == 0)
		break;
	    method5070(class534_sub40, i, -1345898361);
	}
    }
    
    public void method78(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(-1797055058);
	    if (i == 0)
		break;
	    method5070(class534_sub40, i, -1472301684);
	}
    }
    
    public void method81(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(-749402663);
	    if (i == 0)
		break;
	    method5070(class534_sub40, i, -2135546377);
	}
    }
    
    public void method83() {
	if (aString3014 == null)
	    aString3014 = aString3026;
    }
    
    public boolean method5081(Interface20 interface20, int i, byte i_37_) {
	if (null == anIntArray3024 || i < 0 || i >= anIntArray3024.length)
	    return false;
	if (!((Class273)
	      anInterface14_3025.method91(anIntArray3024[i], 1027453251))
		 .method5075(interface20, (byte) -8))
	    return false;
	return true;
    }
    
    public int method5082(int i, int i_38_) {
	if (null == aClass9_3033)
	    return i_38_;
	Class534_Sub39 class534_sub39
	    = (Class534_Sub39) aClass9_3033.method579((long) i);
	if (class534_sub39 == null)
	    return i_38_;
	return -705967177 * class534_sub39.anInt10807;
    }
    
    public String method5083(int i, String string) {
	if (aClass9_3033 == null)
	    return string;
	Class534_Sub6 class534_sub6
	    = (Class534_Sub6) aClass9_3033.method579((long) i);
	if (null == class534_sub6)
	    return string;
	return (String) class534_sub6.anObject10417;
    }
    
    public void method84() {
	if (aString3014 == null)
	    aString3014 = aString3026;
    }
    
    public boolean method5084(Interface20 interface20) {
	if (anIntArrayArray3027 != null) {
	    for (int i = 0; i < anIntArrayArray3027.length; i++) {
		Class150 class150 = (((Interface18) interface20).method108
				     (Class453.aClass453_4946,
				      anIntArrayArray3027[i][0], 1591664045));
		if (interface20.method120(class150, (byte) -116)
		    >= anIntArrayArray3027[i][1])
		    return true;
	    }
	}
	if (anIntArrayArray3016 != null) {
	    for (int i = 0; i < anIntArrayArray3016.length; i++) {
		Class318 class318
		    = ((Interface18) interface20)
			  .method107(anIntArrayArray3016[i][0], 1504047109);
		if (interface20.method119(class318, -1473665337)
		    >= anIntArrayArray3016[i][1])
		    return true;
	    }
	}
	return false;
    }
    
    public boolean method5085(Interface20 interface20) {
	if (Class69.method1400(interface20, anInterface14_3025, (byte) 2)
	    < -950684159 * anInt3034)
	    return false;
	if (null != anIntArrayArray3023) {
	    for (int i = 0; i < anIntArrayArray3023.length; i++) {
		if (((Interface73) interface20)
			.method491(anIntArrayArray3023[i][0], (byte) 0)
		    < anIntArrayArray3023[i][1])
		    return false;
	    }
	}
	if (anIntArray3024 != null) {
	    for (int i = 0; i < anIntArray3024.length; i++) {
		if (!((Class273) anInterface14_3025.method91(anIntArray3024[i],
							     779081936))
			 .method5075(interface20, (byte) -81))
		    return false;
	    }
	}
	if (null != anIntArray3013) {
	    for (int i = 0; i < anIntArray3013.length; i++) {
		Class150 class150
		    = ((Interface18) interface20).method108((Class453
							     .aClass453_4946),
							    anIntArray3013[i],
							    1764220133);
		int i_39_ = interface20.method120(class150, (byte) -17);
		if (i_39_ < anIntArray3012[i] || i_39_ > anIntArray3022[i])
		    return false;
	    }
	}
	if (anIntArray3029 != null) {
	    for (int i = 0; i < anIntArray3029.length; i++) {
		Class318 class318
		    = ((Interface18) interface20).method107(anIntArray3029[i],
							    1504047109);
		int i_40_ = interface20.method119(class318, -1862059444);
		if (i_40_ < anIntArray3030[i] || i_40_ > anIntArray3031[i])
		    return false;
	    }
	}
	return true;
    }
    
    public boolean method5086(Interface20 interface20) {
	if (anIntArrayArray3027 != null) {
	    for (int i = 0; i < anIntArrayArray3027.length; i++) {
		Class150 class150 = (((Interface18) interface20).method108
				     (Class453.aClass453_4946,
				      anIntArrayArray3027[i][0], 1820632204));
		if (interface20.method120(class150, (byte) -68)
		    >= anIntArrayArray3027[i][1])
		    return true;
	    }
	}
	if (anIntArrayArray3016 != null) {
	    for (int i = 0; i < anIntArrayArray3016.length; i++) {
		Class318 class318
		    = ((Interface18) interface20)
			  .method107(anIntArrayArray3016[i][0], 1504047109);
		if (interface20.method119(class318, -1994859448)
		    >= anIntArrayArray3016[i][1])
		    return true;
	    }
	}
	return false;
    }
    
    public boolean method5087(Interface20 interface20, int i) {
	if (anIntArrayArray3023 == null || i < 0
	    || i >= anIntArrayArray3023.length)
	    return false;
	if (((Interface73) interface20).method491(anIntArrayArray3023[i][0],
						  (byte) 0)
	    < anIntArrayArray3023[i][1])
	    return false;
	return true;
    }
    
    static int method5088(Interface20 interface20, Interface14 interface14) {
	int i = 0;
	for (int i_41_ = 0; i_41_ < interface14.method90((byte) 126);
	     i_41_++) {
	    Class273 class273
		= (Class273) interface14.method91(i_41_, -1923483601);
	    if (class273.method5075(interface20, (byte) -10))
		i += -1628667329 * class273.anInt3020;
	}
	return i;
    }
    
    public void method82(int i) {
	if (aString3014 == null)
	    aString3014 = aString3026;
    }
    
    public boolean method5089(Interface20 interface20, int i) {
	if (anIntArrayArray3027 != null) {
	    for (int i_42_ = 0; i_42_ < anIntArrayArray3027.length; i_42_++) {
		Class150 class150
		    = (((Interface18) interface20).method108
		       (Class453.aClass453_4946, anIntArrayArray3027[i_42_][0],
			2021132953));
		if (interface20.method120(class150, (byte) -55)
		    >= anIntArrayArray3027[i_42_][1])
		    return true;
	    }
	}
	if (anIntArrayArray3016 != null) {
	    for (int i_43_ = 0; i_43_ < anIntArrayArray3016.length; i_43_++) {
		Class318 class318
		    = (((Interface18) interface20).method107
		       (anIntArrayArray3016[i_43_][0], 1504047109));
		if (interface20.method119(class318, -1403725372)
		    >= anIntArrayArray3016[i_43_][1])
		    return true;
	    }
	}
	return false;
    }
    
    public boolean method5090(Interface20 interface20, int i) {
	if (anIntArrayArray3023 == null || i < 0
	    || i >= anIntArrayArray3023.length)
	    return false;
	if (((Interface73) interface20).method491(anIntArrayArray3023[i][0],
						  (byte) 0)
	    < anIntArrayArray3023[i][1])
	    return false;
	return true;
    }
    
    public boolean method5091(Interface20 interface20, int i) {
	if (null == anIntArray3013 || i < 0 || i >= anIntArray3013.length)
	    return false;
	Class150 class150
	    = ((Interface18) interface20).method108(Class453.aClass453_4946,
						    anIntArray3013[i],
						    1768607627);
	int i_44_ = interface20.method120(class150, (byte) -112);
	if (i_44_ < anIntArray3012[i] || i_44_ > anIntArray3022[i])
	    return false;
	return true;
    }
    
    public boolean method5092(Interface20 interface20, int i) {
	if (null == anIntArray3029 || i < 0 || i >= anIntArray3029.length)
	    return false;
	Class318 class318
	    = ((Interface18) interface20).method107(anIntArray3029[i],
						    1504047109);
	int i_45_ = interface20.method119(class318, -1246903505);
	if (i_45_ < anIntArray3030[i] || i_45_ > anIntArray3031[i])
	    return false;
	return true;
    }
    
    public boolean method5093(Interface20 interface20, int i) {
	if (null == anIntArray3024 || i < 0 || i >= anIntArray3024.length)
	    return false;
	if (!((Class273)
	      anInterface14_3025.method91(anIntArray3024[i], 37131459))
		 .method5075(interface20, (byte) -66))
	    return false;
	return true;
    }
    
    public boolean method5094(Interface20 interface20, int i) {
	if (null == anIntArray3029 || i < 0 || i >= anIntArray3029.length)
	    return false;
	Class318 class318
	    = ((Interface18) interface20).method107(anIntArray3029[i],
						    1504047109);
	int i_46_ = interface20.method119(class318, -2028454267);
	if (i_46_ < anIntArray3030[i] || i_46_ > anIntArray3031[i])
	    return false;
	return true;
    }
    
    public boolean method5095(Interface20 interface20) {
	if (Class69.method1400(interface20, anInterface14_3025, (byte) 2)
	    < -950684159 * anInt3034)
	    return false;
	if (null != anIntArrayArray3023) {
	    for (int i = 0; i < anIntArrayArray3023.length; i++) {
		if (((Interface73) interface20)
			.method491(anIntArrayArray3023[i][0], (byte) 0)
		    < anIntArrayArray3023[i][1])
		    return false;
	    }
	}
	if (anIntArray3024 != null) {
	    for (int i = 0; i < anIntArray3024.length; i++) {
		if (!((Class273) anInterface14_3025.method91(anIntArray3024[i],
							     782952076))
			 .method5075(interface20, (byte) -47))
		    return false;
	    }
	}
	if (null != anIntArray3013) {
	    for (int i = 0; i < anIntArray3013.length; i++) {
		Class150 class150
		    = ((Interface18) interface20).method108((Class453
							     .aClass453_4946),
							    anIntArray3013[i],
							    1632412284);
		int i_47_ = interface20.method120(class150, (byte) -112);
		if (i_47_ < anIntArray3012[i] || i_47_ > anIntArray3022[i])
		    return false;
	    }
	}
	if (anIntArray3029 != null) {
	    for (int i = 0; i < anIntArray3029.length; i++) {
		Class318 class318
		    = ((Interface18) interface20).method107(anIntArray3029[i],
							    1504047109);
		int i_48_ = interface20.method119(class318, -1077867410);
		if (i_48_ < anIntArray3030[i] || i_48_ > anIntArray3031[i])
		    return false;
	    }
	}
	return true;
    }
    
    static int method5096(Interface20 interface20, Interface14 interface14) {
	int i = 0;
	for (int i_49_ = 0; i_49_ < interface14.method90((byte) 95); i_49_++) {
	    Class273 class273
		= (Class273) interface14.method91(i_49_, 597426384);
	    if (class273.method5075(interface20, (byte) -69))
		i += -1628667329 * class273.anInt3020;
	}
	return i;
    }
    
    static int method5097(Interface20 interface20, Interface14 interface14) {
	int i = 0;
	for (int i_50_ = 0; i_50_ < interface14.method90((byte) 66); i_50_++) {
	    Class273 class273
		= (Class273) interface14.method91(i_50_, 817609840);
	    if (class273.method5075(interface20, (byte) -3))
		i += -1628667329 * class273.anInt3020;
	}
	return i;
    }
    
    public static void method5098(int i, byte i_51_) {
	Class597.anInt7862 = 711542487 * i;
	synchronized (Class631.aClass203_8221) {
	    Class631.aClass203_8221.method3877(1876787674);
	}
	synchronized (Class631.aClass203_8220) {
	    Class631.aClass203_8220.method3877(357015007);
	}
    }
    
    static final void method5099(Class669 class669, int i) {
	int i_52_ = (class669.anIntArray8595
		     [(class669.anInt8600 -= 308999563) * 2088438307]);
	if (i_52_ >= 1 && i_52_ <= 2) {
	    Class44_Sub6.aClass534_Sub35_10989.method16438
		(Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub8_10775,
		 i_52_, -823170016);
	    Class44_Sub6.aClass534_Sub35_10989.method16438
		(Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub8_10752,
		 i_52_, -885825551);
	    Class672.method11096((byte) 1);
	}
    }
    
    static final void method5100(Class669 class669, int i) {
	class669.anInt8600 -= 617999126;
	int i_53_ = class669.anIntArray8595[2088438307 * class669.anInt8600];
	int i_54_
	    = class669.anIntArray8595[2088438307 * class669.anInt8600 + 1];
	method5102(7, i_53_, i_54_, "", (byte) -2);
    }
    
    static final void method5101(int i) {
	for (Class536_Sub3 class536_sub3
		 = ((Class536_Sub3)
		    client.aClass709_11165.method14290(1926478481));
	     null != class536_sub3;
	     class536_sub3 = ((Class536_Sub3)
			      client.aClass709_11165.method14290(-2109120994)))
	    Class464.method7566(class536_sub3, -1687028190);
	int i_55_ = 0;
	int i_56_ = 3;
	if (3 == -1468443459 * client.anInt11155) {
	    for (int i_57_ = i_55_; i_57_ <= i_56_; i_57_++)
		client.method17496(i_57_);
	    client.method17390();
	} else {
	    client.method17385();
	    for (int i_58_ = i_55_; i_58_ <= i_56_; i_58_++) {
		client.method17386();
		client.method17814(i_58_);
		client.method17496(i_58_);
	    }
	    client.method17389();
	    client.method17390();
	}
    }
    
    public static void method5102(int i, int i_59_, int i_60_, String string,
				  byte i_61_) {
	Class247 class247 = Class81.method1637(i_59_, i_60_, 887253453);
	if (class247 != null) {
	    if (class247.anObjectArray2619 != null) {
		Class534_Sub41 class534_sub41 = new Class534_Sub41();
		class534_sub41.aClass247_10818 = class247;
		class534_sub41.anInt10821 = -281580947 * i;
		class534_sub41.aString10825 = string;
		class534_sub41.anObjectArray10819 = class247.anObjectArray2619;
		Class94.method1764(class534_sub41, 1981182801);
	    }
	    if (client.method17392(class247).method16264(i - 1, (byte) -94)) {
		Class100 class100 = Class201.method3864(2095398292);
		if (9 == -1850530127 * client.anInt11039
		    || 16 == -1850530127 * client.anInt11039
		    || 18 == -1850530127 * client.anInt11039) {
		    if (class247.aString2495 != null
			&& null != class247.aClass253_2609) {
			Class534_Sub19 class534_sub19
			    = Class346.method6128(Class404.aClass404_4214,
						  class100.aClass13_1183,
						  1341391005);
			class534_sub19.aClass534_Sub40_Sub1_10513.method16506
			    (7 + Class668.method11029(class247.aString2495,
						      (byte) 0) + 1,
			     767692544);
			class534_sub19.aClass534_Sub40_Sub1_10513
			    .method16617(i_59_, -516157595);
			class534_sub19.aClass534_Sub40_Sub1_10513.method16506
			    (class247.aClass253_2609.method93(), 784456390);
			class534_sub19.aClass534_Sub40_Sub1_10513
			    .method16506(i, 2094025480);
			class534_sub19.aClass534_Sub40_Sub1_10513
			    .method16568(i_60_, 1931584277);
			class534_sub19.aClass534_Sub40_Sub1_10513
			    .method16713(class247.aString2495, -894372874);
			class100.method1863(class534_sub19, (byte) 79);
		    } else
			Class573.method9643(class100, class247, i, i_59_,
					    i_60_, -1694204985);
		}
	    }
	}
    }
}
