/* Class479 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class479
{
    float aFloat5199;
    long aLong5200;
    float aFloat5201;
    float aFloat5202;
    int anInt5203;
    boolean aBool5204;
    int anInt5205;
    float aFloat5206;
    Class497[] aClass497Array5207;
    int anInt5208;
    int anInt5209;
    int anInt5210;
    float[] aFloatArray5211;
    float aFloat5212;
    int anInt5213;
    Object anObject5214;
    int anInt5215;
    int anInt5216;
    int anInt5217;
    int anInt5218;
    int anInt5219;
    int[] anIntArray5220;
    int[] anIntArray5221;
    int anInt5222;
    int anInt5223;
    boolean aBool5224;
    Class485 aClass485_5225;
    Interface40 anInterface40_5226;
    Class367 aClass367_5227;
    boolean aBool5228;
    Interface58 anInterface58_5229;
    Object anObject5230;
    boolean aBool5231;
    int anInt5232;
    byte[] aByteArray5233;
    boolean aBool5234;
    int anInt5235;
    int anInt5236;
    int[] anIntArray5237;
    int anInt5238;
    int anInt5239;
    boolean aBool5240;
    int anInt5241;
    int anInt5242 = 0;
    long aLong5243;
    float aFloat5244;
    Interface56 anInterface56_5245;
    boolean aBool5246;
    int anInt5247;
    boolean aBool5248;
    boolean aBool5249;
    static int anInt5250;
    
    void method7804(float f, int i) {
	if (i <= 0) {
	    aFloat5201 = f;
	    aFloat5206 = aFloat5201;
	    aLong5243 = 0L;
	    aLong5200 = 0L;
	} else {
	    aFloat5199 = aFloat5201;
	    aFloat5206 = f;
	    aLong5243
		= Class250.method4604((byte) -126) * -7945642590094835905L;
	    aLong5200
		= 8994788786014064821L * ((long) i
					  + aLong5243 * 4767994121520197823L);
	}
    }
    
    public boolean method7805(short i) {
	return aBool5240;
    }
    
    public void method7806() {
	synchronized (this) {
	    if (method7853((byte) -13) == Class485.aClass485_5286
		|| (method7853((byte) -54).anInt5283 * -222113559
		    < -222113559 * Class485.aClass485_5281.anInt5283)) {
		/* empty */
	    } else
		method7897(Class485.aClass485_5286, (byte) -98);
	}
    }
    
    boolean method7807(int i) {
	return aClass485_5225 == Class485.aClass485_5286;
    }
    
    boolean method7808(int i) {
	if (anObject5230 != null && anObject5230 instanceof Class491) {
	    Class491 class491 = (Class491) anObject5230;
	    Interface70 interface70 = class491.method8040((byte) 121);
	    return interface70.method454(i, (byte) 29);
	}
	return false;
    }
    
    public void method7809(int i) {
	if (method7853((byte) -16).anInt5283 * -222113559
	    >= Class485.aClass485_5281.anInt5283 * -222113559)
	    throw new RuntimeException("");
	method7897(Class485.aClass485_5281, (byte) -18);
    }
    
    public void method7810(int i) {
	if (method7853((byte) 74).anInt5283 * -222113559
	    < Class485.aClass485_5281.anInt5283 * -222113559)
	    throw new RuntimeException("");
	anObject5230 = null;
	method7897(Class485.aClass485_5289, (byte) -102);
    }
    
    void method7811(float f, int i, int i_0_) {
	if (i <= 0) {
	    aFloat5201 = f;
	    aFloat5206 = aFloat5201;
	    aLong5243 = 0L;
	    aLong5200 = 0L;
	} else {
	    aFloat5199 = aFloat5201;
	    aFloat5206 = f;
	    aLong5243 = Class250.method4604((byte) -4) * -7945642590094835905L;
	    aLong5200
		= 8994788786014064821L * ((long) i
					  + aLong5243 * 4767994121520197823L);
	}
    }
    
    void method7812() {
	if (method7853((byte) -97) != Class485.aClass485_5286
	    && (method7853((byte) -16).anInt5283 * -222113559
		< -222113559 * Class485.aClass485_5284.anInt5283)) {
	    method7897(Class485.aClass485_5284, (byte) -71);
	    aBool5224 = true;
	    method7827(-2071646713);
	    method7818(-862476007);
	}
    }
    
    boolean method7813(byte[] is, Interface56 interface56, int i, float f,
		       boolean bool, boolean bool_1_, int i_2_, float f_3_,
		       Object object, int i_4_) {
	synchronized (this) {
	    if (interface56 != null) {
		aByteArray5233 = null;
		anInterface56_5245 = interface56;
	    } else
		aByteArray5233 = is;
	    anObject5230 = object;
	    anInt5205 = i * -2062056939;
	    aFloat5201 = f;
	    aBool5204 = bool_1_;
	    anInt5247 = i_2_ * -1284714303;
	    aFloat5212 = f_3_;
	    byte[] is_5_ = null;
	    if (null != aByteArray5233)
		is_5_ = aByteArray5233;
	    else if (anInterface56_5245 != null) {
		anInt5208 = 0;
		is_5_ = anInterface56_5245.method378(772436621 * anInt5208,
						     183896620);
	    }
	    if (null != is_5_) {
		Class534_Sub40 class534_sub40 = new Class534_Sub40(is_5_);
		if (class534_sub40.method16527(1544077504) != 74
		    || class534_sub40.method16527(1350324926) != 65
		    || class534_sub40.method16527(-488439408) != 71
		    || class534_sub40.method16527(1786755697) != 65)
		    throw new RuntimeException("");
		anInt5215
		    = class534_sub40.method16533(-258848859) * 1375189959;
		anInt5216 = class534_sub40.method16533(-258848859) * 343475725;
		anInt5218 = class534_sub40.method16533(-258848859) * 406267495;
		anInt5210 = class534_sub40.method16533(-258848859) * 344351509;
		anInt5219 = class534_sub40.method16533(-258848859) * 543475003;
		if (null == anObject5214) {
		    anObject5214
			= aClass367_5227.method6334(anInt5210 * 1232720957,
						    anInt5218 * 1943573847,
						    anInterface40_5226
							.method283(),
						    anInterface40_5226
							.method281(),
						    anInt5223 * -414413569,
						    aFloat5212, 1817214393);
		    if (null == anObject5214) {
			method7897(Class485.aClass485_5288, (byte) -29);
			boolean bool_6_ = false;
			return bool_6_;
		    }
		}
		anIntArray5220 = new int[749325299 * anInt5219];
		anIntArray5221 = new int[anInt5219 * 749325299];
		anIntArray5237 = new int[749325299 * anInt5219];
		int i_7_ = (anInt5219 * 1699635096
			    + class534_sub40.anInt10811 * 31645619);
		for (int i_8_ = 0; i_8_ < anInt5219 * 749325299; i_8_++) {
		    anIntArray5220[i_8_] = i_7_;
		    anIntArray5221[i_8_]
			= class534_sub40.method16533(-258848859);
		    anIntArray5237[i_8_]
			= class534_sub40.method16533(-258848859);
		    i_7_ += anIntArray5221[i_8_];
		}
		anInt5239 = -1431374605 * class534_sub40.anInt10811;
		aClass497Array5207 = new Class497[1995684471 * anInt5209];
		aFloatArray5211 = new float[2];
		for (int i_9_ = 0; i_9_ < aFloatArray5211.length; i_9_++)
		    aFloatArray5211[i_9_] = 1.0F;
		if (anObject5230 instanceof Class491) {
		    Interface70 interface70
			= ((Class491) anObject5230).method8040((byte) 2);
		    aBool5246 = interface70.method453(-1138178553);
		    aBool5231 = !aBool5246;
		}
		anInterface40_5226.method292(aBool5231,
					     (aBool5204
					      ? 1247788865 * anInt5247 : 0),
					     47814135 * anInt5215,
					     -933644091 * anInt5216);
		if (anInterface56_5245 != null) {
		    int i_10_ = (class534_sub40.aByteArray10810.length
				 - -57533887 * anInt5239);
		    byte[] is_11_
			= Class691.method14034(i_10_, true, 2144570859);
		    System.arraycopy(class534_sub40.aByteArray10810,
				     anInt5239 * -57533887, is_11_, 0, i_10_);
		    class534_sub40.aByteArray10810 = is_11_;
		    class534_sub40.anInt10811 = -1387468933 * i_10_;
		    anInterface40_5226.method70(class534_sub40);
		    anInt5208 += -1816944571;
		} else
		    aBool5228 = true;
		boolean bool_12_ = true;
		return bool_12_;
	    }
	    boolean bool_13_ = null != is_5_;
	    return bool_13_;
	}
    }
    
    void method7814(byte i) {
	synchronized (this) {
	    method7826(-732804656);
	}
    }
    
    public void method7815() {
	if (method7853((byte) 3).anInt5283 * -222113559
	    >= Class485.aClass485_5281.anInt5283 * -222113559)
	    throw new RuntimeException("");
	method7897(Class485.aClass485_5281, (byte) -40);
    }
    
    void method7816(Interface58 interface58, byte i) {
	anInterface58_5229 = interface58;
	if (null != anInterface58_5229)
	    anInterface58_5229.method379(anObject5230, aFloatArray5211, null,
					 -2024287005);
    }
    
    boolean method7817(int i) {
	if (null == aFloatArray5211)
	    return false;
	float f = 0.0F;
	float f_14_ = 0.0F;
	for (int i_15_ = 0; i_15_ < aFloatArray5211.length; i_15_++) {
	    float f_16_ = aFloatArray5211[i_15_];
	    if (f_16_ * aFloat5201 > f)
		f = f_16_;
	    if (f_16_ > f_14_)
		f_14_ = f_16_;
	}
	if (f < 1.0E-5F) {
	    if (f_14_ >= 1.0E-5F && aFloat5206 >= 1.0E-5F)
		return true;
	    return false;
	}
	return true;
    }
    
    void method7818(int i) {
	Class388 class388
	    = aClass367_5227.method6342((byte) 90)
		  .method6370(anInt5205 * 1451965757, -1938306741);
	aFloat5202 = null != class388 ? class388.method6510(1158940579) : 1.0F;
	float f = null != class388 ? class388.method6508(-1394804926) : 0.1F;
	f *= aFloat5201;
	float f_17_ = aFloatArray5211.length > 0 ? 0.0F : 1.0F;
	for (int i_18_ = 0; i_18_ < aFloatArray5211.length; i_18_++) {
	    if (aFloatArray5211[i_18_] > f_17_)
		f_17_ = aFloatArray5211[i_18_];
	}
	f *= f_17_;
	if (!method7817(2028547874))
	    f = -1.0F;
	if (method7819(1569130411) != f) {
	    method7820(f, -194225117);
	    aBool5240 = true;
	}
    }
    
    public float method7819(int i) {
	return aFloat5244;
    }
    
    void method7820(float f, int i) {
	synchronized (this) {
	    aFloat5244 = f;
	}
    }
    
    public void method7821(int i) {
	if ((method7853((byte) -21).anInt5283 * -222113559
	     >= Class485.aClass485_5284.anInt5283 * -222113559)
	    && (method7853((byte) -113).anInt5283 * -222113559
		< -222113559 * Class485.aClass485_5286.anInt5283)
	    && method7817(929261009)) {
	    synchronized (this) {
		anInterface40_5226.method84();
		method7867(-407299891);
	    }
	    Thread.yield();
	}
    }
    
    public void method7822(byte i) {
	synchronized (this) {
	    method7823((byte) -71);
	}
    }
    
    void method7823(byte i) {
	if (method7853((byte) -9).anInt5283 * -222113559
	    >= -222113559 * Class485.aClass485_5284.anInt5283) {
	    if (method7853((byte) -120) == Class485.aClass485_5289)
		method7825(-2057240595);
	    else {
		method7824((byte) -75);
		if (aFloat5206 != aFloat5201) {
		    long l = Class250.method4604((byte) -10);
		    if (l > aLong5200 * -6937883969178083939L)
			aFloat5201 = aFloat5206;
		    else {
			float f = aFloat5206 - aFloat5199;
			long l_19_ = (-6937883969178083939L * aLong5200
				      - aLong5243 * 4767994121520197823L);
			float f_20_ = f / (float) l_19_;
			aFloat5201
			    = aFloat5199 + (float) (l - (4767994121520197823L
							 * aLong5243)) * f_20_;
			aFloat5201
			    = Math.max(0.0F, Math.min(1.0F, aFloat5201));
		    }
		}
		method7818(-938048528);
		if (method7853((byte) 40).anInt5283 * -222113559
		    < Class485.aClass485_5286.anInt5283 * -222113559)
		    method7827(-2118724200);
	    }
	}
    }
    
    void method7824(byte i) {
	synchronized (this) {
	    int i_21_ = method7857(-1504393956);
	    if (aBool5246 && i_21_ > -335387595 * anInt5217
		&& anInterface40_5226.method290()) {
		/* empty */
	    } else {
		Class375 class375 = anInterface40_5226.method307();
		if (aBool5228 && class375 != Class375.aClass375_3902) {
		    if (null != aByteArray5233) {
			if (anInt5235 * -1253868879 >= anInt5219 * 749325299) {
			    anInterface40_5226.method70(null);
			    anInt5235 = 0;
			} else {
			    if (-884094607 * anInt5222 < -57533887 * anInt5239)
				anInt5222 = anInt5239 * 1157658065;
			    anInt5222
				= (anIntArray5220[-1253868879 * anInt5235]
				   + anInt5238 * -1867376947) * -143403119;
			    int i_22_
				= anIntArray5221[anInt5235 * -1253868879];
			    int i_23_ = i_22_;
			    int i_24_
				= (anInt5238 * -1867376947 + i_23_ > i_22_
				   ? (anIntArray5220[anInt5235 * -1253868879]
				      + i_22_)
				   : -884094607 * anInt5222 + i_23_);
			    Class479 class479_25_ = this;
			    class479_25_.anInt5238
				= (class479_25_.anInt5238
				   + ((-884094607 * anInt5222 + i_23_
				       > aByteArray5233.length)
				      ? (aByteArray5233.length
					 - anInt5222 * -884094607)
				      : i_23_) * -1133525499);
			    Class534_Sub40 class534_sub40
				= new Class534_Sub40(i_24_ - (anInt5222
							      * -884094607),
						     true);
			    if (aByteArray5233 == null)
				throw new RuntimeException("");
			    if (class534_sub40.aByteArray10810 == null)
				throw new RuntimeException("");
			    class534_sub40.method16519(aByteArray5233,
						       -884094607 * anInt5222,
						       i_24_ - (anInt5222
								* -884094607),
						       -1995676226);
			    anInterface40_5226.method70(class534_sub40);
			    method7881(false, (short) 7215);
			    if (-1867376947 * anInt5238 >= i_22_) {
				anInt5235 += -417821103;
				anInt5238 = 0;
			    }
			}
		    } else if (null != anInterface56_5245) {
			if (anInt5208 * 772436621 >= 749325299 * anInt5219) {
			    anInt5208 = 0;
			    anInterface40_5226.method70(null);
			} else {
			    byte[] is
				= (anInterface56_5245.method378
				   (anIntArray5237[anInt5208 * 772436621],
				    -1892360331));
			    if (null != is) {
				Class534_Sub40 class534_sub40
				    = new Class534_Sub40(is);
				class534_sub40.anInt10811
				    = is.length * -1387468933;
				anInterface40_5226.method70(class534_sub40);
				method7881(false, (short) 20979);
				anInt5208 += -1816944571;
			    }
			}
		    }
		}
	    }
	}
    }
    
    void method7825(int i) {
	synchronized (this) {
	    if (anInterface40_5226.method307() == Class375.aClass375_3908) {
		if (null != anObject5214) {
		    int i_26_
			= aClass367_5227.method6339(anObject5214, -1557077772);
		    if (i_26_ >= anInt5223 * -414413569) {
			aClass367_5227.method6346(anObject5214, -2040151372);
			anObject5214 = null;
			method7897(Class485.aClass485_5287, (byte) -75);
		    }
		} else
		    method7897(Class485.aClass485_5287, (byte) -79);
	    }
	}
	method7843(-957079687);
    }
    
    void method7826(int i) {
	if (method7853((byte) -44) != Class485.aClass485_5286
	    && (method7853((byte) -54).anInt5283 * -222113559
		< -222113559 * Class485.aClass485_5284.anInt5283)) {
	    method7897(Class485.aClass485_5284, (byte) -67);
	    aBool5224 = true;
	    method7827(-2117020810);
	    method7818(-1785336125);
	}
    }
    
    void method7827(int i) {
	if (aBool5224 == true
	    && (method7853((byte) -87).anInt5283 * -222113559
		>= Class485.aClass485_5284.anInt5283 * -222113559)
	    && (method7853((byte) 93).anInt5283 * -222113559
		< Class485.aClass485_5288.anInt5283 * -222113559)
	    && anInterface58_5229 != null && aFloat5206 == aFloat5201)
	    anInterface58_5229.method379(anObject5230, aFloatArray5211, null,
					 -2024287005);
    }
    
    void method7828(byte i) {
	aBool5249 = true;
    }
    
    void method7829(int i) {
	aBool5249 = false;
    }
    
    float method7830(int i) {
	return aFloat5201;
    }
    
    void method7831() {
	synchronized (this) {
	    aByteArray5233 = null;
	    anInt5205 = 0;
	    aFloat5201 = 0.0F;
	    aFloat5202 = 0.0F;
	    aBool5204 = false;
	    anInt5247 = 0;
	    aFloat5212 = 1.0F;
	    anInterface56_5245 = null;
	    anInt5208 = 0;
	    anInt5222 = 0;
	    if (aClass497Array5207 != null) {
		for (int i = 0; i < aClass497Array5207.length; i++) {
		    if (aClass497Array5207[i] != null)
			aClass497Array5207[i].method8134((short) 255);
		}
	    }
	    aClass497Array5207 = null;
	    aBool5246 = false;
	    anInt5213 = 0;
	    anInt5215 = 0;
	    anInt5216 = 0;
	    anInt5210 = 0;
	    anInt5218 = 0;
	    anInt5219 = 0;
	    anIntArray5220 = null;
	    anIntArray5221 = null;
	    anIntArray5237 = null;
	    anInt5239 = 0;
	    aBool5224 = false;
	    anInterface40_5226.method286();
	    aBool5228 = false;
	    anInterface58_5229 = null;
	    anObject5230 = null;
	    anInt5242 = 0;
	    anInt5232 = 0;
	    anInt5241 = 0;
	    aBool5234 = false;
	    anInt5235 = 0;
	    anInt5236 = 0;
	    aBool5248 = false;
	    anInt5238 = 0;
	    method7820(-1.0F, 1710868523);
	    aBool5240 = false;
	    aFloat5206 = 0.0F;
	    aLong5200 = 0L;
	    aLong5243 = 0L;
	    aFloat5199 = 0.0F;
	    anInt5217 = 0;
	    anInt5203 = 0;
	}
    }
    
    void method7832(float f, int i) {
	if (i <= 0) {
	    aFloat5201 = f;
	    aFloat5206 = aFloat5201;
	    aLong5243 = 0L;
	    aLong5200 = 0L;
	} else {
	    aFloat5199 = aFloat5201;
	    aFloat5206 = f;
	    aLong5243 = Class250.method4604((byte) -6) * -7945642590094835905L;
	    aLong5200
		= 8994788786014064821L * ((long) i
					  + aLong5243 * 4767994121520197823L);
	}
    }
    
    public void method7833(byte i) {
	synchronized (this) {
	    if (aBool5249) {
		/* empty */
	    } else if (anObject5214 == null) {
		/* empty */
	    } else {
		int i_27_
		    = aClass367_5227.method6339(anObject5214, -575309094);
		if (i_27_ <= 0) {
		    /* empty */
		} else if (aClass497Array5207 != null
			   && (null
			       != aClass497Array5207[anInt5213 * 1899481443])
			   && aBool5248) {
		    int i_28_
			= ((-980667675 * anInt5242 + i_27_
			    > (aClass497Array5207[anInt5213 * 1899481443]
			       .aClass534_Sub40_5551.anInt10811) * 31645619)
			   ? ((aClass497Array5207[1899481443 * anInt5213]
			       .aClass534_Sub40_5551.anInt10811) * 31645619
			      - anInt5242 * -980667675)
			   : i_27_);
		    if (-980667675 * anInt5242 < (aClass497Array5207
						  [anInt5213 * 1899481443]
						  .anInt5549) * -1804813709
			&& (anInt5242 * -980667675 + i_28_
			    > -1804813709 * (aClass497Array5207
					     [anInt5213 * 1899481443]
					     .anInt5549))
			&& (-1804813709 * (aClass497Array5207
					   [anInt5213 * 1899481443].anInt5549)
			    >= 0)) {
			anInt5242
			    += -1672369545 * (aClass497Array5207
					      [anInt5213 * 1899481443]
					      .anInt5549) - 1 * anInt5242;
			i_28_ = ((-980667675 * anInt5242 + i_27_
				  > 31645619 * (aClass497Array5207
						[anInt5213 * 1899481443]
						.aClass534_Sub40_5551
						.anInt10811))
				 ? ((aClass497Array5207[1899481443 * anInt5213]
				     .aClass534_Sub40_5551.anInt10811)
				    * 31645619) - -980667675 * anInt5242
				 : i_27_);
		    }
		    if ((anInt5242 * -980667675 + i_28_
			 > -204452677 * (aClass497Array5207
					 [1899481443 * anInt5213].anInt5550))
			&& (aClass497Array5207[1899481443 * anInt5213]
			    .anInt5550) * -204452677 >= 0)
			i_28_ = (-204452677 * (aClass497Array5207
					       [1899481443 * anInt5213]
					       .anInt5550)
				 - anInt5242 * -980667675);
		    method7834((aClass497Array5207[1899481443 * anInt5213]
				.aClass534_Sub40_5551.aByteArray10810),
			       anInt5242 * -980667675,
			       i_28_ + anInt5242 * -980667675, (byte) -1);
		    aClass367_5227.method6338(anObject5214,
					      (aClass497Array5207
					       [1899481443 * anInt5213]
					       .aClass534_Sub40_5551
					       .aByteArray10810),
					      -980667675 * anInt5242, i_28_,
					      (byte) 25);
		    anInt5242 += 925448941 * i_28_;
		    i_27_ -= i_28_;
		    if ((anInt5242 * -980667675
			 >= 31645619 * (aClass497Array5207
					[1899481443 * anInt5213]
					.aClass534_Sub40_5551.anInt10811))
			|| (anInt5242 * -980667675 >= (aClass497Array5207
						       [1899481443 * anInt5213]
						       .anInt5550) * -204452677
			    && -204452677 * (aClass497Array5207
					     [anInt5213 * 1899481443]
					     .anInt5550) >= 0)) {
			aClass497Array5207[1899481443 * anInt5213]
			    .method8134((short) 255);
			aClass497Array5207[anInt5213 * 1899481443] = null;
			anInt5213 += 1295515723;
			anInt5213 = 1295515723 * (anInt5213 * 1899481443
						  % aClass497Array5207.length);
			anInt5242 = 0;
		    }
		}
	    }
	}
    }
    
    void method7834(byte[] is, int i, int i_29_, byte i_30_) {
	int i_31_ = i;
	int i_32_ = 0;
	int i_33_ = anInterface40_5226.method283().anInt3929 * -1204190425;
	while (i_31_ < is.length && i_31_ < i_29_) {
	    float f = aFloatArray5211[i_32_] * (aFloat5202 * aFloat5201);
	    if (i_33_ == 16) {
		int i_34_ = (is[i_31_] & 0xff) + (is[1 + i_31_] << 8);
		i_34_ *= f * f;
		is[i_31_++] = (byte) i_34_;
		is[i_31_++] = (byte) (i_34_ >> 8);
	    } else if (8 == i_33_) {
		byte i_35_ = is[i_31_];
		i_35_ *= f;
		is[i_31_++] = i_35_;
	    } else
		throw new RuntimeException("");
	    i_32_ = ++i_32_ % aFloatArray5211.length;
	}
    }
    
    Class534_Sub40 method7835(int i, int i_36_) {
	return anInterface40_5226.method297(i);
    }
    
    void method7836(Class534_Sub40 class534_sub40, int i) {
	if (anObject5230 != null && anObject5230 instanceof Class491) {
	    Class491 class491 = (Class491) anObject5230;
	    Interface70 interface70 = class491.method8040((byte) 107);
	    byte[] is = new byte[class534_sub40.anInt10811 * 31645619];
	    System.arraycopy(class534_sub40.aByteArray10810, 0, is, 0,
			     31645619 * class534_sub40.anInt10811);
	    Class534_Sub40 class534_sub40_37_ = new Class534_Sub40(is);
	    class534_sub40_37_.anInt10811 = 1 * class534_sub40.anInt10811;
	    interface70.method451(class534_sub40_37_, 221359472);
	}
    }
    
    public void method7837() {
	synchronized (this) {
	    if (aBool5249) {
		/* empty */
	    } else if (anObject5214 == null) {
		/* empty */
	    } else {
		int i = aClass367_5227.method6339(anObject5214, -1948509883);
		if (i <= 0) {
		    /* empty */
		} else if (aClass497Array5207 != null
			   && (null
			       != aClass497Array5207[anInt5213 * 1899481443])
			   && aBool5248) {
		    int i_38_
			= ((-980667675 * anInt5242 + i
			    > (aClass497Array5207[anInt5213 * 1899481443]
			       .aClass534_Sub40_5551.anInt10811) * 31645619)
			   ? ((aClass497Array5207[1899481443 * anInt5213]
			       .aClass534_Sub40_5551.anInt10811) * 31645619
			      - anInt5242 * -980667675)
			   : i);
		    if (-980667675 * anInt5242 < (aClass497Array5207
						  [anInt5213 * 1899481443]
						  .anInt5549) * -1804813709
			&& (anInt5242 * -980667675 + i_38_
			    > -1804813709 * (aClass497Array5207
					     [anInt5213 * 1899481443]
					     .anInt5549))
			&& (-1804813709 * (aClass497Array5207
					   [anInt5213 * 1899481443].anInt5549)
			    >= 0)) {
			anInt5242
			    += -1672369545 * (aClass497Array5207
					      [anInt5213 * 1899481443]
					      .anInt5549) - 1 * anInt5242;
			i_38_ = ((-980667675 * anInt5242 + i
				  > 31645619 * (aClass497Array5207
						[anInt5213 * 1899481443]
						.aClass534_Sub40_5551
						.anInt10811))
				 ? ((aClass497Array5207[1899481443 * anInt5213]
				     .aClass534_Sub40_5551.anInt10811)
				    * 31645619) - -980667675 * anInt5242
				 : i);
		    }
		    if ((anInt5242 * -980667675 + i_38_
			 > -204452677 * (aClass497Array5207
					 [1899481443 * anInt5213].anInt5550))
			&& (aClass497Array5207[1899481443 * anInt5213]
			    .anInt5550) * -204452677 >= 0)
			i_38_ = (-204452677 * (aClass497Array5207
					       [1899481443 * anInt5213]
					       .anInt5550)
				 - anInt5242 * -980667675);
		    method7834((aClass497Array5207[1899481443 * anInt5213]
				.aClass534_Sub40_5551.aByteArray10810),
			       anInt5242 * -980667675,
			       i_38_ + anInt5242 * -980667675, (byte) -1);
		    aClass367_5227.method6338(anObject5214,
					      (aClass497Array5207
					       [1899481443 * anInt5213]
					       .aClass534_Sub40_5551
					       .aByteArray10810),
					      -980667675 * anInt5242, i_38_,
					      (byte) 81);
		    anInt5242 += 925448941 * i_38_;
		    i -= i_38_;
		    if ((anInt5242 * -980667675
			 >= 31645619 * (aClass497Array5207
					[1899481443 * anInt5213]
					.aClass534_Sub40_5551.anInt10811))
			|| (anInt5242 * -980667675 >= (aClass497Array5207
						       [1899481443 * anInt5213]
						       .anInt5550) * -204452677
			    && -204452677 * (aClass497Array5207
					     [anInt5213 * 1899481443]
					     .anInt5550) >= 0)) {
			aClass497Array5207[1899481443 * anInt5213]
			    .method8134((short) 255);
			aClass497Array5207[anInt5213 * 1899481443] = null;
			anInt5213 += 1295515723;
			anInt5213 = 1295515723 * (anInt5213 * 1899481443
						  % aClass497Array5207.length);
			anInt5242 = 0;
		    }
		}
	    }
	}
    }
    
    boolean method7838(int i) {
	if (anObject5230 != null && anObject5230 instanceof Class491) {
	    Class491 class491 = (Class491) anObject5230;
	    Interface70 interface70 = class491.method8040((byte) 48);
	    return interface70.method454(i, (byte) 88);
	}
	return false;
    }
    
    boolean method7839(int i, int i_39_) {
	if (anObject5230 != null && anObject5230 instanceof Class491) {
	    Class491 class491 = (Class491) anObject5230;
	    Interface70 interface70 = class491.method8040((byte) 36);
	    return interface70.method454(i, (byte) 77);
	}
	return false;
    }
    
    void method7840() {
	if (method7853((byte) 30) != Class485.aClass485_5286
	    && (method7853((byte) -40).anInt5283 * -222113559
		< -222113559 * Class485.aClass485_5284.anInt5283)) {
	    method7897(Class485.aClass485_5284, (byte) -67);
	    aBool5224 = true;
	    method7827(-2143450994);
	    method7818(-991909060);
	}
    }
    
    void method7841(int i) {
	synchronized (this) {
	    if (anObject5230 != null && anObject5230 instanceof Class491) {
		Class491 class491 = (Class491) anObject5230;
		Interface70 interface70 = class491.method8040((byte) 72);
		if (null != interface70)
		    interface70.method472(true, -1184946251);
	    }
	}
    }
    
    boolean method7842(int i) {
	if (anObject5230 != null && anObject5230 instanceof Class491) {
	    Class491 class491 = (Class491) anObject5230;
	    Interface70 interface70 = class491.method8040((byte) 1);
	    return interface70.method454(i, (byte) 104);
	}
	return false;
    }
    
    void method7843(int i) {
	synchronized (this) {
	    aByteArray5233 = null;
	    anInt5205 = 0;
	    aFloat5201 = 0.0F;
	    aFloat5202 = 0.0F;
	    aBool5204 = false;
	    anInt5247 = 0;
	    aFloat5212 = 1.0F;
	    anInterface56_5245 = null;
	    anInt5208 = 0;
	    anInt5222 = 0;
	    if (aClass497Array5207 != null) {
		for (int i_40_ = 0; i_40_ < aClass497Array5207.length;
		     i_40_++) {
		    if (aClass497Array5207[i_40_] != null)
			aClass497Array5207[i_40_].method8134((short) 255);
		}
	    }
	    aClass497Array5207 = null;
	    aBool5246 = false;
	    anInt5213 = 0;
	    anInt5215 = 0;
	    anInt5216 = 0;
	    anInt5210 = 0;
	    anInt5218 = 0;
	    anInt5219 = 0;
	    anIntArray5220 = null;
	    anIntArray5221 = null;
	    anIntArray5237 = null;
	    anInt5239 = 0;
	    aBool5224 = false;
	    anInterface40_5226.method286();
	    aBool5228 = false;
	    anInterface58_5229 = null;
	    anObject5230 = null;
	    anInt5242 = 0;
	    anInt5232 = 0;
	    anInt5241 = 0;
	    aBool5234 = false;
	    anInt5235 = 0;
	    anInt5236 = 0;
	    aBool5248 = false;
	    anInt5238 = 0;
	    method7820(-1.0F, -2006022796);
	    aBool5240 = false;
	    aFloat5206 = 0.0F;
	    aLong5200 = 0L;
	    aLong5243 = 0L;
	    aFloat5199 = 0.0F;
	    anInt5217 = 0;
	    anInt5203 = 0;
	}
    }
    
    public Class485 method7844() {
	return aClass485_5225;
    }
    
    boolean method7845() {
	return aClass485_5225 == Class485.aClass485_5286;
    }
    
    boolean method7846() {
	return aClass485_5225 == Class485.aClass485_5288;
    }
    
    boolean method7847(int i) {
	synchronized (this) {
	    if (anObject5230 != null && anObject5230 instanceof Class491) {
		Class491 class491 = (Class491) anObject5230;
		Interface70 interface70 = class491.method8040((byte) 44);
		if (interface70 != null) {
		    boolean bool = interface70.method467(-1271568526);
		    return bool;
		}
	    }
	    boolean bool = false;
	    return bool;
	}
    }
    
    public void method7848() {
	if (method7853((byte) -17).anInt5283 * -222113559
	    >= Class485.aClass485_5281.anInt5283 * -222113559)
	    throw new RuntimeException("");
	method7897(Class485.aClass485_5281, (byte) -76);
    }
    
    void method7849() {
	synchronized (this) {
	    int i = method7857(1928293911);
	    if (aBool5246 && i > -335387595 * anInt5217
		&& anInterface40_5226.method290()) {
		/* empty */
	    } else {
		Class375 class375 = anInterface40_5226.method307();
		if (aBool5228 && class375 != Class375.aClass375_3902) {
		    if (null != aByteArray5233) {
			if (anInt5235 * -1253868879 >= anInt5219 * 749325299) {
			    anInterface40_5226.method70(null);
			    anInt5235 = 0;
			} else {
			    if (-884094607 * anInt5222 < -57533887 * anInt5239)
				anInt5222 = anInt5239 * 1157658065;
			    anInt5222
				= (anIntArray5220[-1253868879 * anInt5235]
				   + anInt5238 * -1867376947) * -143403119;
			    int i_41_
				= anIntArray5221[anInt5235 * -1253868879];
			    int i_42_ = i_41_;
			    int i_43_
				= (anInt5238 * -1867376947 + i_42_ > i_41_
				   ? (anIntArray5220[anInt5235 * -1253868879]
				      + i_41_)
				   : -884094607 * anInt5222 + i_42_);
			    Class479 class479_44_ = this;
			    class479_44_.anInt5238
				= (class479_44_.anInt5238
				   + ((-884094607 * anInt5222 + i_42_
				       > aByteArray5233.length)
				      ? (aByteArray5233.length
					 - anInt5222 * -884094607)
				      : i_42_) * -1133525499);
			    Class534_Sub40 class534_sub40
				= new Class534_Sub40(i_43_ - (anInt5222
							      * -884094607),
						     true);
			    if (aByteArray5233 == null)
				throw new RuntimeException("");
			    if (class534_sub40.aByteArray10810 == null)
				throw new RuntimeException("");
			    class534_sub40.method16519(aByteArray5233,
						       -884094607 * anInt5222,
						       i_43_ - (anInt5222
								* -884094607),
						       -1731930195);
			    anInterface40_5226.method70(class534_sub40);
			    method7881(false, (short) 7962);
			    if (-1867376947 * anInt5238 >= i_41_) {
				anInt5235 += -417821103;
				anInt5238 = 0;
			    }
			}
		    } else if (null != anInterface56_5245) {
			if (anInt5208 * 772436621 >= 749325299 * anInt5219) {
			    anInt5208 = 0;
			    anInterface40_5226.method70(null);
			} else {
			    byte[] is
				= (anInterface56_5245.method378
				   (anIntArray5237[anInt5208 * 772436621],
				    1025361582));
			    if (null != is) {
				Class534_Sub40 class534_sub40
				    = new Class534_Sub40(is);
				class534_sub40.anInt10811
				    = is.length * -1387468933;
				anInterface40_5226.method70(class534_sub40);
				method7881(false, (short) 803);
				anInt5208 += -1816944571;
			    }
			}
		    }
		}
	    }
	}
    }
    
    boolean method7850(int i) {
	if (anObject5230 != null && anObject5230 instanceof Class491) {
	    Class491 class491 = (Class491) anObject5230;
	    Interface70 interface70 = class491.method8040((byte) 100);
	    return interface70.method454(i, (byte) 29);
	}
	return false;
    }
    
    Object method7851(byte i) {
	return anObject5230;
    }
    
    void method7852(float f, int i) {
	if (i <= 0) {
	    aFloat5201 = f;
	    aFloat5206 = aFloat5201;
	    aLong5243 = 0L;
	    aLong5200 = 0L;
	} else {
	    aFloat5199 = aFloat5201;
	    aFloat5206 = f;
	    aLong5243
		= Class250.method4604((byte) -24) * -7945642590094835905L;
	    aLong5200
		= 8994788786014064821L * ((long) i
					  + aLong5243 * 4767994121520197823L);
	}
    }
    
    public Class485 method7853(byte i) {
	return aClass485_5225;
    }
    
    void method7854(float f, int i) {
	if (i <= 0) {
	    aFloat5201 = f;
	    aFloat5206 = aFloat5201;
	    aLong5243 = 0L;
	    aLong5200 = 0L;
	} else {
	    aFloat5199 = aFloat5201;
	    aFloat5206 = f;
	    aLong5243
		= Class250.method4604((byte) -54) * -7945642590094835905L;
	    aLong5200
		= 8994788786014064821L * ((long) i
					  + aLong5243 * 4767994121520197823L);
	}
    }
    
    float method7855() {
	return aFloat5201;
    }
    
    void method7856() {
	synchronized (this) {
	    method7826(-732804656);
	}
    }
    
    int method7857(int i) {
	if (null != anObject5230 && anObject5230 instanceof Class491) {
	    Class491 class491 = (Class491) anObject5230;
	    Interface70 interface70 = class491.method8040((byte) 26);
	    return interface70.method452((byte) -67);
	}
	return 0;
    }
    
    void method7858() {
	aBool5249 = true;
    }
    
    public void method7859() {
	synchronized (this) {
	    if (method7853((byte) -27) == Class485.aClass485_5286
		|| (method7853((byte) -46).anInt5283 * -222113559
		    < -222113559 * Class485.aClass485_5281.anInt5283)) {
		/* empty */
	    } else
		method7897(Class485.aClass485_5286, (byte) -93);
	}
    }
    
    void method7860(Interface58 interface58) {
	anInterface58_5229 = interface58;
	if (null != anInterface58_5229)
	    anInterface58_5229.method379(anObject5230, aFloatArray5211, null,
					 -2024287005);
    }
    
    boolean method7861() {
	if (null == aFloatArray5211)
	    return false;
	float f = 0.0F;
	float f_45_ = 0.0F;
	for (int i = 0; i < aFloatArray5211.length; i++) {
	    float f_46_ = aFloatArray5211[i];
	    if (f_46_ * aFloat5201 > f)
		f = f_46_;
	    if (f_46_ > f_45_)
		f_45_ = f_46_;
	}
	if (f < 1.0E-5F) {
	    if (f_45_ >= 1.0E-5F && aFloat5206 >= 1.0E-5F)
		return true;
	    return false;
	}
	return true;
    }
    
    public void method7862(byte i) {
	synchronized (this) {
	    if (method7853((byte) 90) == Class485.aClass485_5286
		|| (method7853((byte) -98).anInt5283 * -222113559
		    < -222113559 * Class485.aClass485_5281.anInt5283)) {
		/* empty */
	    } else
		method7897(Class485.aClass485_5286, (byte) -25);
	}
    }
    
    void method7863() {
	Class388 class388
	    = aClass367_5227.method6342((byte) -5)
		  .method6370(anInt5205 * 1451965757, -1954519679);
	aFloat5202 = null != class388 ? class388.method6510(1481070417) : 1.0F;
	float f = null != class388 ? class388.method6508(1947091351) : 0.1F;
	f *= aFloat5201;
	float f_47_ = aFloatArray5211.length > 0 ? 0.0F : 1.0F;
	for (int i = 0; i < aFloatArray5211.length; i++) {
	    if (aFloatArray5211[i] > f_47_)
		f_47_ = aFloatArray5211[i];
	}
	f *= f_47_;
	if (!method7817(952386396))
	    f = -1.0F;
	if (method7819(2029327501) != f) {
	    method7820(f, 2085547392);
	    aBool5240 = true;
	}
    }
    
    public float method7864() {
	return aFloat5244;
    }
    
    public float method7865() {
	return aFloat5244;
    }
    
    Class534_Sub40 method7866(int i, byte i_48_) {
	if (null != anObject5230 && anObject5230 instanceof Class491) {
	    Class491 class491 = (Class491) anObject5230;
	    Interface70 interface70 = class491.method8040((byte) 81);
	    Class534_Sub40 class534_sub40
		= interface70.method458(i, -1351082268);
	    Class534_Sub40 class534_sub40_49_
		= new Class534_Sub40(class534_sub40.anInt10811 * 31645619,
				     true);
	    class534_sub40_49_.method16520(class534_sub40, 116943691);
	    return class534_sub40_49_;
	}
	return null;
    }
    
    void method7867(int i) {
	if (null != aClass497Array5207) {
	    boolean bool = true;
	    Class375 class375 = anInterface40_5226.method307();
	    if (anInterface40_5226.method290()) {
		if (aBool5246
		    && method7839(anInt5217 * -335387595, -1845880902)) {
		    if (null == aClass497Array5207[-1200180259 * anInt5236]) {
			Class534_Sub40 class534_sub40
			    = method7866(anInt5217 * -335387595, (byte) 10);
			if (class534_sub40 != null) {
			    aClass497Array5207[-1200180259 * anInt5236]
				= new Class497(this, class534_sub40, false);
			    int i_50_ = anInterface40_5226.method280();
			    int i_51_ = (anInterface40_5226.method24
					 (31645619 * (aClass497Array5207
						      [-1200180259 * anInt5236]
						      .aClass534_Sub40_5551
						      .anInt10811) / i_50_));
			    anInt5232 += -899199365 * i_51_;
			    if (!aBool5231 && aBool5204
				&& -919020977 * anInt5203 > 0) {
				if (anInt5232 * -266182477
				    < 47814135 * anInt5215)
				    aClass497Array5207[anInt5236 * -1200180259]
					= null;
				else if ((anInt5232 * -266182477
					  >= anInt5215 * 47814135)
					 && (anInt5232 * -266182477 - i_51_
					     < 47814135 * anInt5215))
				    aClass497Array5207
					[anInt5236 * -1200180259].anInt5549
					= ((anInterface40_5226.method289
					    (i_50_ * (-266182477 * anInt5232
						      - 47814135 * anInt5215)))
					   * 572750523);
			    }
			    anInt5217 += 767086109;
			    if (!aBool5231 && aBool5204
				&& (anInt5232 * -266182477
				    >= -933644091 * anInt5216)
				&& ((anInt5203 * -919020977
				     < anInt5247 * 1247788865)
				    || 1247788865 * anInt5247 < 0)) {
				int i_52_ = (-266182477 * anInt5232
					     - -933644091 * anInt5216);
				int i_53_ = i_51_ - i_52_;
				aClass497Array5207[anInt5236 * -1200180259]
				    .anInt5550
				    = (anInterface40_5226
					   .method289(i_50_ * i_53_)
				       * 1895045235);
				anInt5217 = 0;
				anInt5232 = 0;
				anInt5203 += 1253309615;
			    }
			    if (null != (aClass497Array5207
					 [-1200180259 * anInt5236])) {
				anInt5236 += -797426571;
				anInt5236 = (-1200180259 * anInt5236
					     % aClass497Array5207.length
					     * -797426571);
			    }
			}
		    }
		} else if (!method7847(-264256136) || !aBool5246) {
		    int i_54_ = anInterface40_5226.method9();
		    int i_55_
			= anInterface40_5226.method24(anInt5223 * -414413569);
		    if (false == aBool5234
			&& (null
			    == aClass497Array5207[-1200180259 * anInt5236])) {
			if (class375 != Class375.aClass375_3907 && i_54_ > 0) {
			    Class534_Sub40 class534_sub40
				= method7835(i_55_, -104724003);
			    aClass497Array5207[anInt5236 * -1200180259]
				= (null != class534_sub40
				   ? new Class497(this, class534_sub40, false)
				   : null);
			    int i_56_
				= ((aClass497Array5207[anInt5236 * -1200180259]
				    != null)
				   ? 31645619 * (aClass497Array5207
						 [anInt5236 * -1200180259]
						 .aClass534_Sub40_5551
						 .anInt10811)
				   : 0);
			    int i_57_ = anInterface40_5226.method280();
			    int i_58_
				= anInterface40_5226.method24(i_56_ / i_57_);
			    anInt5241 += 1690605313 * i_58_;
			    if (625764609 * anInt5241
				> anInt5232 * -266182477) {
				anInt5232 += -899199365 * i_58_;
				i_54_ -= i_58_;
				if (!aBool5231 && aBool5204
				    && -919020977 * anInt5203 > 0) {
				    if (anInt5232 * -266182477
					< anInt5215 * 47814135)
					aClass497Array5207[(-1200180259
							    * anInt5236)]
					    = null;
				    else if ((anInt5232 * -266182477
					      >= 47814135 * anInt5215)
					     && (anInt5232 * -266182477 - i_58_
						 < 47814135 * anInt5215))
					aClass497Array5207
					    [-1200180259 * anInt5236].anInt5549
					    = (anInterface40_5226.method289
					       ((-266182477 * anInt5232
						 - 47814135 * anInt5215)
						* i_57_)) * 572750523;
				}
				if (aClass497Array5207[anInt5236 * -1200180259]
				    != null) {
				    if (aBool5246
					&& !method7839(-335387595 * anInt5217,
						       -1844258103))
					method7836((aClass497Array5207
						    [anInt5236 * -1200180259]
						    .aClass534_Sub40_5551),
						   1594231129);
				    if (false == aBool5231 && aBool5204
					&& (-266182477 * anInt5232
					    >= -933644091 * anInt5216)
					&& ((-919020977 * anInt5203
					     <= anInt5247 * 1247788865)
					    || anInt5247 * 1247788865 < 0)) {
					int i_59_ = (-266182477 * anInt5232
						     - anInt5216 * -933644091);
					int i_60_ = i_58_ - i_59_;
					aClass497Array5207
					    [-1200180259 * anInt5236].anInt5550
					    = (anInterface40_5226
						   .method289(i_60_ * i_57_)
					       * 1895045235);
					anInt5217 = 0;
					anInt5203 += 1253309615;
					anInt5232 = 0;
				    } else
					anInt5217 += 767086109;
				    anInt5236 += -797426571;
				    anInt5236 = (anInt5236 * -1200180259
						 % aClass497Array5207.length
						 * -797426571);
				}
			    } else
				aClass497Array5207[-1200180259 * anInt5236]
				    = null;
			} else if (Class375.aClass375_3907 == class375
				   && aBool5204 && false == aBool5231
				   && ((-919020977 * anInt5203
					< anInt5247 * 1247788865)
				       || 1247788865 * anInt5247 < 0))
			    anInterface40_5226.method288(true);
		    }
		}
	    }
	    int i_61_ = 0;
	    for (int i_62_ = 0; i_62_ < aClass497Array5207.length; i_62_++) {
		if (aClass497Array5207[i_62_] != null) {
		    i_61_++;
		    bool = false;
		}
	    }
	    if (!aBool5248 && method7819(344020120) >= 0.0F
		&& (i_61_ >= anInt5209 * 1995684471
		    || class375 == Class375.aClass375_3907
		    || class375 == Class375.aClass375_3902)) {
		aBool5248 = true;
		method7897(Class485.aClass485_5282, (byte) -65);
	    }
	    if (anInterface40_5226.method290() && bool
		&& (method7847(-86680278) || aBool5234
		    || Class375.aClass375_3907 == class375)
		&& (!aBool5204
		    || (!aBool5231
			&& -919020977 * anInt5203 >= anInt5247 * 1247788865
			&& 1247788865 * anInt5247 >= 0))) {
		method7897(Class485.aClass485_5288, (byte) -73);
		aBool5224 = false;
		if (aBool5246)
		    method7841(1895590039);
	    }
	}
    }
    
    void method7868(float f) {
	synchronized (this) {
	    aFloat5244 = f;
	}
    }
    
    void method7869() {
	aBool5249 = false;
    }
    
    public boolean method7870() {
	return aBool5240;
    }
    
    public boolean method7871() {
	return aBool5240;
    }
    
    public boolean method7872() {
	return aBool5240;
    }
    
    public void method7873() {
	synchronized (this) {
	    method7823((byte) -107);
	}
    }
    
    void method7874(byte[] is, int i, int i_63_) {
	int i_64_ = i;
	int i_65_ = 0;
	int i_66_ = anInterface40_5226.method283().anInt3929 * -1204190425;
	while (i_64_ < is.length && i_64_ < i_63_) {
	    float f = aFloatArray5211[i_65_] * (aFloat5202 * aFloat5201);
	    if (i_66_ == 16) {
		int i_67_ = (is[i_64_] & 0xff) + (is[1 + i_64_] << 8);
		i_67_ *= f * f;
		is[i_64_++] = (byte) i_67_;
		is[i_64_++] = (byte) (i_67_ >> 8);
	    } else if (8 == i_66_) {
		byte i_68_ = is[i_64_];
		i_68_ *= f;
		is[i_64_++] = i_68_;
	    } else
		throw new RuntimeException("");
	    i_65_ = ++i_65_ % aFloatArray5211.length;
	}
    }
    
    void method7875() {
	Class388 class388
	    = aClass367_5227.method6342((byte) 30)
		  .method6370(anInt5205 * 1451965757, -1680864758);
	aFloat5202 = null != class388 ? class388.method6510(1398555048) : 1.0F;
	float f = null != class388 ? class388.method6508(1096290212) : 0.1F;
	f *= aFloat5201;
	float f_69_ = aFloatArray5211.length > 0 ? 0.0F : 1.0F;
	for (int i = 0; i < aFloatArray5211.length; i++) {
	    if (aFloatArray5211[i] > f_69_)
		f_69_ = aFloatArray5211[i];
	}
	f *= f_69_;
	if (!method7817(1843398912))
	    f = -1.0F;
	if (method7819(1588688162) != f) {
	    method7820(f, 46556197);
	    aBool5240 = true;
	}
    }
    
    void method7876() {
	synchronized (this) {
	    int i = method7857(839628724);
	    if (aBool5246 && i > -335387595 * anInt5217
		&& anInterface40_5226.method290()) {
		/* empty */
	    } else {
		Class375 class375 = anInterface40_5226.method307();
		if (aBool5228 && class375 != Class375.aClass375_3902) {
		    if (null != aByteArray5233) {
			if (anInt5235 * -1253868879 >= anInt5219 * 749325299) {
			    anInterface40_5226.method70(null);
			    anInt5235 = 0;
			} else {
			    if (-884094607 * anInt5222 < -57533887 * anInt5239)
				anInt5222 = anInt5239 * 1157658065;
			    anInt5222
				= (anIntArray5220[-1253868879 * anInt5235]
				   + anInt5238 * -1867376947) * -143403119;
			    int i_70_
				= anIntArray5221[anInt5235 * -1253868879];
			    int i_71_ = i_70_;
			    int i_72_
				= (anInt5238 * -1867376947 + i_71_ > i_70_
				   ? (anIntArray5220[anInt5235 * -1253868879]
				      + i_70_)
				   : -884094607 * anInt5222 + i_71_);
			    Class479 class479_73_ = this;
			    class479_73_.anInt5238
				= (class479_73_.anInt5238
				   + ((-884094607 * anInt5222 + i_71_
				       > aByteArray5233.length)
				      ? (aByteArray5233.length
					 - anInt5222 * -884094607)
				      : i_71_) * -1133525499);
			    Class534_Sub40 class534_sub40
				= new Class534_Sub40(i_72_ - (anInt5222
							      * -884094607),
						     true);
			    if (aByteArray5233 == null)
				throw new RuntimeException("");
			    if (class534_sub40.aByteArray10810 == null)
				throw new RuntimeException("");
			    class534_sub40.method16519(aByteArray5233,
						       -884094607 * anInt5222,
						       i_72_ - (anInt5222
								* -884094607),
						       -372145384);
			    anInterface40_5226.method70(class534_sub40);
			    method7881(false, (short) -2481);
			    if (-1867376947 * anInt5238 >= i_70_) {
				anInt5235 += -417821103;
				anInt5238 = 0;
			    }
			}
		    } else if (null != anInterface56_5245) {
			if (anInt5208 * 772436621 >= 749325299 * anInt5219) {
			    anInt5208 = 0;
			    anInterface40_5226.method70(null);
			} else {
			    byte[] is
				= (anInterface56_5245.method378
				   (anIntArray5237[anInt5208 * 772436621],
				    198431326));
			    if (null != is) {
				Class534_Sub40 class534_sub40
				    = new Class534_Sub40(is);
				class534_sub40.anInt10811
				    = is.length * -1387468933;
				anInterface40_5226.method70(class534_sub40);
				method7881(false, (short) 24241);
				anInt5208 += -1816944571;
			    }
			}
		    }
		}
	    }
	}
    }
    
    void method7877() {
	Class388 class388
	    = aClass367_5227.method6342((byte) 53)
		  .method6370(anInt5205 * 1451965757, -1831334135);
	aFloat5202 = null != class388 ? class388.method6510(1522745802) : 1.0F;
	float f = null != class388 ? class388.method6508(-883838857) : 0.1F;
	f *= aFloat5201;
	float f_74_ = aFloatArray5211.length > 0 ? 0.0F : 1.0F;
	for (int i = 0; i < aFloatArray5211.length; i++) {
	    if (aFloatArray5211[i] > f_74_)
		f_74_ = aFloatArray5211[i];
	}
	f *= f_74_;
	if (!method7817(1275098693))
	    f = -1.0F;
	if (method7819(1898318836) != f) {
	    method7820(f, 3602494);
	    aBool5240 = true;
	}
    }
    
    void method7878() {
	synchronized (this) {
	    if (anInterface40_5226.method307() == Class375.aClass375_3908) {
		if (null != anObject5214) {
		    int i
			= aClass367_5227.method6339(anObject5214, -503319367);
		    if (i >= anInt5223 * -414413569) {
			aClass367_5227.method6346(anObject5214, -1649177566);
			anObject5214 = null;
			method7897(Class485.aClass485_5287, (byte) -45);
		    }
		} else
		    method7897(Class485.aClass485_5287, (byte) -72);
	    }
	}
	method7843(1684556398);
    }
    
    void method7879() {
	if (method7853((byte) -80) != Class485.aClass485_5286
	    && (method7853((byte) -2).anInt5283 * -222113559
		< -222113559 * Class485.aClass485_5284.anInt5283)) {
	    method7897(Class485.aClass485_5284, (byte) -59);
	    aBool5224 = true;
	    method7827(-2101017955);
	    method7818(-1283523209);
	}
    }
    
    void method7880() {
	if (method7853((byte) 15) != Class485.aClass485_5286
	    && (method7853((byte) -14).anInt5283 * -222113559
		< -222113559 * Class485.aClass485_5284.anInt5283)) {
	    method7897(Class485.aClass485_5284, (byte) -75);
	    aBool5224 = true;
	    method7827(-2140466264);
	    method7818(-1512942978);
	}
    }
    
    void method7881(boolean bool, short i) {
	aBool5228 = bool;
    }
    
    void method7882() {
	if (method7853((byte) -36) != Class485.aClass485_5286
	    && (method7853((byte) 25).anInt5283 * -222113559
		< -222113559 * Class485.aClass485_5284.anInt5283)) {
	    method7897(Class485.aClass485_5284, (byte) -65);
	    aBool5224 = true;
	    method7827(-2117656071);
	    method7818(-957999266);
	}
    }
    
    void method7883() {
	if (method7853((byte) 90).anInt5283 * -222113559
	    >= -222113559 * Class485.aClass485_5284.anInt5283) {
	    if (method7853((byte) 91) == Class485.aClass485_5289)
		method7825(-2089577032);
	    else {
		method7824((byte) -39);
		if (aFloat5206 != aFloat5201) {
		    long l = Class250.method4604((byte) -21);
		    if (l > aLong5200 * -6937883969178083939L)
			aFloat5201 = aFloat5206;
		    else {
			float f = aFloat5206 - aFloat5199;
			long l_75_ = (-6937883969178083939L * aLong5200
				      - aLong5243 * 4767994121520197823L);
			float f_76_ = f / (float) l_75_;
			aFloat5201
			    = aFloat5199 + (float) (l - (4767994121520197823L
							 * aLong5243)) * f_76_;
			aFloat5201
			    = Math.max(0.0F, Math.min(1.0F, aFloat5201));
		    }
		}
		method7818(-1448043814);
		if (method7853((byte) 35).anInt5283 * -222113559
		    < Class485.aClass485_5286.anInt5283 * -222113559)
		    method7827(-2019969144);
	    }
	}
    }
    
    void method7884() {
	if (aBool5224 == true
	    && (method7853((byte) -65).anInt5283 * -222113559
		>= Class485.aClass485_5284.anInt5283 * -222113559)
	    && (method7853((byte) -82).anInt5283 * -222113559
		< Class485.aClass485_5288.anInt5283 * -222113559)
	    && anInterface58_5229 != null && aFloat5206 == aFloat5201)
	    anInterface58_5229.method379(anObject5230, aFloatArray5211, null,
					 -2024287005);
    }
    
    void method7885() {
	if (aBool5224 == true
	    && (method7853((byte) 84).anInt5283 * -222113559
		>= Class485.aClass485_5284.anInt5283 * -222113559)
	    && (method7853((byte) -4).anInt5283 * -222113559
		< Class485.aClass485_5288.anInt5283 * -222113559)
	    && anInterface58_5229 != null && aFloat5206 == aFloat5201)
	    anInterface58_5229.method379(anObject5230, aFloatArray5211, null,
					 -2024287005);
    }
    
    void method7886() {
	if (aBool5224 == true
	    && (method7853((byte) 9).anInt5283 * -222113559
		>= Class485.aClass485_5284.anInt5283 * -222113559)
	    && (method7853((byte) -118).anInt5283 * -222113559
		< Class485.aClass485_5288.anInt5283 * -222113559)
	    && anInterface58_5229 != null && aFloat5206 == aFloat5201)
	    anInterface58_5229.method379(anObject5230, aFloatArray5211, null,
					 -2024287005);
    }
    
    void method7887() {
	if (method7853((byte) -31).anInt5283 * -222113559
	    >= -222113559 * Class485.aClass485_5284.anInt5283) {
	    if (method7853((byte) 82) == Class485.aClass485_5289)
		method7825(-2127092984);
	    else {
		method7824((byte) -23);
		if (aFloat5206 != aFloat5201) {
		    long l = Class250.method4604((byte) -8);
		    if (l > aLong5200 * -6937883969178083939L)
			aFloat5201 = aFloat5206;
		    else {
			float f = aFloat5206 - aFloat5199;
			long l_77_ = (-6937883969178083939L * aLong5200
				      - aLong5243 * 4767994121520197823L);
			float f_78_ = f / (float) l_77_;
			aFloat5201
			    = aFloat5199 + (float) (l - (4767994121520197823L
							 * aLong5243)) * f_78_;
			aFloat5201
			    = Math.max(0.0F, Math.min(1.0F, aFloat5201));
		    }
		}
		method7818(-1618073972);
		if (method7853((byte) 46).anInt5283 * -222113559
		    < Class485.aClass485_5286.anInt5283 * -222113559)
		    method7827(-2042908951);
	    }
	}
    }
    
    void method7888(float f) {
	synchronized (this) {
	    aFloat5244 = f;
	}
    }
    
    void method7889() {
	aBool5249 = true;
    }
    
    void method7890() {
	aBool5249 = false;
    }
    
    void method7891() {
	aBool5249 = false;
    }
    
    void method7892() {
	synchronized (this) {
	    aByteArray5233 = null;
	    anInt5205 = 0;
	    aFloat5201 = 0.0F;
	    aFloat5202 = 0.0F;
	    aBool5204 = false;
	    anInt5247 = 0;
	    aFloat5212 = 1.0F;
	    anInterface56_5245 = null;
	    anInt5208 = 0;
	    anInt5222 = 0;
	    if (aClass497Array5207 != null) {
		for (int i = 0; i < aClass497Array5207.length; i++) {
		    if (aClass497Array5207[i] != null)
			aClass497Array5207[i].method8134((short) 255);
		}
	    }
	    aClass497Array5207 = null;
	    aBool5246 = false;
	    anInt5213 = 0;
	    anInt5215 = 0;
	    anInt5216 = 0;
	    anInt5210 = 0;
	    anInt5218 = 0;
	    anInt5219 = 0;
	    anIntArray5220 = null;
	    anIntArray5221 = null;
	    anIntArray5237 = null;
	    anInt5239 = 0;
	    aBool5224 = false;
	    anInterface40_5226.method286();
	    aBool5228 = false;
	    anInterface58_5229 = null;
	    anObject5230 = null;
	    anInt5242 = 0;
	    anInt5232 = 0;
	    anInt5241 = 0;
	    aBool5234 = false;
	    anInt5235 = 0;
	    anInt5236 = 0;
	    aBool5248 = false;
	    anInt5238 = 0;
	    method7820(-1.0F, -1396316677);
	    aBool5240 = false;
	    aFloat5206 = 0.0F;
	    aLong5200 = 0L;
	    aLong5243 = 0L;
	    aFloat5199 = 0.0F;
	    anInt5217 = 0;
	    anInt5203 = 0;
	}
    }
    
    void method7893(boolean bool) {
	aBool5228 = bool;
    }
    
    public void method7894() {
	if (method7853((byte) 41).anInt5283 * -222113559
	    >= Class485.aClass485_5281.anInt5283 * -222113559)
	    throw new RuntimeException("");
	method7897(Class485.aClass485_5281, (byte) -67);
    }
    
    public void method7895() {
	synchronized (this) {
	    if (aBool5249) {
		/* empty */
	    } else if (anObject5214 == null) {
		/* empty */
	    } else {
		int i = aClass367_5227.method6339(anObject5214, -1930886979);
		if (i <= 0) {
		    /* empty */
		} else if (aClass497Array5207 != null
			   && (null
			       != aClass497Array5207[anInt5213 * 1899481443])
			   && aBool5248) {
		    int i_79_
			= ((-980667675 * anInt5242 + i
			    > (aClass497Array5207[anInt5213 * 1899481443]
			       .aClass534_Sub40_5551.anInt10811) * 31645619)
			   ? ((aClass497Array5207[1899481443 * anInt5213]
			       .aClass534_Sub40_5551.anInt10811) * 31645619
			      - anInt5242 * -980667675)
			   : i);
		    if (-980667675 * anInt5242 < (aClass497Array5207
						  [anInt5213 * 1899481443]
						  .anInt5549) * -1804813709
			&& (anInt5242 * -980667675 + i_79_
			    > -1804813709 * (aClass497Array5207
					     [anInt5213 * 1899481443]
					     .anInt5549))
			&& (-1804813709 * (aClass497Array5207
					   [anInt5213 * 1899481443].anInt5549)
			    >= 0)) {
			anInt5242
			    += -1672369545 * (aClass497Array5207
					      [anInt5213 * 1899481443]
					      .anInt5549) - 1 * anInt5242;
			i_79_ = ((-980667675 * anInt5242 + i
				  > 31645619 * (aClass497Array5207
						[anInt5213 * 1899481443]
						.aClass534_Sub40_5551
						.anInt10811))
				 ? ((aClass497Array5207[1899481443 * anInt5213]
				     .aClass534_Sub40_5551.anInt10811)
				    * 31645619) - -980667675 * anInt5242
				 : i);
		    }
		    if ((anInt5242 * -980667675 + i_79_
			 > -204452677 * (aClass497Array5207
					 [1899481443 * anInt5213].anInt5550))
			&& (aClass497Array5207[1899481443 * anInt5213]
			    .anInt5550) * -204452677 >= 0)
			i_79_ = (-204452677 * (aClass497Array5207
					       [1899481443 * anInt5213]
					       .anInt5550)
				 - anInt5242 * -980667675);
		    method7834((aClass497Array5207[1899481443 * anInt5213]
				.aClass534_Sub40_5551.aByteArray10810),
			       anInt5242 * -980667675,
			       i_79_ + anInt5242 * -980667675, (byte) -1);
		    aClass367_5227.method6338(anObject5214,
					      (aClass497Array5207
					       [1899481443 * anInt5213]
					       .aClass534_Sub40_5551
					       .aByteArray10810),
					      -980667675 * anInt5242, i_79_,
					      (byte) 69);
		    anInt5242 += 925448941 * i_79_;
		    i -= i_79_;
		    if ((anInt5242 * -980667675
			 >= 31645619 * (aClass497Array5207
					[1899481443 * anInt5213]
					.aClass534_Sub40_5551.anInt10811))
			|| (anInt5242 * -980667675 >= (aClass497Array5207
						       [1899481443 * anInt5213]
						       .anInt5550) * -204452677
			    && -204452677 * (aClass497Array5207
					     [anInt5213 * 1899481443]
					     .anInt5550) >= 0)) {
			aClass497Array5207[1899481443 * anInt5213]
			    .method8134((short) 255);
			aClass497Array5207[anInt5213 * 1899481443] = null;
			anInt5213 += 1295515723;
			anInt5213 = 1295515723 * (anInt5213 * 1899481443
						  % aClass497Array5207.length);
			anInt5242 = 0;
		    }
		}
	    }
	}
    }
    
    void method7896(byte[] is, int i, int i_80_) {
	int i_81_ = i;
	int i_82_ = 0;
	int i_83_ = anInterface40_5226.method283().anInt3929 * -1204190425;
	while (i_81_ < is.length && i_81_ < i_80_) {
	    float f = aFloatArray5211[i_82_] * (aFloat5202 * aFloat5201);
	    if (i_83_ == 16) {
		int i_84_ = (is[i_81_] & 0xff) + (is[1 + i_81_] << 8);
		i_84_ *= f * f;
		is[i_81_++] = (byte) i_84_;
		is[i_81_++] = (byte) (i_84_ >> 8);
	    } else if (8 == i_83_) {
		byte i_85_ = is[i_81_];
		i_85_ *= f;
		is[i_81_++] = i_85_;
	    } else
		throw new RuntimeException("");
	    i_82_ = ++i_82_ % aFloatArray5211.length;
	}
    }
    
    void method7897(Class485 class485, byte i) {
	aClass485_5225 = class485;
    }
    
    void method7898(byte[] is, int i, int i_86_) {
	int i_87_ = i;
	int i_88_ = 0;
	int i_89_ = anInterface40_5226.method283().anInt3929 * -1204190425;
	while (i_87_ < is.length && i_87_ < i_86_) {
	    float f = aFloatArray5211[i_88_] * (aFloat5202 * aFloat5201);
	    if (i_89_ == 16) {
		int i_90_ = (is[i_87_] & 0xff) + (is[1 + i_87_] << 8);
		i_90_ *= f * f;
		is[i_87_++] = (byte) i_90_;
		is[i_87_++] = (byte) (i_90_ >> 8);
	    } else if (8 == i_89_) {
		byte i_91_ = is[i_87_];
		i_91_ *= f;
		is[i_87_++] = i_91_;
	    } else
		throw new RuntimeException("");
	    i_88_ = ++i_88_ % aFloatArray5211.length;
	}
    }
    
    void method7899(byte[] is, int i, int i_92_) {
	int i_93_ = i;
	int i_94_ = 0;
	int i_95_ = anInterface40_5226.method283().anInt3929 * -1204190425;
	while (i_93_ < is.length && i_93_ < i_92_) {
	    float f = aFloatArray5211[i_94_] * (aFloat5202 * aFloat5201);
	    if (i_95_ == 16) {
		int i_96_ = (is[i_93_] & 0xff) + (is[1 + i_93_] << 8);
		i_96_ *= f * f;
		is[i_93_++] = (byte) i_96_;
		is[i_93_++] = (byte) (i_96_ >> 8);
	    } else if (8 == i_95_) {
		byte i_97_ = is[i_93_];
		i_97_ *= f;
		is[i_93_++] = i_97_;
	    } else
		throw new RuntimeException("");
	    i_94_ = ++i_94_ % aFloatArray5211.length;
	}
    }
    
    Class534_Sub40 method7900(int i) {
	return anInterface40_5226.method297(i);
    }
    
    void method7901(Class534_Sub40 class534_sub40) {
	if (anObject5230 != null && anObject5230 instanceof Class491) {
	    Class491 class491 = (Class491) anObject5230;
	    Interface70 interface70 = class491.method8040((byte) 57);
	    byte[] is = new byte[class534_sub40.anInt10811 * 31645619];
	    System.arraycopy(class534_sub40.aByteArray10810, 0, is, 0,
			     31645619 * class534_sub40.anInt10811);
	    Class534_Sub40 class534_sub40_98_ = new Class534_Sub40(is);
	    class534_sub40_98_.anInt10811 = 1 * class534_sub40.anInt10811;
	    interface70.method451(class534_sub40_98_, -1909573641);
	}
    }
    
    void method7902(Class534_Sub40 class534_sub40) {
	if (anObject5230 != null && anObject5230 instanceof Class491) {
	    Class491 class491 = (Class491) anObject5230;
	    Interface70 interface70 = class491.method8040((byte) 47);
	    byte[] is = new byte[class534_sub40.anInt10811 * 31645619];
	    System.arraycopy(class534_sub40.aByteArray10810, 0, is, 0,
			     31645619 * class534_sub40.anInt10811);
	    Class534_Sub40 class534_sub40_99_ = new Class534_Sub40(is);
	    class534_sub40_99_.anInt10811 = 1 * class534_sub40.anInt10811;
	    interface70.method451(class534_sub40_99_, -2035784779);
	}
    }
    
    public Class479(Class496 class496, int i, int i_100_,
		    Interface40 interface40, Class367 class367) {
	anInt5232 = 0;
	anInt5241 = 0;
	anInt5235 = 0;
	anInt5236 = 0;
	anInt5238 = 0;
	aFloat5206 = 0.0F;
	aLong5200 = 0L;
	aLong5243 = 0L;
	aFloat5199 = 0.0F;
	anInt5217 = 0;
	anInt5203 = 0;
	aClass367_5227 = class367;
	method7897(Class485.aClass485_5287, (byte) -88);
	anInt5223 = i * -32869633;
	anInt5209 = i_100_ * -1852031161;
	anInterface40_5226 = interface40;
	anInt5213 = 0;
	aBool5228 = false;
	anInterface40_5226.method282(new Class495(this));
	method7843(-961916145);
    }
    
    public void method7903() {
	synchronized (this) {
	    if (method7853((byte) -55) == Class485.aClass485_5286
		|| (method7853((byte) 57).anInt5283 * -222113559
		    < -222113559 * Class485.aClass485_5281.anInt5283)) {
		/* empty */
	    } else
		method7897(Class485.aClass485_5286, (byte) -8);
	}
    }
    
    void method7904() {
	aBool5249 = true;
    }
    
    boolean method7905(int i) {
	if (anObject5230 != null && anObject5230 instanceof Class491) {
	    Class491 class491 = (Class491) anObject5230;
	    Interface70 interface70 = class491.method8040((byte) 28);
	    return interface70.method454(i, (byte) 77);
	}
	return false;
    }
    
    public void method7906() {
	if (method7853((byte) -42).anInt5283 * -222113559
	    < Class485.aClass485_5281.anInt5283 * -222113559)
	    throw new RuntimeException("");
	anObject5230 = null;
	method7897(Class485.aClass485_5289, (byte) -35);
    }
    
    boolean method7907() {
	synchronized (this) {
	    if (anObject5230 != null && anObject5230 instanceof Class491) {
		Class491 class491 = (Class491) anObject5230;
		Interface70 interface70 = class491.method8040((byte) 27);
		if (interface70 != null) {
		    boolean bool = interface70.method467(-1328916692);
		    return bool;
		}
	    }
	    boolean bool = false;
	    return bool;
	}
    }
    
    boolean method7908(int i) {
	return aClass485_5225 == Class485.aClass485_5288;
    }
    
    Object method7909() {
	return anObject5230;
    }
    
    Object method7910() {
	return anObject5230;
    }
    
    Object method7911() {
	return anObject5230;
    }
    
    void method7912() {
	synchronized (this) {
	    if (anObject5230 != null && anObject5230 instanceof Class491) {
		Class491 class491 = (Class491) anObject5230;
		Interface70 interface70 = class491.method8040((byte) 69);
		if (null != interface70)
		    interface70.method472(true, -547395899);
	    }
	}
    }
    
    public float method7913() {
	return aFloat5244;
    }
    
    void method7914() {
	synchronized (this) {
	    aByteArray5233 = null;
	    anInt5205 = 0;
	    aFloat5201 = 0.0F;
	    aFloat5202 = 0.0F;
	    aBool5204 = false;
	    anInt5247 = 0;
	    aFloat5212 = 1.0F;
	    anInterface56_5245 = null;
	    anInt5208 = 0;
	    anInt5222 = 0;
	    if (aClass497Array5207 != null) {
		for (int i = 0; i < aClass497Array5207.length; i++) {
		    if (aClass497Array5207[i] != null)
			aClass497Array5207[i].method8134((short) 255);
		}
	    }
	    aClass497Array5207 = null;
	    aBool5246 = false;
	    anInt5213 = 0;
	    anInt5215 = 0;
	    anInt5216 = 0;
	    anInt5210 = 0;
	    anInt5218 = 0;
	    anInt5219 = 0;
	    anIntArray5220 = null;
	    anIntArray5221 = null;
	    anIntArray5237 = null;
	    anInt5239 = 0;
	    aBool5224 = false;
	    anInterface40_5226.method286();
	    aBool5228 = false;
	    anInterface58_5229 = null;
	    anObject5230 = null;
	    anInt5242 = 0;
	    anInt5232 = 0;
	    anInt5241 = 0;
	    aBool5234 = false;
	    anInt5235 = 0;
	    anInt5236 = 0;
	    aBool5248 = false;
	    anInt5238 = 0;
	    method7820(-1.0F, 511982797);
	    aBool5240 = false;
	    aFloat5206 = 0.0F;
	    aLong5200 = 0L;
	    aLong5243 = 0L;
	    aFloat5199 = 0.0F;
	    anInt5217 = 0;
	    anInt5203 = 0;
	}
    }
    
    void method7915() {
	synchronized (this) {
	    aByteArray5233 = null;
	    anInt5205 = 0;
	    aFloat5201 = 0.0F;
	    aFloat5202 = 0.0F;
	    aBool5204 = false;
	    anInt5247 = 0;
	    aFloat5212 = 1.0F;
	    anInterface56_5245 = null;
	    anInt5208 = 0;
	    anInt5222 = 0;
	    if (aClass497Array5207 != null) {
		for (int i = 0; i < aClass497Array5207.length; i++) {
		    if (aClass497Array5207[i] != null)
			aClass497Array5207[i].method8134((short) 255);
		}
	    }
	    aClass497Array5207 = null;
	    aBool5246 = false;
	    anInt5213 = 0;
	    anInt5215 = 0;
	    anInt5216 = 0;
	    anInt5210 = 0;
	    anInt5218 = 0;
	    anInt5219 = 0;
	    anIntArray5220 = null;
	    anIntArray5221 = null;
	    anIntArray5237 = null;
	    anInt5239 = 0;
	    aBool5224 = false;
	    anInterface40_5226.method286();
	    aBool5228 = false;
	    anInterface58_5229 = null;
	    anObject5230 = null;
	    anInt5242 = 0;
	    anInt5232 = 0;
	    anInt5241 = 0;
	    aBool5234 = false;
	    anInt5235 = 0;
	    anInt5236 = 0;
	    aBool5248 = false;
	    anInt5238 = 0;
	    method7820(-1.0F, -135883699);
	    aBool5240 = false;
	    aFloat5206 = 0.0F;
	    aLong5200 = 0L;
	    aLong5243 = 0L;
	    aFloat5199 = 0.0F;
	    anInt5217 = 0;
	    anInt5203 = 0;
	}
    }
    
    static boolean method7916(int i) {
	return Class677.method11142(Class700.aClass638_8806.aClass603_8309,
				    1198974331);
    }
    
    static final void method7917(Class669 class669, int i) {
	class669.anInt8600 -= 617999126;
	int i_101_ = class669.anIntArray8595[class669.anInt8600 * 2088438307];
	int i_102_
	    = class669.anIntArray8595[1 + 2088438307 * class669.anInt8600];
	Class273.method5102(8, i_101_, i_102_, "", (byte) 2);
    }
    
    static final int method7918(int i, int i_103_) {
	int i_104_ = i & 0x3f;
	int i_105_ = i >> 6 & 0x3;
	if (i_104_ == 18) {
	    if (0 == i_105_)
		return 1;
	    if (i_105_ == 1)
		return 2;
	    if (i_105_ == 2)
		return 4;
	    if (i_105_ == 3)
		return 8;
	} else if (i_104_ == 19 || 21 == i_104_) {
	    if (i_105_ == 0)
		return 16;
	    if (i_105_ == 1)
		return 32;
	    if (2 == i_105_)
		return 64;
	    if (3 == i_105_)
		return 128;
	}
	return 0;
    }
    
    static boolean method7919(Interface65 interface65,
			      Class534_Sub42 class534_sub42, int i) {
	return (interface65 != null
		&& interface65.method437(class534_sub42,
					 client.anInterface63Array11070,
					 -2064179505 * client.anInt11199,
					 Class666.aClass547_8577, -687359472));
    }
}
