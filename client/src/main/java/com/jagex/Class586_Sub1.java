/* Class586_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class586_Sub1 extends Class586 implements Interface6
{
    public Interface13 method63(int i, Interface14 interface14) {
	return new Class578(i, this, interface14);
    }
    
    public Interface13 method58(int i, Interface14 interface14, byte i_0_) {
	return new Class578(i, this, interface14);
    }
    
    public Class method59(short i) {
	return com.jagex.Class578.class;
    }
    
    public Class method61() {
	return com.jagex.Class578.class;
    }
    
    public Interface13 method60(int i, Interface14 interface14) {
	return new Class578(i, this, interface14);
    }
    
    public Interface13 method62(int i, Interface14 interface14) {
	return new Class578(i, this, interface14);
    }
    
    Class586_Sub1(Class472 class472) {
	super(class472);
    }
    
    public Interface13 method64(int i, Interface14 interface14) {
	return new Class578(i, this, interface14);
    }
}
