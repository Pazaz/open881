/* Class654_Sub1_Sub5_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public abstract class Class654_Sub1_Sub5_Sub1 extends Class654_Sub1_Sub5
    implements Interface31
{
    public int[] anIntArray11920;
    static final int anInt11921 = 5;
    public int anInt11922 = 2061161143;
    int anInt11923 = 659989567;
    byte aByte11924;
    public int[] anIntArray11925;
    int anInt11926;
    int anInt11927 = 1208975360;
    public byte aByte11928;
    public int anInt11929;
    int anInt11930;
    public int anInt11931;
    int anInt11932;
    int anInt11933;
    int anInt11934;
    Class526 aClass526_11935;
    public boolean aBool11936;
    public int anInt11937;
    public int[] anIntArray11938;
    public int[] anIntArray11939;
    public int anInt11940;
    public Class709 aClass709_11941;
    public int[] anIntArray11942;
    public int anInt11943;
    public int anInt11944;
    public int anInt11945;
    public int[] anIntArray11946;
    public int anInt11947;
    public Class711 aClass711_11948;
    public Class529[] aClass529Array11949;
    int anInt11950;
    public int anInt11951;
    public int[] anIntArray11952;
    int anInt11953;
    public int anInt11954;
    public int anInt11955;
    public int anInt11956;
    public int anInt11957;
    public int anInt11958;
    public int anInt11959;
    public int anInt11960;
    boolean aBool11961;
    public byte aByte11962;
    public byte aByte11963;
    public byte aByte11964;
    public Class711_Sub1 aClass711_Sub1_11965;
    static final int anInt11966 = 20;
    public int anInt11967;
    public Class711_Sub3_Sub1[] aClass711_Sub3_Sub1Array11968;
    public int anInt11969;
    public int anInt11970;
    public int anInt11971;
    public int anInt11972;
    public Class57 aClass57_11973;
    public Class57 aClass57_11974;
    public Class57 aClass57_11975;
    public int anInt11976;
    public int[] anIntArray11977;
    public int[] anIntArray11978;
    public byte[] aByteArray11979;
    public int anInt11980;
    int anInt11981 = -1458012160;
    public int anInt11982;
    Class629 aClass629_11983;
    boolean aBool11984;
    public int[] anIntArray11985;
    public Class183[] aClass183Array11986;
    public Interface4 anInterface4_11987;
    Class243 aClass243_11988;
    public boolean aBool11989 = true;
    int anInt11990;
    public Interface19 anInterface19_11991;
    
    public int method18521() {
	Class570 class570 = method18531((byte) -118);
	int i = 949937137 * aClass57_11973.anInt457;
	boolean bool;
	if (0 != class570.anInt7653 * 599688861)
	    bool = aClass57_11973.method1229(anInt11972 * 1306061223,
					     599688861 * class570.anInt7653,
					     class570.anInt7641 * -812379773,
					     420971055);
	else
	    bool = aClass57_11973.method1229(1306061223 * anInt11972,
					     anInt11971 * -650254265,
					     anInt11971 * -650254265,
					     132796525);
	int i_0_ = aClass57_11973.anInt457 * 949937137 - i;
	if (0 != i_0_)
	    anInt11970 += -1993578637;
	else {
	    anInt11970 = 0;
	    aClass57_11973.method1241(1306061223 * anInt11972, 1388474990);
	}
	if (bool) {
	    if (0 != -290126451 * class570.anInt7646) {
		if (i_0_ > 0)
		    aClass57_11974.method1229(class570.anInt7657 * 2079472619,
					      class570.anInt7646 * -290126451,
					      985827811 * class570.anInt7656,
					      788193559);
		else
		    aClass57_11974.method1229(-(2079472619
						* class570.anInt7657),
					      -290126451 * class570.anInt7646,
					      class570.anInt7656 * 985827811,
					      1849866406);
	    }
	    if (1537785595 * class570.anInt7658 != 0)
		aClass57_11975.method1229(class570.anInt7660 * -97068925,
					  1537785595 * class570.anInt7658,
					  -2115504123 * class570.anInt7659,
					  1763383755);
	} else {
	    if (-290126451 * class570.anInt7646 != 0)
		aClass57_11974.method1229(0, -290126451 * class570.anInt7646,
					  985827811 * class570.anInt7656,
					  669850851);
	    else
		aClass57_11974.method1241(0, 1388474990);
	    if (0 != class570.anInt7658 * 1537785595)
		aClass57_11975.method1229(0, class570.anInt7658 * 1537785595,
					  class570.anInt7659 * -2115504123,
					  1384577420);
	    else
		aClass57_11975.method1241(0, 1388474990);
	}
	return i_0_;
    }
    
    Class654_Sub1_Sub5_Sub1(Class556 class556, Class110_Sub1 class110_sub1) {
	this(class556, 10, class110_sub1);
    }
    
    boolean method18522() {
	if (anInt11990 * -935424637 == -1)
	    return false;
	return method18561(-935424637 * anInt11990, 1055370457);
    }
    
    boolean method16849(int i) {
	return false;
    }
    
    public void method18523(int i, boolean bool, int i_1_) {
	Class570 class570 = method18531((byte) -58);
	if (bool || 599688861 * class570.anInt7653 != 0
	    || 0 != -650254265 * anInt11971) {
	    anInt11972 = -283038185 * (i & 0x3fff);
	    aClass57_11973.method1241(anInt11972 * 1306061223, 1388474990);
	}
    }
    
    public void method18524(int i, int i_2_) {
	Class570 class570 = method18531((byte) -97);
	if (class570.anInt7653 * 599688861 != 0
	    || -650254265 * anInt11971 != 0) {
	    aClass57_11973.method1232((byte) 43);
	    int i_3_ = i - aClass57_11973.anInt457 * 949937137 & 0x3fff;
	    if (i_3_ > 8192)
		anInt11972 = (aClass57_11973.anInt457 * 949937137
			      - (16384 - i_3_)) * -283038185;
	    else
		anInt11972 = ((949937137 * aClass57_11973.anInt457 + i_3_)
			      * -283038185);
	}
    }
    
    Class654_Sub1_Sub5_Sub1(Class556 class556, int i,
			    Class110_Sub1 class110_sub1) {
	super(class556, 0, 0, 0, 0, 0, 0, 0, 0, 0, false, (byte) 0);
	anInt11929 = 0;
	anInt11969 = 253074569;
	anInt11931 = 0;
	anInt11930 = 0;
	aByte11924 = (byte) 0;
	anIntArray11938
	    = new int[Class620.aClass632_8113.anInt8257 * 521612365];
	anIntArray11939
	    = new int[Class620.aClass632_8113.anInt8257 * 521612365];
	anIntArray11925
	    = new int[Class620.aClass632_8113.anInt8257 * 521612365];
	anIntArray11952
	    = new int[521612365 * Class620.aClass632_8113.anInt8257];
	anIntArray11942
	    = new int[521612365 * Class620.aClass632_8113.anInt8257];
	aClass709_11941 = new Class709();
	anInt11944 = -776069067;
	anIntArray11946 = null;
	aClass711_Sub1_11965 = new Class711_Sub1(this, false);
	aClass711_11948 = new Class711_Sub3(this, false);
	anInt11959 = -1446706933;
	anInt11960 = 1565168329;
	aByte11964 = (byte) 0;
	anInt11926 = 2084571495;
	anInt11953 = 0;
	anInt11967 = 0;
	anInt11970 = 0;
	anInt11971 = 1229027072;
	aClass57_11973 = new Class57();
	aClass57_11974 = new Class57();
	aClass57_11975 = new Class57();
	anInt11980 = 0;
	anInt11947 = 0;
	anInt11937 = 0;
	anInt11982 = 0;
	aBool11984 = false;
	aBool11961 = false;
	anInt11932 = -1079007783;
	anInt11990 = 1712920277;
	anInterface19_11991 = new Class535(this);
	anIntArray11977 = new int[i];
	anIntArray11978 = new int[i];
	aByteArray11979 = new byte[i];
	aClass183Array11986 = new Class183[6];
	aClass529Array11949 = new Class529[5];
	for (int i_4_ = 0; i_4_ < 5; i_4_++)
	    aClass529Array11949[i_4_] = new Class529(this);
	aClass711_Sub3_Sub1Array11968
	    = (new Class711_Sub3_Sub1
	       [Class69.aClass630_728.anIntArray8196.length]);
	anInterface4_11987 = new Class8(class110_sub1);
    }
    
    public int method18525(int i) {
	Class570 class570 = method18531((byte) -33);
	int i_5_ = 949937137 * aClass57_11973.anInt457;
	boolean bool;
	if (0 != class570.anInt7653 * 599688861)
	    bool = aClass57_11973.method1229(anInt11972 * 1306061223,
					     599688861 * class570.anInt7653,
					     class570.anInt7641 * -812379773,
					     1864104508);
	else
	    bool = aClass57_11973.method1229(1306061223 * anInt11972,
					     anInt11971 * -650254265,
					     anInt11971 * -650254265,
					     157529765);
	int i_6_ = aClass57_11973.anInt457 * 949937137 - i_5_;
	if (0 != i_6_)
	    anInt11970 += -1993578637;
	else {
	    anInt11970 = 0;
	    aClass57_11973.method1241(1306061223 * anInt11972, 1388474990);
	}
	if (bool) {
	    if (0 != -290126451 * class570.anInt7646) {
		if (i_6_ > 0)
		    aClass57_11974.method1229(class570.anInt7657 * 2079472619,
					      class570.anInt7646 * -290126451,
					      985827811 * class570.anInt7656,
					      340571394);
		else
		    aClass57_11974.method1229(-(2079472619
						* class570.anInt7657),
					      -290126451 * class570.anInt7646,
					      class570.anInt7656 * 985827811,
					      86460633);
	    }
	    if (1537785595 * class570.anInt7658 != 0)
		aClass57_11975.method1229(class570.anInt7660 * -97068925,
					  1537785595 * class570.anInt7658,
					  -2115504123 * class570.anInt7659,
					  928767615);
	} else {
	    if (-290126451 * class570.anInt7646 != 0)
		aClass57_11974.method1229(0, -290126451 * class570.anInt7646,
					  985827811 * class570.anInt7656,
					  419713082);
	    else
		aClass57_11974.method1241(0, 1388474990);
	    if (0 != class570.anInt7658 * 1537785595)
		aClass57_11975.method1229(0, class570.anInt7658 * 1537785595,
					  class570.anInt7659 * -2115504123,
					  111916871);
	    else
		aClass57_11975.method1241(0, 1388474990);
	}
	return i_6_;
    }
    
    void method18526(Class183 class183) {
	int i = aClass57_11974.anInt457 * 949937137;
	int i_7_ = 949937137 * aClass57_11975.anInt457;
	if (i != 0 || 0 != i_7_) {
	    int i_8_ = class183.method3045() / 2;
	    class183.method3098(0, -i_8_, 0);
	    class183.method3018(i & 0x3fff);
	    class183.method3017(i_7_ & 0x3fff);
	    class183.method3098(0, i_8_, 0);
	}
    }
    
    public boolean method18527(int i, int i_9_, int i_10_) {
	if (anIntArray11920 == null) {
	    if (-1 == i_9_)
		return true;
	    anIntArray11920
		= new int[Class69.aClass630_728.anIntArray8196.length];
	    for (int i_11_ = 0;
		 i_11_ < Class69.aClass630_728.anIntArray8196.length; i_11_++)
		anIntArray11920[i_11_] = -1;
	}
	Class570 class570 = method18531((byte) -91);
	int i_12_ = 256;
	if (null != class570.anIntArray7651 && class570.anIntArray7651[i] > 0)
	    i_12_ = class570.anIntArray7651[i];
	if (i_9_ == -1) {
	    if (anIntArray11920[i] == -1)
		return true;
	    int i_13_ = aClass57_11973.method1231(1430088384);
	    int i_14_ = anIntArray11920[i];
	    int i_15_ = i_13_ - i_14_;
	    if (i_15_ >= -i_12_ && i_15_ <= i_12_) {
		anIntArray11920[i] = -1;
		for (int i_16_ = 0;
		     i_16_ < Class69.aClass630_728.anIntArray8196.length;
		     i_16_++) {
		    if (-1 != anIntArray11920[i_16_])
			return true;
		}
		anIntArray11920 = null;
		return true;
	    }
	    if (i_15_ > 0 && i_15_ <= 8192 || i_15_ <= -8192)
		anIntArray11920[i] = i_12_ + i_14_ & 0x3fff;
	    else
		anIntArray11920[i] = i_14_ - i_12_ & 0x3fff;
	    return false;
	}
	if (anIntArray11920[i] == -1)
	    anIntArray11920[i] = aClass57_11973.method1231(1631251068);
	int i_17_ = anIntArray11920[i];
	int i_18_ = i_9_ - i_17_;
	if (i_18_ >= -i_12_ && i_18_ <= i_12_) {
	    anIntArray11920[i] = i_9_;
	    return true;
	}
	if (i_18_ > 0 && i_18_ <= 8192 || i_18_ <= -8192)
	    anIntArray11920[i] = i_12_ + i_17_ & 0x3fff;
	else
	    anIntArray11920[i] = i_17_ - i_12_ & 0x3fff;
	return false;
    }
    
    void method18528(Class185 class185, Class183[] class183s,
		     Class446 class446, boolean bool, int i) {
	if (!bool) {
	    int i_19_ = 0;
	    int i_20_ = 0;
	    int i_21_ = 0;
	    int i_22_ = 0;
	    int i_23_ = -1;
	    int i_24_ = -1;
	    Class172[][] class172s = new Class172[class183s.length][];
	    Class158[][] class158s = new Class158[class183s.length][];
	    for (int i_25_ = 0; i_25_ < class183s.length; i_25_++) {
		if (null != class183s[i_25_]) {
		    class183s[i_25_].method3073(class446);
		    class172s[i_25_] = class183s[i_25_].method3168();
		    class158s[i_25_] = class183s[i_25_].method3065();
		    if (class172s[i_25_] != null) {
			i_23_ = i_25_;
			i_20_++;
			i_19_ += class172s[i_25_].length;
		    }
		    if (class158s[i_25_] != null) {
			i_24_ = i_25_;
			i_22_++;
			i_21_ += class158s[i_25_].length;
		    }
		}
	    }
	    if ((aClass629_11983 == null || aClass629_11983.aBool8189)
		&& (i_20_ > 0 || i_22_ > 0))
		aClass629_11983
		    = Class629.method10422(client.anInt11101, true);
	    if (null != aClass629_11983) {
		Object object = null;
		Class172[] class172s_26_;
		if (i_20_ == 1)
		    class172s_26_ = class172s[i_23_];
		else {
		    class172s_26_ = new Class172[i_19_];
		    int i_27_ = 0;
		    for (int i_28_ = 0; i_28_ < class183s.length; i_28_++) {
			if (class172s[i_28_] != null) {
			    System.arraycopy(class172s[i_28_], 0,
					     class172s_26_, i_27_,
					     class172s[i_28_].length);
			    i_27_ += class172s[i_28_].length;
			}
		    }
		}
		Object object_29_ = null;
		Class158[] class158s_30_;
		if (1 == i_22_)
		    class158s_30_ = class158s[i_24_];
		else {
		    class158s_30_ = new Class158[i_21_];
		    int i_31_ = 0;
		    for (int i_32_ = 0; i_32_ < class183s.length; i_32_++) {
			if (null != class158s[i_32_]) {
			    System.arraycopy(class158s[i_32_], 0,
					     class158s_30_, i_31_,
					     class158s[i_32_].length);
			    i_31_ += class158s[i_32_].length;
			}
		    }
		}
		aClass629_11983.method10393(class185, (long) client.anInt11101,
					    class172s_26_, class158s_30_,
					    false);
		aBool11984 = true;
	    }
	} else if (null != aClass629_11983)
	    aClass629_11983.method10392((long) client.anInt11101);
	if (aClass629_11983 != null)
	    aClass629_11983.method10396(aByte10854, aShort11900, aShort11896,
					aShort11901, aShort11898);
    }
    
    abstract int method18529(byte i);
    
    public final void method18530(int i) {
	anInt11980 = 0;
	anInt11937 = 0;
    }
    
    public Class570 method18531(byte i) {
	int i_33_ = method18529((byte) 54);
	if (i_33_ == -1)
	    return Class44_Sub14.aClass570_11009;
	return ((Class570)
		Class200_Sub23.aClass44_Sub14_10041.method91(i_33_, 9901316));
    }
    
    public final void method18532(int i, int i_34_, int i_35_, int i_36_,
				  int i_37_, int i_38_, int i_39_) {
	Class579 class579
	    = (Class579) Class470.aClass44_Sub17_5153.method91(i, 533366424);
	Class536_Sub5 class536_sub5 = null;
	Class536_Sub5 class536_sub5_40_ = null;
	int i_41_ = 1151479045 * class579.anInt7753;
	int i_42_ = 0;
	for (Class536_Sub5 class536_sub5_43_
		 = (Class536_Sub5) aClass709_11941.method14301(764908544);
	     class536_sub5_43_ != null;
	     class536_sub5_43_
		 = (Class536_Sub5) aClass709_11941.method14282(-1678402084)) {
	    i_42_++;
	    if (class536_sub5_43_.aClass579_10368 == class579) {
		class536_sub5_43_.method15996(i_34_ + i_36_, i_37_, i_38_,
					      i_35_, (short) 255);
		return;
	    }
	    if (class536_sub5_43_.aClass579_10368.anInt7747 * 1475151865
		<= 1475151865 * class579.anInt7747)
		class536_sub5 = class536_sub5_43_;
	    if (1151479045 * class536_sub5_43_.aClass579_10368.anInt7753
		> i_41_) {
		class536_sub5_40_ = class536_sub5_43_;
		i_41_
		    = class536_sub5_43_.aClass579_10368.anInt7753 * 1151479045;
	    }
	}
	if (null != class536_sub5_40_
	    || i_42_ < Class620.aClass632_8113.anInt8256 * 1210989897) {
	    Class536_Sub5 class536_sub5_44_ = new Class536_Sub5(class579);
	    if (null == class536_sub5)
		aClass709_11941.method14285(class536_sub5_44_, (byte) 61);
	    else
		Class485.method7963(class536_sub5_44_, class536_sub5,
				    (byte) -32);
	    class536_sub5_44_.method15996(i_34_ + i_36_, i_37_, i_38_, i_35_,
					  (short) 255);
	    if (i_42_ >= 1210989897 * Class620.aClass632_8113.anInt8256)
		class536_sub5_40_.method8900(1119251176);
	}
    }
    
    public final void method18533(int i, int i_45_) {
	Class579 class579
	    = (Class579) Class470.aClass44_Sub17_5153.method91(i, -1269102503);
	for (Class536_Sub5 class536_sub5
		 = (Class536_Sub5) aClass709_11941.method14301(764908544);
	     class536_sub5 != null;
	     class536_sub5
		 = (Class536_Sub5) aClass709_11941.method14282(1109530305)) {
	    if (class579 == class536_sub5.aClass579_10368) {
		class536_sub5.method8900(399472638);
		break;
	    }
	}
    }
    
    public int method16897(int i) {
	Class570 class570 = method18531((byte) -113);
	int i_46_;
	if (-1 != class570.anInt7662 * -1329461445)
	    i_46_ = class570.anInt7662 * -1329461445;
	else if (-399165137 * anInt11981 == -32768)
	    i_46_ = 200;
	else
	    i_46_ = -(-399165137 * anInt11981);
	Class444 class444 = method10807();
	int i_47_ = (int) class444.aClass438_4885.aFloat4864 >> 9;
	int i_48_ = (int) class444.aClass438_4885.aFloat4865 >> 9;
	if (aClass556_10855 != null && i_47_ >= 1 && i_48_ >= 1
	    && i_47_ <= client.aClass512_11100.method8417(933214989) - 1
	    && i_48_ <= client.aClass512_11100.method8418(-1533611049) - 1) {
	    Class568 class568 = (aClass556_10855.aClass568ArrayArrayArray7431
				 [aByte10854][i_47_][i_48_]);
	    if (class568 != null && class568.aClass654_Sub1_Sub2_7607 != null)
		return class568.aClass654_Sub1_Sub2_7607.aShort11864 + i_46_;
	}
	return i_46_;
    }
    
    void method18534(Class183 class183) {
	int i = aClass57_11974.anInt457 * 949937137;
	int i_49_ = 949937137 * aClass57_11975.anInt457;
	if (i != 0 || 0 != i_49_) {
	    int i_50_ = class183.method3045() / 2;
	    class183.method3098(0, -i_50_, 0);
	    class183.method3018(i & 0x3fff);
	    class183.method3017(i_49_ & 0x3fff);
	    class183.method3098(0, i_50_, 0);
	}
    }
    
    public void method18535(int i, int i_51_) {
	anInt11923 = i * 659989567;
    }
    
    void method18536(Class185 class185, Class183[] class183s,
		     Class446 class446, boolean bool) {
	if (!bool) {
	    int i = 0;
	    int i_52_ = 0;
	    int i_53_ = 0;
	    int i_54_ = 0;
	    int i_55_ = -1;
	    int i_56_ = -1;
	    Class172[][] class172s = new Class172[class183s.length][];
	    Class158[][] class158s = new Class158[class183s.length][];
	    for (int i_57_ = 0; i_57_ < class183s.length; i_57_++) {
		if (null != class183s[i_57_]) {
		    class183s[i_57_].method3073(class446);
		    class172s[i_57_] = class183s[i_57_].method3168();
		    class158s[i_57_] = class183s[i_57_].method3065();
		    if (class172s[i_57_] != null) {
			i_55_ = i_57_;
			i_52_++;
			i += class172s[i_57_].length;
		    }
		    if (class158s[i_57_] != null) {
			i_56_ = i_57_;
			i_54_++;
			i_53_ += class158s[i_57_].length;
		    }
		}
	    }
	    if ((aClass629_11983 == null || aClass629_11983.aBool8189)
		&& (i_52_ > 0 || i_54_ > 0))
		aClass629_11983
		    = Class629.method10422(client.anInt11101, true);
	    if (null != aClass629_11983) {
		Object object = null;
		Class172[] class172s_58_;
		if (i_52_ == 1)
		    class172s_58_ = class172s[i_55_];
		else {
		    class172s_58_ = new Class172[i];
		    int i_59_ = 0;
		    for (int i_60_ = 0; i_60_ < class183s.length; i_60_++) {
			if (class172s[i_60_] != null) {
			    System.arraycopy(class172s[i_60_], 0,
					     class172s_58_, i_59_,
					     class172s[i_60_].length);
			    i_59_ += class172s[i_60_].length;
			}
		    }
		}
		Object object_61_ = null;
		Class158[] class158s_62_;
		if (1 == i_54_)
		    class158s_62_ = class158s[i_56_];
		else {
		    class158s_62_ = new Class158[i_53_];
		    int i_63_ = 0;
		    for (int i_64_ = 0; i_64_ < class183s.length; i_64_++) {
			if (null != class158s[i_64_]) {
			    System.arraycopy(class158s[i_64_], 0,
					     class158s_62_, i_63_,
					     class158s[i_64_].length);
			    i_63_ += class158s[i_64_].length;
			}
		    }
		}
		aClass629_11983.method10393(class185, (long) client.anInt11101,
					    class172s_58_, class158s_62_,
					    false);
		aBool11984 = true;
	    }
	} else if (null != aClass629_11983)
	    aClass629_11983.method10392((long) client.anInt11101);
	if (aClass629_11983 != null)
	    aClass629_11983.method10396(aByte10854, aShort11900, aShort11896,
					aShort11901, aShort11898);
    }
    
    void method18537(Class183 class183, int i) {
	int i_65_ = aClass57_11974.anInt457 * 949937137;
	int i_66_ = 949937137 * aClass57_11975.anInt457;
	if (i_65_ != 0 || 0 != i_66_) {
	    int i_67_ = class183.method3045() / 2;
	    class183.method3098(0, -i_67_, 0);
	    class183.method3018(i_65_ & 0x3fff);
	    class183.method3017(i_66_ & 0x3fff);
	    class183.method3098(0, i_67_, 0);
	}
    }
    
    void method18538(Class185 class185, Class570 class570, int i, int i_68_,
		     int i_69_, int i_70_, int i_71_) {
	for (int i_72_ = 0; i_72_ < aClass529Array11949.length; i_72_++) {
	    byte i_73_ = 0;
	    if (0 == i_72_)
		i_73_ = (byte) 2;
	    else if (1 == i_72_)
		i_73_ = (byte) 5;
	    else if (2 == i_72_)
		i_73_ = (byte) 1;
	    else if (3 == i_72_)
		i_73_ = (byte) 7;
	    else if (i_72_ == 4)
		i_73_ = (byte) 8;
	    Class529 class529 = aClass529Array11949[i_72_];
	    if (class529.anInt7123 * -1183861629 != -1
		&& !class529.aClass711_7121.method14336(1746882819)) {
		Class684 class684
		    = ((Class684)
		       Class55.aClass44_Sub4_447.method91((-1183861629
							   * (class529
							      .anInt7123)),
							  1187683527));
		int i_74_ = i;
		if (class529.anInt7120 * 734138059 != 0)
		    i_74_ |= 0x5;
		if (class529.anInt7122 * -847145627 != 0)
		    i_74_ |= 0x2;
		if (class529.anInt7124 * 1747167815 >= 0)
		    i_74_ |= 0x7;
		if (class529.anInt7124 * 1747167815 >= 0
		    && null != class570.anIntArrayArray7648
		    && null != (class570.anIntArrayArray7648
				[class529.anInt7124 * 1747167815])) {
		    Class183 class183
			= (aClass183Array11986[i_72_ + 1]
			   = class684.method13928(class185, i_74_,
						  class529.aClass711_7121,
						  i_73_, (byte) 104));
		    if (class183 != null) {
			int i_75_ = 0;
			int i_76_ = 0;
			int i_77_ = 0;
			if (null != class570.anIntArrayArray7648
			    && (class570.anIntArrayArray7648
				[1747167815 * class529.anInt7124]) != null) {
			    i_75_ += (class570.anIntArrayArray7648
				      [1747167815 * class529.anInt7124][0]);
			    i_76_ += (class570.anIntArrayArray7648
				      [1747167815 * class529.anInt7124][1]);
			    i_77_ += (class570.anIntArrayArray7648
				      [class529.anInt7124 * 1747167815][2]);
			}
			if (null != class570.anIntArrayArray7649
			    && (class570.anIntArrayArray7649
				[1747167815 * class529.anInt7124]) != null) {
			    i_75_ += (class570.anIntArrayArray7649
				      [class529.anInt7124 * 1747167815][0]);
			    i_76_ += (class570.anIntArrayArray7649
				      [class529.anInt7124 * 1747167815][1]);
			    i_77_ += (class570.anIntArrayArray7649
				      [class529.anInt7124 * 1747167815][2]);
			}
			if (0 != i_77_ || i_75_ != 0) {
			    int i_78_ = i_70_;
			    if (null != anIntArray11920
				&& (anIntArray11920
				    [1747167815 * class529.anInt7124]) != -1)
				i_78_ = (anIntArray11920
					 [class529.anInt7124 * 1747167815]);
			    int i_79_ = ((class529.anInt7120 * 276191232
					  + i_78_ - i_70_)
					 & 0x3fff);
			    if (0 != i_79_)
				class183.method3015(i_79_);
			    int i_80_ = Class427.anIntArray4806[i_79_];
			    int i_81_ = Class427.anIntArray4805[i_79_];
			    int i_82_ = i_80_ * i_77_ + i_81_ * i_75_ >> 14;
			    i_77_ = i_81_ * i_77_ - i_75_ * i_80_ >> 14;
			    i_75_ = i_82_;
			}
			class183.method3098(i_75_, i_76_, i_77_);
			if (0 != -847145627 * class529.anInt7122)
			    class183.method3098(0,
						-(-847145627
						  * class529.anInt7122) << 2,
						0);
			class183.aBool1971 = false;
		    }
		} else {
		    Class183 class183
			= (aClass183Array11986[1 + i_72_]
			   = class684.method13945(class185, i_74_,
						  (276191232
						   * class529.anInt7120),
						  1177236321 * anInt11950,
						  anInt11933 * -1556313745,
						  956485751 * anInt11934,
						  class529.aClass711_7121,
						  i_73_, -17051407));
		    if (class183 != null) {
			if (0 != -847145627 * class529.anInt7122)
			    class183.method3098(0,
						-(-847145627
						  * class529.anInt7122) << 2,
						0);
			class183.aBool1971 = false;
		    }
		}
	    } else
		aClass183Array11986[i_72_ + 1] = null;
	}
    }
    
    void method18539(int i, int i_83_, int i_84_, int i_85_, int i_86_,
		     byte i_87_) {
	Class438 class438 = method10807().aClass438_4885;
	int i_88_ = aShort11900 + aShort11896 >> 1;
	int i_89_ = aShort11898 + aShort11901 >> 1;
	int i_90_ = Class427.anIntArray4806[i];
	int i_91_ = Class427.anIntArray4805[i];
	int i_92_ = -i_83_ / 2;
	int i_93_ = -i_84_ / 2;
	int i_94_ = i_91_ * i_92_ + i_93_ * i_90_ >> 14;
	int i_95_ = i_91_ * i_93_ - i_92_ * i_90_ >> 14;
	int i_96_ = Class219.method4142(i_94_ + (int) class438.aFloat4864,
					i_95_ + (int) class438.aFloat4865,
					i_88_, i_89_, aByte10854, (byte) 66);
	int i_97_ = i_83_ / 2;
	int i_98_ = -i_84_ / 2;
	int i_99_ = i_90_ * i_98_ + i_91_ * i_97_ >> 14;
	int i_100_ = i_91_ * i_98_ - i_90_ * i_97_ >> 14;
	int i_101_ = Class219.method4142((int) class438.aFloat4864 + i_99_,
					 i_100_ + (int) class438.aFloat4865,
					 i_88_, i_89_, aByte10854, (byte) 49);
	int i_102_ = -i_83_ / 2;
	int i_103_ = i_84_ / 2;
	int i_104_ = i_103_ * i_90_ + i_91_ * i_102_ >> 14;
	int i_105_ = i_103_ * i_91_ - i_90_ * i_102_ >> 14;
	int i_106_ = Class219.method4142(i_104_ + (int) class438.aFloat4864,
					 (int) class438.aFloat4865 + i_105_,
					 i_88_, i_89_, aByte10854, (byte) 33);
	int i_107_ = i_83_ / 2;
	int i_108_ = i_84_ / 2;
	int i_109_ = i_108_ * i_90_ + i_107_ * i_91_ >> 14;
	int i_110_ = i_91_ * i_108_ - i_90_ * i_107_ >> 14;
	int i_111_ = Class219.method4142(i_109_ + (int) class438.aFloat4864,
					 (int) class438.aFloat4865 + i_110_,
					 i_88_, i_89_, aByte10854, (byte) 104);
	int i_112_ = i_96_ < i_101_ ? i_96_ : i_101_;
	int i_113_ = i_106_ < i_111_ ? i_106_ : i_111_;
	int i_114_ = i_101_ < i_111_ ? i_101_ : i_111_;
	int i_115_ = i_96_ < i_106_ ? i_96_ : i_106_;
	anInt11950
	    = ((int) (Math.atan2((double) (i_112_ - i_113_), (double) i_84_)
		      * 2607.5945876176133)
	       & 0x3fff) * -1128091487;
	anInt11933
	    = ((int) (Math.atan2((double) (i_115_ - i_114_), (double) i_83_)
		      * 2607.5945876176133)
	       & 0x3fff) * -723978865;
	if (1177236321 * anInt11950 != 0 && i_85_ != 0) {
	    int i_116_ = 16384 - i_85_;
	    if (1177236321 * anInt11950 > 8192) {
		if (anInt11950 * 1177236321 < i_116_)
		    anInt11950 = -1128091487 * i_116_;
	    } else if (anInt11950 * 1177236321 > i_85_)
		anInt11950 = i_85_ * -1128091487;
	}
	if (-1556313745 * anInt11933 != 0 && 0 != i_86_) {
	    int i_117_ = 16384 - i_86_;
	    if (-1556313745 * anInt11933 > 8192) {
		if (anInt11933 * -1556313745 < i_117_)
		    anInt11933 = i_117_ * -723978865;
	    } else if (anInt11933 * -1556313745 > i_86_)
		anInt11933 = i_86_ * -723978865;
	}
	anInt11934 = (i_96_ + i_111_) * 1564215623;
	if (i_106_ + i_101_ < 956485751 * anInt11934)
	    anInt11934 = (i_106_ + i_101_) * 1564215623;
	anInt11934 = ((956485751 * anInt11934 >> 1)
		      - (int) class438.aFloat4863) * 1564215623;
    }
    
    public final void method18540(int i, int i_118_, int i_119_, int i_120_,
				  boolean bool, int i_121_, byte i_122_) {
	Class529 class529 = aClass529Array11949[i_121_];
	int i_123_ = class529.anInt7123 * -1183861629;
	if (i != -1 && i_123_ != -1) {
	    if (i_123_ == i) {
		Class684 class684
		    = ((Class684)
		       Class55.aClass44_Sub4_447.method91(i, -2007939126));
		if (class684.aBool8691
		    && -1 != class684.anInt8688 * -811043807) {
		    Class205 class205
			= ((Class205)
			   (Class200_Sub12.aClass44_Sub1_9934.method91
			    (class684.anInt8688 * -811043807, -983863385)));
		    int i_124_ = 629077835 * class205.anInt2223;
		    if (0 == i_124_)
			return;
		    if (2 == i_124_) {
			class529.aClass711_7121.method14366((byte) 1);
			return;
		    }
		}
	    } else {
		Class684 class684
		    = ((Class684)
		       Class55.aClass44_Sub4_447.method91(i, 1204187113));
		Class684 class684_125_
		    = ((Class684)
		       Class55.aClass44_Sub4_447.method91(i_123_, -845673213));
		if (-1 != -811043807 * class684.anInt8688
		    && -811043807 * class684_125_.anInt8688 != -1) {
		    Class205 class205
			= ((Class205)
			   (Class200_Sub12.aClass44_Sub1_9934.method91
			    (class684.anInt8688 * -811043807, -146912549)));
		    Class205 class205_126_
			= ((Class205)
			   (Class200_Sub12.aClass44_Sub1_9934.method91
			    (-811043807 * class684_125_.anInt8688,
			     -1574304587)));
		    if (995056269 * class205.anInt2217
			< 995056269 * class205_126_.anInt2217)
			return;
		}
	    }
	}
	int i_127_ = 0;
	if (i != -1
	    && !(((Class684) Class55.aClass44_Sub4_447.method91(i, -825079471))
		 .aBool8691))
	    i_127_ = 2;
	if (-1 != i && bool)
	    i_127_ = 1;
	class529.anInt7123 = 496843307 * i;
	class529.anInt7124 = 1026088823 * i_120_;
	class529.anInt7122 = 1188140141 * (i_118_ >> 16);
	class529.anInt7120 = i_119_ * -1172805917;
	class529.aClass711_7121.method14334((i != -1
					     ? (((Class684)
						 Class55.aClass44_Sub4_447
						     .method91(i, -1273645735))
						.anInt8688) * -811043807
					     : -1),
					    i_118_ & 0xffff, i_127_, false,
					    (byte) -106);
    }
    
    boolean method16850(int i) {
	return aBool11961;
    }
    
    public abstract Class526 method18541();
    
    public Class534_Sub36 method18542(int i) {
	Class438 class438 = method10807().aClass438_4885;
	Class597 class597 = client.aClass512_11100.method8416((byte) 122);
	int i_128_
	    = (int) class438.aFloat4864 + 1852947968 * class597.anInt7859;
	int i_129_ = (int) class438.aFloat4863;
	int i_130_
	    = (int) class438.aFloat4865 + -139729408 * class597.anInt7861;
	return new Class534_Sub36(aByte10854, i_128_, i_129_, i_130_);
    }
    
    public abstract boolean method18543(int i);
    
    public abstract Class526 method18544(int i);
    
    public int method18545(byte i) {
	return anInt11923 * -930919489;
    }
    
    boolean method18546(int i) {
	if (anInt11990 * -935424637 == -1)
	    return false;
	return method18561(-935424637 * anInt11990, 1055370457);
    }
    
    public int method16876(int i) {
	if (-32768 == anInt11927 * -2101930975)
	    return 0;
	return anInt11927 * -2101930975;
    }
    
    public int method16867() {
	if (-32768 == anInt11927 * -2101930975)
	    return 0;
	return anInt11927 * -2101930975;
    }
    
    public void method18547(int i, int i_131_, int i_132_, int i_133_,
			    int i_134_, int i_135_, int i_136_, int i_137_) {
	if (!method18546(-1349202872)) {
	    if (-1 != i)
		client.aBoolArray11180[i] = true;
	    else {
		for (int i_138_ = 0; i_138_ < 108; i_138_++)
		    client.aBoolArray11180[i_138_] = true;
	    }
	} else
	    Class690_Sub37.method17194(aClass243_11988.method4473((byte) 0),
				       -1, i_131_, i_132_, i_133_, i_134_,
				       i_135_, i_136_, i, false, -1961737723);
    }
    
    public void method18548(int i, int i_139_, boolean bool, int i_140_) {
	if (method18546(78868033))
	    Class75_Sub1.method17366(aClass243_11988.aClass247Array2412, -1, i,
				     i_139_, bool, -1124858935);
    }
    
    public abstract int method18549(byte i);
    
    boolean method18550(int i) {
	if (i == anInt11932 * -1597934185)
	    return true;
	aClass243_11988 = Class498.method8262(i, null, null, true, (byte) 34);
	if (aClass243_11988 == null)
	    return false;
	anInt11932 = 1079007783 * i;
	Class658.method10911(aClass243_11988.aClass247Array2412, (byte) 115);
	return true;
    }
    
    boolean method16864() {
	return false;
    }
    
    abstract int method18551();
    
    void method18552() {
	if (aClass629_11983 != null)
	    aClass629_11983.method10389();
    }
    
    void method18553() {
	if (aClass629_11983 != null)
	    aClass629_11983.method10389();
    }
    
    void method18554() {
	if (aClass629_11983 != null)
	    aClass629_11983.method10389();
    }
    
    boolean method16895() {
	return false;
    }
    
    public void finalize() {
	if (aClass629_11983 != null)
	    aClass629_11983.method10389();
    }
    
    void method18555(int i, int i_141_, int i_142_, int i_143_, int i_144_) {
	Class438 class438 = method10807().aClass438_4885;
	int i_145_ = aShort11900 + aShort11896 >> 1;
	int i_146_ = aShort11898 + aShort11901 >> 1;
	int i_147_ = Class427.anIntArray4806[i];
	int i_148_ = Class427.anIntArray4805[i];
	int i_149_ = -i_141_ / 2;
	int i_150_ = -i_142_ / 2;
	int i_151_ = i_148_ * i_149_ + i_150_ * i_147_ >> 14;
	int i_152_ = i_148_ * i_150_ - i_149_ * i_147_ >> 14;
	int i_153_
	    = Class219.method4142(i_151_ + (int) class438.aFloat4864,
				  i_152_ + (int) class438.aFloat4865, i_145_,
				  i_146_, aByte10854, (byte) 78);
	int i_154_ = i_141_ / 2;
	int i_155_ = -i_142_ / 2;
	int i_156_ = i_147_ * i_155_ + i_148_ * i_154_ >> 14;
	int i_157_ = i_148_ * i_155_ - i_147_ * i_154_ >> 14;
	int i_158_
	    = Class219.method4142((int) class438.aFloat4864 + i_156_,
				  i_157_ + (int) class438.aFloat4865, i_145_,
				  i_146_, aByte10854, (byte) 104);
	int i_159_ = -i_141_ / 2;
	int i_160_ = i_142_ / 2;
	int i_161_ = i_160_ * i_147_ + i_148_ * i_159_ >> 14;
	int i_162_ = i_160_ * i_148_ - i_147_ * i_159_ >> 14;
	int i_163_
	    = Class219.method4142(i_161_ + (int) class438.aFloat4864,
				  (int) class438.aFloat4865 + i_162_, i_145_,
				  i_146_, aByte10854, (byte) 73);
	int i_164_ = i_141_ / 2;
	int i_165_ = i_142_ / 2;
	int i_166_ = i_165_ * i_147_ + i_164_ * i_148_ >> 14;
	int i_167_ = i_148_ * i_165_ - i_147_ * i_164_ >> 14;
	int i_168_
	    = Class219.method4142(i_166_ + (int) class438.aFloat4864,
				  (int) class438.aFloat4865 + i_167_, i_145_,
				  i_146_, aByte10854, (byte) 50);
	int i_169_ = i_153_ < i_158_ ? i_153_ : i_158_;
	int i_170_ = i_163_ < i_168_ ? i_163_ : i_168_;
	int i_171_ = i_158_ < i_168_ ? i_158_ : i_168_;
	int i_172_ = i_153_ < i_163_ ? i_153_ : i_163_;
	anInt11950
	    = ((int) (Math.atan2((double) (i_169_ - i_170_), (double) i_142_)
		      * 2607.5945876176133)
	       & 0x3fff) * -1128091487;
	anInt11933
	    = ((int) (Math.atan2((double) (i_172_ - i_171_), (double) i_141_)
		      * 2607.5945876176133)
	       & 0x3fff) * -723978865;
	if (1177236321 * anInt11950 != 0 && i_143_ != 0) {
	    int i_173_ = 16384 - i_143_;
	    if (1177236321 * anInt11950 > 8192) {
		if (anInt11950 * 1177236321 < i_173_)
		    anInt11950 = -1128091487 * i_173_;
	    } else if (anInt11950 * 1177236321 > i_143_)
		anInt11950 = i_143_ * -1128091487;
	}
	if (-1556313745 * anInt11933 != 0 && 0 != i_144_) {
	    int i_174_ = 16384 - i_144_;
	    if (-1556313745 * anInt11933 > 8192) {
		if (anInt11933 * -1556313745 < i_174_)
		    anInt11933 = i_174_ * -723978865;
	    } else if (anInt11933 * -1556313745 > i_144_)
		anInt11933 = i_144_ * -723978865;
	}
	anInt11934 = (i_153_ + i_168_) * 1564215623;
	if (i_163_ + i_158_ < 956485751 * anInt11934)
	    anInt11934 = (i_163_ + i_158_) * 1564215623;
	anInt11934 = ((956485751 * anInt11934 >> 1)
		      - (int) class438.aFloat4863) * 1564215623;
    }
    
    boolean method16869() {
	return aBool11961;
    }
    
    public void method18556(String string, int i, int i_175_, int i_176_) {
	if (null == aClass526_11935)
	    aClass526_11935 = new Class526();
	aClass526_11935.aString7094 = string;
	aClass526_11935.anInt7091 = i * -1755813671;
	aClass526_11935.anInt7092 = 1553816029 * i_175_;
	aClass526_11935.anInt7090
	    = (aClass526_11935.anInt7093 = i_176_ * 697182109) * -124534729;
    }
    
    public int method16866() {
	if (-32768 == anInt11927 * -2101930975)
	    return 0;
	return anInt11927 * -2101930975;
    }
    
    public void method18557(int[] is, int[] is_177_, byte i) {
	if (anIntArray11985 == null && is != null)
	    anIntArray11985
		= new int[Class69.aClass630_728.anIntArray8196.length];
	else if (null == is) {
	    anIntArray11985 = null;
	    return;
	}
	for (int i_178_ = 0; i_178_ < anIntArray11985.length; i_178_++)
	    anIntArray11985[i_178_] = -1;
	for (int i_179_ = 0; i_179_ < is.length; i_179_++) {
	    int i_180_ = is_177_[i_179_];
	    int i_181_ = 0;
	    while (i_181_ < anIntArray11985.length) {
		if ((i_180_ & 0x1) != 0)
		    anIntArray11985[i_181_] = is[i_179_];
		i_181_++;
		i_180_ >>= 1;
	    }
	}
    }
    
    public void method18558(int[] is, int[] is_182_) {
	if (anIntArray11985 == null && is != null)
	    anIntArray11985
		= new int[Class69.aClass630_728.anIntArray8196.length];
	else if (null == is) {
	    anIntArray11985 = null;
	    return;
	}
	for (int i = 0; i < anIntArray11985.length; i++)
	    anIntArray11985[i] = -1;
	for (int i = 0; i < is.length; i++) {
	    int i_183_ = is_182_[i];
	    int i_184_ = 0;
	    while (i_184_ < anIntArray11985.length) {
		if ((i_183_ & 0x1) != 0)
		    anIntArray11985[i_184_] = is[i];
		i_184_++;
		i_183_ >>= 1;
	    }
	}
    }
    
    public void method18559(int i, int i_185_, int i_186_, int i_187_,
			    int i_188_, int i_189_, int i_190_) {
	if (!method18546(207411340)) {
	    if (-1 != i)
		client.aBoolArray11180[i] = true;
	    else {
		for (int i_191_ = 0; i_191_ < 108; i_191_++)
		    client.aBoolArray11180[i_191_] = true;
	    }
	} else
	    Class690_Sub37.method17194(aClass243_11988.method4473((byte) 0),
				       -1, i_185_, i_186_, i_187_, i_188_,
				       i_189_, i_190_, i, false, 1939170199);
    }
    
    public abstract int method18560();
    
    boolean method18561(int i, int i_192_) {
	if (i == anInt11932 * -1597934185)
	    return true;
	aClass243_11988 = Class498.method8262(i, null, null, true, (byte) 34);
	if (aClass243_11988 == null)
	    return false;
	anInt11932 = 1079007783 * i;
	Class658.method10911(aClass243_11988.aClass247Array2412, (byte) 95);
	return true;
    }
    
    public final void method18562(int i, int i_193_, int i_194_, int i_195_,
				  boolean bool, int i_196_) {
	Class529 class529 = aClass529Array11949[i_196_];
	int i_197_ = class529.anInt7123 * -1183861629;
	if (i != -1 && i_197_ != -1) {
	    if (i_197_ == i) {
		Class684 class684
		    = ((Class684)
		       Class55.aClass44_Sub4_447.method91(i, -532796882));
		if (class684.aBool8691
		    && -1 != class684.anInt8688 * -811043807) {
		    Class205 class205
			= ((Class205)
			   (Class200_Sub12.aClass44_Sub1_9934.method91
			    (class684.anInt8688 * -811043807, -71726293)));
		    int i_198_ = 629077835 * class205.anInt2223;
		    if (0 == i_198_)
			return;
		    if (2 == i_198_) {
			class529.aClass711_7121.method14366((byte) 1);
			return;
		    }
		}
	    } else {
		Class684 class684
		    = ((Class684)
		       Class55.aClass44_Sub4_447.method91(i, -1353215808));
		Class684 class684_199_
		    = ((Class684)
		       Class55.aClass44_Sub4_447.method91(i_197_,
							  -1558686095));
		if (-1 != -811043807 * class684.anInt8688
		    && -811043807 * class684_199_.anInt8688 != -1) {
		    Class205 class205
			= ((Class205)
			   (Class200_Sub12.aClass44_Sub1_9934.method91
			    (class684.anInt8688 * -811043807, -1899462958)));
		    Class205 class205_200_
			= ((Class205)
			   (Class200_Sub12.aClass44_Sub1_9934.method91
			    (-811043807 * class684_199_.anInt8688,
			     -1310957578)));
		    if (995056269 * class205.anInt2217
			< 995056269 * class205_200_.anInt2217)
			return;
		}
	    }
	}
	int i_201_ = 0;
	if (i != -1
	    && !((Class684)
		 Class55.aClass44_Sub4_447.method91(i, -1026824812)).aBool8691)
	    i_201_ = 2;
	if (-1 != i && bool)
	    i_201_ = 1;
	class529.anInt7123 = 496843307 * i;
	class529.anInt7124 = 1026088823 * i_195_;
	class529.anInt7122 = 1188140141 * (i_193_ >> 16);
	class529.anInt7120 = i_194_ * -1172805917;
	class529.aClass711_7121.method14334((i != -1
					     ? (((Class684)
						 Class55.aClass44_Sub4_447
						     .method91(i, 1174389407))
						.anInt8688) * -811043807
					     : -1),
					    i_193_ & 0xffff, i_201_, false,
					    (byte) -86);
    }
    
    public void method18483() {
	int i = 240 + (-930919489 * anInt11923 - 1 << 8);
	Class438 class438 = method10807().aClass438_4885;
	aShort11900 = (short) ((int) class438.aFloat4864 - i >> 9);
	aShort11901 = (short) ((int) class438.aFloat4865 - i >> 9);
	aShort11896 = (short) (i + (int) class438.aFloat4864 >> 9);
	aShort11898 = (short) (i + (int) class438.aFloat4865 >> 9);
    }
    
    public void method18563(String string, int i, int i_202_, int i_203_,
			    short i_204_) {
	if (null == aClass526_11935)
	    aClass526_11935 = new Class526();
	aClass526_11935.aString7094 = string;
	aClass526_11935.anInt7091 = i * -1755813671;
	aClass526_11935.anInt7092 = 1553816029 * i_202_;
	aClass526_11935.anInt7090
	    = (aClass526_11935.anInt7093 = i_203_ * 697182109) * -124534729;
    }
    
    public final void method18564() {
	anInt11980 = 0;
	anInt11937 = 0;
    }
    
    public final void method18565(int i, int i_205_, int i_206_, int i_207_,
				  int i_208_, int i_209_, int i_210_) {
	boolean bool = true;
	boolean bool_211_ = true;
	for (int i_212_ = 0;
	     i_212_ < 521612365 * Class620.aClass632_8113.anInt8257;
	     i_212_++) {
	    if (anIntArray11925[i_212_] > i_208_)
		bool = false;
	    else
		bool_211_ = false;
	}
	int i_213_ = -1;
	int i_214_ = -1;
	int i_215_ = 0;
	if (i >= 0) {
	    Class578 class578
		= ((Class578)
		   Class632.aClass44_Sub2_8270.method91(i, -518625000));
	    i_214_ = 692535749 * class578.anInt7733;
	    i_215_ = class578.anInt7724 * 1049226697;
	}
	if (bool_211_) {
	    if (-1 == i_214_)
		return;
	    i_213_ = 0;
	    int i_216_ = 0;
	    if (i_214_ == 0)
		i_216_ = anIntArray11925[0];
	    else if (i_214_ == 1)
		i_216_ = anIntArray11939[0];
	    for (int i_217_ = 1;
		 i_217_ < Class620.aClass632_8113.anInt8257 * 521612365;
		 i_217_++) {
		if (i_214_ == 0) {
		    if (anIntArray11925[i_217_] < i_216_) {
			i_213_ = i_217_;
			i_216_ = anIntArray11925[i_217_];
		    }
		} else if (1 == i_214_ && anIntArray11939[i_217_] < i_216_) {
		    i_213_ = i_217_;
		    i_216_ = anIntArray11939[i_217_];
		}
	    }
	    if (i_214_ == 1 && i_216_ >= i_205_)
		return;
	} else {
	    if (bool)
		aByte11924 = (byte) 0;
	    for (int i_218_ = 0;
		 i_218_ < Class620.aClass632_8113.anInt8257 * 521612365;
		 i_218_++) {
		int i_219_ = aByte11924;
		aByte11924 = (byte) ((1 + aByte11924)
				     % (521612365
					* Class620.aClass632_8113.anInt8257));
		if (anIntArray11925[i_219_] <= i_208_) {
		    i_213_ = i_219_;
		    break;
		}
	    }
	}
	if (i_213_ >= 0) {
	    anIntArray11938[i_213_] = i;
	    anIntArray11939[i_213_] = i_205_;
	    anIntArray11952[i_213_] = i_206_;
	    anIntArray11942[i_213_] = i_207_;
	    anIntArray11925[i_213_] = i_208_ + i_215_ + i_209_;
	}
    }
    
    public void method18487(int i) {
	int i_220_ = 240 + (-930919489 * anInt11923 - 1 << 8);
	Class438 class438 = method10807().aClass438_4885;
	aShort11900 = (short) ((int) class438.aFloat4864 - i_220_ >> 9);
	aShort11901 = (short) ((int) class438.aFloat4865 - i_220_ >> 9);
	aShort11896 = (short) (i_220_ + (int) class438.aFloat4864 >> 9);
	aShort11898 = (short) (i_220_ + (int) class438.aFloat4865 >> 9);
    }
    
    public final void method18566(int i, int i_221_, int i_222_, int i_223_,
				  int i_224_, int i_225_) {
	Class579 class579
	    = (Class579) Class470.aClass44_Sub17_5153.method91(i, -1421943220);
	Class536_Sub5 class536_sub5 = null;
	Class536_Sub5 class536_sub5_226_ = null;
	int i_227_ = 1151479045 * class579.anInt7753;
	int i_228_ = 0;
	for (Class536_Sub5 class536_sub5_229_
		 = (Class536_Sub5) aClass709_11941.method14301(764908544);
	     class536_sub5_229_ != null;
	     class536_sub5_229_
		 = (Class536_Sub5) aClass709_11941.method14282(1143826453)) {
	    i_228_++;
	    if (class536_sub5_229_.aClass579_10368 == class579) {
		class536_sub5_229_.method15996(i_221_ + i_223_, i_224_, i_225_,
					       i_222_, (short) 255);
		return;
	    }
	    if (class536_sub5_229_.aClass579_10368.anInt7747 * 1475151865
		<= 1475151865 * class579.anInt7747)
		class536_sub5 = class536_sub5_229_;
	    if (1151479045 * class536_sub5_229_.aClass579_10368.anInt7753
		> i_227_) {
		class536_sub5_226_ = class536_sub5_229_;
		i_227_ = (class536_sub5_229_.aClass579_10368.anInt7753
			  * 1151479045);
	    }
	}
	if (null != class536_sub5_226_
	    || i_228_ < Class620.aClass632_8113.anInt8256 * 1210989897) {
	    Class536_Sub5 class536_sub5_230_ = new Class536_Sub5(class579);
	    if (null == class536_sub5)
		aClass709_11941.method14285(class536_sub5_230_, (byte) 100);
	    else
		Class485.method7963(class536_sub5_230_, class536_sub5,
				    (byte) -45);
	    class536_sub5_230_.method15996(i_221_ + i_223_, i_224_, i_225_,
					   i_222_, (short) 255);
	    if (i_228_ >= 1210989897 * Class620.aClass632_8113.anInt8256)
		class536_sub5_226_.method8900(1737428515);
	}
    }
    
    public void method18567(int i, int i_231_, int i_232_, int i_233_,
			    int i_234_, int i_235_, int i_236_, int i_237_,
			    byte i_238_) {
	if (method18546(890852887))
	    client.method17391(aClass243_11988,
			       aClass243_11988.method4473((byte) 0), -1, i,
			       i_231_, i_232_, i_233_, i_234_, i_235_, i_236_,
			       i_237_);
    }
    
    public int method16875() {
	Class570 class570 = method18531((byte) -6);
	int i;
	if (-1 != class570.anInt7662 * -1329461445)
	    i = class570.anInt7662 * -1329461445;
	else if (-399165137 * anInt11981 == -32768)
	    i = 200;
	else
	    i = -(-399165137 * anInt11981);
	Class444 class444 = method10807();
	int i_239_ = (int) class444.aClass438_4885.aFloat4864 >> 9;
	int i_240_ = (int) class444.aClass438_4885.aFloat4865 >> 9;
	if (aClass556_10855 != null && i_239_ >= 1 && i_240_ >= 1
	    && i_239_ <= client.aClass512_11100.method8417(2056821835) - 1
	    && i_240_ <= client.aClass512_11100.method8418(-1533611049) - 1) {
	    Class568 class568 = (aClass556_10855.aClass568ArrayArrayArray7431
				 [aByte10854][i_239_][i_240_]);
	    if (class568 != null && class568.aClass654_Sub1_Sub2_7607 != null)
		return class568.aClass654_Sub1_Sub2_7607.aShort11864 + i;
	}
	return i;
    }
    
    public void method18568(int i) {
	anInt11923 = i * 659989567;
    }
    
    public void method18569(int i) {
	anInt11923 = i * 659989567;
    }
    
    public boolean method18570(int i, int i_241_) {
	if (anIntArray11920 == null) {
	    if (-1 == i_241_)
		return true;
	    anIntArray11920
		= new int[Class69.aClass630_728.anIntArray8196.length];
	    for (int i_242_ = 0;
		 i_242_ < Class69.aClass630_728.anIntArray8196.length;
		 i_242_++)
		anIntArray11920[i_242_] = -1;
	}
	Class570 class570 = method18531((byte) -112);
	int i_243_ = 256;
	if (null != class570.anIntArray7651 && class570.anIntArray7651[i] > 0)
	    i_243_ = class570.anIntArray7651[i];
	if (i_241_ == -1) {
	    if (anIntArray11920[i] == -1)
		return true;
	    int i_244_ = aClass57_11973.method1231(572623290);
	    int i_245_ = anIntArray11920[i];
	    int i_246_ = i_244_ - i_245_;
	    if (i_246_ >= -i_243_ && i_246_ <= i_243_) {
		anIntArray11920[i] = -1;
		for (int i_247_ = 0;
		     i_247_ < Class69.aClass630_728.anIntArray8196.length;
		     i_247_++) {
		    if (-1 != anIntArray11920[i_247_])
			return true;
		}
		anIntArray11920 = null;
		return true;
	    }
	    if (i_246_ > 0 && i_246_ <= 8192 || i_246_ <= -8192)
		anIntArray11920[i] = i_243_ + i_245_ & 0x3fff;
	    else
		anIntArray11920[i] = i_245_ - i_243_ & 0x3fff;
	    return false;
	}
	if (anIntArray11920[i] == -1)
	    anIntArray11920[i] = aClass57_11973.method1231(1116154111);
	int i_248_ = anIntArray11920[i];
	int i_249_ = i_241_ - i_248_;
	if (i_249_ >= -i_243_ && i_249_ <= i_243_) {
	    anIntArray11920[i] = i_241_;
	    return true;
	}
	if (i_249_ > 0 && i_249_ <= 8192 || i_249_ <= -8192)
	    anIntArray11920[i] = i_243_ + i_248_ & 0x3fff;
	else
	    anIntArray11920[i] = i_248_ - i_243_ & 0x3fff;
	return false;
    }
    
    public void method18484() {
	int i = 240 + (-930919489 * anInt11923 - 1 << 8);
	Class438 class438 = method10807().aClass438_4885;
	aShort11900 = (short) ((int) class438.aFloat4864 - i >> 9);
	aShort11901 = (short) ((int) class438.aFloat4865 - i >> 9);
	aShort11896 = (short) (i + (int) class438.aFloat4864 >> 9);
	aShort11898 = (short) (i + (int) class438.aFloat4865 >> 9);
    }
    
    public void method18485() {
	int i = 240 + (-930919489 * anInt11923 - 1 << 8);
	Class438 class438 = method10807().aClass438_4885;
	aShort11900 = (short) ((int) class438.aFloat4864 - i >> 9);
	aShort11901 = (short) ((int) class438.aFloat4865 - i >> 9);
	aShort11896 = (short) (i + (int) class438.aFloat4864 >> 9);
	aShort11898 = (short) (i + (int) class438.aFloat4865 >> 9);
    }
    
    public void method18482() {
	int i = 240 + (-930919489 * anInt11923 - 1 << 8);
	Class438 class438 = method10807().aClass438_4885;
	aShort11900 = (short) ((int) class438.aFloat4864 - i >> 9);
	aShort11901 = (short) ((int) class438.aFloat4865 - i >> 9);
	aShort11896 = (short) (i + (int) class438.aFloat4864 >> 9);
	aShort11898 = (short) (i + (int) class438.aFloat4865 >> 9);
    }
    
    public void method18486() {
	int i = 240 + (-930919489 * anInt11923 - 1 << 8);
	Class438 class438 = method10807().aClass438_4885;
	aShort11900 = (short) ((int) class438.aFloat4864 - i >> 9);
	aShort11901 = (short) ((int) class438.aFloat4865 - i >> 9);
	aShort11896 = (short) (i + (int) class438.aFloat4864 >> 9);
	aShort11898 = (short) (i + (int) class438.aFloat4865 >> 9);
    }
    
    void method18571(Class183 class183) {
	int i = aClass57_11974.anInt457 * 949937137;
	int i_250_ = 949937137 * aClass57_11975.anInt457;
	if (i != 0 || 0 != i_250_) {
	    int i_251_ = class183.method3045() / 2;
	    class183.method3098(0, -i_251_, 0);
	    class183.method3018(i & 0x3fff);
	    class183.method3017(i_250_ & 0x3fff);
	    class183.method3098(0, i_251_, 0);
	}
    }
    
    public final void method18572() {
	anInt11980 = 0;
	anInt11937 = 0;
    }
    
    public abstract int method18573();
    
    public void method18574(int i, int i_252_, boolean bool) {
	if (method18546(1962562514))
	    Class75_Sub1.method17366(aClass243_11988.aClass247Array2412, -1, i,
				     i_252_, bool, -483023553);
    }
    
    public Class570 method18575() {
	int i = method18529((byte) 55);
	if (i == -1)
	    return Class44_Sub14.aClass570_11009;
	return ((Class570)
		Class200_Sub23.aClass44_Sub14_10041.method91(i, -1215682280));
    }
    
    public Class570 method18576() {
	int i = method18529((byte) 105);
	if (i == -1)
	    return Class44_Sub14.aClass570_11009;
	return ((Class570)
		Class200_Sub23.aClass44_Sub14_10041.method91(i, -1601618075));
    }
    
    public Class570 method18577() {
	int i = method18529((byte) 97);
	if (i == -1)
	    return Class44_Sub14.aClass570_11009;
	return ((Class570)
		Class200_Sub23.aClass44_Sub14_10041.method91(i, -423846200));
    }
    
    public Class570 method18578() {
	int i = method18529((byte) 113);
	if (i == -1)
	    return Class44_Sub14.aClass570_11009;
	return ((Class570)
		Class200_Sub23.aClass44_Sub14_10041.method91(i, 789773720));
    }
    
    abstract int method18579();
    
    void method18580(Class185 class185, Class183[] class183s,
		     Class446 class446, boolean bool) {
	if (!bool) {
	    int i = 0;
	    int i_253_ = 0;
	    int i_254_ = 0;
	    int i_255_ = 0;
	    int i_256_ = -1;
	    int i_257_ = -1;
	    Class172[][] class172s = new Class172[class183s.length][];
	    Class158[][] class158s = new Class158[class183s.length][];
	    for (int i_258_ = 0; i_258_ < class183s.length; i_258_++) {
		if (null != class183s[i_258_]) {
		    class183s[i_258_].method3073(class446);
		    class172s[i_258_] = class183s[i_258_].method3168();
		    class158s[i_258_] = class183s[i_258_].method3065();
		    if (class172s[i_258_] != null) {
			i_256_ = i_258_;
			i_253_++;
			i += class172s[i_258_].length;
		    }
		    if (class158s[i_258_] != null) {
			i_257_ = i_258_;
			i_255_++;
			i_254_ += class158s[i_258_].length;
		    }
		}
	    }
	    if ((aClass629_11983 == null || aClass629_11983.aBool8189)
		&& (i_253_ > 0 || i_255_ > 0))
		aClass629_11983
		    = Class629.method10422(client.anInt11101, true);
	    if (null != aClass629_11983) {
		Object object = null;
		Class172[] class172s_259_;
		if (i_253_ == 1)
		    class172s_259_ = class172s[i_256_];
		else {
		    class172s_259_ = new Class172[i];
		    int i_260_ = 0;
		    for (int i_261_ = 0; i_261_ < class183s.length; i_261_++) {
			if (class172s[i_261_] != null) {
			    System.arraycopy(class172s[i_261_], 0,
					     class172s_259_, i_260_,
					     class172s[i_261_].length);
			    i_260_ += class172s[i_261_].length;
			}
		    }
		}
		Object object_262_ = null;
		Class158[] class158s_263_;
		if (1 == i_255_)
		    class158s_263_ = class158s[i_257_];
		else {
		    class158s_263_ = new Class158[i_254_];
		    int i_264_ = 0;
		    for (int i_265_ = 0; i_265_ < class183s.length; i_265_++) {
			if (null != class158s[i_265_]) {
			    System.arraycopy(class158s[i_265_], 0,
					     class158s_263_, i_264_,
					     class158s[i_265_].length);
			    i_264_ += class158s[i_265_].length;
			}
		    }
		}
		aClass629_11983.method10393(class185, (long) client.anInt11101,
					    class172s_259_, class158s_263_,
					    false);
		aBool11984 = true;
	    }
	} else if (null != aClass629_11983)
	    aClass629_11983.method10392((long) client.anInt11101);
	if (aClass629_11983 != null)
	    aClass629_11983.method10396(aByte10854, aShort11900, aShort11896,
					aShort11901, aShort11898);
    }
    
    abstract int method18581();
    
    public final void method18582() {
	anInt11980 = 0;
	anInt11937 = 0;
    }
    
    public void method18583() {
	if (aClass526_11935 != null && null != aClass526_11935.aString7094) {
	    aClass526_11935.anInt7090 -= 1650934459;
	    if (0 == -474631565 * aClass526_11935.anInt7090)
		aClass526_11935.aString7094 = null;
	}
    }
    
    public void method18584() {
	if (aClass526_11935 != null && null != aClass526_11935.aString7094) {
	    aClass526_11935.anInt7090 -= 1650934459;
	    if (0 == -474631565 * aClass526_11935.anInt7090)
		aClass526_11935.aString7094 = null;
	}
    }
    
    public abstract boolean method18585();
    
    public abstract boolean method18586();
    
    public abstract boolean method18587();
    
    public abstract boolean method18588();
    
    public void method18589(int i) {
	if (aClass526_11935 != null && null != aClass526_11935.aString7094) {
	    aClass526_11935.anInt7090 -= 1650934459;
	    if (0 == -474631565 * aClass526_11935.anInt7090)
		aClass526_11935.aString7094 = null;
	}
    }
    
    boolean method16879() {
	return aBool11961;
    }
    
    public abstract Class526 method18590();
    
    public void method18591(String string, int i, int i_266_, int i_267_) {
	if (null == aClass526_11935)
	    aClass526_11935 = new Class526();
	aClass526_11935.aString7094 = string;
	aClass526_11935.anInt7091 = i * -1755813671;
	aClass526_11935.anInt7092 = 1553816029 * i_266_;
	aClass526_11935.anInt7090
	    = (aClass526_11935.anInt7093 = i_267_ * 697182109) * -124534729;
    }
    
    public void method18592(String string, int i, int i_268_, int i_269_) {
	if (null == aClass526_11935)
	    aClass526_11935 = new Class526();
	aClass526_11935.aString7094 = string;
	aClass526_11935.anInt7091 = i * -1755813671;
	aClass526_11935.anInt7092 = 1553816029 * i_268_;
	aClass526_11935.anInt7090
	    = (aClass526_11935.anInt7093 = i_269_ * 697182109) * -124534729;
    }
    
    boolean method18593(int i) {
	if (i == anInt11932 * -1597934185)
	    return true;
	aClass243_11988 = Class498.method8262(i, null, null, true, (byte) 34);
	if (aClass243_11988 == null)
	    return false;
	anInt11932 = 1079007783 * i;
	Class658.method10911(aClass243_11988.aClass247Array2412, (byte) -8);
	return true;
    }
    
    public void method18594(String string, int i, int i_270_, int i_271_) {
	if (null == aClass526_11935)
	    aClass526_11935 = new Class526();
	aClass526_11935.aString7094 = string;
	aClass526_11935.anInt7091 = i * -1755813671;
	aClass526_11935.anInt7092 = 1553816029 * i_270_;
	aClass526_11935.anInt7090
	    = (aClass526_11935.anInt7093 = i_271_ * 697182109) * -124534729;
    }
    
    boolean method18595() {
	if (anInt11990 * -935424637 == -1)
	    return false;
	return method18561(-935424637 * anInt11990, 1055370457);
    }
    
    public final void method18596() {
	anInt11980 = 0;
	anInt11937 = 0;
    }
    
    public final void method18597(int i, int i_272_, int i_273_, int i_274_,
				  boolean bool, int i_275_) {
	Class529 class529 = aClass529Array11949[i_275_];
	int i_276_ = class529.anInt7123 * -1183861629;
	if (i != -1 && i_276_ != -1) {
	    if (i_276_ == i) {
		Class684 class684
		    = ((Class684)
		       Class55.aClass44_Sub4_447.method91(i, -1875192291));
		if (class684.aBool8691
		    && -1 != class684.anInt8688 * -811043807) {
		    Class205 class205
			= ((Class205)
			   (Class200_Sub12.aClass44_Sub1_9934.method91
			    (class684.anInt8688 * -811043807, 911747984)));
		    int i_277_ = 629077835 * class205.anInt2223;
		    if (0 == i_277_)
			return;
		    if (2 == i_277_) {
			class529.aClass711_7121.method14366((byte) 1);
			return;
		    }
		}
	    } else {
		Class684 class684
		    = ((Class684)
		       Class55.aClass44_Sub4_447.method91(i, 967857485));
		Class684 class684_278_
		    = ((Class684)
		       Class55.aClass44_Sub4_447.method91(i_276_, -557364782));
		if (-1 != -811043807 * class684.anInt8688
		    && -811043807 * class684_278_.anInt8688 != -1) {
		    Class205 class205
			= ((Class205)
			   (Class200_Sub12.aClass44_Sub1_9934.method91
			    (class684.anInt8688 * -811043807, -359808899)));
		    Class205 class205_279_
			= ((Class205)
			   (Class200_Sub12.aClass44_Sub1_9934.method91
			    (-811043807 * class684_278_.anInt8688,
			     -1483496683)));
		    if (995056269 * class205.anInt2217
			< 995056269 * class205_279_.anInt2217)
			return;
		}
	    }
	}
	int i_280_ = 0;
	if (i != -1
	    && !(((Class684) Class55.aClass44_Sub4_447.method91(i, -545211044))
		 .aBool8691))
	    i_280_ = 2;
	if (-1 != i && bool)
	    i_280_ = 1;
	class529.anInt7123 = 496843307 * i;
	class529.anInt7124 = 1026088823 * i_274_;
	class529.anInt7122 = 1188140141 * (i_272_ >> 16);
	class529.anInt7120 = i_273_ * -1172805917;
	class529.aClass711_7121.method14334((i != -1
					     ? (((Class684)
						 Class55.aClass44_Sub4_447
						     .method91(i, -1330043180))
						.anInt8688) * -811043807
					     : -1),
					    i_272_ & 0xffff, i_280_, false,
					    (byte) -117);
    }
    
    public abstract Class526 method18598();
    
    public int method16854() {
	Class570 class570 = method18531((byte) -9);
	int i;
	if (-1 != class570.anInt7662 * -1329461445)
	    i = class570.anInt7662 * -1329461445;
	else if (-399165137 * anInt11981 == -32768)
	    i = 200;
	else
	    i = -(-399165137 * anInt11981);
	Class444 class444 = method10807();
	int i_281_ = (int) class444.aClass438_4885.aFloat4864 >> 9;
	int i_282_ = (int) class444.aClass438_4885.aFloat4865 >> 9;
	if (aClass556_10855 != null && i_281_ >= 1 && i_282_ >= 1
	    && i_281_ <= client.aClass512_11100.method8417(-53952006) - 1
	    && i_282_ <= client.aClass512_11100.method8418(-1533611049) - 1) {
	    Class568 class568 = (aClass556_10855.aClass568ArrayArrayArray7431
				 [aByte10854][i_281_][i_282_]);
	    if (class568 != null && class568.aClass654_Sub1_Sub2_7607 != null)
		return class568.aClass654_Sub1_Sub2_7607.aShort11864 + i;
	}
	return i;
    }
    
    public void method18599(int i, int i_283_, int i_284_, int i_285_,
			    int i_286_, int i_287_, int i_288_, int i_289_) {
	if (method18546(-376690104))
	    client.method17391(aClass243_11988,
			       aClass243_11988.method4473((byte) 0), -1, i,
			       i_283_, i_284_, i_285_, i_286_, i_287_, i_288_,
			       i_289_);
    }
    
    public abstract int method18600(byte i);
    
    boolean method18601() {
	if (anInt11990 * -935424637 == -1)
	    return false;
	return method18561(-935424637 * anInt11990, 1055370457);
    }
    
    public abstract int method18602();
    
    public Class597 method18603(int i) {
	Class438 class438 = method10807().aClass438_4885;
	Class597 class597 = client.aClass512_11100.method8416((byte) 118);
	int i_290_ = (-424199969 * class597.anInt7859
		      + ((int) class438.aFloat4864 >> 9));
	int i_291_ = (-1166289421 * class597.anInt7861
		      + ((int) class438.aFloat4865 >> 9));
	return new Class597(aByte10854, i_290_, i_291_);
    }
    
    public abstract int method18604();
    
    public abstract int method18605();
    
    public Class597 method18606() {
	Class438 class438 = method10807().aClass438_4885;
	Class597 class597 = client.aClass512_11100.method8416((byte) 43);
	int i = (-424199969 * class597.anInt7859
		 + ((int) class438.aFloat4864 >> 9));
	int i_292_ = (-1166289421 * class597.anInt7861
		      + ((int) class438.aFloat4865 >> 9));
	return new Class597(aByte10854, i, i_292_);
    }
    
    public Class597 method18607() {
	Class438 class438 = method10807().aClass438_4885;
	Class597 class597 = client.aClass512_11100.method8416((byte) 65);
	int i = (-424199969 * class597.anInt7859
		 + ((int) class438.aFloat4864 >> 9));
	int i_293_ = (-1166289421 * class597.anInt7861
		      + ((int) class438.aFloat4865 >> 9));
	return new Class597(aByte10854, i, i_293_);
    }
    
    public Class534_Sub36 method18608() {
	Class438 class438 = method10807().aClass438_4885;
	Class597 class597 = client.aClass512_11100.method8416((byte) 126);
	int i = (int) class438.aFloat4864 + 1852947968 * class597.anInt7859;
	int i_294_ = (int) class438.aFloat4863;
	int i_295_
	    = (int) class438.aFloat4865 + -139729408 * class597.anInt7861;
	return new Class534_Sub36(aByte10854, i, i_294_, i_295_);
    }
    
    public Class534_Sub36 method18609() {
	Class438 class438 = method10807().aClass438_4885;
	Class597 class597 = client.aClass512_11100.method8416((byte) 97);
	int i = (int) class438.aFloat4864 + 1852947968 * class597.anInt7859;
	int i_296_ = (int) class438.aFloat4863;
	int i_297_
	    = (int) class438.aFloat4865 + -139729408 * class597.anInt7861;
	return new Class534_Sub36(aByte10854, i, i_296_, i_297_);
    }
    
    public int method18610() {
	return anInt11923 * -930919489;
    }
    
    public int method18611() {
	return anInt11923 * -930919489;
    }
}
