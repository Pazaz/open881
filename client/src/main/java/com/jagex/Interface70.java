/* Interface70 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public interface Interface70
{
    public Class491 method444(byte i);
    
    public void method229();
    
    public boolean method445(byte i);
    
    public void method446(byte i);
    
    public byte[] method447(int i, byte i_0_);
    
    public int method448();
    
    public int method449();
    
    public int method450();
    
    public void method451(Class534_Sub40 class534_sub40, int i);
    
    public int method452(byte i);
    
    public boolean method453(int i);
    
    public boolean method454(int i, byte i_1_);
    
    public Class496 method455();
    
    public int method456(byte i);
    
    public Class592 method457();
    
    public Class534_Sub40 method458(int i, int i_2_);
    
    public void method226();
    
    public Class491 method459();
    
    public int method30(int i);
    
    public Class491 method460();
    
    public Class491 method461();
    
    public boolean method462();
    
    public boolean method423();
    
    public Class496 method463(int i);
    
    public byte[] method464(int i);
    
    public byte[] method465(int i);
    
    public boolean method466(int i);
    
    public int method222();
    
    public int method223();
    
    public boolean method467(int i);
    
    public int method225();
    
    public int method468();
    
    public void method469(Class534_Sub40 class534_sub40);
    
    public Class592 method470(int i);
    
    public boolean method471(int i);
    
    public void method472(boolean bool, int i);
    
    public boolean method473();
    
    public boolean method273();
    
    public boolean method474();
    
    public void method475(boolean bool);
    
    public Class534_Sub40 method476(int i);
    
    public boolean method477();
    
    public void method478(Class534_Sub40 class534_sub40);
    
    public void method479(Class534_Sub40 class534_sub40);
    
    public void method480(Class534_Sub40 class534_sub40);
    
    public Class491 method481();
    
    public void method482(Class534_Sub40 class534_sub40);
}
