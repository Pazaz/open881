/* Class166 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class166
{
    int anInt1761;
    public int anInt1762;
    public int anInt1763;
    public int anInt1764;
    public int anInt1765;
    public int anInt1766;
    public int anInt1767;
    
    public boolean method2744(Class166 class166_0_) {
	return (2113275141 * class166_0_.anInt1763 == 2113275141 * anInt1763
		&& (anInt1762 * -1095140607
		    == -1095140607 * class166_0_.anInt1762)
		&& class166_0_.anInt1764 * 1446227271 == 1446227271 * anInt1764
		&& anInt1761 * -777717129 == class166_0_.anInt1761 * -777717129
		&& -108174347 * anInt1765 == -108174347 * class166_0_.anInt1765
		&& (-1031192601 * class166_0_.anInt1766
		    == anInt1766 * -1031192601)
		&& (-900529735 * anInt1767
		    == class166_0_.anInt1767 * -900529735));
    }
    
    public Class166(int i, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_,
		    int i_6_) {
	anInt1763 = 929497037 * i;
	anInt1762 = 1063224577 * i_1_;
	anInt1764 = i_2_ * -1101271945;
	anInt1761 = 1456406343 * i_3_;
	anInt1765 = i_4_ * -1961234339;
	anInt1766 = -1338809385 * i_5_;
	anInt1767 = -2041662327 * i_6_;
    }
    
    public boolean method2745(Class166 class166_7_, byte i) {
	return (2113275141 * class166_7_.anInt1763 == 2113275141 * anInt1763
		&& (anInt1762 * -1095140607
		    == -1095140607 * class166_7_.anInt1762)
		&& class166_7_.anInt1764 * 1446227271 == 1446227271 * anInt1764
		&& anInt1761 * -777717129 == class166_7_.anInt1761 * -777717129
		&& -108174347 * anInt1765 == -108174347 * class166_7_.anInt1765
		&& (-1031192601 * class166_7_.anInt1766
		    == anInt1766 * -1031192601)
		&& (-900529735 * anInt1767
		    == class166_7_.anInt1767 * -900529735));
    }
    
    public Class166() {
	/* empty */
    }
    
    public boolean method2746(Class166 class166_8_) {
	return (2113275141 * class166_8_.anInt1763 == 2113275141 * anInt1763
		&& (anInt1762 * -1095140607
		    == -1095140607 * class166_8_.anInt1762)
		&& class166_8_.anInt1764 * 1446227271 == 1446227271 * anInt1764
		&& anInt1761 * -777717129 == class166_8_.anInt1761 * -777717129
		&& -108174347 * anInt1765 == -108174347 * class166_8_.anInt1765
		&& (-1031192601 * class166_8_.anInt1766
		    == anInt1766 * -1031192601)
		&& (-900529735 * anInt1767
		    == class166_8_.anInt1767 * -900529735));
    }
    
    public boolean method2747(Class166 class166_9_) {
	return (2113275141 * class166_9_.anInt1763 == 2113275141 * anInt1763
		&& (anInt1762 * -1095140607
		    == -1095140607 * class166_9_.anInt1762)
		&& class166_9_.anInt1764 * 1446227271 == 1446227271 * anInt1764
		&& anInt1761 * -777717129 == class166_9_.anInt1761 * -777717129
		&& -108174347 * anInt1765 == -108174347 * class166_9_.anInt1765
		&& (-1031192601 * class166_9_.anInt1766
		    == anInt1766 * -1031192601)
		&& (-900529735 * anInt1767
		    == class166_9_.anInt1767 * -900529735));
    }
    
    public boolean method2748(Class166 class166_10_) {
	return (2113275141 * class166_10_.anInt1763 == 2113275141 * anInt1763
		&& (anInt1762 * -1095140607
		    == -1095140607 * class166_10_.anInt1762)
		&& (class166_10_.anInt1764 * 1446227271
		    == 1446227271 * anInt1764)
		&& (anInt1761 * -777717129
		    == class166_10_.anInt1761 * -777717129)
		&& (-108174347 * anInt1765
		    == -108174347 * class166_10_.anInt1765)
		&& (-1031192601 * class166_10_.anInt1766
		    == anInt1766 * -1031192601)
		&& (-900529735 * anInt1767
		    == class166_10_.anInt1767 * -900529735));
    }
    
    public boolean method2749(Class166 class166_11_) {
	return (2113275141 * class166_11_.anInt1763 == 2113275141 * anInt1763
		&& (anInt1762 * -1095140607
		    == -1095140607 * class166_11_.anInt1762)
		&& (class166_11_.anInt1764 * 1446227271
		    == 1446227271 * anInt1764)
		&& (anInt1761 * -777717129
		    == class166_11_.anInt1761 * -777717129)
		&& (-108174347 * anInt1765
		    == -108174347 * class166_11_.anInt1765)
		&& (-1031192601 * class166_11_.anInt1766
		    == anInt1766 * -1031192601)
		&& (-900529735 * anInt1767
		    == class166_11_.anInt1767 * -900529735));
    }
    
    static final void method2750(Class669 class669, int i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class76.method1591(class247, class669, -952731545);
    }
    
    public static void method2751(int i, int i_12_) {
	if (i == 25)
	    Class554_Sub1.aFloat7409 = 2.0F;
	else if (37 == i)
	    Class554_Sub1.aFloat7409 = 3.0F;
	else if (50 == i)
	    Class554_Sub1.aFloat7409 = 4.0F;
	else if (75 == i)
	    Class554_Sub1.aFloat7409 = 6.0F;
	else if (i == 100)
	    Class554_Sub1.aFloat7409 = 8.0F;
	else if (i == 200)
	    Class554_Sub1.aFloat7409 = 16.0F;
	Class554_Sub1.anInt10673 = -2017528667;
	Class554_Sub1.anInt10673 = -2017528667;
    }
    
    static final void method2752(Class669 class669, int i) {
	class669.anInt8600 -= 1235998252;
	int i_13_ = class669.anIntArray8595[2088438307 * class669.anInt8600];
	int i_14_
	    = class669.anIntArray8595[class669.anInt8600 * 2088438307 + 1];
	int i_15_
	    = class669.anIntArray8595[class669.anInt8600 * 2088438307 + 2];
	int i_16_
	    = class669.anIntArray8595[class669.anInt8600 * 2088438307 + 3];
	Class41 class41
	    = ((Class41)
	       Class667.aClass44_Sub21_8582.method91(i_15_, -2031011492));
	if (class41.aClass493_317.method93() != i_13_
	    || class41.aClass493_314.method93() != i_14_)
	    throw new RuntimeException(new StringBuilder().append(i_15_).append
					   (" ").append
					   (i_16_).toString());
	if (i_14_ == Class493.aClass493_5496.method93())
	    class669.anObjectArray8593
		[(class669.anInt8594 += 1460193483) * 1485266147 - 1]
		= class41.method1040(i_16_, (byte) 19);
	else
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= class41.method1039(i_16_, 1106636692);
    }
    
    static void method2753(int i, int i_17_, byte i_18_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(1, (long) i);
	class534_sub18_sub6.method18121(-1982261430);
	class534_sub18_sub6.anInt11666 = 517206857 * i_17_;
    }
}
