/* Class706_Sub5_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class706_Sub5_Sub1 extends Class706_Sub5
{
    void method17336(Class534_Sub40 class534_sub40, int i) {
	/* empty */
    }
    
    float method17328(float f, float f_0_, float f_1_, int i) {
	float f_2_ = f - aFloat11003;
	if (aClass298_8844.method5392((byte) -31).aFloat4864
	    == Float.POSITIVE_INFINITY)
	    f_0_ = aClass298_8844.method5395(349088855).method7012();
	else {
	    float f_3_
		= f_0_ / aClass298_8844.method5392((byte) -58).method7012();
	    float f_4_ = f_3_ * (f_0_ / 2.0F);
	    if (f_4_ > f_2_) {
		f_0_ -= (aClass298_8844.method5392((byte) -73).method7012()
			 * f_1_);
		if (f_0_ < 0.0F)
		    f_0_ = 0.0F;
	    } else if (f_0_
		       < aClass298_8844.method5395(349088855).method7012()) {
		f_0_ += (aClass298_8844.method5392((byte) -8).method7012()
			 * f_1_);
		if (f_0_ > aClass298_8844.method5395(349088855).method7012())
		    f_0_ = aClass298_8844.method5395(349088855).method7012();
	    }
	}
	return f_0_;
    }
    
    void method17342(int i) {
	/* empty */
    }
    
    void method17340(Class534_Sub40 class534_sub40, int i, byte i_5_) {
	/* empty */
    }
    
    float method17332(float f, float f_6_, float f_7_) {
	float f_8_ = f - aFloat11003;
	if (aClass298_8844.method5392((byte) -115).aFloat4864
	    == Float.POSITIVE_INFINITY)
	    f_6_ = aClass298_8844.method5395(349088855).method7012();
	else {
	    float f_9_
		= f_6_ / aClass298_8844.method5392((byte) -118).method7012();
	    float f_10_ = f_9_ * (f_6_ / 2.0F);
	    if (f_10_ > f_8_) {
		f_6_ -= (aClass298_8844.method5392((byte) -37).method7012()
			 * f_7_);
		if (f_6_ < 0.0F)
		    f_6_ = 0.0F;
	    } else if (f_6_
		       < aClass298_8844.method5395(349088855).method7012()) {
		f_6_ += (aClass298_8844.method5392((byte) -13).method7012()
			 * f_7_);
		if (f_6_ > aClass298_8844.method5395(349088855).method7012())
		    f_6_ = aClass298_8844.method5395(349088855).method7012();
	    }
	}
	return f_6_;
    }
    
    float method17333(float f, float f_11_, float f_12_) {
	float f_13_ = f - aFloat11003;
	if (aClass298_8844.method5392((byte) -83).aFloat4864
	    == Float.POSITIVE_INFINITY)
	    f_11_ = aClass298_8844.method5395(349088855).method7012();
	else {
	    float f_14_
		= f_11_ / aClass298_8844.method5392((byte) -45).method7012();
	    float f_15_ = f_14_ * (f_11_ / 2.0F);
	    if (f_15_ > f_13_) {
		f_11_ -= (aClass298_8844.method5392((byte) -95).method7012()
			  * f_12_);
		if (f_11_ < 0.0F)
		    f_11_ = 0.0F;
	    } else if (f_11_
		       < aClass298_8844.method5395(349088855).method7012()) {
		f_11_ += (aClass298_8844.method5392((byte) -101).method7012()
			  * f_12_);
		if (f_11_ > aClass298_8844.method5395(349088855).method7012())
		    f_11_ = aClass298_8844.method5395(349088855).method7012();
	    }
	}
	return f_11_;
    }
    
    void method17339(Class534_Sub40 class534_sub40, int i) {
	/* empty */
    }
    
    float method17334(float f, float f_16_, float f_17_) {
	float f_18_ = f - aFloat11003;
	if (aClass298_8844.method5392((byte) -35).aFloat4864
	    == Float.POSITIVE_INFINITY)
	    f_16_ = aClass298_8844.method5395(349088855).method7012();
	else {
	    float f_19_
		= f_16_ / aClass298_8844.method5392((byte) -44).method7012();
	    float f_20_ = f_19_ * (f_16_ / 2.0F);
	    if (f_20_ > f_18_) {
		f_16_ -= (aClass298_8844.method5392((byte) -63).method7012()
			  * f_17_);
		if (f_16_ < 0.0F)
		    f_16_ = 0.0F;
	    } else if (f_16_
		       < aClass298_8844.method5395(349088855).method7012()) {
		f_16_ += (aClass298_8844.method5392((byte) -58).method7012()
			  * f_17_);
		if (f_16_ > aClass298_8844.method5395(349088855).method7012())
		    f_16_ = aClass298_8844.method5395(349088855).method7012();
	    }
	}
	return f_16_;
    }
    
    public Class706_Sub5_Sub1(Class298 class298) {
	super(class298);
    }
    
    void method17335() {
	/* empty */
    }
    
    void method17337(Class534_Sub40 class534_sub40, int i) {
	/* empty */
    }
}
