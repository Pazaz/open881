/* Class339 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class339
{
    int anInt3534;
    int anInt3535;
    int anInt3536;
    int anInt3537;
    int anInt3538 = 128;
    int anInt3539;
    
    Class339(int i) {
	anInt3536 = 128;
	anInt3539 = i;
    }
    
    Class339(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_) {
	anInt3536 = 128;
	anInt3539 = i;
	anInt3538 = i_0_;
	anInt3536 = i_1_;
	anInt3537 = i_2_;
	anInt3534 = i_3_;
	anInt3535 = i_4_;
    }
    
    Class339 method5933() {
	return new Class339(anInt3539, anInt3538, anInt3536, anInt3537,
			    anInt3534, anInt3535);
    }
    
    Class339 method5934() {
	return new Class339(anInt3539, anInt3538, anInt3536, anInt3537,
			    anInt3534, anInt3535);
    }
    
    void method5935(Class339 class339_5_) {
	anInt3538 = class339_5_.anInt3538;
	anInt3536 = class339_5_.anInt3536;
	anInt3537 = class339_5_.anInt3537;
	anInt3534 = class339_5_.anInt3534;
	anInt3539 = class339_5_.anInt3539;
	anInt3535 = class339_5_.anInt3535;
    }
    
    void method5936(Class339 class339_6_) {
	anInt3538 = class339_6_.anInt3538;
	anInt3536 = class339_6_.anInt3536;
	anInt3537 = class339_6_.anInt3537;
	anInt3534 = class339_6_.anInt3534;
	anInt3539 = class339_6_.anInt3539;
	anInt3535 = class339_6_.anInt3535;
    }
}
