/* Class169 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public abstract class Class169
{
    public abstract int method2759();
    
    public abstract boolean method2760();
    
    public abstract void method2761();
    
    public abstract int method2762();
    
    public abstract int method2763();
    
    public abstract int method2764();
    
    public abstract int method2765();
    
    public abstract int[] method2766(boolean bool);
    
    public abstract int method2767();
    
    public abstract void method2768();
    
    public abstract void method2769(int i);
    
    public abstract int method2770();
    
    public abstract void method2771();
    
    public abstract void method2772(int i);
    
    public abstract boolean method2773();
    
    public abstract int method2774();
    
    public abstract void method2775(int i);
    
    public abstract void method2776(int i, int i_0_, int i_1_);
    
    public abstract int method2777(int i, int i_2_);
    
    public abstract void method2778(int i, int i_3_, int i_4_);
    
    public abstract void method2779();
    
    public abstract boolean method2780();
    
    public abstract int method2781();
    
    public abstract int method2782();
    
    public abstract int method2783();
    
    public abstract int method2784();
    
    public abstract int method2785(int i, int i_5_);
    
    public abstract int method2786();
    
    public abstract int method2787();
    
    public abstract int method2788(int i, int i_6_);
    
    public abstract int method2789();
    
    public abstract int method2790();
    
    public abstract int method2791();
    
    public abstract void method2792(int i);
    
    public abstract int method2793();
    
    public abstract int method2794();
    
    public abstract void method2795(int i);
    
    public abstract void method2796(int i);
    
    public abstract int[] method2797(boolean bool);
    
    public abstract void method2798(int i);
    
    public abstract void method2799(int i);
    
    public abstract void method2800();
    
    public abstract void method2801();
    
    public abstract void method2802();
    
    public abstract void method2803();
    
    public abstract int method2804();
    
    public abstract void method2805();
    
    public abstract void method2806();
    
    public abstract int[] method2807(boolean bool);
    
    public abstract void method2808();
    
    public abstract int[] method2809(boolean bool);
    
    public abstract int method2810();
    
    public abstract void method2811(int i, int i_7_, int i_8_);
    
    public abstract void method2812(int i, int i_9_, int i_10_);
    
    public abstract void method2813(int i, int i_11_, int i_12_);
    
    public abstract int method2814();
    
    Class169() {
	/* empty */
    }
    
    public abstract int method2815(int i, int i_13_);
    
    public abstract int method2816(int i, int i_14_);
    
    public abstract boolean method2817();
    
    public abstract boolean method2818();
    
    public abstract void method2819();
    
    public abstract void method2820(int i);
    
    public abstract void method2821(int i);
}
