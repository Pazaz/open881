/* Class654 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class654
{
    Class654 aClass654_8504;
    Class444 aClass444_8505;
    boolean aBool8506;
    Class444 aClass444_8507;
    boolean aBool8508;
    Class446 aClass446_8509;
    Class444 aClass444_8510 = new Class444();
    boolean aBool8511;
    Class654 aClass654_8512;
    Class654 aClass654_8513;
    protected boolean aBool8514;
    
    public final Class444 method10807() {
	if (aBool8506) {
	    aBool8506 = false;
	    aClass444_8505.method7219(aClass444_8510);
	    if (aClass654_8512 != null)
		aClass444_8505.method7230(aClass654_8512.method10807());
	}
	return aClass444_8505;
    }
    
    public final Class444 method10808() {
	return aClass444_8510;
    }
    
    public final void method10809(Class438 class438) {
	aClass444_8510.aClass438_4885.method6992(class438);
	method10817();
	if (aClass654_8504 != null)
	    aClass654_8504.method10831();
    }
    
    final Class444 method10810() {
	if (aBool8508) {
	    aBool8508 = false;
	    aClass444_8507.method7219(method10807());
	    aClass444_8507.method7220();
	}
	return aClass444_8505;
    }
    
    public final void method10811(float f, float f_0_, float f_1_) {
	aClass444_8510.aClass438_4885.method6997(f, f_0_, f_1_);
	method10817();
	if (aClass654_8504 != null)
	    aClass654_8504.method10831();
    }
    
    final void method10812(Class444 class444) {
	aClass444_8510.method7219(class444);
	method10817();
	if (aClass654_8504 != null)
	    aClass654_8504.method10831();
    }
    
    public final void method10813(Class443 class443) {
	aClass444_8510.aClass443_4886.method7145(class443);
	method10817();
	if (aClass654_8504 != null)
	    aClass654_8504.method10831();
    }
    
    public final void method10814(float f, float f_2_, float f_3_) {
	aClass444_8510.aClass438_4885.method6997(f, f_2_, f_3_);
	method10817();
	if (aClass654_8504 != null)
	    aClass654_8504.method10831();
    }
    
    public final void method10815(float f, float f_4_, float f_5_) {
	aClass444_8510.aClass438_4885.method6997(f, f_4_, f_5_);
	method10817();
	if (aClass654_8504 != null)
	    aClass654_8504.method10831();
    }
    
    final void method10816(Class444 class444) {
	if (aClass654_8512 != null) {
	    Class444 class444_6_ = new Class444(class444);
	    class444_6_.method7230(aClass654_8512.method10810());
	    method10812(class444_6_);
	} else
	    method10812(class444);
    }
    
    final void method10817() {
	aBool8506 = true;
	aBool8508 = true;
	aBool8511 = true;
	aBool8514 = true;
    }
    
    public final void method10818() {
	if (aClass654_8512 != null) {
	    Class654 class654_7_ = aClass654_8512.aClass654_8504;
	    if (class654_7_ == this)
		aClass654_8512.aClass654_8504 = aClass654_8513;
	    else {
		for (/**/; class654_7_.aClass654_8513 != this;
		     class654_7_ = class654_7_.aClass654_8513) {
		    /* empty */
		}
		class654_7_.aClass654_8513 = aClass654_8513;
	    }
	}
	method10817();
	if (aClass654_8504 != null) {
	    aClass654_8504.method10831();
	    Class654 class654_8_ = aClass654_8504;
	    for (;;) {
		class654_8_.aClass444_8510.method7230(aClass444_8510);
		class654_8_.aClass654_8512 = aClass654_8512;
		if (class654_8_.aClass654_8513 == null) {
		    class654_8_.aClass654_8513 = aClass654_8512.aClass654_8504;
		    break;
		}
		class654_8_ = class654_8_.aClass654_8513;
	    }
	    aClass654_8512.aClass654_8504 = aClass654_8504;
	}
	aClass654_8512 = null;
	aClass654_8513 = null;
	aClass654_8504 = null;
    }
    
    Class654() {
	aClass444_8505 = new Class444();
	aBool8506 = true;
	aClass444_8507 = new Class444();
	aBool8508 = true;
	new Class433();
	new Class433();
	aClass446_8509 = new Class446();
	aBool8511 = true;
	aBool8514 = true;
	aClass654_8512 = null;
	aClass654_8504 = null;
	aClass654_8513 = null;
    }
    
    final Class444 method10819() {
	if (aBool8508) {
	    aBool8508 = false;
	    aClass444_8507.method7219(method10807());
	    aClass444_8507.method7220();
	}
	return aClass444_8505;
    }
    
    public final Class444 method10820() {
	return aClass444_8510;
    }
    
    public final Class444 method10821() {
	if (aBool8506) {
	    aBool8506 = false;
	    aClass444_8505.method7219(aClass444_8510);
	    if (aClass654_8512 != null)
		aClass444_8505.method7230(aClass654_8512.method10807());
	}
	return aClass444_8505;
    }
    
    public final Class444 method10822() {
	if (aBool8506) {
	    aBool8506 = false;
	    aClass444_8505.method7219(aClass444_8510);
	    if (aClass654_8512 != null)
		aClass444_8505.method7230(aClass654_8512.method10807());
	}
	return aClass444_8505;
    }
    
    public final Class444 method10823() {
	if (aBool8506) {
	    aBool8506 = false;
	    aClass444_8505.method7219(aClass444_8510);
	    if (aClass654_8512 != null)
		aClass444_8505.method7230(aClass654_8512.method10807());
	}
	return aClass444_8505;
    }
    
    final Class446 method10824() {
	if (aBool8511) {
	    aBool8511 = false;
	    aClass446_8509.method7238(method10807());
	}
	return aClass446_8509;
    }
    
    final Class444 method10825() {
	if (aBool8508) {
	    aBool8508 = false;
	    aClass444_8507.method7219(method10807());
	    aClass444_8507.method7220();
	}
	return aClass444_8505;
    }
    
    final void method10826() {
	method10817();
	if (aClass654_8504 != null)
	    aClass654_8504.method10831();
	if (aClass654_8513 != null)
	    aClass654_8513.method10831();
    }
    
    final Class446 method10827() {
	if (aBool8511) {
	    aBool8511 = false;
	    aClass446_8509.method7238(method10807());
	}
	return aClass446_8509;
    }
    
    public final Class444 method10828() {
	return aClass444_8510;
    }
    
    final void method10829(Class444 class444) {
	aClass444_8510.method7219(class444);
	method10817();
	if (aClass654_8504 != null)
	    aClass654_8504.method10831();
    }
    
    final void method10830(Class444 class444) {
	aClass444_8510.method7219(class444);
	method10817();
	if (aClass654_8504 != null)
	    aClass654_8504.method10831();
    }
    
    final void method10831() {
	method10817();
	if (aClass654_8504 != null)
	    aClass654_8504.method10831();
	if (aClass654_8513 != null)
	    aClass654_8513.method10831();
    }
    
    public final void method10832(Class443 class443) {
	aClass444_8510.aClass443_4886.method7145(class443);
	method10817();
	if (aClass654_8504 != null)
	    aClass654_8504.method10831();
    }
    
    public final void method10833(Class438 class438) {
	aClass444_8510.aClass438_4885.method6992(class438);
	method10817();
	if (aClass654_8504 != null)
	    aClass654_8504.method10831();
    }
    
    final Class446 method10834() {
	if (aBool8511) {
	    aBool8511 = false;
	    aClass446_8509.method7238(method10807());
	}
	return aClass446_8509;
    }
    
    final Class446 method10835() {
	if (aBool8511) {
	    aBool8511 = false;
	    aClass446_8509.method7238(method10807());
	}
	return aClass446_8509;
    }
    
    public final void method10836(float f, float f_9_, float f_10_) {
	aClass444_8510.aClass438_4885.method6997(f, f_9_, f_10_);
	method10817();
	if (aClass654_8504 != null)
	    aClass654_8504.method10831();
    }
    
    final void method10837(Class444 class444) {
	if (aClass654_8512 != null) {
	    Class444 class444_11_ = new Class444(class444);
	    class444_11_.method7230(aClass654_8512.method10810());
	    method10812(class444_11_);
	} else
	    method10812(class444);
    }
    
    final void method10838(Class444 class444) {
	if (aClass654_8512 != null) {
	    Class444 class444_12_ = new Class444(class444);
	    class444_12_.method7230(aClass654_8512.method10810());
	    method10812(class444_12_);
	} else
	    method10812(class444);
    }
    
    final void method10839() {
	aBool8506 = true;
	aBool8508 = true;
	aBool8511 = true;
	aBool8514 = true;
    }
    
    public final void method10840() {
	if (aClass654_8512 != null) {
	    Class654 class654_13_ = aClass654_8512.aClass654_8504;
	    if (class654_13_ == this)
		aClass654_8512.aClass654_8504 = aClass654_8513;
	    else {
		for (/**/; class654_13_.aClass654_8513 != this;
		     class654_13_ = class654_13_.aClass654_8513) {
		    /* empty */
		}
		class654_13_.aClass654_8513 = aClass654_8513;
	    }
	}
	method10817();
	if (aClass654_8504 != null) {
	    aClass654_8504.method10831();
	    Class654 class654_14_ = aClass654_8504;
	    for (;;) {
		class654_14_.aClass444_8510.method7230(aClass444_8510);
		class654_14_.aClass654_8512 = aClass654_8512;
		if (class654_14_.aClass654_8513 == null) {
		    class654_14_.aClass654_8513
			= aClass654_8512.aClass654_8504;
		    break;
		}
		class654_14_ = class654_14_.aClass654_8513;
	    }
	    aClass654_8512.aClass654_8504 = aClass654_8504;
	}
	aClass654_8512 = null;
	aClass654_8513 = null;
	aClass654_8504 = null;
    }
    
    final void method10841(Class444 class444) {
	aClass444_8510.method7219(class444);
	method10817();
	if (aClass654_8504 != null)
	    aClass654_8504.method10831();
    }
    
    public final void method10842() {
	if (aClass654_8512 != null) {
	    Class654 class654_15_ = aClass654_8512.aClass654_8504;
	    if (class654_15_ == this)
		aClass654_8512.aClass654_8504 = aClass654_8513;
	    else {
		for (/**/; class654_15_.aClass654_8513 != this;
		     class654_15_ = class654_15_.aClass654_8513) {
		    /* empty */
		}
		class654_15_.aClass654_8513 = aClass654_8513;
	    }
	}
	method10817();
	if (aClass654_8504 != null) {
	    aClass654_8504.method10831();
	    Class654 class654_16_ = aClass654_8504;
	    for (;;) {
		class654_16_.aClass444_8510.method7230(aClass444_8510);
		class654_16_.aClass654_8512 = aClass654_8512;
		if (class654_16_.aClass654_8513 == null) {
		    class654_16_.aClass654_8513
			= aClass654_8512.aClass654_8504;
		    break;
		}
		class654_16_ = class654_16_.aClass654_8513;
	    }
	    aClass654_8512.aClass654_8504 = aClass654_8504;
	}
	aClass654_8512 = null;
	aClass654_8513 = null;
	aClass654_8504 = null;
    }
    
    public final void method10843() {
	if (aClass654_8512 != null) {
	    Class654 class654_17_ = aClass654_8512.aClass654_8504;
	    if (class654_17_ == this)
		aClass654_8512.aClass654_8504 = aClass654_8513;
	    else {
		for (/**/; class654_17_.aClass654_8513 != this;
		     class654_17_ = class654_17_.aClass654_8513) {
		    /* empty */
		}
		class654_17_.aClass654_8513 = aClass654_8513;
	    }
	}
	method10817();
	if (aClass654_8504 != null) {
	    aClass654_8504.method10831();
	    Class654 class654_18_ = aClass654_8504;
	    for (;;) {
		class654_18_.aClass444_8510.method7230(aClass444_8510);
		class654_18_.aClass654_8512 = aClass654_8512;
		if (class654_18_.aClass654_8513 == null) {
		    class654_18_.aClass654_8513
			= aClass654_8512.aClass654_8504;
		    break;
		}
		class654_18_ = class654_18_.aClass654_8513;
	    }
	    aClass654_8512.aClass654_8504 = aClass654_8504;
	}
	aClass654_8512 = null;
	aClass654_8513 = null;
	aClass654_8504 = null;
    }
    
    final void method10844() {
	method10817();
	if (aClass654_8504 != null)
	    aClass654_8504.method10831();
	if (aClass654_8513 != null)
	    aClass654_8513.method10831();
    }
    
    public final void method10845() {
	if (aClass654_8512 != null) {
	    Class654 class654_19_ = aClass654_8512.aClass654_8504;
	    if (class654_19_ == this)
		aClass654_8512.aClass654_8504 = aClass654_8513;
	    else {
		for (/**/; class654_19_.aClass654_8513 != this;
		     class654_19_ = class654_19_.aClass654_8513) {
		    /* empty */
		}
		class654_19_.aClass654_8513 = aClass654_8513;
	    }
	}
	method10817();
	if (aClass654_8504 != null) {
	    aClass654_8504.method10831();
	    Class654 class654_20_ = aClass654_8504;
	    for (;;) {
		class654_20_.aClass444_8510.method7230(aClass444_8510);
		class654_20_.aClass654_8512 = aClass654_8512;
		if (class654_20_.aClass654_8513 == null) {
		    class654_20_.aClass654_8513
			= aClass654_8512.aClass654_8504;
		    break;
		}
		class654_20_ = class654_20_.aClass654_8513;
	    }
	    aClass654_8512.aClass654_8504 = aClass654_8504;
	}
	aClass654_8512 = null;
	aClass654_8513 = null;
	aClass654_8504 = null;
    }
}
