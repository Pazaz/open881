/* Class618 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class618 implements Interface76
{
    static Class618 aClass618_8097;
    static Class618 aClass618_8098;
    public static Class618 aClass618_8099 = new Class618(0);
    int anInt8100;
    public static Class458 aClass458_8101;
    public static int anInt8102;
    
    public static Class618[] method10236() {
	return (new Class618[]
		{ aClass618_8098, aClass618_8099, aClass618_8097 });
    }
    
    public static Class618[] method10237() {
	return (new Class618[]
		{ aClass618_8098, aClass618_8099, aClass618_8097 });
    }
    
    public int method93() {
	return -161203013 * anInt8100;
    }
    
    public boolean method10238(byte i) {
	return aClass618_8098 == this;
    }
    
    public static Class618[] method10239(int i) {
	return (new Class618[]
		{ aClass618_8098, aClass618_8099, aClass618_8097 });
    }
    
    static {
	aClass618_8098 = new Class618(1);
	aClass618_8097 = new Class618(2);
    }
    
    public int method22() {
	return -161203013 * anInt8100;
    }
    
    public int method53() {
	return -161203013 * anInt8100;
    }
    
    public static Class618[] method10240() {
	return (new Class618[]
		{ aClass618_8098, aClass618_8099, aClass618_8097 });
    }
    
    public boolean method10241() {
	return aClass618_8099 != this;
    }
    
    public boolean method10242(byte i) {
	return aClass618_8099 != this;
    }
    
    public boolean method10243() {
	return aClass618_8098 == this;
    }
    
    Class618(int i) {
	anInt8100 = i * 1215998067;
    }
    
    static final void method10244(Class669 class669, int i) {
	String string
	    = (String) (class669.anObjectArray8593
			[(class669.anInt8594 -= 1460193483) * 1485266147]);
	Class534_Sub19 class534_sub19
	    = Class346.method6128(Class404.aClass404_4212,
				  client.aClass100_11264.aClass13_1183,
				  1341391005);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16506(string.length() + 1, 718092979);
	class534_sub19.aClass534_Sub40_Sub1_10513.method16713(string,
							      -735872288);
	client.aClass100_11264.method1863(class534_sub19, (byte) 14);
    }
    
    static final void method10245(Class669 class669, int i) {
	Class72.anInt775
	    = (class669.anIntArray8595
	       [(class669.anInt8600 -= 308999563) * 2088438307]) * 1095883131;
    }
    
    static void method10246(Class669 class669, byte i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class110_Sub1.method14504(-1382485103);
    }
    
    static final void method10247(Class669 class669, int i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = 100;
    }
    
    static final void method10248(Class669 class669, int i) {
	int i_0_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub14_10755
		  .method14026(i_0_, -2024064741);
    }
}
