/* Class90 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class90 implements Interface13, Interface7
{
    Class493 aClass493_887;
    public int anInt888;
    public String aString889;
    public boolean aBool890 = true;
    static int anInt891;
    static Class169 aClass169_892;
    
    public void method79(Class534_Sub40 class534_sub40, byte i) {
	for (;;) {
	    int i_0_ = class534_sub40.method16527(1025204051);
	    if (0 == i_0_)
		break;
	    method1720(class534_sub40, i_0_, 1903491787);
	}
    }
    
    Class90() {
	/* empty */
    }
    
    public void method81(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(-536198625);
	    if (0 == i)
		break;
	    method1720(class534_sub40, i, 2080015817);
	}
    }
    
    public boolean method1718(int i) {
	return Class493.aClass493_5496 == aClass493_887;
    }
    
    public void method82(int i) {
	/* empty */
    }
    
    public void method67(int i, int i_1_) {
	/* empty */
    }
    
    void method1719(Class534_Sub40 class534_sub40, int i) {
	if (i == 1) {
	    char c = Class502_Sub2.method15983(class534_sub40
						   .method16586((byte) 1),
					       1992540125);
	    aClass493_887 = Class493.method8103(c, -1856283068);
	} else if (i == 2)
	    anInt888 = class534_sub40.method16533(-258848859) * 1880606829;
	else if (i == 4)
	    aBool890 = false;
	else if (5 == i)
	    aString889 = class534_sub40.method16541((byte) -120);
	else if (i == 101)
	    aClass493_887
		= ((Class493)
		   Class448.method7319(Class493.method8108((byte) 26),
				       class534_sub40.method16546(-1706829710),
				       2088438307));
    }
    
    public void method78(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(-1350584640);
	    if (0 == i)
		break;
	    method1720(class534_sub40, i, 1958252936);
	}
    }
    
    public void method66(int i) {
	/* empty */
    }
    
    public void method83() {
	/* empty */
    }
    
    public void method84() {
	/* empty */
    }
    
    public void method80(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(236838833);
	    if (0 == i)
		break;
	    method1720(class534_sub40, i, 2110184866);
	}
    }
    
    void method1720(Class534_Sub40 class534_sub40, int i, int i_2_) {
	if (i == 1) {
	    char c = Class502_Sub2.method15983(class534_sub40
						   .method16586((byte) 1),
					       1959005136);
	    aClass493_887 = Class493.method8103(c, 711168032);
	} else if (i == 2)
	    anInt888 = class534_sub40.method16533(-258848859) * 1880606829;
	else if (i == 4)
	    aBool890 = false;
	else if (5 == i)
	    aString889 = class534_sub40.method16541((byte) -82);
	else if (i == 101)
	    aClass493_887
		= ((Class493)
		   Class448.method7319(Class493.method8108((byte) -72),
				       class534_sub40.method16546(-1706829710),
				       2088438307));
    }
    
    void method1721(Class534_Sub40 class534_sub40, int i) {
	if (i == 1) {
	    char c = Class502_Sub2.method15983(class534_sub40
						   .method16586((byte) 1),
					       2007150867);
	    aClass493_887 = Class493.method8103(c, -861466468);
	} else if (i == 2)
	    anInt888 = class534_sub40.method16533(-258848859) * 1880606829;
	else if (i == 4)
	    aBool890 = false;
	else if (5 == i)
	    aString889 = class534_sub40.method16541((byte) -33);
	else if (i == 101)
	    aClass493_887
		= ((Class493)
		   Class448.method7319(Class493.method8108((byte) 70),
				       class534_sub40.method16546(-1706829710),
				       2088438307));
    }
    
    public void method65(int i) {
	/* empty */
    }
    
    public boolean method1722() {
	return Class493.aClass493_5496 == aClass493_887;
    }
    
    public static final void method1723(int i, boolean bool, int i_3_) {
	if (null != Class574.aClass534_Sub26_7710
	    && (i >= 0 && i < (Class574.aClass534_Sub26_7710.anInt10580
			       * -1406046755))) {
	    Class337 class337
		= Class574.aClass534_Sub26_7710.aClass337Array10579[i];
	    Class100 class100 = Class201.method3864(2095398292);
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4241,
				      class100.aClass13_1183, 1341391005);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16506
		(3 + Class668.method11029(class337.aString3523, (byte) 0),
		 1316685361);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16507(i,
								  743480595);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16506(bool ? 1 : 0,
								  1821828940);
	    class534_sub19.aClass534_Sub40_Sub1_10513
		.method16713(class337.aString3523, -2055816980);
	    class100.method1863(class534_sub19, (byte) 57);
	}
    }
    
    static final void method1724(Class669 class669, int i) {
	class669.anInt8600 -= 617999126;
	int i_4_ = class669.anIntArray8595[2088438307 * class669.anInt8600];
	int i_5_
	    = class669.anIntArray8595[class669.anInt8600 * 2088438307 + 1];
	int i_6_ = client.aClass492ArrayArray11027[i_5_][i_4_]
		       .method8086((byte) -23);
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = i_6_ == 0 ? 1 : 0;
    }
    
    public static int method1725(int i, int i_7_) {
	Class270 class270
	    = (Class270) Class274.aMap3037.get(Integer.valueOf(i));
	if (class270 == null)
	    return 0;
	return class270.method5026(-2062420004);
    }
    
    static final void method1726(Class669 class669, int i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	int i_8_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	i_8_--;
	if (null == class247.aStringArray2487
	    || i_8_ >= class247.aStringArray2487.length
	    || class247.aStringArray2487[i_8_] == null)
	    class669.anObjectArray8593
		[(class669.anInt8594 += 1460193483) * 1485266147 - 1]
		= "";
	else
	    class669.anObjectArray8593
		[(class669.anInt8594 += 1460193483) * 1485266147 - 1]
		= class247.aStringArray2487[i_8_];
    }
    
    static final void method1727(Class669 class669, byte i) {
	int i_9_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = client.aClass214_11359.method4044(i_9_, 1481307617)
		  .method3995((short) 7276) ? 1 : 0;
    }
}
