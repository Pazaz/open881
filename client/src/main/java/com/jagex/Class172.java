/* Class172 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class172
{
    public int anInt1808;
    public Class172 aClass172_1809;
    public int anInt1810;
    public int anInt1811;
    public int anInt1812;
    public int anInt1813;
    public int anInt1814;
    public int anInt1815;
    byte aByte1816;
    public int anInt1817;
    public int anInt1818;
    public int anInt1819;
    public int anInt1820;
    public int anInt1821;
    public int anInt1822;
    public int anInt1823;
    public static Class268 aClass268_1824;
    
    Class172 method2887(int i, int i_0_, int i_1_, int i_2_) {
	return new Class172(1708272351 * anInt1811, i, i_0_, i_1_, i_2_,
			    aByte1816);
    }
    
    public Class385 method2888(Interface48 interface48, byte i) {
	return interface48.method354(1708272351 * anInt1811, -1128370922);
    }
    
    Class172 method2889(int i, int i_3_, int i_4_, int i_5_, int i_6_) {
	return new Class172(1708272351 * anInt1811, i, i_3_, i_4_, i_5_,
			    aByte1816);
    }
    
    Class172 method2890(int i, int i_7_, int i_8_, int i_9_) {
	return new Class172(1708272351 * anInt1811, i, i_7_, i_8_, i_9_,
			    aByte1816);
    }
    
    public Class385 method2891(Interface48 interface48) {
	return interface48.method354(1708272351 * anInt1811, -1478897843);
    }
    
    Class172(int i, int i_10_, int i_11_, int i_12_, int i_13_, byte i_14_) {
	anInt1811 = i * 332140831;
	anInt1810 = i_10_ * 1152409435;
	anInt1814 = -2133929361 * i_11_;
	anInt1812 = i_12_ * 554968493;
	anInt1813 = 1440219603 * i_13_;
	aByte1816 = i_14_;
    }
    
    Class172 method2892(int i, int i_15_, int i_16_, int i_17_) {
	return new Class172(1708272351 * anInt1811, i, i_15_, i_16_, i_17_,
			    aByte1816);
    }
    
    Class172 method2893(int i, int i_18_, int i_19_, int i_20_) {
	return new Class172(1708272351 * anInt1811, i, i_18_, i_19_, i_20_,
			    aByte1816);
    }
    
    public Class385 method2894(Interface48 interface48) {
	return interface48.method354(1708272351 * anInt1811, 2898403);
    }
    
    Class172 method2895(int i, int i_21_, int i_22_, int i_23_) {
	return new Class172(1708272351 * anInt1811, i, i_21_, i_22_, i_23_,
			    aByte1816);
    }
    
    static final void method2896(Class669 class669, int i) {
	int i_24_ = (class669.anIntArray8595
		     [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_24_, -38855288);
	Class243 class243 = Class44_Sub11.aClass243Array11006[i_24_ >> 16];
	Class319.method5759(class247, class243, class669, 2001565081);
    }
    
    static final void method2897(Class669 class669, int i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = class247.anInt2488 * 1994134509;
    }
    
    static final void method2898(Class669 class669, byte i) {
	Class227.method4181(class669, class669.aClass654_Sub1_Sub5_Sub1_8604,
			    (byte) -81);
    }
    
    static final void method2899(Class669 class669, int i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub10_10751
		  .method16970((byte) 12) == 1 ? 1 : 0;
    }
    
    public static void method2900(boolean bool, short i) {
	/* empty */
    }
    
    static final void method2901(Class669 class669, int i) {
	throw new RuntimeException("");
    }
    
    public static boolean method2902(boolean bool, int i) {
	boolean bool_25_ = Class254.aClass185_2683.method3409();
	if (bool_25_ != bool) {
	    if (bool) {
		if (!Class254.aClass185_2683.method3358())
		    bool = false;
	    } else
		Class254.aClass185_2683.method3359();
	    if (bool != bool_25_) {
		Class44_Sub6.aClass534_Sub35_10989.method16438
		    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub19_10741,
		     bool ? 1 : 0, -226184856);
		Class672.method11096((byte) 1);
		return true;
	    }
	    return false;
	}
	return true;
    }
    
    static final void method2903(Class669 class669, int i) {
	int i_26_ = (class669.anIntArray8595
		     [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_26_, 1361056010);
	Class474.method7763(class247, class669, 748757737);
    }
}
