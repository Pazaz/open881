/* Class200_Sub8 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class200_Sub8 extends Class200
{
    int anInt9908;
    public static int anInt9909;
    
    public void method3846() {
	Class539_Sub1.aClass195Array10326[anInt9908 * -969202669]
	    .method3803((byte) 22);
    }
    
    public void method3845(int i) {
	Class539_Sub1.aClass195Array10326[anInt9908 * -969202669]
	    .method3803((byte) 58);
    }
    
    public void method3847() {
	Class539_Sub1.aClass195Array10326[anInt9908 * -969202669]
	    .method3803((byte) 10);
    }
    
    Class200_Sub8(Class534_Sub40 class534_sub40) {
	super(class534_sub40);
	anInt9908 = class534_sub40.method16529((byte) 1) * -985512421;
    }
    
    public static void method15579(boolean bool, boolean bool_0_, int i) {
	Class574.aBool7695 = bool;
	Class574.aBool7708 = bool_0_;
    }
    
    static final void method15580(Class669 class669, short i) {
	int i_1_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_1_, 492410164);
	Class243 class243 = Class44_Sub11.aClass243Array11006[i_1_ >> 16];
	Class663.method10996(class247, class243, class669, -1921736556);
    }
    
    public static void method15581(String string, String string_2_,
				   String string_3_, boolean bool, int i) {
	if (string.length() <= 320 && Class535.method8895((byte) 3)) {
	    client.aClass100_11094.method1866((byte) -66);
	    Class305.method5604(-1428529030);
	    Class65.aString694 = string;
	    Class65.aString665 = string_2_;
	    Class65.aString696 = string_3_;
	    Class65.aBool697 = bool;
	    Class673.method11110(13, -1775401115);
	}
    }
}
