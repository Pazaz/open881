/* Class447 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class447 implements Interface5
{
    String aString4906;
    Class472 aClass472_4907;
    
    public int method52() {
	if (aClass472_4907.method7689(aString4906, -348634583))
	    return 100;
	return 0;
    }
    
    public int method56(int i) {
	if (aClass472_4907.method7689(aString4906, -348634583))
	    return 100;
	return 0;
    }
    
    public Class30 method57() {
	return Class30.aClass30_268;
    }
    
    public int method53() {
	if (aClass472_4907.method7689(aString4906, -348634583))
	    return 100;
	return 0;
    }
    
    public int method22() {
	if (aClass472_4907.method7689(aString4906, -348634583))
	    return 100;
	return 0;
    }
    
    Class447(Class472 class472, String string) {
	aClass472_4907 = class472;
	aString4906 = string;
    }
    
    public Class30 method51(int i) {
	return Class30.aClass30_268;
    }
    
    public Class30 method50() {
	return Class30.aClass30_268;
    }
    
    public Class30 method54() {
	return Class30.aClass30_268;
    }
    
    public Class30 method55() {
	return Class30.aClass30_268;
    }
    
    static Class534_Sub18_Sub6 method7308(int i, long l) {
	Class534_Sub18_Sub6.aBool11689 = false;
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = (Class534_Sub18_Sub6) Class534_Sub18_Sub6.aClass9_11685
					.method579((long) i << 56 | l);
	if (null == class534_sub18_sub6) {
	    class534_sub18_sub6 = new Class534_Sub18_Sub6(i, l);
	    Class534_Sub18_Sub6.aClass9_11685.method581(class534_sub18_sub6,
							(8258869577519436579L
							 * (class534_sub18_sub6
							    .aLong7158)));
	    Class534_Sub18_Sub6.aBool11689 = true;
	}
	return class534_sub18_sub6;
    }
}
