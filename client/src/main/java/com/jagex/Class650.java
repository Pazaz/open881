/* Class650 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import java.io.IOException;
import java.net.URL;

public class Class650 implements Interface76
{
    int anInt8458;
    static Class650 aClass650_8459;
    public static Class650 aClass650_8460 = new Class650(0);
    static Class650 aClass650_8461;
    static Class650 aClass650_8462;
    static Class650 aClass650_8463;
    public static Class44_Sub5 aClass44_Sub5_8464;
    
    public int method22() {
	return anInt8458 * -62410987;
    }
    
    public int method93() {
	return anInt8458 * -62410987;
    }
    
    static {
	aClass650_8459 = new Class650(1);
	aClass650_8463 = new Class650(2);
	aClass650_8461 = new Class650(3);
	aClass650_8462 = new Class650(4);
    }
    
    Class650(int i) {
	anInt8458 = i * 1431316541;
    }
    
    public int method53() {
	return anInt8458 * -62410987;
    }
    
    static final void method10718(int i) {
	if (1114067909 * Class65.anInt692 != 2
	    && 106 != 1114067909 * Class65.anInt692) {
	    do {
		try {
		    int i_0_;
		    if (Class65.anInt678 * 804115323 == 0
			&& 1114067909 * Class65.anInt692 < 94)
			i_0_ = 500;
		    else
			i_0_ = 2000;
		    if (Class65.aBool661
			&& 1114067909 * Class65.anInt692 >= 68)
			i_0_ = 6000;
		    if ((220 == Class680.anInt8668 * 513656689
			 && 1114067909 * Class65.anInt692 != 212
			 && 42 != 1245290027 * Class65.anInt701)
			|| (Class680.anInt8668 * 513656689 == 131
			    && 49 != Class65.anInt671 * 490572819
			    && Class65.anInt671 * 490572819 != 52))
			Class65.anInt698 += -1692059527;
		    if (Class65.anInt698 * 684962249 > i_0_) {
			Class65.aClass100_658.method1866((byte) -89);
			if (Class65.anInt678 * 804115323 < 3) {
			    if (220 == Class680.anInt8668 * 513656689)
				Class685.aClass23_8698.method818((byte) -122);
			    else
				Class5.aClass23_49.method818((byte) -11);
			    Class65.anInt698 = 0;
			    Class65.anInt678 += -585559117;
			    Class65.anInt692 = -937912598;
			} else {
			    Class65.anInt692 = 850224666;
			    Class522.method8720(-5, -2081736435);
			    Class275.method5151(-1284906053);
			    break;
			}
		    }
		    if (Class65.anInt692 * 1114067909 == 18) {
			if (Class680.anInt8668 * 513656689 == 220)
			    Class65.aClass100_658.method1880
				(Class106.method1945((Class685
							  .aClass23_8698
							  .method817
						      (-167927180)),
						     40000, (byte) 103),
				 Class685.aClass23_8698.aString223,
				 -1850530127);
			else
			    Class65.aClass100_658.method1880
				((Class106.method1945
				  (Class5.aClass23_49.method817(-1198626603),
				   40000, (byte) 70)),
				 Class5.aClass23_49.aString223, -1850530127);
			Class65.aClass100_658.method1874(787319278);
			Class534_Sub19 class534_sub19
			    = Class56.method1225(1388643714);
			class534_sub19.aClass534_Sub40_Sub1_10513.method16506
			    (Class413.aClass413_4654.anInt4648 * -100453045,
			     973747599);
			Class65.aClass100_658.method1863(class534_sub19,
							 (byte) 109);
			Class65.aClass100_658.method1868(1805858475);
			Class65.anInt692 = 1568917434;
		    }
		    if (34 == 1114067909 * Class65.anInt692) {
			if (!Class65.aClass100_658.method1867(-1519030209)
				 .method8977(9, (byte) 1))
			    break;
			Class65.aClass100_658.method1867(-1254671283)
			    .method8969
			    ((Class65.aClass100_658.aClass534_Sub40_Sub1_1179
			      .aByteArray10810),
			     0, 9, (byte) 15);
			Class65.aClass100_658.aClass534_Sub40_Sub1_1179
			    .anInt10811
			    = 0;
			int i_1_
			    = Class65.aClass100_658
				  .aClass534_Sub40_Sub1_1179
				  .method16527(1052488397);
			if (i_1_ != 0) {
			    Class65.anInt692 = 850224666;
			    Class638.method10567(i_1_, (byte) 33);
			    Class522.method8720(i_1_, -2095990840);
			    Class65.aClass100_658.method1866((byte) -42);
			    Class275.method5151(674237151);
			    break;
			}
			Class437.aLong4860
			    = (Class65.aClass100_658
				   .aClass534_Sub40_Sub1_1179
				   .method16537(1359621443)
			       * 4180615252336247547L);
			Class65.aClass100_658.aClass534_Sub40_Sub1_1179
			    .anInt10811
			    = 0;
			if (Class65.aBool661)
			    Class65.anInt692 = 1366886916;
			else
			    Class65.anInt692 = -1244820360;
		    }
		    if (276 == Class65.anInt692 * 1114067909) {
			Class534_Sub19 class534_sub19
			    = Class56.method1225(1322127690);
			class534_sub19.aClass534_Sub40_Sub1_10513.method16506
			    (Class413.aClass413_4653.anInt4648 * -100453045,
			     2122407754);
			class534_sub19.aClass534_Sub40_Sub1_10513
			    .method16507(0, 774664425);
			int i_2_ = ((class534_sub19.aClass534_Sub40_Sub1_10513
				     .anInt10811)
				    * 31645619);
			class534_sub19.aClass534_Sub40_Sub1_10513
			    .method16510(881, -1313388290);
			class534_sub19.aClass534_Sub40_Sub1_10513
			    .method16510(1, -718422104);
			if (220 == 513656689 * Class680.anInt8668)
			    class534_sub19.aClass534_Sub40_Sub1_10513
				.method16506
				(-1850530127 * client.anInt11039 == 5 ? 1 : 0,
				 1107881584);
			Class534_Sub40 class534_sub40
			    = Class324_Sub2.method15702((byte) -63);
			Class175_Sub2.method15483(class534_sub40,
						  (long) (1832109523
							  * Class65.anInt662));
			Class690_Sub1.aLong10845
			    = (long) Class65.anInt662 * 3077592020303809937L;
			class534_sub40.method16506((Class65.anInt662
						    * 1832109523),
						   490770857);
			class534_sub40.method16506(Class539.aClass672_7171
						       .method93(),
						   304356297);
			class534_sub40.method16510((client.anInt11020
						    * 1844562269),
						   -156003025);
			for (int i_3_ = 0; i_3_ < 5; i_3_++)
			    class534_sub40.method16510((int) (Math.random()
							      * 9.9999999E7),
						       -635092829);
			class534_sub40.method16505(-4748671220908374923L
						   * client.aLong11058);
			class534_sub40.method16506((-1082924039
						    * (client.aClass675_11016
						       .anInt8642)),
						   1177053543);
			class534_sub40.method16506((int) (Math.random()
							  * 9.9999999E7),
						   1431399751);
			class534_sub40.method16556(Class37.RSA_UPDATE_EXPONENT,
						   Class37.LOGIN_MODULUS,
						   1515466328);
			class534_sub19.aClass534_Sub40_Sub1_10513.method16519
			    (class534_sub40.aByteArray10810, 0,
			     31645619 * class534_sub40.anInt10811, -192779344);
			class534_sub19.aClass534_Sub40_Sub1_10513.method16733
			    (31645619 * (class534_sub19
					 .aClass534_Sub40_Sub1_10513
					 .anInt10811) - i_2_,
			     -592945676);
			Class65.aClass100_658.method1863(class534_sub19,
							 (byte) 111);
			Class65.aClass100_658.method1868(1805858475);
			Class65.anInt692 = -175375864;
		    }
		    if (40 == 1114067909 * Class65.anInt692) {
			if (!Class65.aClass100_658.method1867(527448381)
				 .method8977(2, (byte) 1))
			    break;
			Class65.aClass100_658.method1867(-907173179).method8969
			    ((Class65.aClass100_658.aClass534_Sub40_Sub1_1179
			      .aByteArray10810),
			     0, 2, (byte) 5);
			Class65.aClass100_658.aClass534_Sub40_Sub1_1179
			    .anInt10811
			    = 0;
			Class65.aClass100_658.aClass534_Sub40_Sub1_1179
			    .anInt10811
			    = Class65.aClass100_658
				  .aClass534_Sub40_Sub1_1179
				  .method16529((byte) 1) * -1387468933;
			Class65.anInt692 = -1963513128;
		    }
		    if (56 == Class65.anInt692 * 1114067909) {
			if (!Class65.aClass100_658.method1867(-1479363074)
				 .method8977
			     (31645619 * (Class65.aClass100_658
					  .aClass534_Sub40_Sub1_1179
					  .anInt10811),
			      (byte) 1))
			    break;
			Class65.aClass100_658.method1867(1365591886).method8969
			    ((Class65.aClass100_658.aClass534_Sub40_Sub1_1179
			      .aByteArray10810),
			     0,
			     31645619 * (Class65.aClass100_658
					 .aClass534_Sub40_Sub1_1179
					 .anInt10811),
			     (byte) 51);
			Class65.aClass100_658.aClass534_Sub40_Sub1_1179
			    .method16553(Class65.anIntArray668, (byte) 32);
			Class65.aClass100_658.aClass534_Sub40_Sub1_1179
			    .anInt10811
			    = 0;
			String string
			    = Class65.aClass100_658
				  .aClass534_Sub40_Sub1_1179
				  .method16523(-1705915686);
			Class65.aClass100_658.aClass534_Sub40_Sub1_1179
			    .anInt10811
			    = 0;
			String string_4_
			    = Class403.aClass403_4149.method6597(-397859356);
			Class403.method6611(string, true, string_4_,
					    client.aBool11032, -908420807);
			Class65.anInt692 = -1157132428;
		    }
		    if (1114067909 * Class65.anInt692 == 68) {
			if (!Class65.aClass100_658.method1867(612510238)
				 .method8977(1, (byte) 1))
			    break;
			Class65.aClass100_658.method1867(-1080495994)
			    .method8969
			    ((Class65.aClass100_658.aClass534_Sub40_Sub1_1179
			      .aByteArray10810),
			     0, 1, (byte) 99);
			if (((Class65.aClass100_658.aClass534_Sub40_Sub1_1179
			      .aByteArray10810[0])
			     & 0xff)
			    == 1)
			    Class65.anInt692 = -1200976394;
		    }
		    if (78 == Class65.anInt692 * 1114067909) {
			if (!Class65.aClass100_658.method1867(1931101715)
				 .method8977(16, (byte) 1))
			    break;
			Class65.aClass100_658.method1867(2037820851).method8969
			    ((Class65.aClass100_658.aClass534_Sub40_Sub1_1179
			      .aByteArray10810),
			     0, 16, (byte) 103);
			Class65.aClass100_658.aClass534_Sub40_Sub1_1179
			    .anInt10811
			    = -724666448;
			Class65.aClass100_658.aClass534_Sub40_Sub1_1179
			    .method16553(Class65.anIntArray668, (byte) 99);
			Class65.aClass100_658.aClass534_Sub40_Sub1_1179
			    .anInt10811
			    = 0;
			Class65.aLong663
			    = (Class65.aClass100_658
				   .aClass534_Sub40_Sub1_1179
				   .method16537(1359621443)
			       * -8972729624098644529L);
			Class65.aLong693
			    = (Class65.aClass100_658
				   .aClass534_Sub40_Sub1_1179
				   .method16537(1359621443)
			       * 7601348613429507741L);
			Class65.anInt692 = -1244820360;
		    }
		    if (1114067909 * Class65.anInt692 == 88) {
			Class65.aClass100_658.aClass534_Sub40_Sub1_1179
			    .anInt10811
			    = 0;
			Class65.aClass100_658.method1874(1460006300);
			Class534_Sub19 class534_sub19
			    = Class56.method1225(1765204203);
			Class534_Sub40_Sub1 class534_sub40_sub1
			    = class534_sub19.aClass534_Sub40_Sub1_10513;
			if (220 == 513656689 * Class680.anInt8668) {
			    Class413 class413;
			    if (Class65.aBool661)
				class413 = Class413.aClass413_4656;
			    else
				class413 = Class413.aClass413_4646;
			    class534_sub40_sub1.method16506((class413.anInt4648
							     * -100453045),
							    1967661411);
			    class534_sub40_sub1.method16507(0, 924859610);
			    int i_5_
				= 31645619 * class534_sub40_sub1.anInt10811;
			    int i_6_
				= 31645619 * class534_sub40_sub1.anInt10811;
			    if (!Class65.aBool661) {
				class534_sub40_sub1.method16510(881,
								-252950735);
				class534_sub40_sub1.method16510(1, -940212156);
				class534_sub40_sub1.method16506
				    ((client.anInt11039 * -1850530127 == 5 ? 1
				      : 0),
				     937534545);
				i_6_ = (31645619
					* class534_sub40_sub1.anInt10811);
				Class534_Sub40 class534_sub40
				    = Class493.method8100((byte) 54);
				class534_sub40_sub1.method16519
				    (class534_sub40.aByteArray10810, 0,
				     class534_sub40.anInt10811 * 31645619,
				     162472892);
				i_6_ = (31645619
					* class534_sub40_sub1.anInt10811);
				class534_sub40_sub1.method16506
				    ((Class65.aLong663 * 5952060205682133295L
				      == -1L) ? 1 : 0,
				     302787058);
				if (-1L
				    == 5952060205682133295L * Class65.aLong663)
				    class534_sub40_sub1.method16713
					(Class65.aString694, -1283676635);
				else
				    class534_sub40_sub1.method16505
					(5952060205682133295L
					 * Class65.aLong663);
			    }
			    class534_sub40_sub1.method16506
				(Class63.method1280(1988320033), 526376464);
			    class534_sub40_sub1.method16507(((Class706_Sub4
							      .anInt10994)
							     * 1771907305),
							    821652782);
			    class534_sub40_sub1.method16507((Class18.anInt205
							     * -1091172141),
							    550614895);
			    class534_sub40_sub1.method16506
				(Class44_Sub6.aClass534_Sub35_10989
				     .aClass690_Sub27_10757
				     .method17119((byte) 37),
				 1197086113);
			    Class646.method10690(class534_sub40_sub1,
						 (byte) 69);
			    class534_sub40_sub1
				.method16713(client.aString11130, -1506206801);
			    class534_sub40_sub1.method16510((1844562269
							     * (client
								.anInt11020)),
							    -1322993754);
			    Class534_Sub40 class534_sub40
				= Class44_Sub6.aClass534_Sub35_10989
				      .method16436(1895238865);
			    class534_sub40_sub1.method16506(((class534_sub40
							      .anInt10811)
							     * 31645619),
							    1300834121);
			    class534_sub40_sub1.method16519
				(class534_sub40.aByteArray10810, 0,
				 31645619 * class534_sub40.anInt10811,
				 1670694899);
			    client.aBool11048 = true;
			    Class534_Sub40 class534_sub40_7_
				= (new Class534_Sub40
				   (Class200_Sub23.aClass534_Sub28_10040
					.method16310(73735874)));
			    Class200_Sub23.aClass534_Sub28_10040
				.method16312(class534_sub40_7_, (byte) 23);
			    class534_sub40_sub1.method16519
				(class534_sub40_7_.aByteArray10810, 0,
				 class534_sub40_7_.aByteArray10810.length,
				 -2023689218);
			    class534_sub40_sub1.method16510((client.anInt11144
							     * -983036913),
							    -591067670);
			    class534_sub40_sub1.method16510((-1608886643
							     * (client
								.anInt11024)),
							    -1817947936);
			    class534_sub40_sub1.method16510((1306630125
							     * (client
								.anInt11276)),
							    -985294454);
			    class534_sub40_sub1.method16510((655301323
							     * (client
								.anInt11026)),
							    -1745915238);
			    class534_sub40_sub1.method16510((client.anInt11025
							     * -1383240089),
							    -1618691500);
			    class534_sub40_sub1.method16713(Class29.aString267,
							    -806424586);
			    class534_sub40_sub1.method16506(((client
							      .aString11029)
							     == null) ? 0 : 1,
							    407735016);
			    if (null != client.aString11029)
				class534_sub40_sub1.method16713
				    (client.aString11029, 931110274);
			    class534_sub40_sub1.method16506((client.aBool11017
							     ? 1 : 0),
							    1563676878);
			    class534_sub40_sub1.method16506((client.aBool11033
							     ? 1 : 0),
							    1532605827);
			    class534_sub40_sub1.method16506
				(Class578.anInt7742 * -506156481 & 0x1,
				 2057836242);
			    class534_sub40_sub1.method16510((client.anInt11036
							     * -1840306525),
							    -860861730);
			    class534_sub40_sub1
				.method16713(client.aString11197, 1291154909);
			    class534_sub40_sub1.method16506
				((Class5.aClass23_43 == null
				  || ((Class685.aClass23_8698.anInt227
				       * -1664252895)
				      != -1664252895 * (Class5.aClass23_43
							.anInt227))) ? 1 : 0,
				 1394596223);
			    class534_sub40_sub1.method16507((-1664252895
							     * (Class5
								.aClass23_49
								.anInt227)),
							    1021213321);
			    Class523.method8726(class534_sub40_sub1,
						(byte) 26);
			    class534_sub40_sub1.method16756
				(Class65.anIntArray668, i_6_,
				 31645619 * class534_sub40_sub1.anInt10811,
				 (byte) 16);
			    class534_sub40_sub1.method16733
				((class534_sub40_sub1.anInt10811 * 31645619
				  - i_5_),
				 1039296440);
			} else {
			    Class413 class413;
			    if (Class65.aBool661)
				class413 = Class413.aClass413_4656;
			    else
				class413 = Class413.aClass413_4647;
			    class534_sub40_sub1.method16506((class413.anInt4648
							     * -100453045),
							    1576382054);
			    class534_sub40_sub1.method16507(0, 678500717);
			    int i_8_
				= class534_sub40_sub1.anInt10811 * 31645619;
			    int i_9_
				= 31645619 * class534_sub40_sub1.anInt10811;
			    if (!Class65.aBool661) {
				class534_sub40_sub1.method16510(881,
								-949663353);
				class534_sub40_sub1.method16510(1, -521854549);
				Class534_Sub40 class534_sub40
				    = Class493.method8100((byte) 106);
				class534_sub40_sub1.method16519
				    (class534_sub40.aByteArray10810, 0,
				     class534_sub40.anInt10811 * 31645619,
				     691189883);
				i_9_ = (class534_sub40_sub1.anInt10811
					* 31645619);
				class534_sub40_sub1.method16506
				    ((5952060205682133295L * Class65.aLong663
				      == -1L) ? 1 : 0,
				     1633497350);
				if (-1L
				    == 5952060205682133295L * Class65.aLong663)
				    class534_sub40_sub1.method16713
					(Class65.aString694, -195622443);
				else
				    class534_sub40_sub1.method16505
					(5952060205682133295L
					 * Class65.aLong663);
			    }
			    class534_sub40_sub1.method16506
				((-1082924039
				  * client.aClass675_11016.anInt8642),
				 1774652830);
			    class534_sub40_sub1.method16506(Class539
								.aClass672_7171
								.method93(),
							    2055023491);
			    class534_sub40_sub1.method16506
				(Class63.method1280(250760507), 604703216);
			    class534_sub40_sub1.method16507((1771907305
							     * (Class706_Sub4
								.anInt10994)),
							    1402292202);
			    class534_sub40_sub1.method16507((Class18.anInt205
							     * -1091172141),
							    625928724);
			    class534_sub40_sub1.method16506
				(Class44_Sub6.aClass534_Sub35_10989
				     .aClass690_Sub27_10757
				     .method17119((byte) 3),
				 1523213855);
			    Class646.method10690(class534_sub40_sub1,
						 (byte) 48);
			    class534_sub40_sub1
				.method16713(client.aString11130, 325027265);
			    Class534_Sub40 class534_sub40
				= Class44_Sub6.aClass534_Sub35_10989
				      .method16436(1999431513);
			    class534_sub40_sub1.method16506((31645619
							     * (class534_sub40
								.anInt10811)),
							    2077068620);
			    class534_sub40_sub1.method16519
				(class534_sub40.aByteArray10810, 0,
				 class534_sub40.anInt10811 * 31645619,
				 1154929108);
			    Class534_Sub40 class534_sub40_10_
				= (new Class534_Sub40
				   (Class200_Sub23.aClass534_Sub28_10040
					.method16310(73735874)));
			    Class200_Sub23.aClass534_Sub28_10040
				.method16312(class534_sub40_10_, (byte) -29);
			    class534_sub40_sub1.method16519
				(class534_sub40_10_.aByteArray10810, 0,
				 class534_sub40_10_.aByteArray10810.length,
				 -849784442);
			    class534_sub40_sub1.method16510((client.anInt11144
							     * -983036913),
							    -581894682);
			    class534_sub40_sub1.method16713(Class29.aString267,
							    304565103);
			    class534_sub40_sub1.method16510((client.anInt11020
							     * 1844562269),
							    -543567938);
			    class534_sub40_sub1.method16510((client.anInt11036
							     * -1840306525),
							    -739207847);
			    class534_sub40_sub1
				.method16713(client.aString11197, -801227695);
			    class534_sub40_sub1.method16506
				(Class578.anInt7742 * -506156481 & 0x1,
				 1082093921);
			    Class523.method8726(class534_sub40_sub1,
						(byte) 61);
			    class534_sub40_sub1.method16756
				(Class65.anIntArray668, i_9_,
				 class534_sub40_sub1.anInt10811 * 31645619,
				 (byte) 16);
			    class534_sub40_sub1.method16733
				((31645619 * class534_sub40_sub1.anInt10811
				  - i_8_),
				 1952256352);
			}
			Class65.aClass100_658.method1863(class534_sub19,
							 (byte) 56);
			Class65.aClass100_658.method1868(1805858475);
			Class65.aClass100_658.aClass13_1183
			    = new IsaacCipher(Class65.anIntArray668);
			for (int i_11_ = 0; i_11_ < 4; i_11_++)
			    Class65.anIntArray668[i_11_] += 50;
			Class65.aClass100_658.aClass13_1190
			    = new IsaacCipher(Class65.anIntArray668);
			new IsaacCipher(Class65.anIntArray668);
			Class65.aClass100_658.aClass534_Sub40_Sub1_1179
			    .method18316
			    (Class65.aClass100_658.aClass13_1190, 163694688);
			Class65.anIntArray668 = null;
			Class65.anInt692 = 1305853638;
		    }
		    if (94 == Class65.anInt692 * 1114067909) {
			if (!Class65.aClass100_658.method1867(129366020)
				 .method8977(1, (byte) 1))
			    break;
			Class65.aClass100_658.method1867(1533385741).method8969
			    ((Class65.aClass100_658.aClass534_Sub40_Sub1_1179
			      .aByteArray10810),
			     0, 1, (byte) 48);
			int i_12_
			    = Class65.aClass100_658
				  .aClass534_Sub40_Sub1_1179
				  .method16527(725882850);
			Class65.aClass100_658.aClass534_Sub40_Sub1_1179
			    .anInt10811
			    = 0;
			if (21 == i_12_)
			    Class65.anInt692 = -101015259;
			else {
			    if (1 == i_12_) {
				Class65.anInt692 = 2112234338;
				Class522.method8720(i_12_, -2075897359);
				break;
			    }
			    if (52 == i_12_) {
				Class581.anInt7765 = 1894907683 * i_12_;
				Class65.anInt692 = 735882080;
			    } else if (i_12_ == 2) {
				if (Class65.aBool711) {
				    Class65.aBool711 = false;
				    Class65.anInt692 = -937912598;
				    break;
				}
				if (220 == Class680.anInt8668 * 513656689) {
				    Class77.aClass155_Sub1_819
					.method15461((byte) 102);
				    Class65.anInt692 = 179237849;
				} else
				    Class65.anInt692 = -1507884156;
			    } else if (15 == i_12_) {
				Class65.aClass100_658.anInt1197 = -1853330870;
				Class65.anInt692 = 398457679;
			    } else {
				if (i_12_ == 23
				    && 804115323 * Class65.anInt678 < 3) {
				    Class65.anInt698 = 0;
				    Class65.anInt678 += -585559117;
				    Class65.anInt692 = -937912598;
				    Class65.aClass100_658
					.method1866((byte) -84);
				    break;
				}
				if (42 == i_12_) {
				    Class65.anInt692 = -70498620;
				    Class522.method8720(i_12_, -2145418404);
				    break;
				}
				if (Class680.anInt8668 * 513656689 == 131
				    && 49 == i_12_
				    && client.anInt11039 * -1850530127 != 14) {
				    if (52 != 490572819 * Class65.anInt671)
					Class522.method8720(i_12_,
							    -2080417882);
				    break;
				}
				if (Class65.aBool680 && !Class65.aBool661
				    && -1 != 1832109523 * Class65.anInt662
				    && i_12_ == 35) {
				    Class65.aBool661 = true;
				    Class65.anInt698 = 0;
				    Class65.anInt692 = -937912598;
				    Class65.aClass100_658
					.method1866((byte) -8);
				    break;
				}
				if (i_12_ == 53)
				    Class65.anInt692 = 1073306481;
				else {
				    Class65.anInt692 = 850224666;
				    Class522.method8720(i_12_, -2136542022);
				    Class65.aClass100_658
					.method1866((byte) -61);
				    Class275.method5151(1440731680);
				    break;
				}
			    }
			}
		    }
		    if (121 == Class65.anInt692 * 1114067909) {
			if (Class65.aClass100_658.method1867(-1853720176)
				.method8977(1, (byte) 1)) {
			    Class65.aClass100_658.method1867(1713418513)
				.method8969
				((Class65.aClass100_658
				  .aClass534_Sub40_Sub1_1179.aByteArray10810),
				 0, 1, (byte) 93);
			    int i_13_ = ((Class65.aClass100_658
					  .aClass534_Sub40_Sub1_1179
					  .aByteArray10810[0])
					 & 0xff);
			    Class65.anInt705 = -279730794 * i_13_;
			    Class65.anInt692 = 850224666;
			    Class522.method8720(21, -2118190740);
			    Class65.aClass100_658.method1866((byte) -94);
			    Class275.method5151(-1744455629);
			}
		    } else if (212 == 1114067909 * Class65.anInt692) {
			if (Class65.aClass100_658.method1867(-1973196118)
				.method8977(2, (byte) 1)) {
			    Class65.aClass100_658.method1867(1611782614)
				.method8969
				((Class65.aClass100_658
				  .aClass534_Sub40_Sub1_1179.aByteArray10810),
				 0, 2, (byte) 73);
			    Class65.anInt708 = ((((Class65.aClass100_658
						   .aClass534_Sub40_Sub1_1179
						   .aByteArray10810[0])
						  & 0xff)
						 << 8)
						+ ((Class65.aClass100_658
						    .aClass534_Sub40_Sub1_1179
						    .aByteArray10810[1])
						   & 0xff)) * 650198321;
			    Class65.anInt692 = 1305853638;
			}
		    } else if (Class65.anInt692 * 1114067909 == 245) {
			if (Class65.aClass100_658.method1867(-225719975)
				.method8977(4, (byte) 1)) {
			    Class65.aClass100_658.method1867(-289554607)
				.method8969
				((Class65.aClass100_658
				  .aClass534_Sub40_Sub1_1179.aByteArray10810),
				 0, 4, (byte) 97);
			    Class65.anInt659
				= Class65.aClass100_658
				      .aClass534_Sub40_Sub1_1179
				      .method16533(-258848859) * -316457055;
			    Class65.aClass100_658.aClass534_Sub40_Sub1_1179
				.anInt10811
				= 0;
			    Class65.anInt692 = 850224666;
			    Class522.method8720(53, -2082732144);
			    Class65.aClass100_658.method1866((byte) -83);
			    Class275.method5151(1578099501);
			}
		    } else if (197 == Class65.anInt692 * 1114067909) {
			if (29 == 1654597771 * Class581.anInt7765) {
			    if (!Class65.aClass100_658.method1867
				     (-782461287).method8977(1, (byte) 1))
				break;
			    Class65.aClass100_658.method1867(847609776)
				.method8969
				((Class65.aClass100_658
				  .aClass534_Sub40_Sub1_1179.aByteArray10810),
				 0, 1, (byte) 105);
			    Class65.anInt677
				= -217400935 * ((Class65.aClass100_658
						 .aClass534_Sub40_Sub1_1179
						 .aByteArray10810[0])
						& 0xff);
			} else if (Class581.anInt7765 * 1654597771 == 45) {
			    if (!Class65.aClass100_658.method1867
				     (1729558561).method8977(3, (byte) 1))
				break;
			    Class65.aClass100_658.method1867(201895079)
				.method8969
				((Class65.aClass100_658
				  .aClass534_Sub40_Sub1_1179.aByteArray10810),
				 0, 3, (byte) 102);
			    Class65.anInt677 = ((Class65.aClass100_658
						 .aClass534_Sub40_Sub1_1179
						 .aByteArray10810[0])
						& 0xff) * -217400935;
			    Class65.anInt706 = (((Class65.aClass100_658
						  .aClass534_Sub40_Sub1_1179
						  .aByteArray10810[2])
						 & 0xff)
						+ (((Class65.aClass100_658
						     .aClass534_Sub40_Sub1_1179
						     .aByteArray10810[1])
						    & 0xff)
						   << 8)) * 1087612101;
			} else
			    throw new IllegalStateException();
			Class65.anInt692 = 850224666;
			Class522.method8720(1654597771 * Class581.anInt7765,
					    -2016338080);
			Class65.aClass100_658.method1866((byte) -79);
			Class275.method5151(879202741);
			if (Class192.method3789((-1850530127
						 * client.anInt11039),
						-1575485351)) {
			    Class622.method10291(true, 1117755828);
			    Class65.anInt701 = Class581.anInt7765 * 1164431137;
			}
		    } else if (224 == 1114067909 * Class65.anInt692) {
			if (Class65.aClass100_658.method1867(335324975)
				.method8977(2, (byte) 1)) {
			    Class65.aClass100_658.method1867(-87630148)
				.method8969
				((Class65.aClass100_658
				  .aClass534_Sub40_Sub1_1179.aByteArray10810),
				 0, 2, (byte) 56);
			    Class65.aClass100_658.aClass534_Sub40_Sub1_1179
				.anInt10811
				= 0;
			    Class605.anInt7980
				= Class65.aClass100_658
				      .aClass534_Sub40_Sub1_1179
				      .method16529((byte) 1) * -2034198373;
			    Class65.aClass100_658.aClass534_Sub40_Sub1_1179
				.anInt10811
				= 0;
			    Class65.anInt692 = -1008411218;
			}
		    } else if (1114067909 * Class65.anInt692 == 230) {
			if (Class65.aClass100_658.method1867(1985379431)
				.method8977
			    (Class605.anInt7980 * 608275859, (byte) 1)) {
			    Class65.aClass100_658.method1867(1564814746)
				.method8969
				((Class65.aClass100_658
				  .aClass534_Sub40_Sub1_1179.aByteArray10810),
				 0, 608275859 * Class605.anInt7980,
				 (byte) 106);
			    Class65.aClass100_658.aClass534_Sub40_Sub1_1179
				.anInt10811
				= 0;
			    byte[] is
				= new byte[Class605.anInt7980 * 608275859 + 1];
			    Class65.aClass100_658.aClass534_Sub40_Sub1_1179
				.method18291
				(is, 0, Class605.anInt7980 * 608275859,
				 (byte) 1);
			    Class65.aClass100_658.aClass534_Sub40_Sub1_1179
				.anInt10811
				= 0;
			    Class534_Sub40 class534_sub40
				= new Class534_Sub40(is);
			    String string
				= class534_sub40.method16541((byte) -37);
			    Class468.method7622(string, true,
						client.aBool11032, 1411490484);
			    Class522.method8720((Class581.anInt7765
						 * 1654597771),
						-2038137932);
			    if (Class680.anInt8668 * 513656689 == 131
				&& client.anInt11039 * -1850530127 != 14)
				Class65.anInt692 = 1305853638;
			    else {
				Class65.anInt692 = 850224666;
				Class65.aClass100_658.method1866((byte) -9);
				Class275.method5151(888971383);
			    }
			}
		    } else {
			if (253 == Class65.anInt692 * 1114067909) {
			    if (!Class65.aClass100_658.method1867
				     (-1903826796).method8977(2, (byte) 1))
				break;
			    Class65.aClass100_658.method1867(2029051237)
				.method8969
				((Class65.aClass100_658
				  .aClass534_Sub40_Sub1_1179.aByteArray10810),
				 0, 2, (byte) 58);
			    Class65.aClass100_658.aClass534_Sub40_Sub1_1179
				.anInt10811
				= 0;
			    Class65.aClass100_658.anInt1197
				= Class65.aClass100_658
				      .aClass534_Sub40_Sub1_1179
				      .method16529((byte) 1) * -1220818213;
			    Class65.anInt692 = -2034011748;
			}
			if (1114067909 * Class65.anInt692 == 268) {
			    if (!Class65.aClass100_658.method1867
				     (331557505).method8977
				 ((-1013636781
				   * Class65.aClass100_658.anInt1197),
				  (byte) 1))
				break;
			    Class65.aClass100_658.method1867(406688001)
				.method8969
				((Class65.aClass100_658
				  .aClass534_Sub40_Sub1_1179.aByteArray10810),
				 0,
				 -1013636781 * Class65.aClass100_658.anInt1197,
				 (byte) 101);
			    Class65.aClass100_658.aClass534_Sub40_Sub1_1179
				.anInt10811
				= 0;
			    boolean bool = (Class65.aClass100_658
						.aClass534_Sub40_Sub1_1179
						.method16527(-1020350682)
					    == 1);
			    while (((Class65.aClass100_658
				     .aClass534_Sub40_Sub1_1179.anInt10811)
				    * 31645619)
				   < (-1013636781
				      * Class65.aClass100_658.anInt1197)) {
				Class429 class429
				    = (Class407.aClass110_Sub1_Sub2_4312
					   .method14495
				       ((Class65.aClass100_658
					 .aClass534_Sub40_Sub1_1179),
					(byte) -99));
				Class77.aClass155_Sub1_819
				    .anInterface4_1742.method29
				    (class429.anInt4820 * -608978823,
				     class429.anObject4819, (short) -22822);
			    }
			    if (bool) {
				Class534_Sub19 class534_sub19
				    = Class56.method1225(1658108383);
				Class534_Sub40_Sub1 class534_sub40_sub1
				    = (class534_sub19
				       .aClass534_Sub40_Sub1_10513);
				class534_sub40_sub1.method16506
				    ((-100453045
				      * Class413.aClass413_4649.anInt4648),
				     357013528);
				Class65.aClass100_658
				    .method1863(class534_sub19, (byte) 112);
				Class65.aClass100_658.method1868(1805858475);
				Class65.anInt692 = 1555590107;
			    } else
				Class65.anInt692 = 179237849;
			}
			if (Class65.anInt692 * 1114067909 == 135) {
			    if (!Class65.aClass100_658.method1867
				     (-1703472701).method8977(1, (byte) 1))
				break;
			    Class65.aClass100_658.method1867(353522028)
				.method8969
				((Class65.aClass100_658
				  .aClass534_Sub40_Sub1_1179.aByteArray10810),
				 0, 1, (byte) 94);
			    int i_14_ = ((Class65.aClass100_658
					  .aClass534_Sub40_Sub1_1179
					  .aByteArray10810[0])
					 & 0xff);
			    if (2 != i_14_) {
				if (i_14_ == 29 || i_14_ == 45) {
				    Class581.anInt7765 = 1894907683 * i_14_;
				    Class65.anInt692 = 2142750977;
				} else {
				    Class65.anInt692 = 850224666;
				    Class522.method8720(i_14_, -2114022718);
				    Class65.aClass100_658
					.method1866((byte) -66);
				    Class275.method5151(977216653);
				    if (Class192.method3789((-1850530127
							     * (client
								.anInt11039)),
							    -1649615463)) {
					Class622.method10291(true, 337617244);
					Class65.anInt701 = 1385443459 * i_14_;
				    }
				    break;
				}
				break;
			    }
			    Class65.anInt692 = -1507884156;
			}
			if (148 == 1114067909 * Class65.anInt692) {
			    if (!Class65.aClass100_658.method1867
				     (-1690558833).method8977(1, (byte) 1))
				break;
			    Class65.aClass100_658.method1867(1904065027)
				.method8969
				((Class65.aClass100_658
				  .aClass534_Sub40_Sub1_1179.aByteArray10810),
				 0, 1, (byte) 33);
			    Class534_Sub41.anInt10827
				= 2019631559 * ((Class65.aClass100_658
						 .aClass534_Sub40_Sub1_1179
						 .aByteArray10810[0])
						& 0xff);
			    Class65.anInt692 = 1893014508;
			}
			if (156 == 1114067909 * Class65.anInt692) {
			    Class534_Sub40_Sub1 class534_sub40_sub1
				= (Class65.aClass100_658
				   .aClass534_Sub40_Sub1_1179);
			    if (220 == 513656689 * Class680.anInt8668) {
				if (!Class65.aClass100_658.method1867
					 (2088750727).method8977
				     (-666611721 * Class534_Sub41.anInt10827,
				      (byte) 1))
				    break;
				Class65.aClass100_658.method1867
				    (-1512138275).method8969
				    (class534_sub40_sub1.aByteArray10810, 0,
				     -666611721 * Class534_Sub41.anInt10827,
				     (byte) 66);
				class534_sub40_sub1.anInt10811 = 0;
				Class489.method8003(class534_sub40_sub1,
						    (byte) 42);
				client.anInt11194
				    = (class534_sub40_sub1
					   .method16527(-578891413)
				       * 1147858925);
				client.anInt11195
				    = (class534_sub40_sub1
					   .method16527(-520418507)
				       * -1286726269);
				client.aBool11223
				    = class534_sub40_sub1
					  .method16527(-1610822808) == 1;
				client.aBool11196
				    = class534_sub40_sub1
					  .method16527(-593346054) == 1;
				client.aBool11198
				    = class534_sub40_sub1
					  .method16527(1051029937) == 1;
				client.aBool11076
				    = class534_sub40_sub1
					  .method16527(-1854669543) == 1;
				client.anInt11037
				    = class534_sub40_sub1
					  .method16529((byte) 1) * -2083494349;
				client.aBool11296
				    = class534_sub40_sub1
					  .method16527(-274480181) == 1;
				Class200_Sub4.anInt9897
				    = (class534_sub40_sub1
					   .method16532(-676065872)
				       * -1278504939);
				client.aBool11157
				    = class534_sub40_sub1
					  .method16527(421280299) == 1;
				Class114.aString1397
				    = class534_sub40_sub1
					  .method16541((byte) -75);
				Class212.aLong2274
				    = ((class534_sub40_sub1
					    .method16693(718066982)
					- Class250.method4604((byte) -89))
				       * -886165369587119355L);
				client.aClass512_11100.method8428
				    (-1486655428)
				    .method17353(client.aBool11157, (byte) 11);
				Class159.aClass509_1754.method8390
				    ((byte) -48).method8428
				    (-1486655428)
				    .method17353(client.aBool11157, (byte) 11);
				Class531.aClass44_Sub7_7135
				    .method17308(client.aBool11157, 65280);
				Class578.aClass44_Sub3_7743
				    .method17249(client.aBool11157, (byte) 50);
			    } else {
				if (!Class65.aClass100_658.method1867
					 (-176560507).method8977
				     (-666611721 * Class534_Sub41.anInt10827,
				      (byte) 1))
				    break;
				Class65.aClass100_658.method1867
				    (-818718187).method8969
				    (class534_sub40_sub1.aByteArray10810, 0,
				     -666611721 * Class534_Sub41.anInt10827,
				     (byte) 78);
				class534_sub40_sub1.anInt10811 = 0;
				Class489.method8003(class534_sub40_sub1,
						    (byte) 109);
				client.anInt11194
				    = (class534_sub40_sub1
					   .method16527(-668417352)
				       * 1147858925);
				client.anInt11195
				    = (class534_sub40_sub1
					   .method16527(1168930063)
				       * -1286726269);
				client.aBool11223
				    = class534_sub40_sub1
					  .method16527(-1814762554) == 1;
				Class200_Sub4.anInt9897
				    = (class534_sub40_sub1
					   .method16532(-676065872)
				       * -1278504939);
				Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419
				    .aByte12214
				    = (byte) class534_sub40_sub1
						 .method16527(-1733613769);
				client.aBool11196
				    = class534_sub40_sub1
					  .method16527(588025953) == 1;
				client.aBool11198
				    = class534_sub40_sub1
					  .method16527(2098850812) == 1;
				Class531.aLong7136
				    = (class534_sub40_sub1
					   .method16537(1359621443)
				       * 5369997409366051919L);
				Class212.aLong2274
				    = (((-4861593676329809233L
					 * Class531.aLong7136)
					- Class250.method4604((byte) -93)
					- class534_sub40_sub1
					      .method16535(16847821))
				       * -886165369587119355L);
				int i_15_ = class534_sub40_sub1
						.method16527(-756557085);
				client.aBool11296 = 0 != (i_15_ & 0x1);
				Class233.aBool2359 = 0 != (i_15_ & 0x2);
				Class492.anInt5342
				    = (class534_sub40_sub1
					   .method16533(-258848859)
				       * -1461317767);
				Class221.anInt2310
				    = (class534_sub40_sub1
					   .method16533(-258848859)
				       * -2075541409);
				Class280.anInt3059
				    = class534_sub40_sub1
					  .method16529((byte) 1) * 1442610963;
				Class328.anInt3478
				    = class534_sub40_sub1
					  .method16529((byte) 1) * 1217957533;
				Class618.anInt8102
				    = class534_sub40_sub1
					  .method16529((byte) 1) * 812300401;
				Class460_Sub1.anInt10346
				    = (class534_sub40_sub1
					   .method16533(-258848859)
				       * -650478585);
				Class376.aClass43_3911
				    = new Class43(Class460_Sub1.anInt10346
						  * -1712574025);
				new Thread(Class376.aClass43_3911).start();
				Class633.anInt8278
				    = (class534_sub40_sub1
					   .method16527(-686319123)
				       * 1860026049);
				Class207.anInt2239
				    = class534_sub40_sub1
					  .method16529((byte) 1) * 1168569291;
				Class578.anInt7744
				    = class534_sub40_sub1
					  .method16529((byte) 1) * -360849473;
				Class298_Sub1.aBool10063
				    = class534_sub40_sub1
					  .method16527(967594081) == 1;
				Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419
				    .aString12211
				    = Class322
					  .aClass654_Sub1_Sub5_Sub1_Sub2_3419
					  .aString12228
				    = RuntimeException_Sub1.aString12085
				    = class534_sub40_sub1
					  .method16523(-1600462726);
				Class99.anInt1178
				    = (class534_sub40_sub1
					   .method16527(1053335833)
				       * 1198047845);
				Class473.anInt5174
				    = (class534_sub40_sub1
					   .method16533(-258848859)
				       * -2104794943);
				Class5.aClass23_43 = new RSSocket();
				Class5.aClass23_43.anInt227
				    = class534_sub40_sub1
					  .method16529((byte) 1) * 1619197921;
				if (65535 == (-1664252895
					      * Class5.aClass23_43.anInt227))
				    Class5.aClass23_43.anInt227 = -1619197921;
				Class5.aClass23_43.aString223
				    = class534_sub40_sub1
					  .method16523(-1596231822);
				Class5.aClass23_43.anInt222
				    = class534_sub40_sub1
					  .method16529((byte) 1) * 1852523987;
				Class5.aClass23_43.anInt225
				    = class534_sub40_sub1
					  .method16529((byte) 1) * -102059163;
				if ((client.aClass665_11211
				     != Class665.aClass665_8559)
				    && ((Class665.aClass665_8572
					 != client.aClass665_11211)
					|| 365872613 * client.anInt11194 < 2)
				    && (Class685.aClass23_8698.method819
					(Class331.aClass23_3498, 1039742086)))
				    Class321.method5774((byte) 0);
			    }
			    if (client.aBool11198) {
				try {
				    Class31.method887(Class305.anApplet3271,
						      "zap", -1654297837);
				} catch (Throwable throwable) {
				    if (client.aBool11097) {
					try {
					    Class305.anApplet3271
						.getAppletContext
						().showDocument
						(new URL(Class305
							     .anApplet3271
							     .getCodeBase(),
							 "blank.ws"),
						 "tbi");
					} catch (Exception exception) {
					    /* empty */
					}
				    }
				}
			    } else {
				try {
				    Class31.method887(Class305.anApplet3271,
						      "unzap", -1165796051);
				} catch (Throwable throwable) {
				    /* empty */
				}
			    }
			    if (Class665.aClass665_8561
				== client.aClass665_11211)
				Class403.aClass403_4139.method6601(421734097);
			    if (220 == 513656689 * Class680.anInt8668)
				Class65.anInt692 = -1639416054;
			    else {
				Class65.anInt692 = 850224666;
				Class522.method8720(2, -2086175614);
				Class638.method10568(-16777216);
				Class673.method11110(9, -1095663022);
				Class65.aClass100_658.aClass409_1186 = null;
				break;
			    }
			}
			if (Class65.anInt692 * 1114067909 == 178) {
			    if (!Class65.aClass100_658.method1867
				     (-994277000).method8977(3, (byte) 1))
				break;
			    Class65.aClass100_658.method1867(-15238886)
				.method8969
				((Class65.aClass100_658
				  .aClass534_Sub40_Sub1_1179.aByteArray10810),
				 0, 3, (byte) 110);
			    Class65.anInt692 = 486145611;
			}
			if (1114067909 * Class65.anInt692 == 183) {
			    Class534_Sub40_Sub1 class534_sub40_sub1
				= (Class65.aClass100_658
				   .aClass534_Sub40_Sub1_1179);
			    class534_sub40_sub1.anInt10811 = 0;
			    if (class534_sub40_sub1.method18302(469188942)) {
				if (!Class65.aClass100_658.method1867
					 (-469963738).method8977(1, (byte) 1))
				    break;
				Class65.aClass100_658.method1867(-31757569)
				    .method8969
				    (class534_sub40_sub1.aByteArray10810, 3, 1,
				     (byte) 99);
			    }
			    Class65.aClass100_658.aClass409_1186
				= (Class617.method10231(-1918480052)
				   [class534_sub40_sub1
					.method18313(-1352610328)]);
			    Class65.aClass100_658.anInt1197
				= (class534_sub40_sub1.method16529((byte) 1)
				   * -1220818213);
			    Class65.anInt692 = 1849170542;
			}
			if (1114067909 * Class65.anInt692 == 166) {
			    if (Class65.aClass100_658.method1867(182547972)
				    .method8977
				(Class65.aClass100_658.anInt1197 * -1013636781,
				 (byte) 1)) {
				Class65.aClass100_658.method1867
				    (-1919406385).method8969
				    ((Class65.aClass100_658
				      .aClass534_Sub40_Sub1_1179
				      .aByteArray10810),
				     0,
				     (Class65.aClass100_658.anInt1197
				      * -1013636781),
				     (byte) 68);
				Class65.aClass100_658
				    .aClass534_Sub40_Sub1_1179.anInt10811
				    = 0;
				int i_16_ = (Class65.aClass100_658.anInt1197
					     * -1013636781);
				Class65.anInt692 = 850224666;
				Class522.method8720(2, -2069199903);
				Class537.method8910(-2046296794);
				Class504.method8325
				    ((Class65.aClass100_658
				      .aClass534_Sub40_Sub1_1179),
				     (byte) 9);
				int i_17_
				    = (i_16_
				       - 31645619 * (Class65.aClass100_658
						     .aClass534_Sub40_Sub1_1179
						     .anInt10811));
				Class534_Sub40_Sub1 class534_sub40_sub1
				    = new Class534_Sub40_Sub1(i_17_);
				System.arraycopy((Class65.aClass100_658
						  .aClass534_Sub40_Sub1_1179
						  .aByteArray10810),
						 (31645619
						  * (Class65.aClass100_658
						     .aClass534_Sub40_Sub1_1179
						     .anInt10811)),
						 (class534_sub40_sub1
						  .aByteArray10810),
						 0, i_17_);
				Class65.aClass100_658
				    .aClass534_Sub40_Sub1_1179.anInt10811
				    += -1387468933 * i_17_;
				if (Class65.aClass100_658.aClass409_1186
				    == Class409.aClass409_4458)
				    client.aClass512_11100.method8442
					(new Class511(Class499.aClass499_5598,
						      class534_sub40_sub1),
					 514369498);
				else
				    client.aClass512_11100.method8442
					(new Class511(Class499.aClass499_5595,
						      class534_sub40_sub1),
					 1191906078);
				if (i_16_ != (Class65.aClass100_658
					      .aClass534_Sub40_Sub1_1179
					      .anInt10811) * 31645619)
				    throw new RuntimeException
					      (new StringBuilder().append
						   (31645619
						    * (Class65.aClass100_658
						       .aClass534_Sub40_Sub1_1179
						       .anInt10811))
						   .append
						   (" ").append
						   (i_16_).toString());
				Class65.aClass100_658.aClass409_1186 = null;
			    }
			} else {
			    if (1114067909 * Class65.anInt692 != 203)
				break;
			    if (-2 == (-1013636781
				       * Class65.aClass100_658.anInt1197)) {
				if (!Class65.aClass100_658.method1867
					 (815734532).method8977(2, (byte) 1))
				    break;
				Class65.aClass100_658.method1867(618981591)
				    .method8969
				    ((Class65.aClass100_658
				      .aClass534_Sub40_Sub1_1179
				      .aByteArray10810),
				     0, 2, (byte) 94);
				Class65.aClass100_658
				    .aClass534_Sub40_Sub1_1179.anInt10811
				    = 0;
				Class65.aClass100_658.anInt1197
				    = Class65.aClass100_658
					  .aClass534_Sub40_Sub1_1179
					  .method16529((byte) 1) * -1220818213;
			    }
			    if (Class65.aClass100_658.method1867
				    (1560074851).method8977
				(-1013636781 * Class65.aClass100_658.anInt1197,
				 (byte) 1)) {
				Class65.aClass100_658.method1867(468308475)
				    .method8969
				    ((Class65.aClass100_658
				      .aClass534_Sub40_Sub1_1179
				      .aByteArray10810),
				     0,
				     (Class65.aClass100_658.anInt1197
				      * -1013636781),
				     (byte) 5);
				Class65.aClass100_658
				    .aClass534_Sub40_Sub1_1179.anInt10811
				    = 0;
				int i_18_
				    = (-1013636781
				       * Class65.aClass100_658.anInt1197);
				Class65.anInt692 = 850224666;
				Class522.method8720(15, -2048696872);
				Class413.method6733(-671790920);
				Class504.method8325
				    ((Class65.aClass100_658
				      .aClass534_Sub40_Sub1_1179),
				     (byte) 9);
				if (i_18_ != (Class65.aClass100_658
					      .aClass534_Sub40_Sub1_1179
					      .anInt10811) * 31645619)
				    throw new RuntimeException
					      (new StringBuilder().append
						   ((Class65.aClass100_658
						     .aClass534_Sub40_Sub1_1179
						     .anInt10811) * 31645619)
						   .append
						   (" ").append
						   (i_18_).toString());
				Class65.aClass100_658.aClass409_1186 = null;
			    }
			}
		    }
		} catch (IOException ioexception) {
		    Class65.aClass100_658.method1866((byte) -4);
		    if (804115323 * Class65.anInt678 < 3) {
			if (220 == 513656689 * Class680.anInt8668)
			    Class685.aClass23_8698.method818((byte) -34);
			else
			    Class5.aClass23_49.method818((byte) -68);
			Class65.anInt698 = 0;
			Class65.anInt678 += -585559117;
			Class65.anInt692 = -937912598;
		    } else {
			Class65.anInt692 = 850224666;
			Class522.method8720(-4, -2057319192);
			Class275.method5151(219392819);
		    }
		    break;
		}
		break;
	    } while (false);
	}
    }
    
    static Class264[] method10719(int i) {
	return (new Class264[]
		{ Class264.aClass264_2922, Class264.aClass264_2805,
		  Class264.aClass264_2806, Class264.aClass264_2807,
		  Class264.aClass264_2808, Class264.aClass264_2849,
		  Class264.aClass264_2810, Class264.aClass264_2811,
		  Class264.aClass264_2812, Class264.aClass264_2919,
		  Class264.aClass264_2814, Class264.aClass264_2925,
		  Class264.aClass264_2816, Class264.aClass264_2846,
		  Class264.aClass264_2830, Class264.aClass264_2819,
		  Class264.aClass264_2820, Class264.aClass264_2821,
		  Class264.aClass264_2866, Class264.aClass264_2823,
		  Class264.aClass264_2813, Class264.aClass264_2881,
		  Class264.aClass264_2857, Class264.aClass264_2898,
		  Class264.aClass264_2926, Class264.aClass264_2829,
		  Class264.aClass264_2815, Class264.aClass264_2831,
		  Class264.aClass264_2832, Class264.aClass264_2833,
		  Class264.aClass264_2920, Class264.aClass264_2835,
		  Class264.aClass264_2836, Class264.aClass264_2837,
		  Class264.aClass264_2826, Class264.aClass264_2839,
		  Class264.aClass264_2840, Class264.aClass264_2841,
		  Class264.aClass264_2825, Class264.aClass264_2843,
		  Class264.aClass264_2895, Class264.aClass264_2845,
		  Class264.aClass264_2897, Class264.aClass264_2838,
		  Class264.aClass264_2848, Class264.aClass264_2827,
		  Class264.aClass264_2850, Class264.aClass264_2858,
		  Class264.aClass264_2852, Class264.aClass264_2853,
		  Class264.aClass264_2854, Class264.aClass264_2855,
		  Class264.aClass264_2856, Class264.aClass264_2887,
		  Class264.aClass264_2884, Class264.aClass264_2859,
		  Class264.aClass264_2860, Class264.aClass264_2861,
		  Class264.aClass264_2862, Class264.aClass264_2863,
		  Class264.aClass264_2864, Class264.aClass264_2865,
		  Class264.aClass264_2818, Class264.aClass264_2804,
		  Class264.aClass264_2868, Class264.aClass264_2869,
		  Class264.aClass264_2870, Class264.aClass264_2878,
		  Class264.aClass264_2872, Class264.aClass264_2873,
		  Class264.aClass264_2874, Class264.aClass264_2828,
		  Class264.aClass264_2876, Class264.aClass264_2877,
		  Class264.aClass264_2890, Class264.aClass264_2842,
		  Class264.aClass264_2880, Class264.aClass264_2867,
		  Class264.aClass264_2882, Class264.aClass264_2883,
		  Class264.aClass264_2847, Class264.aClass264_2885,
		  Class264.aClass264_2886, Class264.aClass264_2879,
		  Class264.aClass264_2888, Class264.aClass264_2889,
		  Class264.aClass264_2817, Class264.aClass264_2891,
		  Class264.aClass264_2892, Class264.aClass264_2893,
		  Class264.aClass264_2910, Class264.aClass264_2894,
		  Class264.aClass264_2896, Class264.aClass264_2903,
		  Class264.aClass264_2851, Class264.aClass264_2907,
		  Class264.aClass264_2900, Class264.aClass264_2901,
		  Class264.aClass264_2902, Class264.aClass264_2899,
		  Class264.aClass264_2904, Class264.aClass264_2905,
		  Class264.aClass264_2906, Class264.aClass264_2844,
		  Class264.aClass264_2908, Class264.aClass264_2834,
		  Class264.aClass264_2875, Class264.aClass264_2911,
		  Class264.aClass264_2912, Class264.aClass264_2913,
		  Class264.aClass264_2914, Class264.aClass264_2915,
		  Class264.aClass264_2916, Class264.aClass264_2917,
		  Class264.aClass264_2918, Class264.aClass264_2927,
		  Class264.aClass264_2822, Class264.aClass264_2921,
		  Class264.aClass264_2809, Class264.aClass264_2923,
		  Class264.aClass264_2924, Class264.aClass264_2824,
		  Class264.aClass264_2909, Class264.aClass264_2871,
		  Class264.aClass264_2928 });
    }
    
    static final void method10720(Class669 class669, int i) {
	if (Class713.aBool8884 && Class29.aFrame266 != null)
	    Class159.method2611(Class44_Sub6.aClass534_Sub35_10989
				    .aClass690_Sub8_10775
				    .method16947(666374095),
				-1, -1, false, -1184177054);
	if (Class309.method5657((byte) 39) == Class508.aClass508_5669) {
	    Class568.method9574(-1977670688);
	    System.exit(0);
	} else
	    Class200.method3856((byte) 49);
    }
    
    static final void method10721(Class669 class669, byte i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = (Class599.aClass298_Sub1_7871.method5388((byte) 44) == null ? -1
	       : (Class599.aClass298_Sub1_7871.method5388((byte) -16).anInt3269
		  * -1861794503));
    }
    
    public static String method10722(int i) {
	return Class504.aFile5631.getAbsolutePath();
    }
    
    static final void method10723(Class669 class669, int i) {
	int i_19_ = (class669.anIntArray8595
		     [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_19_, -17423232);
	Class243 class243 = Class44_Sub11.aClass243Array11006[i_19_ >> 16];
	Class509.method8393(class247, class243, class669, 1635068062);
    }
    
    public static Class595[] method10724(int i) {
	return (new Class595[]
		{ Class595.aClass595_7850, Class595.aClass595_7830,
		  Class595.aClass595_7842, Class595.aClass595_7839,
		  Class595.aClass595_7843, Class595.aClass595_7829,
		  Class595.aClass595_7837, Class595.aClass595_7851,
		  Class595.aClass595_7840, Class595.aClass595_7847,
		  Class595.aClass595_7844, Class595.aClass595_7833,
		  Class595.aClass595_7849, Class595.aClass595_7832,
		  Class595.aClass595_7834, Class595.aClass595_7841,
		  Class595.aClass595_7838, Class595.aClass595_7831,
		  Class595.aClass595_7846, Class595.aClass595_7836,
		  Class595.aClass595_7835, Class595.aClass595_7845,
		  Class595.aClass595_7848 });
    }
}
