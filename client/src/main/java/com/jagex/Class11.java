/* Class11 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class11 implements Interface76
{
    public int anInt76;
    public static Class11 aClass11_77;
    public static Class11 aClass11_78;
    public static Class11 aClass11_79;
    public static Class11 aClass11_80 = new Class11(2, 0);
    int anInt81;
    
    static {
	aClass11_79 = new Class11(0, 1);
	aClass11_78 = new Class11(1, 2);
	aClass11_77 = new Class11(3, 3);
    }
    
    public int method93() {
	return -589672693 * anInt81;
    }
    
    Class11(int i, int i_0_) {
	anInt76 = -354486817 * i;
	anInt81 = i_0_ * -1887796061;
    }
    
    public int method22() {
	return -589672693 * anInt81;
    }
    
    public int method53() {
	return -589672693 * anInt81;
    }
    
    static final void method610(Class669 class669, int i) {
	Class227.method4181(class669,
			    Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419,
			    (byte) -69);
    }
    
    public static final void method611(boolean bool, int i) {
	if (!bool) {
	    Class555.aClass44_Sub16_7417.method1080(65280);
	    Class88.aClass44_Sub12_884.method1080(65280);
	    Class307.aClass44_Sub15_3349.method1080(65280);
	    client.aClass512_11100.method8428(-1486655428).method1080(65280);
	    Class578.aClass44_Sub3_7743.method1080(65280);
	    Class531.aClass44_Sub7_7135.method1080(65280);
	    Class200_Sub12.aClass44_Sub1_9934.method1080(65280);
	    Class55.aClass44_Sub4_447.method1080(65280);
	    Class84.aClass44_Sub11_840.method1080(65280);
	    Class562.aClass110_Sub1_Sub1_7560.method17853(-2135090181);
	    Class535.aClass110_Sub1_Sub2_7162.method17882((short) 20618);
	    Class78.aClass110_Sub1_Sub2_826.method17882((short) 18042);
	    Class279.aClass110_Sub1_Sub2_3053.method17882((short) -3805);
	    Class522.aClass110_Sub1_Sub2_7083.method17882((short) 7359);
	    Class534_Sub24.aClass110_Sub1_Sub2_10565
		.method17882((short) 12912);
	    Class200_Sub23.aClass44_Sub14_10041.method1080(65280);
	    Class394_Sub1.aClass44_Sub18_10148.method1080(65280);
	    Class222.aClass44_Sub9_2313.method1080(65280);
	    Class534_Sub11_Sub13.aClass44_Sub22_11730.method1080(65280);
	    Class184.aClass44_Sub6_1988.method1080(65280);
	    Class492.aClass44_Sub10_5341.method1080(65280);
	    Class200_Sub10.aClass44_Sub20_9926.method1080(65280);
	    Class650.aClass44_Sub5_8464.method1080(65280);
	    Class632.aClass44_Sub2_8270.method1080(65280);
	    Class470.aClass44_Sub17_5153.method1080(65280);
	    Class663.method10997(-472960267);
	    Class106.method1944(-1607425120);
	    Class351.aClass406_3620.method6652((byte) 1);
	    Class560.method9434(926998689);
	    if (client.aClass665_11211 != Class665.aClass665_8561) {
		for (int i_1_ = 0; i_1_ < Class305.aByteArrayArray3272.length;
		     i_1_++)
		    Class305.aByteArrayArray3272[i_1_] = null;
		client.anInt11046 = 0;
	    }
	    client.aClass512_11100.method8501((byte) -78)
		.method10147(1306630125);
	    Class560.method9431(22153643);
	    Class177.method2934((short) 6529);
	    Class269.aClass396_2956.method6568((byte) 94);
	    Class48.aClass387_363.method6500(593576122);
	    Class679.aClass203_8660.method3877(-1731014653);
	    client.aClass203_11078.method3877(574760943);
	    client.aClass203_11079.method3877(-174864930);
	    client.aClass203_11336.method3877(-427440316);
	    Class150_Sub1.aClass44_8902.method1080(65280);
	    Class706.aClass44_8845.method1080(65280);
	    client.aClass203_11082.method3877(1641045388);
	}
	Class255.method4650(-213765258);
	Class690_Sub37.method17195(1542061823);
    }
    
    public static int method612(Class665 class665, Class668 class668, int i,
				byte i_2_) {
	if (Class665.aClass665_8561 == class665)
	    return 43594;
	return i + 40000;
    }
    
    static final void method613(Class669 class669, int i) {
	int i_3_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub6_10743
		  .method14026(i_3_, -2024064741);
    }
    
    static final void method614(Class669 class669, int i) {
	int i_4_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_4_, 2015022154);
	Class243 class243 = Class44_Sub11.aClass243Array11006[i_4_ >> 16];
	Class36.method937(class247, class243, class669, (short) 256);
    }
    
    static final void method615(Class669 class669, int i) {
	Class318 class318
	    = (Class318) (class669.aClass534_Sub18_Sub8_8614.anObjectArray11753
			  [662605117 * class669.anInt8613]);
	Interface19 interface19
	    = ((Interface19)
	       (0 == class669.anIntArray8591[662605117 * class669.anInt8613]
		? class669.aMap8607.get(class318.aClass150_3392.aClass453_1695)
		: class669.aMap8608
		      .get(class318.aClass150_3392.aClass453_1695)));
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = interface19.method119(class318, -1425362079);
    }
}
