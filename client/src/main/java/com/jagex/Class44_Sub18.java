/* Class44_Sub18 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class44_Sub18 extends Class44
{
    public static final int anInt11011 = 64;
    public static final int anInt11012 = 64;
    
    public void method1091() {
	super.method1085(1909442989);
	((Class75) anInterface6_326).method1583((byte) 0);
    }
    
    public void method17359(int i, int i_0_, int i_1_) {
	super.method1079(i, 200075749);
	((Class75) anInterface6_326).method1575(i_0_, (byte) 30);
    }
    
    public void method1080(int i) {
	super.method1080(65280);
	((Class75) anInterface6_326).method1579(-1317860657);
    }
    
    public void method1081(int i, short i_2_) {
	super.method1081(i, (short) 19764);
	((Class75) anInterface6_326).method1576(i, 8995604);
    }
    
    public void method1089() {
	super.method1085(2022057245);
	((Class75) anInterface6_326).method1583((byte) 0);
    }
    
    public void method1077() {
	super.method1080(65280);
	((Class75) anInterface6_326).method1579(-344235085);
    }
    
    public void method1085(int i) {
	super.method1085(2095461929);
	((Class75) anInterface6_326).method1583((byte) 0);
    }
    
    public void method1076() {
	super.method1080(65280);
	((Class75) anInterface6_326).method1579(-691573251);
    }
    
    public Class44_Sub18(Class675 class675, Class672 class672,
			 Class472 class472, Class472 class472_3_) {
	super(class675, class672, class472, Class649.aClass649_8412, 64,
	      new Class75_Sub1(class472_3_, 64));
    }
    
    public void method1098() {
	super.method1080(65280);
	((Class75) anInterface6_326).method1579(-1040657398);
    }
    
    public void method1092(int i) {
	super.method1081(i, (short) 17840);
	((Class75) anInterface6_326).method1576(i, -816158843);
    }
    
    public void method1087() {
	super.method1085(1885236230);
	((Class75) anInterface6_326).method1583((byte) 0);
    }
    
    public void method1084(int i) {
	super.method1081(i, (short) 17255);
	((Class75) anInterface6_326).method1576(i, 1033999932);
    }
    
    public void method1078() {
	super.method1080(65280);
	((Class75) anInterface6_326).method1579(-351218379);
    }
    
    public void method1090() {
	super.method1085(1988353552);
	((Class75) anInterface6_326).method1583((byte) 0);
    }
    
    public void method1088() {
	super.method1085(2041312709);
	((Class75) anInterface6_326).method1583((byte) 0);
    }
    
    static final void method17360(Class669 class669, byte i) {
	int i_4_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class272 class272
	    = ((Class272)
	       Class222.aClass44_Sub9_2313.method91(i_4_, -643669360));
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = 1747122653 * class272.anInt2967;
    }
}
