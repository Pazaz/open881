/* Class200_Sub19 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class200_Sub19 extends Class200
{
    int anInt9979;
    int anInt9980;
    
    Class200_Sub19(Class534_Sub40 class534_sub40) {
	super(class534_sub40);
	anInt9980 = class534_sub40.method16529((byte) 1) * -1395211243;
	anInt9979 = class534_sub40.method16533(-258848859) * 1394269939;
    }
    
    public void method3845(int i) {
	client.anInt11238 = client.anInt11101 * 1445783593;
	client.anInt11114
	    = (1350888253 * anInt9980 + client.anInt11101) * 1672441741;
	Class690_Sub36.anInt10950 = client.anInt11166 * -114919765;
	Class482.anInt5270 = -1803007125 * client.anInt11116;
	Class383.anInt3943 = client.anInt11187 * -1592469857;
	Class396.anInt4107 = -631650605 * client.anInt11118;
	client.anInt11166 = (anInt9979 * -1324717509 >>> 24) * 1504160899;
	client.anInt11116
	    = -1213142739 * (-1324717509 * anInt9979 >>> 16 & 0xff);
	client.anInt11187
	    = (-1324717509 * anInt9979 >>> 8 & 0xff) * 1236159619;
	client.anInt11118 = (-1324717509 * anInt9979 & 0xff) * 994848893;
    }
    
    public void method3846() {
	client.anInt11238 = client.anInt11101 * 1445783593;
	client.anInt11114
	    = (1350888253 * anInt9980 + client.anInt11101) * 1672441741;
	Class690_Sub36.anInt10950 = client.anInt11166 * -114919765;
	Class482.anInt5270 = -1803007125 * client.anInt11116;
	Class383.anInt3943 = client.anInt11187 * -1592469857;
	Class396.anInt4107 = -631650605 * client.anInt11118;
	client.anInt11166 = (anInt9979 * -1324717509 >>> 24) * 1504160899;
	client.anInt11116
	    = -1213142739 * (-1324717509 * anInt9979 >>> 16 & 0xff);
	client.anInt11187
	    = (-1324717509 * anInt9979 >>> 8 & 0xff) * 1236159619;
	client.anInt11118 = (-1324717509 * anInt9979 & 0xff) * 994848893;
    }
    
    public void method3847() {
	client.anInt11238 = client.anInt11101 * 1445783593;
	client.anInt11114
	    = (1350888253 * anInt9980 + client.anInt11101) * 1672441741;
	Class690_Sub36.anInt10950 = client.anInt11166 * -114919765;
	Class482.anInt5270 = -1803007125 * client.anInt11116;
	Class383.anInt3943 = client.anInt11187 * -1592469857;
	Class396.anInt4107 = -631650605 * client.anInt11118;
	client.anInt11166 = (anInt9979 * -1324717509 >>> 24) * 1504160899;
	client.anInt11116
	    = -1213142739 * (-1324717509 * anInt9979 >>> 16 & 0xff);
	client.anInt11187
	    = (-1324717509 * anInt9979 >>> 8 & 0xff) * 1236159619;
	client.anInt11118 = (-1324717509 * anInt9979 & 0xff) * 994848893;
    }
    
    public static Class392 method15633(Class534_Sub40 class534_sub40, int i) {
	Class392 class392 = Class681.method13865(class534_sub40, (byte) 5);
	int i_0_ = class534_sub40.method16532(-676065872);
	return new Class392_Sub1(class392.anInt4077 * 1909682011,
				 class392.aClass401_4078,
				 class392.aClass391_4080,
				 -1151439181 * class392.anInt4079,
				 -963484815 * class392.anInt4081, i_0_);
    }
    
    public static Class534_Sub36 method15634(Class534_Sub36 class534_sub36,
					     int i) {
	synchronized (Class534_Sub36.aClass534_Sub36Array10794) {
	    if (0 == Class534_Sub36.anInt10795 * 2450901) {
		Class534_Sub36 class534_sub36_1_
		    = new Class534_Sub36(class534_sub36);
		return class534_sub36_1_;
	    }
	    Class534_Sub36.aClass534_Sub36Array10794
		[(Class534_Sub36.anInt10795 -= 1809361789) * 2450901]
		.method16466(class534_sub36, (byte) 1);
	    Class534_Sub36 class534_sub36_2_
		= (Class534_Sub36.aClass534_Sub36Array10794
		   [Class534_Sub36.anInt10795 * 2450901]);
	    return class534_sub36_2_;
	}
    }
}
