/* Class690_Sub21 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class690_Sub21 extends Class690
{
    static final int anInt10911 = 3;
    static final int anInt10912 = 0;
    static final int anInt10913 = 1;
    
    void method14020(int i, int i_0_) {
	anInt8753 = i * 1823770475;
    }
    
    public Class690_Sub21(int i, Class534_Sub35 class534_sub35) {
	super(i, class534_sub35);
    }
    
    public void method17065(byte i) {
	if (189295939 * anInt8753 < 0 || 189295939 * anInt8753 > 3)
	    anInt8753 = method14017(2128308794) * 1823770475;
    }
    
    int method14017(int i) {
	return 1;
    }
    
    void method14023(int i) {
	anInt8753 = i * 1823770475;
    }
    
    public void method17066() {
	if (189295939 * anInt8753 < 0 || 189295939 * anInt8753 > 3)
	    anInt8753 = method14017(2146537363) * 1823770475;
    }
    
    public int method17067(int i) {
	return anInt8753 * 189295939;
    }
    
    int method14021() {
	return 1;
    }
    
    int method14022() {
	return 1;
    }
    
    public Class690_Sub21(Class534_Sub35 class534_sub35) {
	super(class534_sub35);
    }
    
    void method14024(int i) {
	anInt8753 = i * 1823770475;
    }
    
    void method14025(int i) {
	anInt8753 = i * 1823770475;
    }
    
    int method14018() {
	return 1;
    }
    
    int method14027(int i) {
	return 3;
    }
    
    int method14028(int i) {
	return 3;
    }
    
    int method14029(int i) {
	return 3;
    }
    
    int method14030(int i) {
	return 3;
    }
    
    public void method17068() {
	if (189295939 * anInt8753 < 0 || 189295939 * anInt8753 > 3)
	    anInt8753 = method14017(2117337461) * 1823770475;
    }
    
    public void method17069() {
	if (189295939 * anInt8753 < 0 || 189295939 * anInt8753 > 3)
	    anInt8753 = method14017(2113279998) * 1823770475;
    }
    
    int method14026(int i, int i_1_) {
	return 3;
    }
    
    public void method17070() {
	if (189295939 * anInt8753 < 0 || 189295939 * anInt8753 > 3)
	    anInt8753 = method14017(2131699892) * 1823770475;
    }
    
    public int method17071() {
	return anInt8753 * 189295939;
    }
    
    public int method17072() {
	return anInt8753 * 189295939;
    }
    
    static final void method17073(Class669 class669, int i) {
	int i_2_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class15 class15
	    = (Class15) Class531.aClass44_Sub7_7135.method91(i_2_, 650049057);
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = class15.aBool129 ? 1 : 0;
    }
    
    public static void method17074(String string, int i, int i_3_) {
	if (Class5.aClass23_49 == null)
	    Class5.aClass23_49 = new RSSocket();
	Class5.aClass23_49.aString223 = string;
	Class5.aClass23_49.anInt227 = 1619197921 * (1099 + i);
	Class5.aClass23_49.anInt222
	    = Class11.method612(client.aClass665_11211,
				Class668.aClass668_8584,
				Class5.aClass23_49.anInt227 * -1664252895,
				(byte) -23) * 1852523987;
	Class5.aClass23_49.anInt225
	    = Class488.method7999(client.aClass665_11211,
				  Class668.aClass668_8584,
				  -1664252895 * Class5.aClass23_49.anInt227,
				  353134756) * -102059163;
    }
    
    static void method17075(short i) {
	Class534_Sub18_Sub6.aClass9_11685.method578((byte) -9);
	Class534_Sub18_Sub6.aClass696_11669.method14075(958896847);
	Class534_Sub18_Sub6.aClass696_11659.method14075(958896847);
    }
}
