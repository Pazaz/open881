/* Class200_Sub17 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class200_Sub17 extends Class200
{
    int anInt9966;
    int anInt9967;
    
    boolean method3849() {
	Class205 class205
	    = (Class205) Class200_Sub12.aClass44_Sub1_9934
			     .method91(421268609 * anInt9967, 1322945761);
	return class205.method3913(-1538620133);
    }
    
    public void method3845(int i) {
	Class195 class195
	    = Class539_Sub1.aClass195Array10326[-1837750051 * anInt9966];
	Class50.method1163(-1762601189 * class195.anInt2155,
			   class195.anInt2157 * -836779145,
			   class195.anInt2153 * -1455419505,
			   403227023 * class195.aClass595_2154.anInt7853,
			   847393323 * class195.aClass595_2154.anInt7852,
			   class195.anInt2158 * -1387240389,
			   421268609 * anInt9967, 0, (byte) -32);
    }
    
    boolean method3844(int i) {
	Class205 class205
	    = (Class205) Class200_Sub12.aClass44_Sub1_9934
			     .method91(421268609 * anInt9967, 609612618);
	return class205.method3913(-135956423);
    }
    
    public void method3846() {
	Class195 class195
	    = Class539_Sub1.aClass195Array10326[-1837750051 * anInt9966];
	Class50.method1163(-1762601189 * class195.anInt2155,
			   class195.anInt2157 * -836779145,
			   class195.anInt2153 * -1455419505,
			   403227023 * class195.aClass595_2154.anInt7853,
			   847393323 * class195.aClass595_2154.anInt7852,
			   class195.anInt2158 * -1387240389,
			   421268609 * anInt9967, 0, (byte) -94);
    }
    
    Class200_Sub17(Class534_Sub40 class534_sub40) {
	super(class534_sub40);
	anInt9966 = class534_sub40.method16529((byte) 1) * 1160638325;
	anInt9967 = class534_sub40.method16550((byte) -108) * 308294529;
    }
    
    public void method3847() {
	Class195 class195
	    = Class539_Sub1.aClass195Array10326[-1837750051 * anInt9966];
	Class50.method1163(-1762601189 * class195.anInt2155,
			   class195.anInt2157 * -836779145,
			   class195.anInt2153 * -1455419505,
			   403227023 * class195.aClass595_2154.anInt7853,
			   847393323 * class195.aClass595_2154.anInt7852,
			   class195.anInt2158 * -1387240389,
			   421268609 * anInt9967, 0, (byte) -100);
    }
    
    boolean method3848() {
	Class205 class205
	    = (Class205) Class200_Sub12.aClass44_Sub1_9934
			     .method91(421268609 * anInt9967, 1001754837);
	return class205.method3913(1573415964);
    }
    
    public static void method15616(int i, byte i_0_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(5, (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
}
