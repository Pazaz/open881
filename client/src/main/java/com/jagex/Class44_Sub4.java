/* Class44_Sub4 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class44_Sub4 extends Class44
{
    public void method17261(int i) {
	((Class670) anInterface6_326).method11033(i, (byte) -57);
    }
    
    public void method1091() {
	super.method1085(1958366602);
	((Class670) anInterface6_326).method11036((byte) -2);
    }
    
    public void method1080(int i) {
	super.method1080(65280);
	((Class670) anInterface6_326).method11034(1045476867);
    }
    
    public void method1081(int i, short i_0_) {
	super.method1081(i, (short) 26041);
	((Class670) anInterface6_326).method11032(i, 555273017);
    }
    
    public void method17262(int i) {
	((Class670) anInterface6_326).method11033(i, (byte) 96);
    }
    
    public void method1077() {
	super.method1080(65280);
	((Class670) anInterface6_326).method11034(1037951095);
    }
    
    public void method1085(int i) {
	super.method1085(2106310015);
	((Class670) anInterface6_326).method11036((byte) -60);
    }
    
    public void method1076() {
	super.method1080(65280);
	((Class670) anInterface6_326).method11034(1203079332);
    }
    
    public void method1078() {
	super.method1080(65280);
	((Class670) anInterface6_326).method11034(-1461058027);
    }
    
    public void method1084(int i) {
	super.method1081(i, (short) 10488);
	((Class670) anInterface6_326).method11032(i, 555273017);
    }
    
    public void method1092(int i) {
	super.method1081(i, (short) 1941);
	((Class670) anInterface6_326).method11032(i, 555273017);
    }
    
    public void method1087() {
	super.method1085(1879083304);
	((Class670) anInterface6_326).method11036((byte) -94);
    }
    
    public void method1098() {
	super.method1080(65280);
	((Class670) anInterface6_326).method11034(344264704);
    }
    
    public void method1089() {
	super.method1085(1882031938);
	((Class670) anInterface6_326).method11036((byte) -30);
    }
    
    public void method1090() {
	super.method1085(2029312067);
	((Class670) anInterface6_326).method11036((byte) -54);
    }
    
    public void method1088() {
	super.method1085(2108252705);
	((Class670) anInterface6_326).method11036((byte) -29);
    }
    
    public Class44_Sub4(Class675 class675, Class672 class672,
			Class472 class472, Class472 class472_1_) {
	super(class675, class672, class472, Class649.aClass649_8391, 64,
	      new Class670_Sub1(class472_1_));
    }
    
    public void method17263(int i, int i_2_) {
	((Class670) anInterface6_326).method11033(i, (byte) 64);
    }
    
    public void method17264(int i) {
	((Class670) anInterface6_326).method11033(i, (byte) -13);
    }
    
    static final void method17265(Class669 class669, int i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class243 class243 = class666.aClass243_8575;
	Class603.method10026(class247, class243, class669, 951316307);
    }
    
    public static void method17266(int i, int i_3_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(9, (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
}
