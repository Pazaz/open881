/* Class26 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class26 implements Interface76
{
    public static Class26 aClass26_233;
    static Class26 aClass26_234;
    static Class26 aClass26_235;
    public static Class26 aClass26_236;
    static Class26 aClass26_237 = new Class26(0);
    static Class26 aClass26_238;
    static Class26 aClass26_239;
    static Class26 aClass26_240;
    public static Class26 aClass26_241;
    public static Class26 aClass26_242;
    public static Class26 aClass26_243;
    static Class26 aClass26_244;
    public static Class26 aClass26_245;
    public static Class26 aClass26_246;
    int anInt247;
    static int anInt248;
    
    static {
	aClass26_234 = new Class26(1);
	aClass26_235 = new Class26(2);
	aClass26_236 = new Class26(3);
	aClass26_246 = new Class26(4);
	aClass26_238 = new Class26(5);
	aClass26_239 = new Class26(6);
	aClass26_240 = new Class26(7);
	aClass26_241 = new Class26(8);
	aClass26_242 = new Class26(9);
	aClass26_245 = new Class26(10);
	aClass26_244 = new Class26(11);
	aClass26_243 = new Class26(12);
	aClass26_233 = new Class26(13);
    }
    
    public int method93() {
	return -399954881 * anInt247;
    }
    
    Class26(int i) {
	anInt247 = -1100168257 * i;
    }
    
    public int method22() {
	return -399954881 * anInt247;
    }
    
    public int method53() {
	return -399954881 * anInt247;
    }
    
    static int method858(Class472 class472, Class649 class649, int i) {
	if (class472 != null) {
	    if (class649.method10707((byte) 7) > 1) {
		int i_0_ = class472.method7679(1554434211) - 1;
		return (i_0_ * class649.method10707((byte) 91)
			+ class472.method7726(i_0_, (byte) 125));
	    }
	    return class472.method7726(class649.anInt8385 * 1570156841,
				       (byte) 30);
	}
	return 0;
    }
}
