/* Class690_Sub8 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class690_Sub8 extends Class690
{
    public static final int anInt10870 = 2;
    public static final int anInt10871 = 3;
    public static final int anInt10872 = 1;
    
    void method14025(int i) {
	anInt8753 = 1823770475 * i;
    }
    
    public Class690_Sub8(int i, Class534_Sub35 class534_sub35) {
	super(i, class534_sub35);
    }
    
    public Class690_Sub8(Class534_Sub35 class534_sub35) {
	super(class534_sub35);
    }
    
    int method14017(int i) {
	return (aClass534_Sub35_8752.method16442(-531493946)
		    .method14106((byte) 35)
		? 3 : 2);
    }
    
    int method14026(int i, int i_0_) {
	return 1;
    }
    
    void method14020(int i, int i_1_) {
	anInt8753 = 1823770475 * i;
    }
    
    public int method16947(int i) {
	return 189295939 * anInt8753;
    }
    
    void method14024(int i) {
	anInt8753 = 1823770475 * i;
    }
    
    int method14022() {
	return (aClass534_Sub35_8752.method16442(-531493946)
		    .method14106((byte) 36)
		? 3 : 2);
    }
    
    int method14018() {
	return (aClass534_Sub35_8752.method16442(-531493946)
		    .method14106((byte) 0)
		? 3 : 2);
    }
    
    public void method16948() {
	if (189295939 * anInt8753 < 1 || 189295939 * anInt8753 > 3)
	    anInt8753 = method14017(2092365501) * 1823770475;
    }
    
    public int method16949() {
	return 189295939 * anInt8753;
    }
    
    void method14023(int i) {
	anInt8753 = 1823770475 * i;
    }
    
    int method14027(int i) {
	return 1;
    }
    
    int method14028(int i) {
	return 1;
    }
    
    public int method16950() {
	return 189295939 * anInt8753;
    }
    
    int method14030(int i) {
	return 1;
    }
    
    public void method16951(int i) {
	if (189295939 * anInt8753 < 1 || 189295939 * anInt8753 > 3)
	    anInt8753 = method14017(2140959062) * 1823770475;
    }
    
    int method14029(int i) {
	return 1;
    }
    
    public void method16952() {
	if (189295939 * anInt8753 < 1 || 189295939 * anInt8753 > 3)
	    anInt8753 = method14017(2133175867) * 1823770475;
    }
    
    int method14021() {
	return (aClass534_Sub35_8752.method16442(-531493946)
		    .method14106((byte) 9)
		? 3 : 2);
    }
    
    public void method16953() {
	if (189295939 * anInt8753 < 1 || 189295939 * anInt8753 > 3)
	    anInt8753 = method14017(2122968244) * 1823770475;
    }
    
    public void method16954() {
	if (189295939 * anInt8753 < 1 || 189295939 * anInt8753 > 3)
	    anInt8753 = method14017(2097786877) * 1823770475;
    }
    
    public int method16955() {
	return 189295939 * anInt8753;
    }
    
    public int method16956() {
	return 189295939 * anInt8753;
    }
    
    static final void method16957(Class247 class247, byte i) {
	if (261242275 * Class247.anInt2430
	    == 1231845787 * class247.anInt2457) {
	    if (Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419.aString12211
		== null) {
		class247.anInt2497 = 0;
		class247.anInt2550 = 0;
	    } else {
		class247.anInt2502 = -664967838;
		class247.anInt2503
		    = ((int) (Math.sin((double) client.anInt11101 / 40.0)
			      * 256.0)
		       & 0x7ff) * 2087543833;
		class247.anInt2496 = -1070830455;
		class247.anInt2497 = client.anInt11037 * 2118781167;
		class247.anInt2550
		    = Class511.method8410((Class322
					   .aClass654_Sub1_Sub5_Sub1_Sub2_3419
					   .aString12211),
					  1926529991) * 1933783845;
		Class711_Sub1 class711_sub1
		    = (Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419
		       .aClass711_Sub1_11965);
		if (null != class711_sub1) {
		    if (class247.aClass711_2442 == null)
			class247.aClass711_2442 = new Class711_Sub2();
		    class247.anInt2524
			= class711_sub1.method14329(1969158474) * -1933558971;
		    class247.aClass711_2442.method14326(class711_sub1,
							(byte) 5);
		} else
		    class247.aClass711_2442 = null;
	    }
	}
    }
}
