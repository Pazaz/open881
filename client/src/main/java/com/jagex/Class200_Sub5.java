/* Class200_Sub5 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class200_Sub5 extends Class200
{
    int anInt9898;
    int anInt9899;
    int anInt9900;
    int anInt9901;
    int anInt9902;
    public static int anInt9903;
    
    Class200_Sub5(Class534_Sub40 class534_sub40) {
	super(class534_sub40);
	anInt9901 = class534_sub40.method16529((byte) 1) * -861735383;
	int i = class534_sub40.method16533(-258848859);
	anInt9899 = (i >>> 16) * -1794576893;
	anInt9898 = (i & 0xffff) * 2136667201;
	anInt9900 = class534_sub40.method16527(354962195) * -791220237;
	anInt9902 = class534_sub40.method16545((byte) -83) * 1593831139;
    }
    
    public void method3847() {
	Class65.aClass192Array712[anInt9901 * -806812135].method3772
	    (anInt9899 * -1179276117, anInt9898 * -303359039,
	     anInt9900 * 1926119739, 1588512459 * anInt9902, (byte) 1);
    }
    
    public void method3846() {
	Class65.aClass192Array712[anInt9901 * -806812135].method3772
	    (anInt9899 * -1179276117, anInt9898 * -303359039,
	     anInt9900 * 1926119739, 1588512459 * anInt9902, (byte) 1);
    }
    
    public void method3845(int i) {
	Class65.aClass192Array712[anInt9901 * -806812135].method3772
	    (anInt9899 * -1179276117, anInt9898 * -303359039,
	     anInt9900 * 1926119739, 1588512459 * anInt9902, (byte) 1);
    }
    
    public static int method15573(byte i) {
	return Class620.aClass632_8113.aBool8243 ? 3 : 2;
    }
}
