/* Class175_Sub2_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import java.awt.Canvas;

public abstract class Class175_Sub2_Sub1 extends Class175_Sub2
{
    float[] aFloatArray11397;
    Canvas aCanvas11398;
    int anInt11399;
    int[] anIntArray11400;
    Class185_Sub2 aClass185_Sub2_11401;
    int anInt11402;
    boolean aBool11403;
    
    public int method2913() {
	return -150461501 * anInt11399;
    }
    
    final boolean method2916() {
	aBool11403 = false;
	return true;
    }
    
    final void method15477(int i, int i_0_) {
	if (i != -150461501 * anInt11399 || 787243643 * anInt11402 != i_0_) {
	    anInt11399 = i * 1313170667;
	    anInt11402 = 1111370931 * i_0_;
	    method17932(1313711519);
	}
    }
    
    public int method2910() {
	return -150461501 * anInt11399;
    }
    
    public int method2911() {
	return anInt11402 * 787243643;
    }
    
    void method17929() {
	anIntArray11400
	    = new int[anInt11399 * -150461501 * (787243643 * anInt11402)];
	aFloatArray11397
	    = new float[-150461501 * anInt11399 * (787243643 * anInt11402)];
	if (aBool11403)
	    aClass185_Sub2_11401.method15029(anInt11399 * -150461501,
					     787243643 * anInt11402,
					     anIntArray11400,
					     aFloatArray11397);
    }
    
    final boolean method360() {
	aClass185_Sub2_11401.method15029(anInt11399 * -150461501,
					 787243643 * anInt11402,
					 anIntArray11400, aFloatArray11397);
	aBool11403 = true;
	return true;
    }
    
    final boolean method2912() {
	aBool11403 = false;
	return true;
    }
    
    public int method2914() {
	return anInt11402 * 787243643;
    }
    
    public int method2915() {
	return anInt11402 * 787243643;
    }
    
    final boolean method207() {
	aClass185_Sub2_11401.method15029(anInt11399 * -150461501,
					 787243643 * anInt11402,
					 anIntArray11400, aFloatArray11397);
	aBool11403 = true;
	return true;
    }
    
    final boolean method206() {
	aClass185_Sub2_11401.method15029(anInt11399 * -150461501,
					 787243643 * anInt11402,
					 anIntArray11400, aFloatArray11397);
	aBool11403 = true;
	return true;
    }
    
    final boolean method358() {
	aClass185_Sub2_11401.method15029(anInt11399 * -150461501,
					 787243643 * anInt11402,
					 anIntArray11400, aFloatArray11397);
	aBool11403 = true;
	return true;
    }
    
    final boolean method357() {
	aClass185_Sub2_11401.method15029(anInt11399 * -150461501,
					 787243643 * anInt11402,
					 anIntArray11400, aFloatArray11397);
	aBool11403 = true;
	return true;
    }
    
    Class175_Sub2_Sub1(Class185_Sub2 class185_sub2, Canvas canvas, int i,
		       int i_1_) {
	aCanvas11398 = canvas;
	aClass185_Sub2_11401 = class185_sub2;
	anInt11399 = i * 1313170667;
	anInt11402 = 1111370931 * i_1_;
    }
    
    final boolean method2917() {
	aBool11403 = false;
	return true;
    }
    
    final boolean method2918() {
	aBool11403 = false;
	return true;
    }
    
    void method17930() {
	anIntArray11400
	    = new int[anInt11399 * -150461501 * (787243643 * anInt11402)];
	aFloatArray11397
	    = new float[-150461501 * anInt11399 * (787243643 * anInt11402)];
	if (aBool11403)
	    aClass185_Sub2_11401.method15029(anInt11399 * -150461501,
					     787243643 * anInt11402,
					     anIntArray11400,
					     aFloatArray11397);
    }
    
    void method17931() {
	anIntArray11400
	    = new int[anInt11399 * -150461501 * (787243643 * anInt11402)];
	aFloatArray11397
	    = new float[-150461501 * anInt11399 * (787243643 * anInt11402)];
	if (aBool11403)
	    aClass185_Sub2_11401.method15029(anInt11399 * -150461501,
					     787243643 * anInt11402,
					     anIntArray11400,
					     aFloatArray11397);
    }
    
    void method17932(int i) {
	anIntArray11400
	    = new int[anInt11399 * -150461501 * (787243643 * anInt11402)];
	aFloatArray11397
	    = new float[-150461501 * anInt11399 * (787243643 * anInt11402)];
	if (aBool11403)
	    aClass185_Sub2_11401.method15029(anInt11399 * -150461501,
					     787243643 * anInt11402,
					     anIntArray11400,
					     aFloatArray11397);
    }
    
    void method17933() {
	anIntArray11400
	    = new int[anInt11399 * -150461501 * (787243643 * anInt11402)];
	aFloatArray11397
	    = new float[-150461501 * anInt11399 * (787243643 * anInt11402)];
	if (aBool11403)
	    aClass185_Sub2_11401.method15029(anInt11399 * -150461501,
					     787243643 * anInt11402,
					     anIntArray11400,
					     aFloatArray11397);
    }
    
    final void method15478(int i, int i_2_) {
	if (i != -150461501 * anInt11399 || 787243643 * anInt11402 != i_2_) {
	    anInt11399 = i * 1313170667;
	    anInt11402 = 1111370931 * i_2_;
	    method17932(1313711519);
	}
    }
    
    static final void method17934(Class669 class669, short i) {
	int i_3_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	String string
	    = (String) (class669.anObjectArray8593
			[(class669.anInt8594 -= 1460193483) * 1485266147]);
	Class307.method5648(i_3_, string, -1197927688);
    }
    
    static int[] method17935(Class534_Sub19 class534_sub19, int i) {
	Class534_Sub40 class534_sub40 = new Class534_Sub40(518);
	int[] is = new int[4];
	for (int i_4_ = 0; i_4_ < 4; i_4_++)
	    is[i_4_] = (int) (Math.random() * 9.9999999E7);
	class534_sub40.method16506(10, 1735288428);
	class534_sub40.method16510(is[0], -646227354);
	class534_sub40.method16510(is[1], -454712740);
	class534_sub40.method16510(is[2], -1015042172);
	class534_sub40.method16510(is[3], -1324935436);
	for (int i_5_ = 0; i_5_ < 10; i_5_++)
	    class534_sub40.method16510((int) (Math.random() * 9.9999999E7),
				       -1865348967);
	class534_sub40.method16507((int) (Math.random() * 9.9999999E7),
				   485640285);
	class534_sub40.method16556(Class37.RSA_UPDATE_EXPONENT,
				   Class37.LOGIN_MODULUS, -1128892498);
	class534_sub19.aClass534_Sub40_Sub1_10513.method16519
	    (class534_sub40.aByteArray10810, 0,
	     class534_sub40.anInt10811 * 31645619, -695573950);
	return is;
    }
    
    public static Class534_Sub18_Sub7 method17936(int i) {
	return Class271.aClass534_Sub18_Sub7_2963;
    }
}
