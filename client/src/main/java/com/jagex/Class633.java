/* Class633 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import com.jagex.twitchtv.TwitchWebcamDevice;

public class Class633
{
    int anInt8271;
    Class620 aClass620_8272;
    boolean aBool8273;
    int anInt8274;
    int anInt8275;
    int anInt8276;
    int anInt8277;
    public static int anInt8278;
    
    public int method10474(int i) {
	return -646560609 * anInt8271;
    }
    
    public int method10475(int i) {
	int i_0_ = i / 10;
	return method10480(i_0_, 1855572700);
    }
    
    public boolean method10476(byte i) {
	return aBool8273;
    }
    
    public int method10477(int i) {
	int i_1_ = (aClass620_8272.method10270(i, 1552280996)
		    + anInt8277 * -2116389107);
	if (i_1_ > 128134639 * anInt8276)
	    return anInt8276 * 128134639;
	return i_1_;
    }
    
    int method10478(int i) {
	return 1139544391 * anInt8275;
    }
    
    public boolean method10479(byte i) {
	return -1 != anInt8274 * 843667509;
    }
    
    public int method10480(int i, int i_2_) {
	int i_3_ = (aClass620_8272.method10270(i, 1353717465)
		    + anInt8277 * -2116389107);
	if (i_3_ > 128134639 * anInt8276)
	    return anInt8276 * 128134639;
	return i_3_;
    }
    
    public int method10481(int i, int i_4_) {
	int i_5_ = i / 10;
	return method10480(i_5_, 1855572700);
    }
    
    Class633(int i, int i_6_, boolean bool, boolean bool_7_, int i_8_,
	     Class620 class620, int i_9_) {
	anInt8275 = 676874359 * i;
	anInt8276 = -1740398321 * i_6_;
	aBool8273 = bool;
	aClass620_8272 = class620;
	anInt8277 = 1633890245 * i_9_;
	if (bool) {
	    anInt8271 = -509225121 * i_8_;
	    anInt8274 = method10482(i_8_, 323897393) * -377406947;
	} else {
	    anInt8271 = 509225121;
	    anInt8274 = 377406947;
	}
    }
    
    int method10482(int i, int i_10_) {
	return method10494(i, (byte) -76) * 10;
    }
    
    int method10483() {
	return 1139544391 * anInt8275;
    }
    
    public int method10484() {
	return -646560609 * anInt8271;
    }
    
    public int method10485() {
	return 843667509 * anInt8274;
    }
    
    public int method10486() {
	return -646560609 * anInt8271;
    }
    
    public boolean method10487() {
	return aBool8273;
    }
    
    public boolean method10488() {
	return -1 != anInt8274 * 843667509;
    }
    
    public int method10489(int i) {
	int i_11_ = (aClass620_8272.method10270(i, -1259831604)
		     + anInt8277 * -2116389107);
	if (i_11_ > 128134639 * anInt8276)
	    return anInt8276 * 128134639;
	return i_11_;
    }
    
    int method10490(int i) {
	return method10494(i, (byte) -73) * 10;
    }
    
    public int method10491(int i) {
	int i_12_ = (aClass620_8272.method10270(i, 1462955305)
		     + anInt8277 * -2116389107);
	if (i_12_ > 128134639 * anInt8276)
	    return anInt8276 * 128134639;
	return i_12_;
    }
    
    public int method10492(byte i) {
	return 843667509 * anInt8274;
    }
    
    int method10493(int i) {
	if (i > anInt8276 * 128134639)
	    i = anInt8276 * 128134639;
	return aClass620_8272.method10265(i - -2116389107 * anInt8277,
					  -138046989);
    }
    
    int method10494(int i, byte i_13_) {
	if (i > anInt8276 * 128134639)
	    i = anInt8276 * 128134639;
	return aClass620_8272.method10265(i - -2116389107 * anInt8277,
					  1094604345);
    }
    
    static final void method10495(Class669 class669, int i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class243 class243 = class666.aClass243_8575;
	Class517.method8635(class247, class243, class669, (byte) 66);
    }
    
    static final void method10496(Class669 class669, short i) {
	Class103.method1920(class669.aClass654_Sub1_Sub4_Sub1_8609, class669,
			    (byte) -36);
    }
    
    static final void method10497(Class669 class669, int i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class660.method10948(class247, class669, (byte) 87);
    }
    
    public static TwitchWebcamDevice method10498(String string, int i) {
	if (Class574.aTwitchWebcamDeviceArray7704 == null)
	    return null;
	for (int i_14_ = 0;
	     i_14_ < Class574.aTwitchWebcamDeviceArray7704.length; i_14_++) {
	    if (Class574.aTwitchWebcamDeviceArray7704[i_14_].aString1139
		    .equals(string))
		return Class574.aTwitchWebcamDeviceArray7704[i_14_];
	}
	return null;
    }
    
    public static int method10499(Class472 class472, byte i) {
	int i_15_ = 0;
	if (class472.method7670(Class202.anInt2192 * 149327611, (byte) -84))
	    i_15_++;
	if (class472.method7670(-1795683815 * Class67.anInt718, (byte) -45))
	    i_15_++;
	if (class472.method7670(1750510521 * Class68.anInt722, (byte) -89))
	    i_15_++;
	if (class472.method7670(1069313081 * Class523.anInt7087, (byte) -19))
	    i_15_++;
	if (class472.method7670(Class216.anInt2299 * -1687969609, (byte) -79))
	    i_15_++;
	if (class472.method7670(140074605 * Class67.anInt719, (byte) -127))
	    i_15_++;
	if (class472.method7670(1714366393 * Class530.anInt7134, (byte) -119))
	    i_15_++;
	if (class472.method7670(2006108777 * Class215.anInt2297, (byte) -53))
	    i_15_++;
	if (class472.method7670(Class610.anInt8010 * 2116470397, (byte) -100))
	    i_15_++;
	if (class472.method7670(1474934323 * Class406.anInt4298, (byte) -121))
	    i_15_++;
	return i_15_;
    }
}
