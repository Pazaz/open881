/* Class534_Sub28 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import java.util.HashMap;

import jaclib.hardware_info.HardwareInfo;

public class Class534_Sub28 extends Class534
{
    public static final int anInt10594 = 0;
    String aString10595;
    static final int anInt10596 = 2;
    static final int anInt10597 = 1;
    static final int anInt10598 = 4;
    int anInt10599;
    static final int anInt10600 = 3;
    static final int anInt10601 = 23;
    static final int anInt10602 = 2;
    static final int anInt10603 = 25;
    static final int anInt10604 = 4;
    static final int anInt10605 = 5;
    static final int anInt10606 = 6;
    static final int anInt10607 = 7;
    static final int anInt10608 = 4;
    static final int anInt10609 = 9;
    static final int anInt10610 = 10;
    static final int anInt10611 = 8;
    int anInt10612;
    static final int anInt10613 = 21;
    static final int anInt10614 = 22;
    String aString10615;
    static final int anInt10616 = 3;
    static final int anInt10617 = 11;
    static final int anInt10618 = 26;
    static final int anInt10619 = 27;
    int anInt10620;
    static final int anInt10621 = 1;
    static final int anInt10622 = 2;
    static final int anInt10623 = 3;
    static final int anInt10624 = 24;
    static final int anInt10625 = 5;
    int anInt10626;
    int anInt10627;
    int anInt10628;
    int anInt10629;
    boolean aBool10630;
    int anInt10631;
    static final int anInt10632 = 1;
    static final int anInt10633 = 0;
    boolean aBool10634;
    public int anInt10635;
    int anInt10636;
    String aString10637;
    int anInt10638;
    String aString10639;
    int anInt10640;
    int anInt10641;
    int anInt10642;
    static final int anInt10643 = 20;
    int anInt10644;
    String aString10645;
    String aString10646;
    int[] anIntArray10647 = new int[3];
    static final int anInt10648 = 7;
    String aString10649;
    
    void method16307() {
	if (aString10637.length() > 40)
	    aString10637 = aString10637.substring(0, 40);
	if (aString10595.length() > 40)
	    aString10595 = aString10595.substring(0, 40);
	if (aString10639.length() > 10)
	    aString10639 = aString10639.substring(0, 10);
	if (aString10615.length() > 10)
	    aString10615 = aString10615.substring(0, 10);
	if (aString10649.length() > 120)
	    aString10649 = aString10649.substring(0, 120);
    }
    
    public int method16308() {
	int i = 38;
	i += Class422.method6786(aString10637, -838480330);
	i += Class422.method6786(aString10595, -1814018816);
	i += Class422.method6786(aString10639, -2042408220);
	i += Class422.method6786(aString10615, -831898864);
	i += Class422.method6786(aString10645, -1371238419);
	i += Class422.method6786(aString10646, -1503541038);
	i += Class422.method6786(aString10649, -1271924597);
	return i;
    }
    
    void method16309(int i) {
	if (aString10637.length() > 40)
	    aString10637 = aString10637.substring(0, 40);
	if (aString10595.length() > 40)
	    aString10595 = aString10595.substring(0, 40);
	if (aString10639.length() > 10)
	    aString10639 = aString10639.substring(0, 10);
	if (aString10615.length() > 10)
	    aString10615 = aString10615.substring(0, 10);
	if (aString10649.length() > 120)
	    aString10649 = aString10649.substring(0, 120);
    }
    
    public int method16310(int i) {
	int i_0_ = 38;
	i_0_ += Class422.method6786(aString10637, -991420453);
	i_0_ += Class422.method6786(aString10595, -2049255956);
	i_0_ += Class422.method6786(aString10639, -1298526887);
	i_0_ += Class422.method6786(aString10615, -2016504370);
	i_0_ += Class422.method6786(aString10645, -794399889);
	i_0_ += Class422.method6786(aString10646, -1935681881);
	i_0_ += Class422.method6786(aString10649, -1107498164);
	return i_0_;
    }
    
    void method16311() {
	if (aString10637.length() > 40)
	    aString10637 = aString10637.substring(0, 40);
	if (aString10595.length() > 40)
	    aString10595 = aString10595.substring(0, 40);
	if (aString10639.length() > 10)
	    aString10639 = aString10639.substring(0, 10);
	if (aString10615.length() > 10)
	    aString10615 = aString10615.substring(0, 10);
	if (aString10649.length() > 120)
	    aString10649 = aString10649.substring(0, 120);
    }
    
    public void method16312(Class534_Sub40 class534_sub40, byte i) {
	class534_sub40.method16506(7, 1758886940);
	class534_sub40.method16506(-169994563 * anInt10599, 487735029);
	class534_sub40.method16506(aBool10634 ? 1 : 0, 2098423757);
	class534_sub40.method16506(anInt10620 * 1639805781, 307538769);
	class534_sub40.method16506(618457647 * anInt10626, 1642216243);
	class534_sub40.method16506(758172927 * anInt10627, 190148695);
	class534_sub40.method16506(anInt10628 * -2057205629, 2056257368);
	class534_sub40.method16506(anInt10612 * -2076366157, 1197510913);
	class534_sub40.method16506(aBool10630 ? 1 : 0, 1014602655);
	class534_sub40.method16507(-314204203 * anInt10631, 1502043032);
	class534_sub40.method16506(anInt10629 * 111068721, 1385692928);
	class534_sub40.method16509(-686202593 * anInt10635, -40585929);
	class534_sub40.method16507(1626055995 * anInt10636, 1445444495);
	class534_sub40.method16517(aString10637, (byte) 4);
	class534_sub40.method16517(aString10595, (byte) 4);
	class534_sub40.method16517(aString10639, (byte) 4);
	class534_sub40.method16517(aString10615, (byte) 4);
	class534_sub40.method16506(anInt10642 * -42031865, 510284244);
	class534_sub40.method16507(anInt10641 * -174700213, 804310875);
	class534_sub40.method16517(aString10645, (byte) 4);
	class534_sub40.method16517(aString10646, (byte) 4);
	class534_sub40.method16506(-1330352411 * anInt10644, 731311757);
	class534_sub40.method16506(713740933 * anInt10638, 1963952669);
	for (int i_1_ = 0; i_1_ < anIntArray10647.length; i_1_++)
	    class534_sub40.method16510(anIntArray10647[i_1_], -807373461);
	class534_sub40.method16510(anInt10640 * -1768400225, -1164294984);
	class534_sub40.method16517(aString10649, (byte) 4);
    }
    
    public void method16313(Class534_Sub40 class534_sub40) {
	class534_sub40.method16506(7, 2000256549);
	class534_sub40.method16506(-169994563 * anInt10599, 2034235666);
	class534_sub40.method16506(aBool10634 ? 1 : 0, 1672724275);
	class534_sub40.method16506(anInt10620 * 1639805781, 1977957997);
	class534_sub40.method16506(618457647 * anInt10626, 1458630523);
	class534_sub40.method16506(758172927 * anInt10627, 1503397929);
	class534_sub40.method16506(anInt10628 * -2057205629, 977289590);
	class534_sub40.method16506(anInt10612 * -2076366157, 1738344590);
	class534_sub40.method16506(aBool10630 ? 1 : 0, 571380892);
	class534_sub40.method16507(-314204203 * anInt10631, 573146414);
	class534_sub40.method16506(anInt10629 * 111068721, 824022011);
	class534_sub40.method16509(-686202593 * anInt10635, -1566428730);
	class534_sub40.method16507(1626055995 * anInt10636, 1956224032);
	class534_sub40.method16517(aString10637, (byte) 4);
	class534_sub40.method16517(aString10595, (byte) 4);
	class534_sub40.method16517(aString10639, (byte) 4);
	class534_sub40.method16517(aString10615, (byte) 4);
	class534_sub40.method16506(anInt10642 * -42031865, 609771114);
	class534_sub40.method16507(anInt10641 * -174700213, 1529333039);
	class534_sub40.method16517(aString10645, (byte) 4);
	class534_sub40.method16517(aString10646, (byte) 4);
	class534_sub40.method16506(-1330352411 * anInt10644, 981231157);
	class534_sub40.method16506(713740933 * anInt10638, 1627006260);
	for (int i = 0; i < anIntArray10647.length; i++)
	    class534_sub40.method16510(anIntArray10647[i], -175960407);
	class534_sub40.method16510(anInt10640 * -1768400225, -1692951861);
	class534_sub40.method16517(aString10649, (byte) 4);
    }
    
    public Class534_Sub28(boolean bool) {
	if (bool) {
	    if (Class262.aString2801.startsWith("win"))
		anInt10599 = -1372711787;
	    else if (Class262.aString2801.startsWith("mac"))
		anInt10599 = 1549543722;
	    else if (Class262.aString2801.startsWith("linux"))
		anInt10599 = 176831935;
	    else
		anInt10599 = -1195879852;
	    if (Class498.aString5592.startsWith("amd64")
		|| Class498.aString5592.startsWith("x86_64"))
		aBool10634 = true;
	    else
		aBool10634 = false;
	    if (1 == anInt10599 * -169994563) {
		if (Class477.aString5191.indexOf("4.0") != -1)
		    anInt10620 = 426439165;
		else if (Class477.aString5191.indexOf("4.1") != -1)
		    anInt10620 = 852878330;
		else if (Class477.aString5191.indexOf("4.9") != -1)
		    anInt10620 = 1279317495;
		else if (Class477.aString5191.indexOf("5.0") != -1)
		    anInt10620 = 1705756660;
		else if (Class477.aString5191.indexOf("5.1") != -1)
		    anInt10620 = 2132195825;
		else if (Class477.aString5191.indexOf("5.2") != -1)
		    anInt10620 = -883453976;
		else if (Class477.aString5191.indexOf("6.0") != -1)
		    anInt10620 = -1736332306;
		else if (Class477.aString5191.indexOf("6.1") != -1)
		    anInt10620 = -1309893141;
		else if (Class477.aString5191.indexOf("6.2") != -1)
		    anInt10620 = -457014811;
		else if (Class477.aString5191.indexOf("6.3") != -1)
		    anInt10620 = -30575646;
		else if (Class477.aString5191.indexOf("10.0") != -1)
		    anInt10620 = 395863519;
	    } else if (2 == anInt10599 * -169994563) {
		if (Class477.aString5191.indexOf("10.4") != -1)
		    anInt10620 = -61151292;
		else if (Class477.aString5191.indexOf("10.5") != -1)
		    anInt10620 = 365287873;
		else if (Class477.aString5191.indexOf("10.6") != -1)
		    anInt10620 = 791727038;
		else if (Class477.aString5191.indexOf("10.7") != -1)
		    anInt10620 = 1218166203;
		else if (Class477.aString5191.indexOf("10.8") != -1)
		    anInt10620 = 1644605368;
		else if (Class477.aString5191.indexOf("10.9") != -1)
		    anInt10620 = 2071044533;
		else if (Class477.aString5191.indexOf("10.10") != -1)
		    anInt10620 = -1797483598;
		else if (Class477.aString5191.indexOf("10.11") != -1)
		    anInt10620 = -1371044433;
	    }
	    if (Class461.aString5073.toLowerCase().indexOf("sun") != -1)
		anInt10626 = 796302543;
	    else if (Class461.aString5073.toLowerCase().indexOf("microsoft")
		     != -1)
		anInt10626 = 1592605086;
	    else if (Class461.aString5073.toLowerCase().indexOf("apple") != -1)
		anInt10626 = -1906059667;
	    else if (Class461.aString5073.toLowerCase().indexOf("oracle")
		     != -1)
		anInt10626 = -313454581;
	    else
		anInt10626 = -1109757124;
	    int i = 2;
	    int i_2_ = 0;
	    try {
		for (/**/; i < Class690_Sub2.aString10846.length(); i++) {
		    int i_3_ = Class690_Sub2.aString10846.charAt(i);
		    if (i_3_ < 48 || i_3_ > 57)
			break;
		    i_2_ = i_3_ - 48 + 10 * i_2_;
		}
	    } catch (Exception exception) {
		/* empty */
	    }
	    anInt10627 = 2040935167 * i_2_;
	    i = Class690_Sub2.aString10846.indexOf('.', 2) + 1;
	    i_2_ = 0;
	    try {
		for (/**/; i < Class690_Sub2.aString10846.length(); i++) {
		    int i_4_ = Class690_Sub2.aString10846.charAt(i);
		    if (i_4_ < 48 || i_4_ > 57)
			break;
		    i_2_ = i_4_ - 48 + i_2_ * 10;
		}
	    } catch (Exception exception) {
		/* empty */
	    }
	    anInt10628 = i_2_ * -654768597;
	    i = Class690_Sub2.aString10846.indexOf('_', 4) + 1;
	    i_2_ = 0;
	    try {
		for (/**/; i < Class690_Sub2.aString10846.length(); i++) {
		    int i_5_ = Class690_Sub2.aString10846.charAt(i);
		    if (i_5_ < 48 || i_5_ > 57)
			break;
		    i_2_ = i_5_ - 48 + 10 * i_2_;
		}
	    } catch (Exception exception) {
		/* empty */
	    }
	    anInt10612 = 2027918459 * i_2_;
	    aBool10630 = false;
	    anInt10631 = -2078220253 * Class498.anInt5589;
	    if (758172927 * anInt10627 > 3)
		anInt10629 = Class498.anInt5576 * -1829000755;
	    else
		anInt10629 = 0;
	    try {
		int[] is = HardwareInfo.getCPUInfo();
		if (null != is && is.length == 3) {
		    anInt10644 = 238329581 * is[0];
		    anInt10636 = 815786995 * is[1];
		    anInt10635 = -2049910049 * is[2];
		}
		int[] is_6_ = HardwareInfo.getRawCPUInfo();
		if (is_6_ != null && is_6_.length % 5 == 0) {
		    HashMap hashmap = new HashMap();
		    for (int i_7_ = 0; i_7_ < is_6_.length / 5; i_7_++) {
			int i_8_ = is_6_[i_7_ * 5];
			int i_9_ = is_6_[1 + i_7_ * 5];
			int i_10_ = is_6_[2 + 5 * i_7_];
			int i_11_ = is_6_[5 * i_7_ + 3];
			int i_12_ = is_6_[4 + i_7_ * 5];
			Class59 class59
			    = new Class59(i_8_, i_9_, i_10_, i_11_, i_12_);
			hashmap.put(Integer.valueOf(i_8_), class59);
		    }
		    Class59 class59
			= (Class59) hashmap.get(Integer.valueOf(0));
		    if (class59 != null) {
			Class534_Sub40 class534_sub40 = new Class534_Sub40(13);
			class534_sub40.method16511((class59.anInt639
						    * 591064797),
						   1425010886);
			class534_sub40.method16511((849627281
						    * class59.anInt637),
						   437609136);
			class534_sub40.method16511((-2091928773
						    * class59.anInt641),
						   -701677958);
			class534_sub40.anInt10811 = 0;
			aString10645 = class534_sub40.method16541((byte) -12);
		    }
		    Class59 class59_13_
			= (Class59) hashmap.get(Integer.valueOf(1));
		    if (null != class59_13_) {
			anInt10640 = 2017253763 * class59_13_.anInt638;
			int i_14_ = 591064797 * class59_13_.anInt639;
			anInt10638 = (i_14_ >> 16 & 0xff) * -917612979;
			anIntArray10647[0]
			    = -2091928773 * class59_13_.anInt641;
			anIntArray10647[1] = class59_13_.anInt637 * 849627281;
		    }
		    Class59 class59_15_
			= (Class59) hashmap.get(Integer.valueOf(-2147483647));
		    if (class59_15_ != null)
			anIntArray10647[2] = class59_15_.anInt637 * 849627281;
		    Class534_Sub40 class534_sub40 = new Class534_Sub40(49);
		    for (int i_16_ = -2147483646; i_16_ <= -2147483644;
			 i_16_++) {
			Class59 class59_17_
			    = (Class59) hashmap.get(Integer.valueOf(i_16_));
			if (class59_17_ != null) {
			    class534_sub40.method16511(-99932579 * (class59_17_
								    .anInt638),
						       1777501517);
			    class534_sub40.method16511(591064797 * (class59_17_
								    .anInt639),
						       1791764539);
			    class534_sub40.method16511((-2091928773
							* (class59_17_
							   .anInt641)),
						       -1032508509);
			    class534_sub40.method16511(849627281 * (class59_17_
								    .anInt637),
						       1213638607);
			}
		    }
		    class534_sub40.anInt10811 = 0;
		    aString10646 = class534_sub40.method16541((byte) -80);
		}
		String[][] strings
		    = HardwareInfo.getDXDiagDisplayDevicesProps();
		if (strings != null && strings.length > 0
		    && strings[0] != null) {
		    for (int i_18_ = 0; i_18_ < strings[0].length;
			 i_18_ += 2) {
			if (strings[0][i_18_]
				.equalsIgnoreCase("szDescription"))
			    aString10637 = strings[0][1 + i_18_];
			else if (strings[0][i_18_].equalsIgnoreCase
				 ("szDriverDateEnglish")) {
			    String string = strings[0][1 + i_18_];
			    try {
				int i_19_ = string.indexOf("/");
				int i_20_ = string.indexOf("/", 1 + i_19_);
				anInt10642 = ((Integer.parseInt
					       (string.substring(0, i_19_)))
					      * 1770895543);
				anInt10641 = ((Integer.parseInt
					       (string.substring
						(1 + i_20_,
						 string.indexOf(" ", i_20_))))
					      * 1006893155);
			    } catch (Exception exception) {
				/* empty */
			    }
			}
		    }
		}
		String[] strings_21_ = HardwareInfo.getDXDiagSystemProps();
		if (strings_21_ != null) {
		    String string = "";
		    String string_22_ = "";
		    String string_23_ = "";
		    for (int i_24_ = 0; i_24_ < strings_21_.length;
			 i_24_ += 2) {
			if (strings_21_[i_24_]
				.equalsIgnoreCase("dwDirectXVersionMajor"))
			    string = strings_21_[1 + i_24_];
			else if (strings_21_[i_24_].equalsIgnoreCase
				 ("dwDirectXVersionMinor"))
			    string_22_ = strings_21_[i_24_ + 1];
			else if (strings_21_[i_24_].equalsIgnoreCase
				 ("dwDirectXVersionLetter"))
			    string_23_ = strings_21_[i_24_ + 1];
		    }
		    aString10639
			= new StringBuilder().append(string).append(".").append
			      (string_22_).append
			      (string_23_).toString();
		}
	    } catch (Throwable throwable) {
		anInt10635 = 0;
	    }
	}
	if (aString10637 == null)
	    aString10637 = "";
	if (null == aString10595)
	    aString10595 = "";
	if (null == aString10639)
	    aString10639 = "";
	if (null == aString10615)
	    aString10615 = "";
	if (null == aString10645)
	    aString10645 = "";
	if (aString10646 == null)
	    aString10646 = "";
	if (null == aString10649)
	    aString10649 = "";
	method16309(2146874511);
    }
    
    public int method16314() {
	int i = 38;
	i += Class422.method6786(aString10637, -686848621);
	i += Class422.method6786(aString10595, -1504524545);
	i += Class422.method6786(aString10639, -1728577198);
	i += Class422.method6786(aString10615, -1233269760);
	i += Class422.method6786(aString10645, -1430451493);
	i += Class422.method6786(aString10646, -843875656);
	i += Class422.method6786(aString10649, -716833271);
	return i;
    }
    
    public int method16315() {
	int i = 38;
	i += Class422.method6786(aString10637, -2008039040);
	i += Class422.method6786(aString10595, -1452173526);
	i += Class422.method6786(aString10639, -1320359568);
	i += Class422.method6786(aString10615, -1549784969);
	i += Class422.method6786(aString10645, -1829404447);
	i += Class422.method6786(aString10646, -1680284769);
	i += Class422.method6786(aString10649, -2033479798);
	return i;
    }
    
    public void method16316(Class534_Sub40 class534_sub40) {
	class534_sub40.method16506(7, 1120515465);
	class534_sub40.method16506(-169994563 * anInt10599, 1313978297);
	class534_sub40.method16506(aBool10634 ? 1 : 0, 654834754);
	class534_sub40.method16506(anInt10620 * 1639805781, 911552053);
	class534_sub40.method16506(618457647 * anInt10626, 335388334);
	class534_sub40.method16506(758172927 * anInt10627, 659054389);
	class534_sub40.method16506(anInt10628 * -2057205629, 895547976);
	class534_sub40.method16506(anInt10612 * -2076366157, 1614029063);
	class534_sub40.method16506(aBool10630 ? 1 : 0, 843780083);
	class534_sub40.method16507(-314204203 * anInt10631, 633190398);
	class534_sub40.method16506(anInt10629 * 111068721, 834600543);
	class534_sub40.method16509(-686202593 * anInt10635, 210372308);
	class534_sub40.method16507(1626055995 * anInt10636, 1579811547);
	class534_sub40.method16517(aString10637, (byte) 4);
	class534_sub40.method16517(aString10595, (byte) 4);
	class534_sub40.method16517(aString10639, (byte) 4);
	class534_sub40.method16517(aString10615, (byte) 4);
	class534_sub40.method16506(anInt10642 * -42031865, 291392690);
	class534_sub40.method16507(anInt10641 * -174700213, 1899524282);
	class534_sub40.method16517(aString10645, (byte) 4);
	class534_sub40.method16517(aString10646, (byte) 4);
	class534_sub40.method16506(-1330352411 * anInt10644, 244645790);
	class534_sub40.method16506(713740933 * anInt10638, 725875427);
	for (int i = 0; i < anIntArray10647.length; i++)
	    class534_sub40.method16510(anIntArray10647[i], -710890952);
	class534_sub40.method16510(anInt10640 * -1768400225, -939258593);
	class534_sub40.method16517(aString10649, (byte) 4);
    }
    
    static void method16317(int i) {
	int i_25_ = 1771907305 * Class706_Sub4.anInt10994;
	int i_26_ = Class18.anInt205 * -1091172141;
	if (Class391.anInt4076 * -166028671 < i_25_)
	    i_25_ = Class391.anInt4076 * -166028671;
	if (client.anInt5561 * 8272787 < i_26_)
	    i_26_ = client.anInt5561 * 8272787;
	try {
	    if (Class44_Sub6.aClass534_Sub35_10989 != null)
		Class403.aClass403_4143.method6596
		    ((new Object[]
		      { Integer.valueOf(i_25_), Integer.valueOf(i_26_),
			Integer.valueOf(Class63.method1280(1668530589)),
			Integer.valueOf(Class44_Sub6.aClass534_Sub35_10989
					    .aClass690_Sub20_10742
					    .method17058(-2029719482)) }),
		     (byte) 0);
	} catch (Throwable throwable) {
	    /* empty */
	}
    }
}
