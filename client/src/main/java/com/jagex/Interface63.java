/* Interface63 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public interface Interface63
{
    public static final int anInt1 = 4;
    public static final int anInt2 = 1;
    public static final int anInt3 = 2;
    public static final int anInt4 = 3;
    public static final int anInt5 = 1;
    public static final int anInt6 = 2;
    public static final int anInt7 = 0;
    
    public int method85();
    
    public char method424(byte i);
    
    public int method145();
    
    public long method425(int i);
    
    public int method426(byte i);
    
    public int method56(int i);
    
    public char method427();
    
    public int method181();
    
    public char method428();
    
    public char method429();
    
    public long method365();
    
    public int method190();
    
    public int method430();
    
    public int method431(byte i);
}
