/* Interface19 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public interface Interface19 extends Interface20
{
    public void method113(Class150 class150, long l);
    
    public void method114(Class150 class150, int i, int i_0_);
    
    public void method115(Class150 class150, long l);
    
    public void method116(Class150 class150, long l);
    
    public int method117(Class150 class150);
    
    public void method118(Class150 class150, Object object, byte i);
    
    public int method119(Class318 class318, int i);
    
    public int method120(Class150 class150, byte i);
    
    public void method121(Class150 class150, int i);
    
    public void method122(Class318 class318, int i, byte i_1_)
	throws Exception_Sub6;
    
    public int method123(Class318 class318);
    
    public Object method124(Class150 class150, int i);
    
    public int method125(Class318 class318);
    
    public int method126(Class318 class318);
    
    public long method127(Class150 class150, byte i);
    
    public void method128(Class150 class150, int i);
    
    public long method129(Class150 class150);
    
    public void method130(Class150 class150, long l);
    
    public long method131(Class150 class150);
    
    public long method132(Class150 class150);
    
    public long method133(Class150 class150);
    
    public Object method134(Class150 class150);
    
    public int method135(Class318 class318);
    
    public int method136(Class150 class150);
    
    public Object method137(Class150 class150);
    
    public void method138(Class150 class150, Object object);
    
    public void method139(Class318 class318, int i) throws Exception_Sub6;
    
    public void method140(Class318 class318, int i) throws Exception_Sub6;
}
