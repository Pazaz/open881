/* Class513 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import java.io.IOException;

public class Class513
{
    static Class534_Sub18_Sub7 aClass534_Sub18_Sub7_5727;
    
    public static void method8573(boolean bool) {
	Class171_Sub4.aClass232_9944.method4270((byte) -1);
	if (Class44_Sub22.method17372(-1850530127 * client.anInt11039,
				      1247852284)) {
	    Class100[] class100s = client.aClass100Array11096;
	    for (int i = 0; i < class100s.length; i++) {
		Class100 class100 = class100s[i];
		class100.anInt1187 += -286235183;
		if (-351399119 * class100.anInt1187 < 50 && !bool)
		    return;
		class100.anInt1187 = 0;
		if (!class100.aBool1198
		    && class100.method1867(2101558723) != null) {
		    Class534_Sub19 class534_sub19
			= Class346.method6128(Class404.aClass404_4275,
					      class100.aClass13_1183,
					      1341391005);
		    class100.method1863(class534_sub19, (byte) 126);
		    try {
			class100.method1868(1805858475);
		    } catch (IOException ioexception) {
			class100.aBool1198 = true;
		    }
		}
	    }
	    Class171_Sub4.aClass232_9944.method4270((byte) -1);
	}
    }
    
    public static void method8574(boolean bool) {
	Class171_Sub4.aClass232_9944.method4270((byte) -1);
	if (Class44_Sub22.method17372(-1850530127 * client.anInt11039,
				      -1307911006)) {
	    Class100[] class100s = client.aClass100Array11096;
	    for (int i = 0; i < class100s.length; i++) {
		Class100 class100 = class100s[i];
		class100.anInt1187 += -286235183;
		if (-351399119 * class100.anInt1187 < 50 && !bool)
		    return;
		class100.anInt1187 = 0;
		if (!class100.aBool1198
		    && class100.method1867(221390234) != null) {
		    Class534_Sub19 class534_sub19
			= Class346.method6128(Class404.aClass404_4275,
					      class100.aClass13_1183,
					      1341391005);
		    class100.method1863(class534_sub19, (byte) 33);
		    try {
			class100.method1868(1805858475);
		    } catch (IOException ioexception) {
			class100.aBool1198 = true;
		    }
		}
	    }
	    Class171_Sub4.aClass232_9944.method4270((byte) -1);
	}
    }
    
    public static void method8575(boolean bool) {
	Class171_Sub4.aClass232_9944.method4270((byte) -1);
	if (Class44_Sub22.method17372(-1850530127 * client.anInt11039,
				      1027588437)) {
	    Class100[] class100s = client.aClass100Array11096;
	    for (int i = 0; i < class100s.length; i++) {
		Class100 class100 = class100s[i];
		class100.anInt1187 += -286235183;
		if (-351399119 * class100.anInt1187 < 50 && !bool)
		    return;
		class100.anInt1187 = 0;
		if (!class100.aBool1198
		    && class100.method1867(-1419827574) != null) {
		    Class534_Sub19 class534_sub19
			= Class346.method6128(Class404.aClass404_4275,
					      class100.aClass13_1183,
					      1341391005);
		    class100.method1863(class534_sub19, (byte) 9);
		    try {
			class100.method1868(1805858475);
		    } catch (IOException ioexception) {
			class100.aBool1198 = true;
		    }
		}
	    }
	    Class171_Sub4.aClass232_9944.method4270((byte) -1);
	}
    }
    
    Class513() throws Throwable {
	throw new Error();
    }
    
    static void method8576(Class669 class669, int i) {
	class669.anObjectArray8593
	    [(class669.anInt8594 += 1460193483) * 1485266147 - 1]
	    = ((Class273)
	       Class223.aClass53_Sub2_2316.method91((class669.anIntArray8595
						     [((class669.anInt8600
							-= 308999563)
						       * 2088438307)]),
						    -1342620514)).aString3026;
    }
    
    static final void method8577(Class669 class669, byte i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class243 class243 = class666.aClass243_8575;
	Class419.method6765(class247, class243, class669, 1054642155);
    }
    
    static final void method8578(Class669 class669, byte i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class592.method9887(class247, class669, (byte) 108);
    }
    
    static final void method8579(Class669 class669, int i) {
	String string
	    = (String) (class669.anObjectArray8593
			[(class669.anInt8594 -= 1460193483) * 1485266147]);
	class669.anInt8600 -= 617999126;
	int i_0_ = class669.anIntArray8595[class669.anInt8600 * 2088438307];
	int i_1_
	    = class669.anIntArray8595[1 + class669.anInt8600 * 2088438307];
	Class16 class16
	    = Class351.aClass406_3620.method6666(client.anInterface52_11081,
						 i_1_, (byte) 123);
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = class16.method733(string, i_0_, Class658.aClass163Array8541,
				(byte) -62);
    }
    
    static final void method8580(Class669 class669, int i) {
	int i_2_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	int i_3_ = i_2_ >> 16;
	if (null == Class44_Sub11.aClass243Array11006[i_3_])
	    class669.anObjectArray8593
		[(class669.anInt8594 += 1460193483) * 1485266147 - 1]
		= "";
	else {
	    String string = (Class44_Sub11.aClass243Array11006[i_3_]
			     .aClass247Array2412[i_2_].aString2592);
	    if (string == null)
		class669.anObjectArray8593
		    [(class669.anInt8594 += 1460193483) * 1485266147 - 1]
		    = "";
	    else
		class669.anObjectArray8593
		    [(class669.anInt8594 += 1460193483) * 1485266147 - 1]
		    = string;
	}
    }
    
    static final void method8581(Class669 class669, byte i) {
	class669.anInt8600 -= 617999126;
	int i_4_ = class669.anIntArray8595[class669.anInt8600 * 2088438307];
	int i_5_
	    = class669.anIntArray8595[1 + class669.anInt8600 * 2088438307];
	Class273.method5102(4, i_4_, i_5_, "", (byte) 58);
    }
    
    static final void method8582(int i) {
	Class200_Sub12.method15586((byte) 55);
	client.aClass512_11100.method8434(1862053724);
	for (int i_6_ = 0; i_6_ < client.aClass99Array11053.length; i_6_++)
	    client.aClass99Array11053[i_6_] = null;
	for (int i_7_ = 0; i_7_ < client.aClass530Array11054.length; i_7_++)
	    client.aClass530Array11054[i_7_] = null;
	Class710.method14322(-1536796248);
	for (int i_8_ = 0; i_8_ < 2048; i_8_++)
	    client.aClass654_Sub1_Sub5_Sub1_Sub2Array11279[i_8_] = null;
	client.anInt11321 = 0;
	client.aClass9_11331.method578((byte) -10);
	client.anInt11148 = 0;
	client.aClass9_11209.method578((byte) -26);
	Class235.method4408(Class200_Sub5.method15573((byte) -20), (byte) 79);
	client.anInt11144 = 0;
	Class78.aClass103_825.aClass612_1294.method10098(-1789204418);
	Class19.aClass352_211 = null;
	Class162.aClass352_1758 = null;
	Class489.aClass534_Sub26_5312 = null;
	Class574.aClass534_Sub26_7710 = null;
	client.aClass214_11359 = null;
	client.aBool11339 = true;
	Class52.aClass641_436 = null;
	Class36.aLong288 = 0L;
	Class44_Sub16.method17357((byte) 125);
	Class710.method14323((byte) -84);
    }
}
