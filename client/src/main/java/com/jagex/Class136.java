/* Class136 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public abstract class Class136
{
    Class185_Sub3 aClass185_Sub3_1600;
    
    abstract void method2326();
    
    abstract void method2327();
    
    abstract void method2328(boolean bool);
    
    abstract void method2329(boolean bool);
    
    abstract void method2330(int i, int i_0_);
    
    abstract void method2331(int i, int i_1_);
    
    abstract void method2332(boolean bool);
    
    Class136(Class185_Sub3 class185_sub3) {
	aClass185_Sub3_1600 = class185_sub3;
    }
    
    abstract boolean method2333();
    
    abstract boolean method2334();
    
    abstract void method2335(boolean bool);
    
    abstract void method2336(boolean bool);
    
    abstract void method2337(boolean bool);
    
    abstract void method2338(boolean bool);
    
    abstract void method2339(boolean bool);
    
    abstract void method2340(boolean bool);
    
    abstract void method2341();
    
    abstract boolean method2342();
    
    abstract void method2343(Class141 class141, int i);
    
    abstract void method2344(boolean bool);
    
    abstract void method2345();
    
    abstract void method2346(int i, int i_2_);
    
    abstract boolean method2347();
    
    abstract void method2348(Class141 class141, int i);
}
