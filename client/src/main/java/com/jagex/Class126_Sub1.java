/* Class126_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import jaclib.memory.Buffer;

public class Class126_Sub1 extends Class126 implements Interface15
{
    int anInt8903;
    
    public long method96() {
	return aBuffer1503.method2();
    }
    
    Class126_Sub1(Class185_Sub3 class185_sub3, int i, Buffer buffer) {
	super(class185_sub3, buffer);
	anInt8903 = i;
    }
    
    public void method95(int i, byte[] is, int i_0_) {
	method2191(is, i_0_);
	anInt8903 = i;
    }
    
    public int method93() {
	return anInt8903;
    }
    
    public long method94() {
	return aBuffer1503.method2();
    }
    
    Class126_Sub1(Class185_Sub3 class185_sub3, int i, byte[] is, int i_1_) {
	super(class185_sub3, is, i_1_);
	anInt8903 = i;
    }
    
    public void method99(int i, byte[] is, int i_2_) {
	method2191(is, i_2_);
	anInt8903 = i;
    }
    
    public int method85() {
	return 0;
    }
    
    public int method88() {
	return anInt8903;
    }
    
    public int method8() {
	return anInt8903;
    }
    
    public int method53() {
	return 0;
    }
    
    public int method1() {
	return 0;
    }
    
    public void method97(int i, byte[] is, int i_3_) {
	method2191(is, i_3_);
	anInt8903 = i;
    }
    
    public void method98(int i, byte[] is, int i_4_) {
	method2191(is, i_4_);
	anInt8903 = i;
    }
    
    public void method92(int i, byte[] is, int i_5_) {
	method2191(is, i_5_);
	anInt8903 = i;
    }
    
    public void method100(int i, byte[] is, int i_6_) {
	method2191(is, i_6_);
	anInt8903 = i;
    }
}
