/* Class425 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import java.awt.Canvas;

public class Class425
{
    public static Class185 method6793
	(Canvas canvas, Class177 class177, Interface25 interface25,
	 Interface45 interface45, Interface48 interface48,
	 Interface46 interface46, Class472 class472, int i) {
	Class185 class185;
	try {
	    Class185_Sub1.method14906();
	    Class112.method2018(-1327590673).method400("jagdx", 706683494);
	    class185
		= Class185_Sub1_Sub1.method17996(canvas, class177, interface25,
						 interface45, interface48,
						 interface46, class472,
						 Integer.valueOf(i));
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Throwable throwable) {
	    throw new RuntimeException("");
	}
	return class185;
    }
    
    public static Class185 method6794
	(Canvas canvas, Class177 class177, Interface25 interface25,
	 Interface45 interface45, Interface48 interface48,
	 Interface46 interface46, Class472 class472, int i) {
	Class185 class185;
	try {
	    Class185_Sub1.method14906();
	    Class112.method2018(-1327590673).method400("jagdx", 458799362);
	    class185
		= Class185_Sub1_Sub1.method17996(canvas, class177, interface25,
						 interface45, interface48,
						 interface46, class472,
						 Integer.valueOf(i));
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Throwable throwable) {
	    throw new RuntimeException("");
	}
	return class185;
    }
    
    public static Class185 method6795
	(Canvas canvas, Class177 class177, Interface25 interface25,
	 Interface45 interface45, Interface48 interface48,
	 Interface46 interface46, Class472 class472, int i) {
	Class185 class185;
	try {
	    Class185_Sub1.method14906();
	    Class112.method2018(-1327590673).method400("jagdx", -412556192);
	    class185
		= Class185_Sub1_Sub1.method17996(canvas, class177, interface25,
						 interface45, interface48,
						 interface46, class472,
						 Integer.valueOf(i));
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Throwable throwable) {
	    throw new RuntimeException("");
	}
	return class185;
    }
    
    Class425() throws Throwable {
	throw new Error();
    }
}
