/* Class626 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class626
{
    int anInt8167;
    static final int anInt8168 = 2000000000;
    boolean aBool8169;
    int anInt8170 = 0;
    Class633 aClass633_8171;
    int anInt8172;
    
    public int method10332(Class681 class681, byte i) {
	if (class681 == Class681.aClass681_8670
	    && aClass633_8171.method10476((byte) 11)
	    && aClass633_8171.method10479((byte) 39)) {
	    int i_0_ = aClass633_8171.method10474(-1067560967);
	    if (anInt8167 * 2029520991 > i_0_)
		return i_0_;
	}
	return 2029520991 * anInt8167;
    }
    
    public int method10333(int i) {
	return anInt8170 * -452422487;
    }
    
    void method10334(int i) {
	if (aBool8169)
	    anInt8167 = aClass633_8171.method10481(anInt8170 * -452422487,
						   637800244) * 105141151;
	else
	    anInt8167 = aClass633_8171.method10480(-452422487 * anInt8170,
						   1855572700) * 105141151;
    }
    
    public void method10335(int i) {
	anInt8172 = i * 492907033;
    }
    
    public int method10336(int i) {
	return 2029520991 * anInt8167;
    }
    
    public Class626(Class633 class633, boolean bool) {
	anInt8167 = 105141151;
	anInt8172 = 492907033;
	aClass633_8171 = class633;
	aBool8169 = bool;
    }
    
    public int method10337(Class681 class681) {
	if (Class681.aClass681_8670 == class681
	    && aClass633_8171.method10476((byte) 83)
	    && aClass633_8171.method10479((byte) 65)) {
	    int i = aClass633_8171.method10492((byte) 0);
	    if (!aBool8169)
		i /= 10;
	    if (anInt8170 * -452422487 > i)
		return i;
	}
	return -452422487 * anInt8170;
    }
    
    public void method10338(int i, int i_1_) {
	anInt8172 = i * 492907033;
    }
    
    public int method10339(Class681 class681) {
	if (class681 == Class681.aClass681_8670
	    && aClass633_8171.method10476((byte) 24)
	    && aClass633_8171.method10479((byte) 15)) {
	    int i = aClass633_8171.method10474(1815049816);
	    if (anInt8167 * 2029520991 > i)
		return i;
	}
	return 2029520991 * anInt8167;
    }
    
    public int method10340() {
	return anInt8170 * -452422487;
    }
    
    public int method10341() {
	return anInt8170 * -452422487;
    }
    
    public int method10342() {
	return anInt8170 * -452422487;
    }
    
    public void method10343(int i, int i_2_) {
	anInt8170 = i * 1751601049;
	if (-452422487 * anInt8170 < 0)
	    anInt8170 = 0;
	else if (aBool8169 && -452422487 * anInt8170 > 2000000000)
	    anInt8170 = 1404138496;
	else if (!aBool8169 && -452422487 * anInt8170 > 200000000)
	    anInt8170 = -289082880;
	method10334(1748003629);
    }
    
    public int method10344(Class681 class681) {
	if (Class681.aClass681_8670 == class681
	    && aClass633_8171.method10476((byte) 99)
	    && aClass633_8171.method10479((byte) 89)) {
	    int i = aClass633_8171.method10492((byte) 0);
	    if (!aBool8169)
		i /= 10;
	    if (anInt8170 * -452422487 > i)
		return i;
	}
	return -452422487 * anInt8170;
    }
    
    public int method10345(Class681 class681) {
	if (Class681.aClass681_8670 == class681
	    && aClass633_8171.method10476((byte) 47)
	    && aClass633_8171.method10479((byte) 40)) {
	    int i = aClass633_8171.method10492((byte) 0);
	    if (!aBool8169)
		i /= 10;
	    if (anInt8170 * -452422487 > i)
		return i;
	}
	return -452422487 * anInt8170;
    }
    
    public int method10346(int i) {
	return anInt8172 * -694250967;
    }
    
    public void method10347(int i) {
	anInt8170 = i * 1751601049;
	if (-452422487 * anInt8170 < 0)
	    anInt8170 = 0;
	else if (aBool8169 && -452422487 * anInt8170 > 2000000000)
	    anInt8170 = 1404138496;
	else if (!aBool8169 && -452422487 * anInt8170 > 200000000)
	    anInt8170 = -289082880;
	method10334(1748003629);
    }
    
    public void method10348(int i) {
	anInt8170 = i * 1751601049;
	if (-452422487 * anInt8170 < 0)
	    anInt8170 = 0;
	else if (aBool8169 && -452422487 * anInt8170 > 2000000000)
	    anInt8170 = 1404138496;
	else if (!aBool8169 && -452422487 * anInt8170 > 200000000)
	    anInt8170 = -289082880;
	method10334(1748003629);
    }
    
    public void method10349(int i) {
	anInt8170 = i * 1751601049;
	if (-452422487 * anInt8170 < 0)
	    anInt8170 = 0;
	else if (aBool8169 && -452422487 * anInt8170 > 2000000000)
	    anInt8170 = 1404138496;
	else if (!aBool8169 && -452422487 * anInt8170 > 200000000)
	    anInt8170 = -289082880;
	method10334(1748003629);
    }
    
    void method10350() {
	if (aBool8169)
	    anInt8167 = aClass633_8171.method10481(anInt8170 * -452422487,
						   1064030084) * 105141151;
	else
	    anInt8167 = aClass633_8171.method10480(-452422487 * anInt8170,
						   1855572700) * 105141151;
    }
    
    public int method10351() {
	return anInt8172 * -694250967;
    }
    
    public int method10352() {
	return 2029520991 * anInt8167;
    }
    
    public void method10353(int i) {
	anInt8170 = i * 1751601049;
	if (-452422487 * anInt8170 < 0)
	    anInt8170 = 0;
	else if (aBool8169 && -452422487 * anInt8170 > 2000000000)
	    anInt8170 = 1404138496;
	else if (!aBool8169 && -452422487 * anInt8170 > 200000000)
	    anInt8170 = -289082880;
	method10334(1748003629);
    }
    
    public int method10354(Class681 class681) {
	if (class681 == Class681.aClass681_8670
	    && aClass633_8171.method10476((byte) 20)
	    && aClass633_8171.method10479((byte) 35)) {
	    int i = aClass633_8171.method10474(930431986);
	    if (anInt8167 * 2029520991 > i)
		return i;
	}
	return 2029520991 * anInt8167;
    }
    
    public int method10355(Class681 class681) {
	if (class681 == Class681.aClass681_8670
	    && aClass633_8171.method10476((byte) 10)
	    && aClass633_8171.method10479((byte) 51)) {
	    int i = aClass633_8171.method10474(1136310118);
	    if (anInt8167 * 2029520991 > i)
		return i;
	}
	return 2029520991 * anInt8167;
    }
    
    void method10356() {
	if (aBool8169)
	    anInt8167 = aClass633_8171.method10481(anInt8170 * -452422487,
						   1118389748) * 105141151;
	else
	    anInt8167 = aClass633_8171.method10480(-452422487 * anInt8170,
						   1855572700) * 105141151;
    }
    
    public void method10357(int i) {
	anInt8172 = i * 492907033;
    }
    
    public int method10358() {
	return anInt8172 * -694250967;
    }
    
    public void method10359(int i) {
	anInt8172 = i * 492907033;
    }
    
    public int method10360() {
	return 2029520991 * anInt8167;
    }
    
    public int method10361() {
	return anInt8172 * -694250967;
    }
    
    void method10362() {
	if (aBool8169)
	    anInt8167 = aClass633_8171.method10481(anInt8170 * -452422487,
						   430944949) * 105141151;
	else
	    anInt8167 = aClass633_8171.method10480(-452422487 * anInt8170,
						   1855572700) * 105141151;
    }
    
    public int method10363(Class681 class681, int i) {
	if (Class681.aClass681_8670 == class681
	    && aClass633_8171.method10476((byte) 106)
	    && aClass633_8171.method10479((byte) 92)) {
	    int i_3_ = aClass633_8171.method10492((byte) 0);
	    if (!aBool8169)
		i_3_ /= 10;
	    if (anInt8170 * -452422487 > i_3_)
		return i_3_;
	}
	return -452422487 * anInt8170;
    }
    
    public int method10364() {
	return 2029520991 * anInt8167;
    }
    
    void method10365() {
	if (aBool8169)
	    anInt8167 = aClass633_8171.method10481(anInt8170 * -452422487,
						   1862613280) * 105141151;
	else
	    anInt8167 = aClass633_8171.method10480(-452422487 * anInt8170,
						   1855572700) * 105141151;
    }
    
    public static void method10366(int i, int i_4_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(4, (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    static final void method10367(Class669 class669, byte i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub19_10741
		  .method17053((byte) -87) == 1 ? 1 : 0;
    }
}
