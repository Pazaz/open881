/* Class654_Sub1_Sub5_Sub4 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class654_Sub1_Sub5_Sub4 extends Class654_Sub1_Sub5
{
    Class530 aClass530_12131;
    
    void method16851(Class185 class185, Class654_Sub1 class654_sub1, int i,
		     int i_0_, int i_1_, boolean bool, int i_2_) {
	/* empty */
    }
    
    boolean method16850(int i) {
	return false;
    }
    
    boolean method16848(byte i) {
	return false;
    }
    
    boolean method16874(Class185 class185, int i, int i_3_) {
	return false;
    }
    
    boolean method16846(Class185 class185, int i, int i_4_, byte i_5_) {
	return false;
    }
    
    boolean method16864() {
	return true;
    }
    
    boolean method16879() {
	return false;
    }
    
    Class550 method16853(Class185 class185, int i) {
	if (aClass530_12131.aClass183_7127 == null)
	    return null;
	Class446 class446 = class185.method3665();
	Class446 class446_6_ = method10834();
	Class444 class444 = method10807();
	class446.method7236(class446_6_);
	Class568 class568
	    = (aClass556_10855.aClass568ArrayArrayArray7431[aByte10854]
	       [(int) class444.aClass438_4885.aFloat4864 >> 9]
	       [(int) class444.aClass438_4885.aFloat4865 >> 9]);
	if (null != class568 && class568.aClass654_Sub1_Sub2_7607 != null)
	    class446.method7287(0.0F,
				(float) -(class568.aClass654_Sub1_Sub2_7607
					  .aShort11864),
				0.0F);
	aClass530_12131.aClass183_7127.method3034(class446, null, 0);
	return null;
    }
    
    void method16868(Class185 class185, int i) {
	/* empty */
    }
    
    public Class564 method16855(Class185 class185, short i) {
	return null;
    }
    
    boolean method16895() {
	return true;
    }
    
    public int method16876(int i) {
	if (null == aClass530_12131.aClass183_7127)
	    return 0;
	return aClass530_12131.aClass183_7127.method3045();
    }
    
    boolean method16849(int i) {
	return true;
    }
    
    public Class564 method16872(Class185 class185) {
	return null;
    }
    
    boolean method16869() {
	return false;
    }
    
    public int method16867() {
	if (null == aClass530_12131.aClass183_7127)
	    return 0;
	return aClass530_12131.aClass183_7127.method3045();
    }
    
    public int method16866() {
	if (null == aClass530_12131.aClass183_7127)
	    return 0;
	return aClass530_12131.aClass183_7127.method3045();
    }
    
    void method16845() {
	/* empty */
    }
    
    public Class564 method16870(Class185 class185) {
	return null;
    }
    
    void method16871(Class185 class185) {
	/* empty */
    }
    
    boolean method16880(Class185 class185, int i, int i_7_) {
	return false;
    }
    
    boolean method16873(Class185 class185, int i, int i_8_) {
	return false;
    }
    
    Class654_Sub1_Sub5_Sub4(Class556 class556, Class530 class530, int i,
			    int i_9_, int i_10_, int i_11_, int i_12_) {
	super(class556, i, i_9_, i_10_, i_11_, i_12_, i_10_ >> 9, i_10_ >> 9,
	      i_12_ >> 9, i_12_ >> 9, false, (byte) 0);
	aClass530_12131 = class530;
    }
    
    boolean method16882(Class185 class185, int i, int i_13_) {
	return false;
    }
    
    boolean method16861() {
	return false;
    }
    
    void method16877(Class185 class185, Class654_Sub1 class654_sub1, int i,
		     int i_14_, int i_15_, boolean bool) {
	/* empty */
    }
    
    void method16883(Class185 class185, Class654_Sub1 class654_sub1, int i,
		     int i_16_, int i_17_, boolean bool) {
	/* empty */
    }
    
    void method16852(int i) {
	/* empty */
    }
    
    void method16865() {
	/* empty */
    }
    
    void method16881() {
	/* empty */
    }
    
    Class550 method16884(Class185 class185) {
	if (aClass530_12131.aClass183_7127 == null)
	    return null;
	Class446 class446 = class185.method3665();
	Class446 class446_18_ = method10834();
	Class444 class444 = method10807();
	class446.method7236(class446_18_);
	Class568 class568
	    = (aClass556_10855.aClass568ArrayArrayArray7431[aByte10854]
	       [(int) class444.aClass438_4885.aFloat4864 >> 9]
	       [(int) class444.aClass438_4885.aFloat4865 >> 9]);
	if (null != class568 && class568.aClass654_Sub1_Sub2_7607 != null)
	    class446.method7287(0.0F,
				(float) -(class568.aClass654_Sub1_Sub2_7607
					  .aShort11864),
				0.0F);
	aClass530_12131.aClass183_7127.method3034(class446, null, 0);
	return null;
    }
}
